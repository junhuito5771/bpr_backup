module.exports = {
  jsxBracketSameLine: true,
  semi: true,
  singleQuote: true,
  arrowParens: 'avoid',
  trailingComma: 'es5',
};
