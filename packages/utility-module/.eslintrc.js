const { peerDependencies } = require('./package.json');

module.exports = {
  extends: ['../../.eslintrc'],
  rules: {
    'import/no-unresolved': [
      'error',
      { ignore: Object.keys(peerDependencies) },
    ],
  },
};
