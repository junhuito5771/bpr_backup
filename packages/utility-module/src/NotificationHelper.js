/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
import { Platform } from 'react-native';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';

import ApiService from './ApiWrapper';
import { Analytic } from './AmplifyHelper';

import { promptUserAutoStartIfNeeded } from './AutostartHelper';

class NotificationHelper {
  static storageKey = 'notificationToken';

  static syncToServerKey = 'syncToServer';

  constructor() {
    if (!NotificationHelper.instance) {
      NotificationHelper.instance = this;
    }
    this.canInit = true;
    this.storage = AsyncStorage;
  }

  async addNotificationToQueue(notification) {
    this.notificationList.push(notification);
  }

  async init(provider, notificationHandler) {
    if (this.canInit) {
      this.isGMS = provider === 'GMS';
      this.interface = require('@react-native-firebase/messaging').default;
      if (provider === 'HMS') {
        this.interface = require('@hmscore/react-native-hms-push');
      }
      this.canInit = false;

      this.notificationHandler = notificationHandler;
      this.notificationList = [];
      await this.interface().registerDeviceForRemoteMessages();

      this.setHandler();

      if (this.isGMS) {
        PushNotification.configure({
          requestPermissions: true,
          onNotification(notification) {
            if (notification.userInteraction && notificationHandler) {
              if (Platform.OS === 'android') notificationHandler(notification);
              else if (Platform.OS === 'ios') {
                // iOS unable pass the data to onNotification in foreground
                // hence, need to rely on firebase onMessage to add the notification
                // to the queue
                if (notification.foreground) {
                  NotificationHelper.instance.processNotificationQueue(
                    notification
                  );
                } else {
                  // Handle notification in background state
                  notificationHandler(notification);
                }
              }
            }
            // (required) Called when a remote is received or opened, or local notification is opened
            notification.finish(PushNotificationIOS.FetchResult.NoData);
          },
        });

        // For Android API 7 (> Oreo) and above, need to create the channel
        // in order to for notification to pop out
        if (Platform.OS === 'android') {
          PushNotification.channelExists('default_channel', isChannelExists => {
            if (!isChannelExists) {
              PushNotification.createChannel({
                channelId: 'default_channel',
                channelName: 'Default Notification Channel',
                channelDescription: 'A Channel to receive push notification',
                importance: 4,
                vibrate: true,
              });
            }
          });
        }
      }

      this.hasPermission = await this.requestUserPermission();

      if (this.hasPermission) {
        const token = await this.getTokenFrmProvider();
        this.saveTokenToStg(token);

        // Only show this dialog when app fresh installed
        const isSyncToServer = (await this.getSyncToServerFrmStg()) === '1';
        if (!isSyncToServer) promptUserAutoStartIfNeeded();
      }
    }
  }

  // Received the data from the fcm and display local notification
  setHandler() {
    if (this.isGMS) {
      this.onMessageHandler = this.interface().onMessage(
        async remoteMessage => {
          const { title, body, android } = remoteMessage.notification;
          PushNotification.localNotification({
            title,
            message: body,
            data: remoteMessage.data,
            ...(!!android && android),
          });

          if (Platform.OS === 'ios') {
            this.addNotificationToQueue(remoteMessage);
          }
        }
      );

      // setBackgroundMessageHandler only working for android
      this.interface().setBackgroundMessageHandler(async remoteMessage => {
        this.addNotificationToQueue(remoteMessage);
      });
    }
  }

  // Remove the local notification handler
  removeHandler() {
    if (typeof this.onMessageHandler !== 'undefined') {
      this.onMessageHandler();
    }
  }

  async requestUserPermission() {
    // HMS do not need user permission
    let isAllowed = !this.isGMS;
    if (this.isGMS) {
      const authStatus = await this.interface().requestPermission();

      isAllowed =
        authStatus === this.interface.AuthorizationStatus.AUTHORIZED ||
        authStatus === this.interface.AuthorizationStatus.PROVISIONAL;
    }
    return isAllowed;
  }

  saveTokenToStg(token) {
    this.storage.setItem(NotificationHelper.storageKey, token);
  }

  getTokenFrmStg() {
    return this.storage.getItem(NotificationHelper.storageKey);
  }

  saveSyncToServerToStg(isSync) {
    const value = isSync ? '1' : '0';
    this.storage.setItem(NotificationHelper.syncToServerKey, value);
  }

  getSyncToServerFrmStg() {
    return this.storage.getItem(NotificationHelper.syncToServerKey);
  }

  async getTokenFrmProvider() {
    let token;
    if (this.isGMS) {
      token = await this.interface().getToken();
    } else {
      const { HmsPushInstanceId, HmsPushResultCode } = this.interface;

      const getTokenPromise = () =>
        new Promise(resolve => {
          HmsPushInstanceId.getToken((result, hmsToken) => {
            if (Number(result) === HmsPushResultCode.SUCCESS) {
              resolve(hmsToken);
            } else {
              resolve(undefined);
            }
          });
        });

      token = await getTokenPromise();
    }

    return token;
  }

  processNotificationQueue(notification) {
    if (
      this.notificationList &&
      this.notificationHandler &&
      this.notificationList.length > 0
    ) {
      const notificationIndex = this.notificationList.findIndex(
        ({ notification: curNotification }) =>
          curNotification.title === notification.title &&
          curNotification.body === notification.message
      );

      if (notificationIndex > -1) {
        this.notificationHandler(this.notificationList[notificationIndex]);

        // Clear the notification list after process
        this.notificationList.splice(notificationIndex, 1);
      }
    }
  }

  handleBackgroundNotification() {
    if (this.isGMS) {
      this.interface()
        .getInitialNotification()
        .then(remoteMessage => {
          if (remoteMessage)
            NotificationHelper.instance.notificationHandler(remoteMessage);
        });
    }
  }

  async syncTokenToServer(username, token, isLogin) {
    try {
      if (token) {
        // Do not sync to server if no username
        if (username && isLogin) {
          const requestData = {
            username,
            IMEI: DeviceInfo.getUniqueId(),
            Token: token,
            type: this.isGMS ? '1' : '2',
          };

          ApiService.post('insertappinfo', { requestData });
        }

        if (this.isGMS) Analytic.updateEndpoint({ address: token });
      }
    } catch (error) {
      // Empty catch block since we need to keep it going regardless got error or not
    }
  }

  async syncNewAppInfoToServer(token) {
    try {
      if (token) {
        const requestData = {
          IMEI: DeviceInfo.getUniqueId(),
          Token: token,
          type: this.isGMS ? '1' : '2',
        };

        const response = await ApiService.postWithoutAuth('newappinfo', {
          requestData,
        });

        return response && response.data && response.data.message === 'success';
      }
    } catch (error) {
      // Empty
    }

    return false;
  }

  async executeSyncTokenProcess({
    token,
    username,
    isLogin,
    forceUpdate = false,
    retryNr = 0,
  }) {
    try {
      const curToken =
        token === undefined || token === null
          ? await this.getTokenFrmProvider()
          : token;

      const isSyncToServer = (await this.getSyncToServerFrmStg()) === '1';

      if (curToken) this.syncTokenToServer(username, curToken, isLogin);

      if (curToken && (!isSyncToServer || forceUpdate)) {
        const isSyncAppInfoSuccess = await this.syncNewAppInfoToServer(
          curToken
        );

        this.saveSyncToServerToStg(isSyncAppInfoSuccess);
      }
    } catch (error) {
      if (retryNr <= 5) {
        const timeoutDuration = retryNr * 1000;
        setTimeout(() => {
          this.executeSyncTokenProcess({
            token,
            username,
            isLogin,
            forceUpdate,
            retryNr: retryNr + 1,
          });
        }, timeoutDuration);
      }
    }
  }
}

export default new NotificationHelper();
