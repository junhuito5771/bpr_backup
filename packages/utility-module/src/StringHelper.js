function removePrefix(value, prefix = 'Daikin_') {
  return value ? value.replace(new RegExp(`^${prefix}`), '') : undefined;
}

function toCamelCase(value) {
  return value ? value.charAt(0).toUpperCase() + value.slice(1) : undefined;
}

function capitalize(value) {
  if (typeof value === 'string') {
    return value
      .toLowerCase()
      .replace(/(^\w{1})|((\s|\.)+\w{1})/g, letter => letter.toUpperCase());
  }
  return value;
}

function getTimeInString(hour, minute) {
  if (Number.isNaN(hour) || Number.isNaN(minute)) return '';

  return `${hour.toString().padStart(2, '0')}:${minute
    .toString()
    .padStart(2, '0')}`;
}

function removeSmartPunc(value) {
  return value
    ? value.replace(/[`'‘’]/g, "'").replace(/[“”]/g, '"')
    : undefined;
}

function removeDuplicateComma(value) {
  return value.replace(/,+/g, ',');
}

function trim(value) {
  if (typeof value === 'string') {
    return value.trim();
  }

  return value;
}

export default {
  removePrefix,
  toCamelCase,
  capitalize,
  getTimeInString,
  removeSmartPunc,
  removeDuplicateComma,
  trim,
};
