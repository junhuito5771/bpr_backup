// We can disable eslint rule on un-resolved import fort react native
// Since this package will be used together with app package which already
// contains react native
import { Platform } from 'react-native'; // eslint-disable-line
import VersionCheck from 'react-native-version-check';

export const getPackageName = () => VersionCheck.getPackageName();

export const getCountryCode = () => VersionCheck.getCountry();

export const getLatestVersion = async (userPackageName, userCountry) => {
  const packageName = userPackageName || getPackageName();
  const country = userCountry || (await getCountryCode());

  return VersionCheck.getLatestVersion({
    provider: Platform.select({ ios: 'appStore', android: 'playStore' }),
    packageName,
    country,
    fetchOptions: {
      headers: {
        'Cache-Control': 'no-cache',
      },
    },
  });
};

export const getCurrentVersion = () => VersionCheck.getCurrentVersion();

export const getCurrentBuildNumber = () => VersionCheck.getCurrentBuildNumber();

export const needUpdate = async () => {
  const latestVersion = await getLatestVersion();
  const currentVersion = await getCurrentVersion();

  const { isNeeded } = await VersionCheck.needUpdate({
    currentVersion,
    latestVersion,
  });

  return isNeeded;
};

export const getStoreURL = async (appID, userAppName, userCountry) => {
  const appName = userAppName || (await getPackageName());
  const country = userCountry || (await getCountryCode());

  return VersionCheck.getStoreUrl({ appID, appName, country });
};
