/* eslint-disable no-bitwise */
const StringToByte = str =>
  str ? str.split('').map(value => value.charCodeAt(0)) : undefined;
const ByteToString = bytes =>
  bytes && bytes.length > 0
    ? bytes
        .reduce((acc, item) => [...acc, String.fromCharCode(item)], [])
        .join('')
    : undefined;

const b64ToUint6 = nChr => {
  if (nChr > 64 && nChr < 91) return nChr - 65;
  else if (nChr > 96 && nChr < 123) return nChr - 71;
  else if (nChr > 47 && nChr < 58) return nChr + 4;
  else if (nChr === 43) return 62;
  else if (nChr === 47) return 63;

  return 0;
};
const base64DecToArr = (sBase64, nBlockSize) => {
  const sB64Enc = sBase64.replace(/[^A-Za-z0-9+/]/g, '');
  const nInLen = sB64Enc.length;
  const nOutLen = nBlockSize
    ? Math.ceil(((nInLen * 3 + 1) >>> 2) / nBlockSize) * nBlockSize
    : (nInLen * 3 + 1) >>> 2;
  const aBytes = new Uint8Array(nOutLen);

  for (
    let nMod3, nMod4, nUint24 = 0, nOutIdx = 0, nInIdx = 0;
    nInIdx < nInLen;
    nInIdx += 1
  ) {
    nMod4 = nInIdx & 3;
    nUint24 |= b64ToUint6(sB64Enc.charCodeAt(nInIdx)) << (18 - 6 * nMod4);
    if (nMod4 === 3 || nInLen - nInIdx === 1) {
      for (
        nMod3 = 0;
        nMod3 < 3 && nOutIdx < nOutLen;
        nMod3 += 1, nOutIdx += 1
      ) {
        aBytes[nOutIdx] = (nUint24 >>> ((16 >>> nMod3) & 24)) & 255;
      }
      nUint24 = 0;
    }
  }

  return aBytes;
};
/* eslint-enable no-bitwise */

const convertCharToASCIICodes = (value, offset) => {
  const result = [];

  for (let i = 0; i < value.length; i += 1) {
    result.push(value.charCodeAt(i) + offset);
  }

  return result;
};

const asciiToAscii85 = (text, hasEncloser = true) => {
  // 1. Convert the hex to decimal
  let result = '';
  const offset = 33;
  for (let i = 0; i < text.length; i += 8) {
    const chunkHexString = text.substring(i, i + 8);
    const chunkInDecimal = parseInt(chunkHexString, 16);

    const n0 = Math.floor((chunkInDecimal / 52200625) % 85) + offset;
    const n1 = Math.floor((chunkInDecimal / 614125) % 85) + offset;
    const n2 = Math.floor((chunkInDecimal / 7225) % 85) + offset;
    const n3 = Math.floor((chunkInDecimal / 85) % 85) + offset;
    const n4 = Math.floor(chunkInDecimal % 85) + offset;

    result += `${String.fromCharCode(n0)}${String.fromCharCode(
      n1
    )}${String.fromCharCode(n2)}${String.fromCharCode(n3)}${String.fromCharCode(
      n4
    )}`;
  }

  if (hasEncloser) {
    result = `<~${result}~>`;
  }

  return result;
};

const ascii85ToAscii = text => {
  const value = convertCharToASCIICodes(text, -33);
  let result = '';
  for (let i = 0; i < value.length; i += 5) {
    const chunkTotal =
      value[i + 4] +
      value[i + 3] * 85 +
      value[i + 2] * 85 ** 2 +
      value[i + 1] * 85 ** 3 +
      value[i] * 85 ** 4;

    const chunkTotalInHex = chunkTotal.toString(16);

    result += chunkTotalInHex.padStart(8, '0');
  }

  return result;
};

export default {
  StringToByte,
  ByteToString,
  base64DecToArr,
  asciiToAscii85,
  ascii85ToAscii,
};
