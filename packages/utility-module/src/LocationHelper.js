import Geolocation from '@react-native-community/geolocation';
import { Analytic } from './AmplifyHelper';

class LocationHelper {
  constructor() {
    if (!LocationHelper.instance) {
      LocationHelper.instance = this;
    }
    this.canInit = true;
  }

  async init(provider) {
    if (this.canInit) {
      this.isGMS = provider === 'GMS';
      this.interface = require('react-native-android-location-enabler').default;
      if (provider === 'HMS') {
        this.interface = require('@hmscore/react-native-hms-location').default;
        await this.interface.LocationKit.Native.init();
      }

      this.canInit = false;
    }
  }

  async enableLocationIfNeeded() {
    let status = false;
    try {
      if (this.isGMS) {
        const locationHandler = () =>
          new Promise(resolve => {
            this.interface
              .promptForEnableLocationIfNeeded({
                interval: 10000,
                fastInterval: 5000,
              })
              .then(() => {
                resolve(true);
              })
              .catch(() => {
                resolve(false);
              });
          });
        status = await locationHandler();
      } else {
        const locationRequest = {
          priority: this.interface.FusedLocation.PriorityConstants
            .PRIORITY_HIGH_ACCURACY,
          interval: 5000,
          numUpdates: 20,
          fastestInterval: 6000,
          expirationTime: 100000,
          expirationTimeDuration: 100000,
          smallestDisplacement: 0,
          maxWaitTime: 1000.0,
          needAddress: false,
          language: 'en',
          countryCode: 'en',
        };

        const locationSettingsRequest = {
          locationRequests: [locationRequest],
          alwaysShow: true,
          needBle: true,
        };

        const result = await this.interface.FusedLocation.Native.checkLocationSettings(
          locationSettingsRequest
        );

        return (
          result &&
          result.locationSettingsStates &&
          result.locationSettingsStates.isLocationPresent &&
          result.locationSettingsStates.isLocationUsable
        );
      }
    } catch (error) {
      // Empty error block
    }

    return status;
  }

  getCurrentLocation() {
    return new Promise((resolve, reject) => {
      if (this.isGMS) {
        Geolocation.getCurrentPosition(
          position => {
            if (position) {
              resolve(position);
            }
          },
          error => reject(error)
        );
      }
    });
  }

  async syncGeocode() {
    let locInfo;
    try {
      const {
        coords: { longitude, latitude },
      } = await this.getCurrentLocation();

      if (this.isGMS && longitude && latitude)
        Analytic.updateEndpoint({
          location: {
            latitude: +latitude.toFixed(2),
            longitude: +longitude.toFixed(2),
          },
        });
    } catch (error) {
      // Empty block
    }

    return locInfo;
  }
}

const locationHelperInstance = new LocationHelper();

export default locationHelperInstance;
