import { NativeModules, Alert, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';

const { AutoStarterModule } = NativeModules;

export function getIsCustomAndroid() {
  const constants = AutoStarterModule.getConstants();

  return constants.IS_CUSTOM_ANDROID;
}

export async function promptUserAutoStartIfNeeded() {
  if (
    Platform.OS === 'android' &&
    DeviceInfo.getBrand() === 'xiaomi' &&
    getIsCustomAndroid()
  ) {
    const isAutoStartAvailable = await AutoStarterModule.isAutoStartPermissionAvailable();
    if (isAutoStartAvailable) {
      Alert.alert(
        'Enable autostart',
        `Detected that current android OS require extra steps to enable notification\n\nPlease open Security app > Permissions > Autostart, then enable "Autostart" for this app `,
        [
          { text: 'Cancel' },
          {
            text: 'Open autostart',
            onPress: () => {
              AutoStarterModule.openAutoStartSetting();
            },
          },
        ]
      );
    }
  }
}
