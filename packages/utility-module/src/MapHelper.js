const isGmsProvider = provider => provider === 'GMS';

const getMapViewComponent = provider => {
  if (isGmsProvider(provider)) {
    const { default: MapView, Marker } = require('react-native-maps');
    return { MapView, Marker };
  } else {
    const {
      default: MapView,
      Marker,
    } = require('@hmscore/react-native-hms-map');
    return { MapView, Marker };
  }
};

export default {
  getMapViewComponent,
};
