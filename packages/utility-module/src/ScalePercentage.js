/* eslint import/no-extraneous-dependencies: 0 */
/* eslint import/extensions: 0 */
/* eslint import/no-unresolved: 0 */
import { Dimensions, Platform, StatusBar, PixelRatio } from 'react-native';
import StaticSafeAreaInsets from 'react-native-static-safe-area-insets';
// import DeviceInfo from 'react-native-device-info';
import AndroidExtraDimension from 'react-native-extra-dimensions-android';

const STATUSBAR_HEIGHT = Platform.select({
  ios: 0,
  android: AndroidExtraDimension.getStatusBarHeight(),
});

const SOFT_MENU_HEIGHT = Platform.select({
  ios: 0,
  android: AndroidExtraDimension.getSoftMenuBarHeight(),
});

export const viewportWidth = Dimensions.get('window').width;
export const viewportHeight = Platform.select({
  ios: Dimensions.get('window').height,
  android: AndroidExtraDimension.getRealWindowHeight(),
});

export const SAFE_AREA_INSET_TOP = Platform.select({
  ios: StaticSafeAreaInsets.safeAreaInsetsTop,
  android: 0,
});
export const SAFE_AREA_INSET_BOTTOM = Platform.select({
  ios: StaticSafeAreaInsets.safeAreaInsetsBottom,
  android: 0,
});

export const SAFE_VIEWPORT_HEIGHT =
  viewportHeight -
  SAFE_AREA_INSET_TOP -
  SAFE_AREA_INSET_BOTTOM -
  STATUSBAR_HEIGHT -
  SOFT_MENU_HEIGHT;

export const SAFE_VIEWPORT_WIDTH =
  viewportWidth -
  StaticSafeAreaInsets.safeAreaInsetsLeft -
  StaticSafeAreaInsets.safeAreaInsetsRight;

const [shortDimension, longDimension] =
  viewportWidth < viewportHeight
    ? [viewportWidth, viewportHeight]
    : [viewportHeight, viewportWidth];

// Figures based on https://blog.solutotlv.com/size-matters/. Previously 360 // 640
export const guidelineBaseWidth = 350;
export const guidelineBaseHeight = 680;

export const scale = size => (shortDimension / guidelineBaseWidth) * size;
export const verticalScale = size =>
  (longDimension / guidelineBaseHeight) * size;
export const moderateScale = (size, factor = 0.5) =>
  (size + (scale(size) - size)) * factor;

export const isSmallDevice = viewportHeight < guidelineBaseHeight;

export const widthPercentage = percentage => {
  const value = percentage * viewportWidth;
  return PixelRatio.roundToNearestPixel(value / 100);
};

export const heightPercentage = percentage => {
  const value = percentage * viewportHeight;
  return PixelRatio.roundToNearestPixel(value / 100);
};

export const safeHeightPercentage = percentage => {
  const value = percentage * SAFE_VIEWPORT_HEIGHT;
  return Math.round(value / 100);
};

export const safeWidthPercentage = percentage => {
  const value = percentage * SAFE_VIEWPORT_WIDTH;
  return Math.round(value / 100);
};

export const iphoneXS =
  Platform.OS === 'ios' && Dimensions.get('window').height >= 812;
