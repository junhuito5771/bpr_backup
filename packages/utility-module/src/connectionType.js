const WLAN_MODE = 'WLAN';
const IOT_MODE = 'IOT';
const BLE_MDOE = 'BLE';

export default { IOT_MODE, WLAN_MODE, BLE_MDOE };
