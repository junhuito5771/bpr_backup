/* eslint import/no-extraneous-dependencies: 0 */
/* eslint import/extensions: 0 */
/* eslint import/no-unresolved: 0 */
import { Dimensions } from 'react-native';

import { guidelineBaseWidth } from './ScalePercentage';

const { width: SCREEN_WIDTH } = Dimensions.get('window');
const scale = SCREEN_WIDTH / guidelineBaseWidth;

export default function normalize(size) {
  return Math.round(scale * size);
}
