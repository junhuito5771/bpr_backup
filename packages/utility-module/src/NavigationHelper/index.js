// eslint-disable-next-line import/no-unresolved
import { createRef } from 'react';
import {
  DrawerActions,
  TabActions,
  StackActions,
} from '@react-navigation/native';

let isNavigatorReady;

const navigationRef = createRef();

function getTopLevelNavigator(navigation) {
  const parent = navigation.dangerouslyGetParent();

  if (parent) {
    return getTopLevelNavigator(parent);
  }
  return navigation;
}

export function navigate(name, params) {
  // Only perform navigation if app has mounted
  // and navigator has been initialized

  if (navigationRef.current) {
    navigationRef.current.navigate(name, params);
  }
}

export function reset(name) {
  // Only perform navigation if app has mounted
  // and navigator has been initialized

  if (navigationRef.current) {
    navigationRef.current.reset({ index: 0, routes: [{ name }] });
  }
}

function setOptions(navigation, options, useParents = true) {
  const rootNavigation = useParents
    ? getTopLevelNavigator(navigation)
    : navigation;

  if (rootNavigation) {
    rootNavigation.setOptions({
      headerRight: () => { }, // to prevent cascade effect of header right options
      ...options,
    });
  }
}

function setNavigationReady(value) {
  isNavigatorReady = value;
}

function replace(routeName, params = null) {
  if (navigationRef.current) {
    navigationRef.current.dispatch(StackActions.replace(routeName, params));
  }
}

function drawerReset(childRouteName, params) {
  if (navigationRef.current) {
    navigationRef.current.dispatch(DrawerActions.jumpTo('WeeklyTimer'));
    setTimeout(
      () =>
        navigationRef.current.dispatch(
          TabActions.jumpTo(childRouteName, params)
        ),
      1000
    );
  }
}

export default {
  navigate,
  isNavigatorReady,
  navigationRef,
  setNavigationReady,
  setOptions,
  reset,
  replace,
  drawerReset,
};

/*
import { NavigationActions, StackActions } from '@react-navigation/compat';
import { DrawerActions, TabActions } from '@react-navigation/native';

let navigator;

function getTopLevelNavigator(navigation) {
  const parent = navigation.dangerouslyGetParent();

  if (parent) {
    return getTopLevelNavigator(parent);
  }
  return navigation;
}

function setOptions(navigation, options) {
  const rootNavigation = getTopLevelNavigator(navigation);
  if (rootNavigation) {
    rootNavigation.setOptions({
      headerRight: () => {}, // to prevent cascade effect of header right options
      ...options,
    });
  }
}

function setTopLevelNavigator(navigatorRef) {
  navigator = navigatorRef;
}

function navigate(routeName, params) {
  if (navigator) {
    navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      })
    );
  }
}

function reset(index, routeName, params) {
  navigator.dispatch(
    StackActions.reset({
      index,
      actions: [NavigationActions.navigate({ routeName, params })],
    })
  );
}

function drawerReset(childRouteName, params) {
  navigator.dispatch(DrawerActions.jumpTo('WeeklyTimer'));
  setTimeout(
    () => navigator.dispatch(TabActions.jumpTo(childRouteName, params)),
    1000
  );
}

function setParams(params) {
  navigator.setParams(params);
}

export default {
  getTopLevelNavigator,
  setOptions,
  navigate,
  setTopLevelNavigator,
  reset,
  drawerReset,
  setParams,
};
*/
