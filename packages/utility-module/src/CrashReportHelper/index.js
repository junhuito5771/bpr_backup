import { Client, Configuration } from 'bugsnag-react-native';

let bugsnag = null;

const DEFAULT_CONFIG = {
  notifyReleaseStages: ['beta', 'production', 'testflight'],
};

export function init(apiKey, configs = DEFAULT_CONFIG) {
  const configuration = new Configuration(apiKey);
  Object.keys(configs).forEach(key => {
    configuration[key] = configs[key];
  });
  bugsnag = new Client(configuration);
}

export function notify(error) {
  bugsnag.notify(error);
}

export function setUser({ id, name, email }) {
  bugsnag.setUser(id, name, email);
}

export function clearUser() {
  bugsnag.clearUser();
}
