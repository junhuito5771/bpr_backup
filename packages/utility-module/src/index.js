import DeviceInfo from 'react-native-device-info';
import ScaleText from './ScaleText';
import {
  widthPercentage,
  heightPercentage,
  isSmallDevice,
  iphoneXS,
  verticalScale,
  safeHeightPercentage,
  SAFE_VIEWPORT_HEIGHT,
  SAFE_VIEWPORT_WIDTH,
  SAFE_AREA_INSET_TOP,
  SAFE_AREA_INSET_BOTTOM,
  scale,
  viewportHeight,
  viewportWidth,
} from './ScalePercentage';
import ApiService, { setupApi } from './ApiWrapper';
import ConnectionType from './connectionType';
import * as NetworkHelper from './NetworkHelper';
import StringHelper from './StringHelper';
import {
  getCurrentDateInCalendarFormat,
  getCalendarDateAfterInterval,
  getDateFormat,
  timeConvert,
  API_TIME_FORMAT,
  STANDARD_DATE_FORMAT,
  STANDARD_MONTH_FORMAT,
  STANDARD_YEAR_FORMAT,
} from './DateHelper';
import StringConverter from './StringConverter';
import VersionCompare from './VersionCompare';
import * as StoreHelper from './StoreHelper';
import '../shim';
import * as ObjectHelper from './ObjectHelper';
import SetIndoorTemperature from './SetIndoorTemperature';
import * as CrashReportHelper from './CrashReportHelper';
import LocationHelper from './LocationHelper';
import QXInfoConstant from './Constants/QXInfo';
import NotificationHelper from './NotificationHelper';
import MapHelper from './MapHelper';
import * as URLHelper from './URLHelper';

export {
  ScaleText,
  widthPercentage,
  heightPercentage,
  SetIndoorTemperature,
  safeHeightPercentage,
  SAFE_VIEWPORT_HEIGHT,
  SAFE_VIEWPORT_WIDTH,
  SAFE_AREA_INSET_TOP,
  SAFE_AREA_INSET_BOTTOM,
  viewportHeight,
  viewportWidth,
  scale,
  verticalScale,
  iphoneXS,
  ApiService,
  isSmallDevice,
  ConnectionType,
  NetworkHelper,
  StringHelper,
  StringConverter,
  VersionCompare,
  StoreHelper,
  ObjectHelper,
  CrashReportHelper,
  getCurrentDateInCalendarFormat,
  getCalendarDateAfterInterval,
  getDateFormat,
  timeConvert,
  API_TIME_FORMAT,
  STANDARD_DATE_FORMAT,
  STANDARD_MONTH_FORMAT,
  STANDARD_YEAR_FORMAT,
  DeviceInfo,
  QXInfoConstant,
  NotificationHelper,
  LocationHelper,
  MapHelper,
  URLHelper,
  setupApi,
};

export {
  Auth,
  Pubsub,
  Analytic,
  AnalyticHelper,
  syncAmplifyTime,
} from './AmplifyHelper';
export * from './Validator';
export { default as NavigationService } from './NavigationHelper';
