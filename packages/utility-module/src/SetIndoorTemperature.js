export default function setIndoorTemperature(tempValue) {
  if (tempValue >= 8 && tempValue <= 36) {
    return `${tempValue}°C`;
  } else if (tempValue === 252) {
    return 'Low';
  } else if (tempValue === 253) {
    return 'High';
  }
  return '-';
}
