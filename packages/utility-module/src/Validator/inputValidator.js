class PreInputValidator {
  init(value) {
    this.value = value;
    return this;
  }

  noSpace() {
    this.value = this.value.replace(/\s/g, '');
    return this;
  }

  lowerCase() {
    this.value = this.value.toLowerCase();
    return this;
  }

  phoneNoFormat() {
    this.value = this.value.replace(/(\d{2})(\d{7})/, '$1-$2');
    return this;
  }

  noLeadingSpace() {
    this.value = this.value && this.value === ' ' ? '' : this.value;
    return this;
  }

  noDuplicateSpace() {
    this.value = this.value.replace(/\s\s+/g, ' ');
    return this;
}

  noSpecialCharacter() {
    this.value = this.value.replaceAll(/[!@#$%^.,&*]/g, '');
    return this;
  }

  numberOnly() {
    this.value = this.value.replace(/[^0-9]/g, '');
    return this;
  }

  dotOnly() {
    this.value = this.value.replace(/[^\w.]|_/g, '');
    return this;
  }

  decimalSingleDotOnly() {
    // eslint-disable-next-line
    this.value = this.value.replace(/[\.]/g, function (match, offset, all) {
      // eslint-disable-next-line
      return match === "." ? (all.indexOf(".") === offset ? "." : "") : "";
    });
    return this;
  }

  result() {
    return this.value;
  }
}

export default new PreInputValidator();

export const generalInputPreValidator = value =>
  new PreInputValidator()
  .init(value)
  .noLeadingSpace()
  .noDuplicateSpace()
  .result();

export const emailPreValidator = value =>
  new PreInputValidator().init(value).noSpace().lowerCase().result();

export const passwordPreValidator = value =>
  new PreInputValidator().init(value).result();

export const phoneNoPreValidator = value =>
  new PreInputValidator().init(value).noSpace().phoneNoFormat().result();

export const decimalValidator = value =>
  new PreInputValidator()
    .init(value)
    .noLeadingSpace()
    .noSpace()
    .dotOnly()
    .decimalSingleDotOnly()
    .result();

export const integerValidator = value =>
  new PreInputValidator()
    .init(value)
    .numberOnly()
    .result();