import moment from 'moment';

export const API_TIME_FORMAT = 'MM/DD/YYYY h:mm:ss a';
export const STANDARD_DATE_FORMAT = 'DD MMMM YYYY';
export const STANDARD_MONTH_FORMAT = 'MMMM YYYY';
export const STANDARD_YEAR_FORMAT = 'YYYY';

export const getDateFormat = (periodType, date) => {
  switch (periodType) {
    case 'weekly': {
      const firstDayOfWeek = moment(date, STANDARD_DATE_FORMAT)
        .startOf('isoWeek')
        .format(STANDARD_DATE_FORMAT);
      const lastDayOfWeek = moment(date, STANDARD_DATE_FORMAT)
        .endOf('isoWeek')
        .format(STANDARD_DATE_FORMAT);

      return `${firstDayOfWeek} - ${lastDayOfWeek}`;
    }
    case 'monthly':
      return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_MONTH_FORMAT);
    case 'yearly':
      return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_YEAR_FORMAT);
    default:
      return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT);
  }
};

export const timeConvert = val => {
  const num = val;
  const hours = num / 60;
  const rhours = Math.floor(hours);
  const minutes = (hours - rhours) * 60;
  const rminutes = Math.round(minutes);
  return { rhours, rminutes };
};

export const getCurrentDateInCalendarFormat = currentDate => {
  const date = currentDate ? new Date(currentDate) : new Date();
  return `${date.getFullYear()}-${date.getMonth() + 1}-${
    date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
  }`;
};

export const getCalendarDateAfterInterval = (currentDate, interval) => {
  const date = moment(currentDate, 'YYYY-MM-DD');

  switch (interval.frequency) {
    case 'weekly':
      date.add(7, 'd');
      break;
    case 'yearly':
      date.add(1, 'y');
      break;
    default:
      date.add(interval.interval, 'M');
  }

  return `${date.get('year')}-${date.get('month') + 1}-${
    date.get('date') < 10 ? '0' + date.get('date') : date.get('date')
  }`;
};
