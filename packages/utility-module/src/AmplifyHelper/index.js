import { DateUtils } from '@aws-amplify/core';
import Auth from '@aws-amplify/auth';
import Analytic from '@aws-amplify/analytics';
import Pubsub from '@aws-amplify/pubsub';
import DeviceInfo from 'react-native-device-info';
import NtpClient from 'react-native-ntp-client';

async function syncDeviceInfo() {
  const appVersion = await DeviceInfo.getVersion();
  const make = await DeviceInfo.getManufacturer();
  const model = await DeviceInfo.getModel();
  const modelVersion = await DeviceInfo.getDeviceId();
  const platform = await DeviceInfo.getSystemName();
  const platformVersion = await DeviceInfo.getSystemVersion();

  const demographic = {
    appVersion,
    make,
    model,
    modelVersion,
    platform,
    platformVersion,
  };

  Analytic.updateEndpoint({ demographic });
}

function syncAmplifyTime() {
  NtpClient.getNetworkTime('pool.ntp.org', 123, (error, date) => {
    if (date) {
      const localTime = new Date().getTime();
      const syncTime = date.getTime();
      const drift = localTime - syncTime;
      DateUtils.setClockOffset(drift * -1);
    }
  });
}

const AnalyticHelper = {
  syncDeviceInfo,
};

export { AnalyticHelper, Analytic, Auth, Pubsub, syncAmplifyTime };
