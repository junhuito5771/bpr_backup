import { NetworkInfo } from 'react-native-network-info';
import NetInfo from '@react-native-community/netinfo';

export const getSSID = () => NetworkInfo.getSSID();
export const getBSSID = () => NetworkInfo.getBSSID();
export const getBroadcastAddress = () => NetworkInfo.getBroadcast();
export const getLocalIPAddress = () => NetworkInfo.getIPV4Address();
export const isConnectedToWifi = async () => {
  const { type, isConnected } = await NetInfo.fetch();

  return isConnected && type === 'wifi';
};
export const isConnectedToInternet = async () => {
  const { isConnected, isInternetReachable } = await NetInfo.fetch();

  return isConnected && isInternetReachable;
};

export const isConnectedToInternetOnLaunch = () =>
  new Promise(resolve => {
    const timeoutHandlder = setTimeout(() => {
      if (timeoutHandlder) clearTimeout(timeoutHandlder);
      resolve(false);
    }, 10000);
    NetInfo.addEventListener(state => {
      if (
        state.type !== 'unknown' &&
        state.isConnected !== undefined &&
        state.isConnected !== null &&
        state.isInternetReachable !== undefined &&
        state.isInternetReachable !== null
      ) {
        if (timeoutHandlder) clearTimeout(timeoutHandlder);

        resolve(state.isConnected && state.isInternetReachable);
      }
    });
  });
