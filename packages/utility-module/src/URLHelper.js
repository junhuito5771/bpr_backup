function getUrlVars(url) {
  const vars = {};
  url.replace(/[?&]+([^=&]+)=([^&]*)/gi, (m, key, value) => {
    vars[key] = value;
  });
  return vars;
}

export function getUrlParam(url, paramName, defaultValue = '') {
  const value = getUrlVars(url)[paramName];

  return value || defaultValue;
}
