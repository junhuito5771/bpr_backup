import axios from 'axios';

let getTokenParams = null;
let awsEndpoint = null;
let awsClientId = null;
let apiEndpoint = null;
let onRefreshedToken = null;

export const setupApi = config => {
  getTokenParams = config.getTokenParams;
  awsClientId = config.awsClientId;
  awsEndpoint = config.awsEndpoint;
  apiEndpoint = config.apiEndpoint;
  onRefreshedToken = config.onRefreshedToken;
};

export class ApiError extends Error {
  constructor(message, errorCode) {
    super(message);
    this.errorMessage = message;
    this.errorCode = errorCode;
  }
}

const unknownError = () => new ApiError('An error occured');

const contentType = {
  Text: 'text',
  JSON: 'json',
  multipart: 'multipart',
};

const getHeader = type => {
  switch (type) {
    case contentType.Text:
      return {
        'Content-Type': 'text/plain',
        Accept: 'application/json',
      };
    case contentType.multipart:
      return {
        'Content-Type': 'multipart/form-data',
        Accept: '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
      };
    default:
      return {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      };
  }
};

const getRefreshTokenHeaders = () => ({
  headers: {
    'X-Amz-Target': 'AWSCognitoIdentityProviderService.InitiateAuth',
    'Content-Type': 'application/x-amz-json-1.1',
  },
});

const throwIfNotSuccess = result => {
  if (
    result.data.message &&
    result.data.message !== 'success' &&
    result.data.message !== 'No data'
  ) {
    throw new ApiError(result.data.message, result.data.code);
  }
};

const postWithoutAuth = (url, body, type = contentType.JSON) =>
  new Promise((resolve, reject) => {
    axios
      .post(`${apiEndpoint}/${url}`, body, getHeader(type))
      .then(
        result => {
          try {
            resolve(result);
          } catch (e) {
            reject(e);
          }
        },
        () => {
          reject(unknownError());
        }
      )
      .catch(e => reject(e));
  });

const post = async (url, body, type = contentType.JSON) => {
  const isTokenExpired =
    getTokenParams().lastTokenRefreshedDT + 3500 <= Date.now() / 1000;
  let { idToken } = getTokenParams();

  // No need for token refresh if user never login before
  if (isTokenExpired && getTokenParams().refreshToken) {
    const response = await axios.post(
      awsEndpoint,
      {
        ClientId: awsClientId,
        AuthFlow: 'REFRESH_TOKEN_AUTH',
        AuthParameters: {
          REFRESH_TOKEN: getTokenParams().refreshToken,
        },
      },
      getRefreshTokenHeaders()
    );
    idToken = response.data.AuthenticationResult.IdToken;
    onRefreshedToken(idToken);
  }

  let result;
  try {
    result = await axios.post(`${apiEndpoint}/${url}`, body, {
      headers: {
        ...getHeader(type),
        Authorization: idToken,
      },
    });
  } catch (e) {
    throw unknownError();
  }
  throwIfNotSuccess(result);

  return result.data;
};

const getWithAuth = async (endpoint, headers = {}, type = contentType.JSON) => {
  let result;
  try {
    result = await axios.get(`${endpoint}`, {
      headers: {
        ...getHeader(type),
        ...headers,
      },
    });
  } catch (e) {
    throw unknownError();
  }
  throwIfNotSuccess(result);

  return result.data;
};

const get = async (url, type = contentType.JSON) => {
  let result;

  try {
    result = await axios.get(`${url}`, {
      headers: {
        'Cache-Control': 'no-cache',
        ...getHeader(type),
      },
    });
  } catch (error) {
    throw new Error(error);
  }
  return result ? result.data : result;
};

const postWithoutAuthCustom = (url, body, auth, type = contentType.JSON) =>
  new Promise((resolve, reject) => {
    const header = auth
      ? {
          headers: {
            Authorization: auth,
            ...getHeader(type),
          },
        }
      : getHeader(type);

    axios
      .post(`${url}`, body, header)
      .then(
        result => {
          try {
            resolve(result);
          } catch (e) {
            reject(e);
          }
        },
        () => {
          reject(unknownError());
        }
      )
      .catch(e => reject(e));
  });

const getAuthToken = async () => {
  const isTokenExpired =
    getTokenParams().lastTokenRefreshedDT + 3500 <= Date.now() / 1000;
  let { idToken } = getTokenParams();

  if (isTokenExpired) {
    const response = await axios.post(
      awsEndpoint,
      {
        ClientId: awsClientId,
        AuthFlow: 'REFRESH_TOKEN_AUTH',
        AuthParameters: {
          REFRESH_TOKEN: getTokenParams().refreshToken,
        },
      },
      getRefreshTokenHeaders()
    );
    idToken = response.data.AuthenticationResult.IdToken;
  }

  return idToken;
};

export default {
  get,
  post,
  getWithAuth,
  postWithoutAuth,
  postWithoutAuthCustom,
  contentType,
  getAuthToken,
};
