export default function orderBy(item, key) {
  item.sort((firstItem, secondItem) => {
    if (firstItem[key] > secondItem[key]) {
      return 1;
    } if (firstItem[key] < secondItem[key]) {
      return -1;
    }
    return 0;
  });

  return item;
}
