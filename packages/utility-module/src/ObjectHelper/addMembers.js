export default function addMembersToObj(obj, newMembers, condition) {
  return {
    ...obj,
    ...(condition && newMembers),
  };
}
