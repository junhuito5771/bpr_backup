import React, { useCallback } from 'react';
import { SafeAreaView, Text, StyleSheet } from 'react-native';

import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { HeaderText, NavDrawerMenu } from '@module/daikin-ui';
import { NavigationService } from '@module/utility';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default function DummyScreen() {
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Home</HeaderText>,
        headerLeft: () => <NavDrawerMenu navigation={navigation} />,
      });
    }, [navigation])
  );
  return (
    <SafeAreaView style={styles.flex}>
      <Text>Dummy Smart Control Screen</Text>
    </SafeAreaView>
  );
}
