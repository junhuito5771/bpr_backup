import { HOME_SCREEN } from '../../shared/constants/routeNames';

import HomeScreen from '../../screens/dummyScreen';

const Drawer = {
  [HOME_SCREEN]: {
    name: HOME_SCREEN,
    label: 'Smart Control',
    component: HomeScreen,
    icon: require('../../shared/assets/remote.png'),
  },
};

export default Drawer;
