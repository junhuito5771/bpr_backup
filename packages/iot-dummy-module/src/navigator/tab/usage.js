import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../../screens/dummyScreen';
import DefaultStackOptions from './defaultStackOptions';

const UsageStack = createStackNavigator();

function UsageStackNav() {
  return (
    <UsageStack.Navigator screenOptions={DefaultStackOptions}>
      <UsageStack.Screen
        name="UsageList"
        component={HomeScreen}
        options={() => ({
          title: 'Energy Consumption',
          headerLeft: null,
        })}
      />
    </UsageStack.Navigator>
  );
}

export default UsageStackNav;
