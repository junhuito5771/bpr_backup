import React from 'react';
import { ScaleText, heightPercentage } from '@module/utility';
import { Colors, BackButton } from '@module/acson-ui';

const defaultScreenOptions = ({ navigation }) => ({
  headerTitleStyle: {
    fontFamily: 'Avenir-Black',
    color: Colors.blueGrey,
    fontWeight: '700',
    fontSize: ScaleText(18),
  },
  headerTitleAllowFontScaling: false,
  headerTitleContainerStyle: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerStyle: {
    height: heightPercentage(10),
    elevation: 5,
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
    },
  },
  cardStyle: {
    backgroundColor: 'white',
  },
  headerTitleAlign: 'center',
  headerLeft: () =>
    navigation.canGoBack() && <BackButton navigation={navigation} />,
});

export default defaultScreenOptions;
