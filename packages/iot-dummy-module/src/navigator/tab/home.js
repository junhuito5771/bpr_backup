import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../../screens/dummyScreen';
import DefaultStackOptions from './defaultStackOptions';

const HomeStack = createStackNavigator();

function HomeStackNav() {
  return (
    <HomeStack.Navigator screenOptions={DefaultStackOptions}>
      <HomeStack.Screen
        name="Home"
        component={HomeScreen}
        options={() => ({
          title: 'Remote Control',
          headerLeft: null,
        })}
      />
    </HomeStack.Navigator>
  );
}

export default HomeStackNav;
