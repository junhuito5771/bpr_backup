import React from 'react';
import { Image } from 'react-native';
import { HOME_SCREEN, USAGE_SCREEN } from '../../shared/constants/routeNames';

import HomeStack from './home';
import UsageStack from './usage';

const Tab = {
  [HOME_SCREEN]: {
    name: HOME_SCREEN,
    component: HomeStack,
    options: () => ({
      tabBarLabel: 'Remote',
      tabBarIcon: ({ focused }) => (
        <Image
          source={
            focused
              ? require('../../shared/assets/tabbar/remote/remoteRed.png')
              : require('../../shared/assets/tabbar/remote/remoteGrey.png')
          }
        />
      ),
    }),
  },
  [USAGE_SCREEN]: {
    name: USAGE_SCREEN,
    component: UsageStack,
    options: () => ({
      tabBarLabel: 'Energy Consumption',
      tabBarIcon: ({ focused }) => (
        <Image
          source={
            focused
              ? require('../../shared/assets/tabbar/statistics/chartRed.png')
              : require('../../shared/assets/tabbar/statistics/chartGrey.png')
          }
        />
      ),
    }),
  },
};

export default Tab;
