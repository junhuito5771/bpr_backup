import React, { useState } from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import {
  widthPercentage,
  heightPercentage,
  NavigationService,
} from '@module/utility';
import Config from 'react-native-config';

import {
  NavDrawerMenu,
  Colors,
  Text,
  Button,
  HeaderText,
} from '@module/daikin-ui';

import { PRODUCT_DETAILS_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: Colors.white,
  },
  toogleContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    margin: 10,
  },
  buttonContainer: {
    marginHorizontal: widthPercentage(5),
    marginBottom: heightPercentage(5),
  },
  activeTextLabel: {
    color: Colors.white,
    fontWeight: '500',
  },
  passiveTextLabel: {
    color: Colors.black,
    fontWeight: '500',
  },
  activeContainer: {
    backgroundColor: 'rgb(74,144,226)',
    height: heightPercentage(20),
    width: widthPercentage(45),
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 5,
    marginHorizontal: 4,
    borderRadius: 5,
    padding: 8,
  },
  passiveContainer: {
    backgroundColor: 'rgb(247,248,251)',
    height: heightPercentage(20),
    width: widthPercentage(45),
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 5,
    marginHorizontal: 4,
    borderRadius: 5,
    padding: 8,
  },
});

const PRODUCT_LIST = [
  {
    name: 'airCond',
    label: 'Air Conditioner',
    image: require('../../shared/assets/product/aircon.png'),
    url: `${Config.DAIKIN_URL}/product-type/air-conditioner/`,
  },
  {
    name: 'smartControl',
    label: 'Smart Control',
    image: require('../../shared/assets/product/smartcontrol.png'),
    url: `${Config.DAIKIN_URL}/smart-control/`,
  },
  {
    name: 'airPurifier',
    label: 'Air Purifier',
    image: require('../../shared/assets/product/airpurifier.png'),
    url: `${Config.DAIKIN_URL}/daikin_products/mck55uvmm-2021/`,
  },
  {
    name: 'homeCentral',
    label: 'Home Central',
    image: require('../../shared/assets/product/homecentral.png'),
    url: `${Config.DAIKIN_URL}/daikin_products/home-central-air-conditioning/`,
  },
  {
    name: 'airCurtain',
    label: 'Air Curtain',
    image: require('../../shared/assets/product/aircurtain.png'),
    url: `${Config.DAIKIN_URL}/product-type/air-curtain/`,
  },
  {
    name: 'accessories',
    label: 'Accessories',
    image: require('../../shared/assets/product/acessories.png'),
    url: `${Config.DAIKIN_URL}/core-part/`,     
  },
];

const ProductScreen = () => {
  const navigation = useNavigation();
  const [isSelected, setIsSelected] = useState(0);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Product</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
    });
  }, []);

  const handleSelect = () => {
    const title = PRODUCT_LIST[isSelected].label;
    const selectedUrl = PRODUCT_LIST[isSelected].url;
    NavigationService.navigate(PRODUCT_DETAILS_SCREEN, {
      title,
      selectedUrl,
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.toogleContainer}>
        {PRODUCT_LIST.map((id, index) => (
          <TouchableOpacity
            key={id.name}
            style={
              isSelected === index
                ? styles.activeContainer
                : styles.passiveContainer
            }
            onPress={() => setIsSelected(index)}>
            <Image source={id.image} resizeMode="cover" />
            <Text
              style={
                isSelected === index
                  ? styles.activeTextLabel
                  : styles.passiveTextLabel
              }>
              {id.label}
            </Text>
          </TouchableOpacity>
        ))}
      </View>

      <Button
        primary
        disabledPrimary
        onPress={() => handleSelect()}
        style={styles.buttonContainer}>
        Select
      </Button>
    </View>
  );
};

export default ProductScreen;
