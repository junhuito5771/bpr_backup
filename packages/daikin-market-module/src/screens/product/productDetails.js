import React, { useState } from 'react';
import { ActivityIndicator, View, StyleSheet, Share, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { WebView } from 'react-native-webview';
import { NavigationService, scale } from '@module/utility';

import { HeaderText, HeaderBackButton } from '@module/daikin-ui';

const renderNavigationOptions = (title, navigation, currentPageUrl) => ({
  title: <HeaderText>{title}</HeaderText>,
  headerLeft: () => <HeaderBackButton navigation={navigation} />,
  headerRight: () => (
    <TouchableOpacity
      style={styles.shareButton}
      onPress={()=> handleShare(currentPageUrl)}>
      <Image source={require('../../shared/assets/icons/share.png')} />
    </TouchableOpacity>
  ),
});

const handleShare = async(currentPageUrl) => {
  try {
    const result = await Share.share({
      message: currentPageUrl,
    });
    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {
    // alert(error.message);
  }
};

const resetRenderNavigationOptions = (title, navigation, currentPageUrl) => {
  NavigationService.setOptions(
    navigation,
    renderNavigationOptions(title, navigation, currentPageUrl)
  );
}


const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'center' },
  shareButton: {
    marginRight: scale(16), // Ref: daikin-mobile\packages\daikin-market-module\src\screens\maintenance\maintenanceReminderScreen.js
  }
});

const ProductDetailsScreen = ({ route }) => {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(true);

  const { title, selectedUrl } = route.params;

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(title, navigation, urlProcess(selectedUrl))
    );
  }, []);

  const Loading = () => (
    <View style={styles.container}>
      <ActivityIndicator size="large" />
    </View>
  );

  const getScreenLayout = () => {
    if (isLoading) return { flex: 0 };
    return { flex: 1 };
  };

  const INJECTED_JAVASCRIPT = `(function() {
    window.localStorage.removeItem('alikeData');
    window.ReactNativeWebView.postMessage();
  })();`;
  
  const urlProcess = (url) => {
	var DAIKIN_URL = "https://www.daikin.com.my";

	if(url.match(DAIKIN_URL)){
	  return url;
	}else{
	  return (DAIKIN_URL + url);
	}
  }


  return (
    <WebView
      source={{ uri: urlProcess(selectedUrl) }}
      startInLoadingState={isLoading}
      renderLoading={() => <Loading />}
      injectedJavaScript={INJECTED_JAVASCRIPT}
      onLoadEnd={syntheticEvent => {
        // update component to be aware of loading status
        const { nativeEvent } = syntheticEvent;
        setIsLoading(nativeEvent.loading);
      }}
      onMessage={() => {
        /* executed INJECTED_JAVASCRIPT */
      }}
      style={getScreenLayout()}
	  onNavigationStateChange={(event)=> resetRenderNavigationOptions(title, navigation, event.url) }
    />
  );
};

ProductDetailsScreen.propTypes = {
  route: PropTypes.object.isRequired,
};

export default ProductDetailsScreen;
