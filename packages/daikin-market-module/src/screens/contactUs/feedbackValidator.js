import { ValidatorHelper } from '@module/utility';

const {
  isValidPhoneNumber,
  hasFieldValue,
  isCorrectEmailFormat,
  addError,
  isNotExceedLength,
} = ValidatorHelper;

const feedbackValidator = values => {
  const errors = {};

  const { fullName, mobileNo, email, subject, message } = values;

  addError(errors, 'fullName', [
    hasFieldValue(fullName.trim()),
    'Full name is required',
  ]);

  addError(
    errors,
    'email',
    [hasFieldValue(email.trim()), 'Email address is required'],
    [isCorrectEmailFormat(email), 'Incorrect email format']
  );

  addError(
    errors,
    'mobileNo',
    [hasFieldValue(mobileNo), 'Phone number is required'],
    [isValidPhoneNumber(mobileNo), 'Please enter a valid phone number'],
    [
      isNotExceedLength(mobileNo, 10),
      'Mobile number cannot be more than 10 characters',
    ]
  );

  addError(errors, 'subject', [
    hasFieldValue(subject.trim()),
    'Subject is required',
  ]);

  addError(errors, 'message', [hasFieldValue(message), 'Message is required']);

  return errors;
};

export default feedbackValidator;
