import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import {
  Alert,
  Linking,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect, contactUs } from '@module/redux-marketing';
import {
  ScaleText,
  verticalScale,
  scale,
  NavigationService,
} from '@module/utility';

import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { Formik } from 'formik';

import {
  Colors,
  FormikDropdown,
  FormikImagePicker,
  NavDrawerMenu,
  HeaderText,
  FormikButton,
  FormikTextInput,
} from '@module/daikin-ui';

import feedbackValidator from './feedbackValidator';
import { inquiryType } from '../../constants/enum';
import stateEnum from '../../constants/stateEnum';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    backgroundColor: Colors.white,
  },
  contentContainer: {
    paddingVertical: verticalScale(15),
    paddingHorizontal: scale(15),
  },
  messageField: {
    height: verticalScale(140),
    lineHeight: verticalScale(16),
    textAlignVertical: 'top',
  },
  remark: {
    paddingVertical: verticalScale(15),
  },
  remarkText1: {
    textTransform: 'uppercase',
    fontWeight: '700',
    fontSize: ScaleText(12),
    color: Colors.darkGrey,
  },
  remarkText2: {
    fontWeight: '400',
    fontSize: ScaleText(12),
    paddingVertical: verticalScale(6),
  },
  remarkText3: {
    fontWeight: '400',
    fontStyle: 'italic',
    fontSize: ScaleText(9),
    paddingBottom: verticalScale(12),
    lineHeight: verticalScale(12),
    paddingVertical: verticalScale(18),
  },
  textBtn: {
    color: Colors.blue,
  },
});

const ContactUsScreen = ({
  submitSuccess,
  submitFeedbackParams,
  setInitialState,
  setSubmitSuccess,
  message,
  isLoading,
}) => {
  const navigation = useNavigation();
  const [resetFormState, setResetFormState] = React.useState(() => {});

  const fullNameRef = useRef();
  const mobileNoRef = useRef();
  const emailRef = useRef();
  const subjectRef = useRef();
  const messageRef = useRef();

  const inquiryLookup = React.useMemo(
    () =>
      inquiryType.map(data => ({
        key: data.value,
        label: data.label,
        value: data.value,
      })),
    []
  );

  const stateLookup = React.useMemo(
    () =>
      stateEnum.map(state => ({
        key: state,
        label: state,
        value: state,
      })),
    []
  );

  useFocusEffect(
    React.useCallback(() => {
      if (message) {
        Alert.alert('Feedback', message, [
          { text: 'OK', onPress: setInitialState },
        ]);
      }
    }, [message])
  );

  useFocusEffect(
    React.useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Inquiry</HeaderText>,
        headerLeft: () => <NavDrawerMenu navigation={navigation} />,
      });
    }, [navigation])
  );

  const callDaikin = () => {
    const phoneNumber = 'tel:130088324546';
    Linking.openURL(phoneNumber);
  };

  const initialValues = {
    inquiryType: inquiryLookup[0].value,
    fullName: '',
    mobileNo: '',
    email: '',
    state: stateLookup[0].value,
    subject: '',
    message: '',
    attachment: [],
  };

  // only reset form if submit success
  useFocusEffect(
    React.useCallback(() => {
      if (submitSuccess) {
        resetFormState();

        // reset submit success state
        setSubmitSuccess(false);
      }
    }, [submitSuccess])
  );

  return (
    <ScrollView style={[styles.container, styles.flex]}>
      <View style={styles.contentContainer}>
        <Formik
          initialValues={initialValues}
          onSubmit={(value, { resetForm }) => {
            setResetFormState(() => resetForm);
            submitFeedbackParams(value);
          }}
          validate={feedbackValidator}>
          <View style={styles.flex}>
            <FormikDropdown
              label="Inquiry Type *"
              name="inquiryType"
              items={inquiryLookup}
            />
            <FormikTextInput
              label="Full Name *"
              name="fullName"
              keyboardType="default"
              autoCapitalize="none"
              returnKeyType="next"
              forwardRef={fullNameRef}
              onSubmitEditing={() => mobileNoRef.current.focus()}
            />
            <FormikTextInput
              label="Mobile No *"
              name="mobileNo"
              textContentType="telephoneNumber"
              keyboardType="numeric"
              returnKeyType="next"
              maxLength={10}
              forwardRef={mobileNoRef}
              onSubmitEditing={() => emailRef.current.focus()}
            />
            <FormikTextInput
              label="Email *"
              name="email"
              autoCapitalize="none"
              textContentType="emailAddress"
              keyboardType="email-address"
              returnKeyType="next"
              forwardRef={emailRef}
            />
            <FormikDropdown label="State *" name="state" items={stateLookup} />
            <FormikTextInput
              label="Subject *"
              name="subject"
              forwardRef={subjectRef}
              returnKeyType="next"
              onSubmitEditing={() => messageRef.current.focus()}
            />
            <FormikTextInput
              textInputstyle={styles.messageField}
              label="Your Message *"
              name="message"
              multiline
              numberOfLines={10}
              returnKeyType="next"
              forwardRef={messageRef}
              onSubmitEditing={() => {}}
            />
            <FormikImagePicker name="attachment" />
            <View style={styles.remark}>
              <Text style={styles.remarkText1}>Service Line:</Text>
              <View style={styles.flexRow}>
                <Text style={styles.remarkText2}>Call Center:</Text>
                <TouchableOpacity onPress={callDaikin}>
                  <Text style={[styles.remarkText2, styles.textBtn]}>
                    1300-88-324526
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.flexRow}>
                <Text style={styles.remarkText2}>
                  Call center operating hours:{' '}
                </Text>
                <Text style={[styles.remarkText2, styles.textBtn]}>
                  Mon-Sat, 8.00am-8.00pm
                </Text>
              </View>
              <Text style={styles.remarkText3}>
                With the enforcement of the Personal Data Protection Act 2010
                (&quot;PDPA&quot;) since November 2013, we, at DAIKIN Group of
                Companies in Malaysia. wish to inform you that we are committed
                to the protection of our customers&quot; and business
                partners&quot; personal information. Please visit{' '}
                <Text
                  style={styles.textBtn}
                  onPress={() =>
                    Linking.openURL(
                      'http://www.daikinmalaysia.com/privacy-policy'
                    )
                  }>
                  http://www.daikinmalaysia.com/privacy-policy
                </Text>{' '}
                to view our Privacy Notice. Unless you have otherwise notified
                us in writing, we will presume that you have read and consented
                to our processing of your personal data as set out in the
                Privacy Notice
              </Text>
            </View>
            <FormikButton primary disabledPrimary isLoading={isLoading}>
              Send
            </FormikButton>
          </View>
        </Formik>
      </View>
    </ScrollView>
  );
};

const mapStateToProps = ({ contactUs: contactUsState }) => ({
  message: contactUsState.message,
  isLoading: contactUsState.isLoading,
  submitSuccess: contactUsState.submitSuccess,
});

const mapDispatchToProps = dispatch => ({
  submitFeedbackParams: feedbackParams => {
    dispatch(contactUs.submitFeedbackParams(feedbackParams));
  },
  setInitialState: () => {
    dispatch(contactUs.setInitialState());
  },
  setSubmitSuccess: submit => {
    dispatch(contactUs.setSubmitSuccess(submit));
  },
});

ContactUsScreen.propTypes = {
  message: PropTypes.string,
  isLoading: PropTypes.bool,
  submitFeedbackParams: PropTypes.func,
  setInitialState: PropTypes.func,
  submitSuccess: PropTypes.bool,
  setSubmitSuccess: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsScreen);
