import React from 'react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import WebView from 'react-native-webview';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import {
  verticalScale,
  viewportHeight,
  NavigationService,
} from '@module/utility';

import { Colors, HeaderText, HeaderBackButton } from '@module/daikin-ui';

const styles = StyleSheet.create({
  loadingCenter: {
    paddingTop: verticalScale(30),
    height: viewportHeight,
    justifyContent: 'flex-start',
  },
});

const HelpWebScreen = () => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Help</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const renderLoading = React.useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  return (
    <WebView
      startInLoadingState
      renderLoading={renderLoading}
      source={{
        uri:
          'http://public.daikin.io/DMSS_CMS/XPOCMS_PromotionLink/21/Content.html',
      }}
    />
  );
};

export default HelpWebScreen;
