import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { ActivityIndicator, View, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { NavigationService } from '@module/utility';

import { HeaderBackButton, HeaderText } from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'center' },
});

const WebviewScreen = ({ route }) => {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(true);

  const { title, selectedUrl } = route.params;

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>{title}</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const renderLoading = () => (
    <View style={styles.container}>
      <ActivityIndicator size="large" />
    </View>
  );

  const getScreenLayout = () => {
    if (isLoading) return { flex: 0 };
    return { flex: 1 };
  };

  return (
    <WebView
      source={{ uri: selectedUrl }}
      startInLoadingState={isLoading}
      renderLoading={renderLoading}
      onLoadEnd={syntheticEvent => {
        // update component to be aware of loading status
        const { nativeEvent } = syntheticEvent;
        setIsLoading(nativeEvent.loading);
      }}
      style={getScreenLayout()}
    />
  );
};

WebviewScreen.propTypes = {
  route: PropTypes.object,
};

export default WebviewScreen;
