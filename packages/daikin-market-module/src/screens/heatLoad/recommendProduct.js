import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from '@module/redux-marketing';
import Config from 'react-native-config';
import {
  View,
  StyleSheet,
  Image,
  Linking,
  Platform,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  scale,
  verticalScale,
  ScaleText,
  NavigationService,
  isSmallDevice,
  iphoneXS,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import Modal from 'react-native-modal';

import {
  CacheImage,
  Link as LinkText,
  Colors,
  Checkbox,
  Button,
  HeaderText,
  Text,
  HeaderBackButton,
} from '@module/daikin-ui';
import { WEBVIEW_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  topCaintainer: {
    backgroundColor: Colors.aliceBlue,
    height: verticalScale(80),
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: ScaleText(20),
    fontWeight: 'bold',
  },
  rowContainer: {
    margin: 5,
    flexDirection: 'row',
  },
  ColumnContainer: {
    flexDirection: 'column',
	width: scale(150),
  },
  xLabelColumnContainer: {
    flexDirection: 'column',
	width: scale(20),
	marginRight: scale(10),
  },
  flexCenter:{
	alignSelf: 'center',
  },
  infoIcon: {
    right: 10,
    top: 23,
    position: 'absolute',
  },
  heatLoadHeader: {
    margin: 15,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  heatLoadDetails: {
    margin: 15,
  },
  header: {
    fontSize: ScaleText(16),
    textAlign: 'center'
  },
  detailLabel: {
    color: Colors.tabBarLabelGrey,
  },
  sectionContainer: {
    backgroundColor: Colors.weeklyTimerGroupHeaderGrey,
    height: verticalScale(25),
    justifyContent: 'center',
    paddingLeft: 10,
  },
  sectionLabel: {
    color: Colors.sectionHeaderTitleGrey,
  },
  productContainer: {
    margin: 20,
    paddingBottom: verticalScale(80),
  },
  disclaimerContainer: {
    padding: 15,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: scale(30),
    marginVertical: Platform.select({
      default: isSmallDevice ? scale(120) : scale(200),
      ios: iphoneXS ? scale(225) : scale(180),
    }),
    borderRadius: 15,
    backgroundColor: Colors.white,
  },
  modalTop: {
    position: 'absolute',
    top: 15,
    width: '100%',
  },
  modalFooter: {
    position: 'absolute',
    bottom: 10,
    width: '100%',
  },
  textAlign: {
    textAlign: 'center',
    marginTop: 10,
  },
  textJustify: {
    textAlign: 'justify',
  },
  dismissBtn: {
    marginTop: 5
  },
  dismissLabel: {
    color: Colors.activeBlue,
    fontSize: ScaleText(18),
    textAlign: 'center'
  },
  line: {
    borderWidth: 0.5,
    borderColor: Colors.stoneGrey,
  },
  productWrap: {
    backgroundColor: Colors.aliceBlue,
    width: scale(150),
    borderRadius: 5,
    marginTop: 10,
    padding: 10,
  },
  activeProductWrap: {
    backgroundColor: Colors.activeBlue,
  },
  itemsIcon: {
    width: scale(130),
    height: verticalScale(100),
    resizeMode: 'cover',
  },
  itemsLabel: {
    fontSize: ScaleText(11),
  },
  activeItemLabel: {
    color: Colors.white,
  },
  compareBtn: {
    position: 'absolute',
    bottom: 15,
    width: scale(310),
    margin: 20,
    zIndex: 1,
  },
  marginTop10: {
    marginTop: 10,
  },
  marginLeft5: {
    marginLeft: 5,
  },
  windowContainer: {
    height: verticalScale(40),
    width: scale(280),
    flexDirection: 'column',
  },
  windowWrap: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
	marginLeft: 5,
  },
  inverterWrap: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  productAlert: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
  },
  marginLeft55: {
    marginLeft: scale(55),
  },
  marginLeft30: {
    marginLeft: scale(30),
  },
  unSelectContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function addEnergyUnit(val) {
  const rangeValue = val
    .split(/[^0-9,]/)
    .filter(k => k)
    .map(k => `${k}W`);
  return `${rangeValue[0]} ${rangeValue[1] !== undefined ? `(${rangeValue[1]} - ${rangeValue[2]})` : ''
    }`;
}

function inlineTextButton(text) {
  return text.split(' ').map(k => <Text>{k} </Text>);
}

const RecommendProduct = ({
  room,
  noOfPeople,
  totalBTU,
  windows,
  measurementScale,
  allProduct,
}) => {
  const [moreDetail, setMoreDetail] = useState(false);
  const [openDisclaimer, setOpenDisclaimer] = useState(false);
  const [openCheckBox, setOpenCheckBox] = useState(false);
  const [inverterProduct, setInverterProduct] = useState([]);
  const [nonInverterProduct, setNonInverterProduct] = useState([]);
  const [selectItem, setSelectItem] = useState([]);
  const navigation = useNavigation();

  let inverterList = [];
  let nonInverterList = [];

  function splitProduct(data) {
    data.forEach(value => {
      if (value.Type === 'Inverter') {
        inverterList.push(value);
      } else if (value.Type === 'Non Inverter') {
        nonInverterList.push(value);
      }
    });
    inverterList = inverterList.filter(
      k =>
        Number(k.RatedCapacity) >= totalBTU &&
        Number(k.RatedCapacity) <= totalBTU + 5000
    );
    nonInverterList = nonInverterList.filter(
      k =>
        Number(k.RatedCapacity) >= totalBTU &&
        Number(k.RatedCapacity) <= totalBTU + 5000
    );
    setInverterProduct(inverterList);
    setNonInverterProduct(nonInverterList);

    const allFiltered = inverterList.concat(nonInverterList);
    setSelectItem(
      allFiltered.map(k => ({
        Oid: k.Oid,
        ProductSeriesID: k.ProductSeriesID,
        checked: false,
      }))
    );
  }

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Heat Load Calculator</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  React.useEffect(() => {
    splitProduct(allProduct);
  }, []);

  const NoProductAlert = () => (
    <View style={styles.productAlert}>
      {inlineTextButton(
        'Sorry, there are no products matching your search. Please call'
      )}
      <LinkText onPress={() => Linking.openURL('tel:130088324546')}>
        1300-88-324546{' '}
      </LinkText>
      {inlineTextButton('for products recommendation. Thank you.')}
    </View>
  );

  const checkedProduct = k => {
    const item = selectItem[selectItem.findIndex(j => j.Oid === k.Oid)];
    return item !== undefined ? item.checked : null;
  };

  const handleCompare = () => {
    if (selectItem.filter(k => k.checked).length < 1) {
      setOpenCheckBox(true);
    } else {
      NavigationService.navigate(WEBVIEW_SCREEN, {
        title: 'Compare Product',
        selectedUrl: `${Config.DAIKIN_URL}/compare-product/?ids=${selectItem
          .filter(k => k.checked)
          .map(k => k.ProductSeriesID)
          .join(',')}`,
      });
    }
  };

  const handleUnselect = () => {
    setOpenCheckBox(false);
    const allFiltered = inverterProduct.concat(nonInverterProduct);
    setSelectItem(
      allFiltered.map(k => ({
        Oid: k.Oid,
        ProductSeriesID: k.ProductSeriesID,
        checked: false,
      }))
    );
  };

  const handleCheckbox = id => {
    const newSelected = selectItem.map(k => {
      const data = k;
      if (data.Oid === id) {
        data.checked = !data.checked;
      }
      return data;
    });
    setSelectItem(newSelected);
  };

  return (
    <>
      <ScrollView style={styles.container} bounces={false}>
        <Modal
          isVisible={openDisclaimer}
          animationIn="slideInUp"
          animationOut="slideOutDown"
          useNativeDriver={Platform.os === 'android'}
          style={styles.modalContainer}
          onBackdropPress={() => setOpenDisclaimer(false)}
        >
          <View style={styles.modalTop}>
            <Text bold style={styles.header}>
              Disclaimer
              </Text>
          </View>
          <View style={styles.disclaimerContainer}>
            <Text fullText style={styles.textJustify}>
              This calculator is provided for a quick and approximate check of
              heat loads. It should not be considered as a replacement for
              expert advice or a precise and detailed heat load calculation.
              It is advised to reconfirm with your air conditioner contractors
              again before you place your order. We accept no responsibility
                or liability resulting or howsoever arising from its use.{' '}
            </Text>
          </View>
          <View style={styles.modalFooter}>
            <View style={styles.line} />
            <TouchableOpacity
              onPress={() => setOpenDisclaimer(false)}
              style={styles.dismissBtn}>
              <Text bold style={styles.dismissLabel}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <View style={styles.topCaintainer}>
          <Text>RECOMMENDED COOLING CAPACITY :</Text>
          <TouchableWithoutFeedback onPress={() => setOpenDisclaimer(true)}>
            <View style={styles.infoIcon}>
              <Image source={require('../../shared/assets/info.png')} />
            </View>
          </TouchableWithoutFeedback>
          <Text style={styles.title}>{numberWithCommas(totalBTU)} BTU/HR</Text>
        </View>

        <View style={styles.heatLoadHeader}>
          <Text style={styles.header}>Heat load calculator detail</Text>
          <TouchableWithoutFeedback onPress={() => setMoreDetail(!moreDetail)}>
            <Image source={require('../../shared/assets/dropDownBold.png')} />
          </TouchableWithoutFeedback>
        </View>

        {moreDetail && (
          <View style={styles.heatLoadDetails}>
            <View style={styles.rowContainer}>
              <View style={styles.ColumnContainer}>
                <Text style={styles.detailLabel}>Measurement Scale</Text>
                <Text style={styles.detailLabel}>
                  {measurementScale ? 'Feet' : 'Meter'}
                </Text>
              </View>
			  <View style={[styles.xLabelColumnContainer]}></View>
              <View style={[styles.ColumnContainer]}>
                <Text style={styles.detailLabel}>No Of People</Text>
                <Text style={styles.detailLabel}>{noOfPeople}</Text>
              </View>
            </View>
            <View style={styles.rowContainer}>
              <View style={styles.ColumnContainer}>
                <Text style={styles.detailLabel}>Room Width</Text>
                <Text style={styles.detailLabel}>{room.width}</Text>
              </View>
              <View style={[styles.xLabelColumnContainer, styles.flexCenter]}>
                <Text>X</Text>
              </View>
              <View style={[styles.ColumnContainer]}>
                <Text style={styles.detailLabel}>Room Length</Text>
                <Text style={styles.detailLabel}>{room.length}</Text>
              </View>
            </View>
            <Text style={[styles.detailLabel, styles.marginTop10, styles.marginLeft5]}>Window</Text>

            <View style={styles.windowWrap}>
              {windows.map((data, index) => (
                <View
                  style={styles.windowContainer}
                  // eslint-disable-next-line react/no-array-index-key
                  key={`${data.height}_${data.length}_${index}`}
                >
                  <Text style={styles.detailLabel}>
                    {`Length x Height: ${data.length} x ${data.height}`}
                  </Text>
                  <Text style={styles.detailLabel}>
                    {data.direction ? 'East/West' : 'North/South'}
                  </Text>
                </View>
              ))}
            </View>
          </View>
        )}

        <View style={styles.sectionContainer}>
          <Text style={styles.sectionLabel}>Recommended Product</Text>
        </View>

        <View style={styles.productContainer}>
          {inverterProduct.length === 0 && nonInverterProduct.length === 0 ? (
            <NoProductAlert />
          ) : (
            <>
              <View style={styles.unSelectContainer}>
                <Text style={styles.header}>Inverter</Text>
                {openCheckBox && (
                  <LinkText onPress={() => handleUnselect()}>Unselect</LinkText>
                )}
              </View>

              <View style={styles.inverterWrap}>
                {inverterProduct.length === 0 ? (
                  <NoProductAlert />
                ) : (
                  inverterProduct.map(k => (
                    <>
                      <TouchableOpacity
                        style={StyleSheet.flatten([
                          styles.productWrap,
                          checkedProduct(k) && styles.activeProductWrap,
                        ])}
                        onPress={() =>
                          NavigationService.navigate(WEBVIEW_SCREEN, {
                            title: 'Product Details',
                            selectedUrl: k.ProductUrlLink,
                          })
                        }>
                        {openCheckBox && (
                          <Checkbox
                            value={checkedProduct(k)}
                            onPress={() => handleCheckbox(k.Oid)}
                          />
                        )}
                        <CacheImage
                          source={{ uri: k.ProductImageLink }}
                          style={styles.itemsIcon}
                        />
                        <Text
                          bold
                          style={StyleSheet.flatten([
                            styles.textAlign,
                            checkedProduct(k) && styles.activeItemLabel,
                          ])}>
                          {k.Series}
                        </Text>
                        <Text
                          style={StyleSheet.flatten([
                            styles.itemsLabel,
                            checkedProduct(k) && styles.activeItemLabel,
                          ])}>
                          {k.EnergyRating === '0'
                            ? ''
                            : `${k.EnergyRating} Star Rating`}
                        </Text>
                        <Text
                          style={StyleSheet.flatten([
                            styles.itemsLabel,
                            checkedProduct(k) && styles.activeItemLabel,
                          ])}>{`${addEnergyUnit(
                            numberWithCommas(k.RatedPowerConsumption)
                          )}`}</Text>
                        <Text
                          style={StyleSheet.flatten([
                            styles.itemsLabel,
                            checkedProduct(k) && styles.activeItemLabel,
                          ])}>{`${numberWithCommas(
                            k.RatedCapacity
                          )} BTU/HR `}</Text>
                      </TouchableOpacity>
                    </>
                  ))
                )}
              </View>

              <Text style={styles.header}>Non - Inverter</Text>

              <View style={styles.inverterWrap}>
                {nonInverterProduct.length === 0 ? (
                  <NoProductAlert />
                ) : (
                  nonInverterProduct.map(k => (
                    <>
                      <TouchableOpacity
                        style={StyleSheet.flatten([
                          styles.productWrap,
                          checkedProduct(k) && styles.activeProductWrap,
                        ])}
                        onPress={() =>
                          NavigationService.navigate(WEBVIEW_SCREEN, {
                            title: 'Product Details',
                            selectedUrl: k.ProductUrlLink,
                          })
                        }>
                        {openCheckBox && (
                          <Checkbox
                            value={checkedProduct(k)}
                            onPress={() => handleCheckbox(k.Oid)}
                          />
                        )}
                        <CacheImage
                          source={{ uri: k.ProductImageLink }}
                          style={styles.itemsIcon}
                        />
                        <Text
                          bold
                          style={StyleSheet.flatten([
                            styles.textAlign,
                            checkedProduct(k) && styles.activeItemLabel,
                          ])}>
                          {k.Series}
                        </Text>
                        <Text
                          style={StyleSheet.flatten([
                            styles.itemsLabel,
                            checkedProduct(k) && styles.activeItemLabel,
                          ])}>
                          {k.EnergyRating === '0'
                            ? ''
                            : `${k.EnergyRating} Star Rating`}
                        </Text>
                        <Text
                          style={StyleSheet.flatten([
                            styles.itemsLabel,
                            checkedProduct(k) && styles.activeItemLabel,
                          ])}>{`${addEnergyUnit(
                            numberWithCommas(k.RatedPowerConsumption)
                          )}`}</Text>
                        <Text
                          style={StyleSheet.flatten([
                            styles.itemsLabel,
                            checkedProduct(k) && styles.activeItemLabel,
                          ])}>{`${numberWithCommas(
                            k.RatedCapacity
                          )} BTU/HR `}</Text>
                      </TouchableOpacity>
                    </>
                  ))
                )}
              </View>
            </>
          )}
        </View>
      </ScrollView>
      <View style={styles.compareBtn}>
        <Button
          primary
          disabledPrimary
          disabled={
            inverterProduct.length === 0 && nonInverterProduct.length === 0
          }
          onPress={() => handleCompare()}>
          Compare
        </Button>
      </View>
    </>
  );
};

const mapStateToProps = ({ heatLoadCalculator: heatLoadCalculatorState }) => {
  const {
    room,
    noOfPeople,
    totalBTU,
    windows,
    measurementScale,
    allProduct,
  } = heatLoadCalculatorState;
  return {
    room,
    noOfPeople,
    totalBTU,
    windows,
    measurementScale,
    allProduct,
  };
};

const mapDispatchToProps = () => ({});

RecommendProduct.propTypes = {
  room: PropTypes.object,
  noOfPeople: PropTypes.number,
  totalBTU: PropTypes.number,
  measurementScale: PropTypes.number,
  windows: PropTypes.array,
  allProduct: PropTypes.array,
};

export default connect(mapStateToProps, mapDispatchToProps)(RecommendProduct);
