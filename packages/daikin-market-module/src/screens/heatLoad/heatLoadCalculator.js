import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Platform,
  View,
  TextInput,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Keyboard,
  FlatList,
  Alert,
  Image,
} from 'react-native';
import { heatLoadCalculator, connect } from '@module/redux-marketing';
import {
  scale,
  verticalScale,
  ScaleText,
  NavigationService,
  decimalValidator,
  integerValidator,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import Modal from 'react-native-modal';

import {
  NavDrawerMenu,
  Colors,
  Text,
  HeaderText,
  Button,
} from '@module/daikin-ui';

import { RECOMMEND_PRODUCT_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  margin20: {
    margin: 20,
  },
  padding20: {
    padding: 20,
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 2,
	marginBottom: 7,
  },
  measurementBtn: {
    borderWidth: 2,
    padding: 10,
    width: scale(140),
    height: verticalScale(35),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    borderColor: Colors.stoneGrey,
  },
  inputContainer: {
    width: scale(140),
  },
  inputBorder: {
    borderWidth: 2,
    borderRadius: 6,
    borderColor: Colors.stoneGrey,
    paddingStart: 10,
    padding: 0,
    marginBottom: 5,
	marginTop: 2,
    height: verticalScale(35),
  },
  activeMeasurementBtn: {
    backgroundColor: Colors.activeBlue,
  },
  measurementText: {
    color: Colors.white,
    //fontWeight: 'bold',
  },
  columnContainer: {
    flexDirection: 'column',
  },
  xLabel: {
    alignSelf: 'center',
    marginTop: verticalScale(10),
  },
  leftMargin: {
    marginLeft: 15,
  },
  fullWidth: {
    width: scale(310),
  },
  window: {
    backgroundColor: Colors.weeklyTimerGroupHeaderGrey,
    height: verticalScale(25),
    justifyContent: 'center',
    paddingLeft: 10,
  },
  calculateBtn: {
    position: 'absolute',
    bottom: 15,
    width: scale(310),
  },
});

const modalStyles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  innerContainer: {
    minHeight: verticalScale(250),
    backgroundColor: Colors.white,
  },
  title: {
    fontSize: ScaleText(14),
    fontWeight: 'bold',
  },
  buttonText: {
    fontSize: ScaleText(14),
    fontWeight: 'bold',
    color: Colors.blue,
  },
  titleSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: scale(15),
  },
  windowsContainer: {
    height: verticalScale(190),
    width: scale(315),
  },
});

const HeatLoadCalculatorScreen = ({
  room,
  windows,
  setRooms,
  setWindows,
  initialize,
  noOfPeople,
  calculateBTU,
  setNoOfPeople,
  setMeasurementScale,
}) => {
  const navigation = useNavigation();
  const [isFeet, setIsFeet] = useState(1);
  const [isNorthSouth, setIsNorthSouth] = useState(true);
  const [isAddWindow, setIsAddWindow] = useState(false);
  const [windowHeight, setWindowHeight] = useState(0);
  const [windowLength, setWindowLength] = useState(0);
  const [extraData, setExtraData] = useState(0);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Heat Load Calculator</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
    });
  }, [navigation]);

  useFocusEffect(
    React.useCallback(() => {
      initialize();
    }, [])
  );

  const handleMesurementBtn = value => {
    setIsFeet(1 - value);
    setMeasurementScale(1 - value);
  };
  const handleNorthSouthBtn = value => {
    setIsNorthSouth(!value);
  };
  const handleOnClose = () => {
    setIsAddWindow(false);
    Keyboard.dismiss();
  };
  const handleOnConfirm = () => {
    if (windowHeight && windowLength) {
      setWindows([
        ...windows,
        {
          length: windowHeight,
          height: windowLength,
          direction: !isNorthSouth,
        },
      ]);
      setWindowHeight(0);
      setWindowLength(0);
      handleOnClose();
    } else Alert.alert('Error', 'Please insert text input');
  };

  const onChangeText = (value, type) => {
    switch (type) {
      case 'roomWidth':
        setRooms({ width: decimalValidator(value), length: room.length });
        break;
      case 'roomLength':
        setRooms({ width: room.width, length: decimalValidator(value) });
        break;
      case 'noOfPeople':
        setNoOfPeople(integerValidator(value));
        break;
      case 'windowLength':
        setWindowLength(decimalValidator(value));
        break;
      default:
        setWindowHeight(decimalValidator(value));
        break;
    }
  };
  const handleCalculate = () => {
    const allEmpty = !room.width || !room.length || !noOfPeople;
    if (allEmpty) {
      Alert.alert('Error', 'Please insert all input');
    } else {
      calculateBTU();

      NavigationService.navigate(RECOMMEND_PRODUCT_SCREEN);
    }
  };

  const renderItem = (item, index) => (
    <TouchableWithoutFeedback>
      <View style={styles.rowContainer}>
        <View style={styles.columnContainer}>
          <Text>
            Length x Height: {item.length} x {item.height}
          </Text>
          <Text>{item.direction ? 'West / East' : 'North / South'}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            Alert.alert(
              '',
              'Are you sure want to delete ?',
              [
                {
                  text: 'Cancel',
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => {
                    windows.splice(index, 1);
                    setExtraData(extraData + 1);
                  },
                },
              ],
              { cancelable: false }
            );
          }}>
          <Image source={require('../../shared/assets/delete.png')} />
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.margin20}>
          <Text>Measurement Scale*</Text>
          <View style={styles.rowContainer}>
            <TouchableWithoutFeedback
              onPress={() => {
                handleMesurementBtn(isFeet);
              }}
              disabled={isFeet}>
              <View
                style={StyleSheet.flatten([
                  styles.measurementBtn,
                  isFeet && styles.activeMeasurementBtn,
                ])}>
                <Text
                  style={StyleSheet.flatten([
                    isFeet && styles.measurementText,
                  ])}>
                  Feet
                </Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              style={StyleSheet.flatten([
                styles.measurementBtn,
                !isFeet && styles.activeMeasurementBtn,
              ])}
              onPress={() => {
                handleMesurementBtn(isFeet);
              }}
              disabled={!isFeet}>
              <View
                style={StyleSheet.flatten([
                  styles.measurementBtn,
                  !isFeet && styles.activeMeasurementBtn,
                ])}>
                <Text
                  style={StyleSheet.flatten([
                    !isFeet && styles.measurementText,
                  ])}>
                  Meter
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <View style={styles.rowContainer}>
            <View style={styles.columnContainer}>
              <Text>Room Width*</Text>
              <TextInput
                style={[styles.inputBorder, styles.inputContainer]}
                value={room.width === 0 ? '' : `${room.width}`}
                onChangeText={text => onChangeText(text, 'roomWidth')}
                keyboardType="numeric"
              />
            </View>
            <View style={styles.xLabel}>
              <Text bold>x</Text>
            </View>
            <View style={styles.columnContainer}>
              <Text>Room Length*</Text>
              <TextInput
                style={[styles.inputBorder, styles.inputContainer]}
                value={room.length === 0 ? '' : `${room.length}`}
                onChangeText={text => onChangeText(text, 'roomLength')}
                keyboardType="numeric"
              />
            </View>
          </View>

          <View style={styles.columnContainer}>
            <Text>No Of People*</Text>
            <TextInput
              style={[styles.inputBorder]}
              value={noOfPeople === 0 ? '' : `${noOfPeople}`}
              onChangeText={text => onChangeText(text, 'noOfPeople')}
              keyboardType="numeric"
            />
          </View>
        </View>

        <View style={styles.window}>
          <Text>Window</Text>
        </View>
        <Modal
          isVisible={isAddWindow}
          onBackdropPress={handleOnClose}
          useNativeDriver={Platform.OS === 'android'}
          style={modalStyles.container}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            enabled={Platform.OS !== 'android'}
            style={{ justifyContent: 'flex-end' }}>
            <View style={modalStyles.innerContainer}>
              <View style={modalStyles.titleSection}>
                <Text style={modalStyles.title}>Add Window</Text>
                <TouchableOpacity onPress={() => handleOnConfirm()}>
                  <Text style={modalStyles.buttonText}>Done</Text>
                </TouchableOpacity>
              </View>
              <View style={[styles.rowContainer, styles.padding20]}>
                <View style={styles.columnContainer}>
                  <Text>Window Length*</Text>
                  <TextInput
                    style={[styles.inputBorder, styles.inputContainer]}
                    value={windowLength === 0 ? '' : `${windowLength}`}
                    onChangeText={text => onChangeText(text, 'windowLength')}
                    keyboardType="numeric"
                  />
                </View>
                <View style={styles.xLabel}>
                  <Text bold>x</Text>
                </View>
                <View style={styles.columnContainer}>
                  <Text>Window Height*</Text>
                  <TextInput
                    style={[styles.inputBorder, styles.inputContainer]}
                    value={windowHeight === 0 ? '' : `${windowHeight}`}
                    onChangeText={text => onChangeText(text)}
                    keyboardType="numeric"
                  />
                </View>
              </View>
              <View style={styles.padding20}>
                <Text>Window Direction*</Text>
                <View style={styles.rowContainer}>
                  <TouchableWithoutFeedback
                    onPress={() => {
                      handleNorthSouthBtn(isNorthSouth);
                    }}
                    disabled={isNorthSouth}>
                    <View
                      style={StyleSheet.flatten([
                        styles.measurementBtn,
                        isNorthSouth && styles.activeMeasurementBtn,
                      ])}>
                      <Text
                        style={StyleSheet.flatten([
                          isNorthSouth && styles.measurementText,
                        ])}>
                        North / South
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback
                    style={StyleSheet.flatten([
                      styles.measurementBtn,
                      !isNorthSouth && styles.activeMeasurementBtn,
                    ])}
                    onPress={() => {
                      handleNorthSouthBtn(isNorthSouth);
                    }}
                    disabled={!isNorthSouth}>
                    <View
                      style={StyleSheet.flatten([
                        styles.measurementBtn,
                        !isNorthSouth && styles.activeMeasurementBtn,
                      ])}>
                      <Text
                        style={StyleSheet.flatten([
                          !isNorthSouth && styles.measurementText,
                        ])}>
                        East / West
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </Modal>

        <View style={styles.margin20}>
          <Button onPress={() => setIsAddWindow(true)}>Add Window</Button>

          <FlatList
            data={windows}
            extraData={extraData}
            renderItem={({ item, index }) => renderItem(item, index)}
            keyExtractor={item => item.id}
            style={styles.windowsContainer}
          />
        </View>

        <View style={[styles.margin20, styles.calculateBtn]}>
          <Button primary disabledPrimary onPress={() => handleCalculate()}>
            Calculate
          </Button>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const mapStateToProps = ({ heatLoadCalculator: heatLoadCalculatorState }) => {
  const { room, noOfPeople, windows } = heatLoadCalculatorState;
  return {
    room,
    windows,
    noOfPeople,
  };
};

const mapDispatchToProps = dispatch => ({
  initialize: () => {
    dispatch(heatLoadCalculator.init());
  },
  setMeasurementScale: measurementScale => {
    dispatch(heatLoadCalculator.setMeasurementScale(measurementScale));
  },
  setRooms: room => {
    dispatch(heatLoadCalculator.setRooms(room));
  },
  setNoOfPeople: noOfPeople => {
    dispatch(heatLoadCalculator.setNoOfPeople(noOfPeople));
  },
  setWindows: windows => {
    dispatch(heatLoadCalculator.setWindows(windows));
  },
  calculateBTU: () => {
    dispatch(heatLoadCalculator.calculateBTU());
  },
});

HeatLoadCalculatorScreen.propTypes = {
  initialize: PropTypes.func,
  setMeasurementScale: PropTypes.func,
  setRooms: PropTypes.func,
  setNoOfPeople: PropTypes.func,
  setWindows: PropTypes.func,
  calculateBTU: PropTypes.func,
  room: PropTypes.object,
  noOfPeople: PropTypes.string,
  windows: PropTypes.array,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeatLoadCalculatorScreen);
