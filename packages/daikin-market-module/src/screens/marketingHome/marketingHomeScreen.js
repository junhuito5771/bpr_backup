import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { verticalScale, scale, NavigationService } from '@module/utility';
import {
  connect,
  content,
  marketingHome,
  setConfigure,
} from '@module/redux-marketing';
import Modal from 'react-native-modal';
import Config from 'react-native-config';
import SplashScreen from 'react-native-splash-screen';

import {
  NavDrawerMenu,
  Spacing,
  GridBox,
  Colors,
  MainCarousel,
  CacheImage,
  Text,
  HeaderText,
} from '@module/daikin-ui';

import {
  HOME_SCREEN,
  LOCATE_DEALER_SCREEN,
  LOGIN_SCREEN,
  PROMOTION_SCREEN,
  HEAT_LOAD_CALCULATOR_SCREEN,
  PRODUCT_SCREEN,
  SERVICE_BOOKING,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  overlay: {
    alignItems: 'center',
  },
  container: {
    padding: scale(12),
    backgroundColor: Colors.white,
    height: '100%',
  },
  exploreContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginStart: scale(6),
  },
  carouselContainer: {
    marginVertical: verticalScale(6),
  },
  carouselContentContainer: {
    alignItems: 'center',
  },
  carouselViewText: {
    textAlign: 'right',
    color: Colors.blue,
    marginHorizontal: scale(12),
  },
  otherContainer: {
    marginBottom: verticalScale(25),
    marginStart: scale(6),
  },
  otherContentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: verticalScale(12),
  },
  otherTextContainer: {
    flex: 0.8,
  },
  otherButtonContainer: {
    flex: 0.2,
    alignItems: 'center',
  },
  otherButton: {
    borderWidth: 1,
    borderRadius: 100,
    borderColor: 'rgba(140, 140, 140, 0.3)',
    height: verticalScale(45),
    width: verticalScale(45),
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadow: {
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  popUpImage: {
    width: scale(290),
    height: verticalScale(380),
  },
  popUpButton: {
    position: 'absolute',
    width: 25,
    height: 25,
    backgroundColor: Colors.white,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    top: -10,
    right: -10,
    zIndex: 1,
  },
  popUpButtonImage: {
    width: 15,
    height: 15,
  },
});

const MarketingHomeScreen = ({
  promotions,
  popUp,
  error,
  isPopUpShown,
  isLogin,
  init,
  setIsPopUpShown,
  isWelcomed,
  setError,
}) => {
  const navigation = useNavigation();
  const [showPopUp, setShowPopUp] = React.useState(true);

  // prevent pop up on other pages other than marketing home
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      setShowPopUp(false);
    });

    return unsubscribe;
  }, [navigation]);

  useFocusEffect(
    React.useCallback(() => {
      setConfigure({
        bucketUrl: Config.BUCKET_URL,
        bucketEndpoint: Config.BUCKET_ENDPOINT,
      });
      init();

      SplashScreen.hide();
    }, [])
  );

  useFocusEffect(
    React.useCallback(() => {
      if (error) {
        Alert.alert('Error', error, [{ text: 'OK', onPress: setError }]);
      }
    }, [error])
  );

  useFocusEffect(
    React.useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Daikin</HeaderText>,
        headerLeft: () => <NavDrawerMenu navigation={navigation} />,
      });
    }, [navigation])
  );

  const handlePress = React.useCallback(
    item => {
      NavigationService.navigate('MarketingPromotion', {
        link: item.link,
        title: item.title,
      });
    },
    [navigation, promotions]
  );

  const renderExploreOptions = React.useMemo(() => {
    const options = {
      SmartControl: {
        key: 'smartcontrol',
        icon: require('../../shared/assets/marketing/smartControl.png'),
        label: <Text>Smart Control</Text>,
        onPress: () => {
          NavigationService.navigate(isLogin ? HOME_SCREEN : LOGIN_SCREEN);
        },
      },
      EnergyConsumption: {
        key: 'energyconsumption',
        icon: require('../../shared/assets/marketing/energy.png'),
        label: <Text>Energy Consumption</Text>,
        onPress: () => {
          NavigationService.navigate(isLogin ? 'Statistics' : LOGIN_SCREEN);
        },
      },
      Products: {
        key: 'product',
        icon: require('../../shared/assets/marketing/products.png'),
        label: <Text>Products</Text>,
        onPress: () => {
          NavigationService.navigate(PRODUCT_SCREEN);
        },
      },
      // WarrantyRegisteration: {
      //   icon: require('common/shared/images/marketing/warranty.png'),
      //   label: <Text>Warranty Registration/Status</Text>,
      //   onPress: () => {},
      // },
      HeatloadCalculator: {
        key: 'heatload',
        icon: require('../../shared/assets/marketing/calculator.png'),
        label: <Text>Heat Load Calculator</Text>,
        onPress: () => {
          NavigationService.navigate(HEAT_LOAD_CALCULATOR_SCREEN);
        },
      },
      WhereToBuy: {
        key: 'whereToBuy',
        icon: require('../../shared/assets/marketing/whereToBuy.png'),
        label: <Text>Where To Buy</Text>,
        onPress: () => {
          NavigationService.navigate(LOCATE_DEALER_SCREEN);
        },
      },
      ServiceBooking: {
        key: 'serviceBooking',
        icon: require('../../shared/assets/marketing/serviceBooking.png'),
        label: <Text>Service Booking</Text>,
        onPress: () => {
          NavigationService.navigate(isLogin ? SERVICE_BOOKING : LOGIN_SCREEN);
        },
      },
    };

    return Object.values(options).map(option => {
      const { key, icon, label, onPress } = option;
      return <GridBox key={key} icon={icon} label={label} onPress={onPress} />;
    });
  }, []);

  const renderCarouselOptions = React.useMemo(() => {
    const data = promotions.map(item => ({
      image: { uri: item.imageLink },
      title: item.title,
      link: item.hyperlink,
    }));

    return (
      <>
        <View style={styles.carouselContentContainer}>
          <MainCarousel
            data={data}
            sliderWidth={scale(310)}
            itemWidth={scale(300)}
            handlePress={handlePress}
          />
        </View>
        {data.length > 1 && (
          <TouchableOpacity
            onPress={() => {
              NavigationService.navigate(PROMOTION_SCREEN);
            }}
            hitSlop={{
              top: 10,
              bottom: 10,
              left: 10,
              right: 10,
            }}>
            <Text small style={styles.carouselViewText}>
              View All
            </Text>
          </TouchableOpacity>
        )}
      </>
    );
  }, [promotions]);

  const renderOtherOptions = React.useMemo(() => {
    const options = {
      // TODO: hide info maintenance at current phase
      // InfoMaintenance: {
      //   title: (
      //     <Text medium bold>
      //       Info &amp; Maintenance
      //     </Text>
      //   ),
      //   description: (
      //     <Text>
      //       Set maintenance reminder to keep your air-conditioner in good shape.
      //     </Text>
      //   ),
      //   buttonIcon: require('common/shared/images/marketing/btnInfo.png'),
      //   onPress: () => {},
      // },
      Chatbot: {
        title: (
          <Text medium bold>
            Daikin Chatbot
          </Text>
        ),
        description: <Text>Chat with Us.</Text>,
        buttonIcon: require('../../shared/assets/marketing/chatbot.png'),
        onPress: () => {
          NavigationService.navigate('Chatbot');
        },
      },
    };

    return Object.values(options).map(
      ({ title, description, buttonIcon, onPress }) => (
        <View key={title} style={styles.otherContentContainer}>
          <View style={styles.otherTextContainer}>
            {title}
            {description}
          </View>
          <View style={styles.otherButtonContainer}>
            <TouchableOpacity onPress={onPress}>
              <View style={[styles.otherButton]}>
                <Image source={buttonIcon} />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )
    );
  }, []);

  const handleClosePopUp = React.useCallback(() => {
    setShowPopUp(false);
    setIsPopUpShown(true);
  }, [isPopUpShown, showPopUp]);

  const renderPopUp = React.useMemo(
    () =>
      popUp && popUp.length > 0 && showPopUp && !isPopUpShown && isWelcomed ? (
        <Modal
          onBackButtonPress={handleClosePopUp}
          isVisible={showPopUp}
          style={styles.overlay}>
          <TouchableWithoutFeedback onPress={handleClosePopUp}>
            <View>
              <TouchableOpacity
                hitSlop={{
                  top: 20,
                  bottom: 20,
                  right: 20,
                }}
                onPress={handleClosePopUp}
                style={styles.popUpButton}>
                <Image
                  style={styles.popUpButtonImage}
                  source={require('../../shared/assets/marketing/close.png')}
                />
              </TouchableOpacity>
              <CacheImage
                style={styles.popUpImage}
                source={{ uri: popUp[popUp.length - 1].imageLink }}
                resizeMode="stretch"
              />
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      ) : (
        <></>
      ),
    [popUp, showPopUp, isWelcomed]
  );

  return (
    <>
      {renderPopUp}
      <ScrollView style={styles.container}>
        <Spacing />
        <Text medium bold>
          Explore Daikin
        </Text>
        <Spacing />
        <View style={styles.exploreContainer}>{renderExploreOptions}</View>
        <Spacing />
        <View style={styles.carouselContainer}>{renderCarouselOptions}</View>
        <View style={styles.otherContainer}>{renderOtherOptions}</View>
      </ScrollView>
    </>
  );
};

MarketingHomeScreen.propTypes = {
  isLogin: PropTypes.bool,
  init: PropTypes.func,
  promotions: PropTypes.array,
  popUp: PropTypes.array,
  setIsPopUpShown: PropTypes.func,
  isPopUpShown: PropTypes.bool,
  isWelcomed: PropTypes.bool,
  error: PropTypes.string,
  setError: PropTypes.func,
};

const mapStateToProps = ({
  login,
  marketingHome: marketingHomeState,
  content: contentState,
  welcome: welcomeState,
}) => ({
  isLogin: login.isLogin,
  promotions: marketingHomeState.promotions,
  popUp: marketingHomeState.popUp,
  error: contentState.error,
  isPopUpShown: marketingHomeState.isPopUpShown,
  isWelcomed: welcomeState.isWelcomed,
});

const mapDispatchToProps = dispatch => ({
  init: () => {
    dispatch(marketingHome.init());
  },
  setIsPopUpShown: isPopUpShown => {
    dispatch(marketingHome.setIsPopUpShown(isPopUpShown));
  },
  setError: () => {
    dispatch(content.setContentError(null));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarketingHomeScreen);
