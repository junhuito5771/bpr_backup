import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import WebView from 'react-native-webview';
import {
  verticalScale,
  viewportHeight,
  NavigationService,
} from '@module/utility';

import { HeaderBackButton, Colors, HeaderText } from '@module/daikin-ui';

const styles = StyleSheet.create({
  loadingCenter: {
    paddingTop: verticalScale(30),
    height: viewportHeight,
    justifyContent: 'flex-start',
  },
});

const ChatBotScreen = () => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Daikin Chatbot</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const renderLoading = React.useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  return (
    <WebView
      startInLoadingState
      renderLoading={renderLoading}
      source={{ uri: 'https://www.daikin.com.my/chat/' }}
    />
  );
};

export default ChatBotScreen;
