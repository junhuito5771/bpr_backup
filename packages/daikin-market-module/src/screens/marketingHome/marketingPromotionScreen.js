import React from 'react';
import PropTypes from 'prop-types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import WebView from 'react-native-webview';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import {
  verticalScale,
  viewportHeight,
  NavigationService,
} from '@module/utility';

import { HeaderBackButton, Colors, HeaderText } from '@module/daikin-ui';

const styles = StyleSheet.create({
  loadingCenter: {
    paddingTop: verticalScale(30),
    height: viewportHeight,
    justifyContent: 'flex-start',
  },
});

const MarketingPromotionScreen = ({ route }) => {
  const navigation = useNavigation();
  const { link, title } = route.params;

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>{title}</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const renderLoading = React.useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  return (
    <WebView
      startInLoadingState
      renderLoading={renderLoading}
      source={{ uri: link }}
    />
  );
};

MarketingPromotionScreen.propTypes = {
  route: PropTypes.object.isRequired,
};

export default MarketingPromotionScreen;
