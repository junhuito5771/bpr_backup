import React from 'react';
import PropTypes from 'prop-types';
import { verticalScale, scale, NavigationService } from '@module/utility';

import { View, StyleSheet, ScrollView, SafeAreaView } from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  HeaderBackButton,
  Colors,
  Button,
  HeaderText,
  ImageBoxButton,
} from '@module/daikin-ui';
import { errorCode, connect } from '@module/redux-marketing';
import remoteEnums from '../../../constants/remoteEnum';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  container: {
    backgroundColor: Colors.white,
  },
  scrollContainer: {
    marginHorizontal: scale(10),
    marginVertical: verticalScale(16),
  },
  remoteContainer: {
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: verticalScale(40),
    marginTop: verticalScale(20),
  },
  button: {
    marginHorizontal: scale(10),
    marginVertical: verticalScale(16),
  },
});

const SelectRemoteScreen = ({ selectedRemote, setSelectedRemote }) => {
  const navigation = useNavigation();
  const [focusRemote, setFocusRemote] = React.useState(selectedRemote);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Select Remote Control Type</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const handleSelectPress = () => {
    setSelectedRemote(focusRemote);
    navigation.goBack();
  };

  return (
    <SafeAreaView style={[styles.container, styles.flex]}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={[styles.flex, styles.scrollContainer]}>
        <View style={[styles.remoteContainer, styles.row]}>
          {remoteEnums.map(remote => (
            <ImageBoxButton
              isSelected={focusRemote && focusRemote.key === remote.key}
              key={remote.key}
              label={remote.label}
              image={remote.image}
              onPress={() => {
                setFocusRemote(remote);
              }}
            />
          ))}
        </View>
      </ScrollView>
      <Button
        style={styles.button}
        disabled={focusRemote === null}
        disabledPrimary
        primary
        onPress={handleSelectPress}>
        Select
      </Button>
    </SafeAreaView>
  );
};

SelectRemoteScreen.propTypes = {
  selectedRemote: PropTypes.string,
  setSelectedRemote: PropTypes.func,
};

const mapStateToProps = ({ errorCode: errorCodeState }) => ({
  selectedRemote: errorCodeState.selectedRemote,
});

const mapDispatchToProps = dispatch => ({
  setSelectedRemote: selectedRemote => {
    dispatch(errorCode.setSelectedRemote(selectedRemote));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectRemoteScreen);
