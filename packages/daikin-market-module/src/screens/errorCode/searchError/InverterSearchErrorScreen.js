import React from 'react';
import PropTypes from 'prop-types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import {
  scale,
  ScaleText,
  verticalScale,
  NavigationService,
} from '@module/utility';
import { connect, errorCode } from '@module/redux-marketing';

import {
  HeaderBackButton,
  Colors,
  Dropdown,
  Button,
  HeaderText,
} from '@module/daikin-ui';
import {
  damaFirstCodes,
  damaSecondCodes,
  ditFirstCodes,
  ditSecondCodes,
} from '../../../constants/errorCodes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
  },
  contentContainer: {
    paddingHorizontal: scale(16),
    paddingVertical: verticalScale(16),
  },
  header: {
    backgroundColor: Colors.aliceBlue,
    paddingVertical: verticalScale(25),
    paddingHorizontal: scale(33),
    marginVertical: verticalScale(24),
    marginHorizontal: verticalScale(24),
    minHeight: verticalScale(150),
    borderColor: Colors.lightCoolGrey,
    borderWidth: 1,
    borderRadius: 4,
  },
  headerTitle: {
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
  },
  title: {
    color: Colors.darkGrey,
    fontWeight: '700',
    fontSize: ScaleText(18),
    marginHorizontal: scale(10),
  },
  label: {
    marginHorizontal: scale(4),
    marginTop: verticalScale(20),
    fontSize: ScaleText(12),
    fontWeight: '500',
    color: Colors.black,
  },
  content: {
    color: Colors.sectionHeaderTitleGrey,
    fontSize: ScaleText(12),
    marginVertical: verticalScale(25),
  },
  dropdown: {
    marginHorizontal: scale(4),
  },
  error: {
    color: Colors.red,
  },
});

const InverterSearchErrorScreen = ({
  setInitialState,
  selectedRemote,
  setSelectedErrorCode,
  getResult,
  result,
}) => {
  const navigation = useNavigation();
  const [errorCodeState, setErrorCodeState] = React.useState('00');
  const [isResultDirty, setIsResultDirty] = React.useState(false);

  React.useEffect(() => {
    setInitialState();
  }, []);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Search Error Code</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const handleErrorCodeChange = React.useCallback(
    (value, replaceIndex = 0) => {
      const errorCodeSplit = errorCodeState.split('');
      errorCodeSplit[replaceIndex] = value;

      setErrorCodeState(errorCodeSplit.join(''));
    },
    [errorCodeState]
  );

  const firstErrorCodes = React.useMemo(() => {
    let errorCodes;

    if (selectedRemote) {
      errorCodes =
        selectedRemote.type === 'dit' ? ditFirstCodes : damaFirstCodes;
    } else {
      errorCodes = damaFirstCodes;
    }
    return errorCodes.map(item => ({
      label: item,
      value: item,
    }));
  }, [selectedRemote]);

  const secondErrorCodes = React.useMemo(() => {
    let errorCodes;
    if (selectedRemote) {
      errorCodes =
        selectedRemote.type === 'dit' ? ditSecondCodes : damaSecondCodes;
    } else {
      errorCodes = damaSecondCodes;
    }
    return errorCodes.map(item => ({
      label: item,
      value: item,
    }));
  }, [selectedRemote]);

  const handleSubmit = React.useCallback(() => {
    setIsResultDirty(true);
    setSelectedErrorCode(errorCodeState);
    getResult();
  }, [selectedRemote, errorCodeState, result, setSelectedErrorCode]);

  const renderResult = React.useCallback(() => {
    const noResult = isResultDirty && result === null;

    return (
      <Text style={[styles.content, noResult && styles.error]}>
        {/* eslint-disable-next-line no-nested-ternary */}
        {result === null
          ? isResultDirty
            ? 'Error Code not found.'
            : '-'
          : result}
      </Text>
    );
  }, [isResultDirty, result]);

  const handleClear = () => {
    setInitialState();
    setIsResultDirty(false);
  };

  return (
    <SafeAreaView style={[styles.container, styles.flex]}>
      <View style={styles.header}>
        <View style={[styles.row, styles.headerTitle]}>
          <Image
            source={require('../../../shared/assets/general/errorResult.png')}
          />
          <Text style={[styles.title]}>Search Result</Text>
        </View>
        {renderResult()}
      </View>
      <View style={[styles.contentContainer, styles.flex]}>
        <View style={styles.flex}>
          <Dropdown
            navigateTo={() => navigation.navigate('SelectRemote')}
            label="Select Models"
            placeholder={
              selectedRemote ? selectedRemote.key : 'Select Remote Control Type'
            }
          />
          <Text style={styles.label}>Choose the error code as displayed</Text>
          <View style={[styles.row]}>
            <View style={[styles.flex, styles.dropdown]}>
              <Dropdown
                items={firstErrorCodes}
                onValueChange={value => handleErrorCodeChange(value, 0)}
              />
            </View>
            <View style={[styles.flex, styles.dropdown]}>
              <Dropdown
                items={secondErrorCodes}
                onValueChange={value => handleErrorCodeChange(value, 1)}
              />
            </View>
          </View>
        </View>
        <Button
          onPress={handleSubmit}
          disabled={!selectedRemote}
          primary
          disabledPrimary>
          Search
        </Button>
        {(result || isResultDirty) && (
          <Button onPress={handleClear}>Clear</Button>
        )}
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = ({ errorCode: errorCodeState }) => {
  const { selectedRemote, result } = errorCodeState;

  return {
    selectedRemote,
    result,
  };
};

const mapDispatchToProps = dispatch => ({
  setInitialState: () => {
    dispatch(errorCode.setInitialState());
  },
  setSelectedRemote: selectedRemote => {
    dispatch(errorCode.setSelectedRemote(selectedRemote));
  },
  setSelectedErrorCode: code => {
    dispatch(errorCode.setSelectedErrorCode(code));
  },
  getResult: () => {
    dispatch(errorCode.getResult());
  },
});

InverterSearchErrorScreen.propTypes = {
  selectedRemote: PropTypes.object,
  setInitialState: PropTypes.func,
  setSelectedErrorCode: PropTypes.func,
  getResult: PropTypes.func,
  result: PropTypes.string,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InverterSearchErrorScreen);
