import React from 'react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import {
  Linking,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {
  scale,
  ScaleText,
  verticalScale,
  NavigationService,
} from '@module/utility';

import {
  HeaderBackButton,
  Colors,
  Button,
  HeaderText,
} from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
  },
  contentContainer: {
    paddingHorizontal: scale(16),
    paddingVertical: verticalScale(20),
  },
  row: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
  },
  flexSub: {
    flex: 3,
  },
  title: {
    color: Colors.darkGrey,
    fontWeight: '700',
    fontSize: ScaleText(18),
  },
  subtitle: {
    color: Colors.darkGrey,
    fontWeight: '500',
    fontSize: ScaleText(12),
  },
  content: {
    color: Colors.sectionHeaderTitleGrey,
    fontSize: ScaleText(12),
  },
  link: {
    color: Colors.blue,
  },
  tableHeader: {
    backgroundColor: Colors.tableHeaderBlue,
    height: verticalScale(50),
    alignItems: 'center',
  },
  tableBody: {
    minHeight: verticalScale(50),
    alignItems: 'center',
  },
  oddRow: {
    backgroundColor: Colors.aliceBlue,
  },
  columnCenter: {
    textAlign: 'center',
  },
  verticalSpacing: {
    marginVertical: verticalScale(6),
  },
  button: {
    marginHorizontal: scale(16),
    marginVertical: verticalScale(16),
  },
});

const errorCodes = [
  {
    errorCode: 'E1',
    faultIndication: 'Room air sensor contact loose / short',
  },
  {
    errorCode: 'E2',
    faultIndication: 'Indoor coil sensor open',
  },
  {
    errorCode: 'E4',
    faultIndication:
      'Compressor overload/\nIndoor coil sensor short/\nOutdoor coil sensor short',
  },
  {
    errorCode: 'E5',
    faultIndication: 'Outdoor abnormal',
  },
  {
    errorCode: 'E8',
    faultIndication: 'Hardware error (tact switch pin short)',
  },
  {
    errorCode: 'E9',
    faultIndication: 'No feedback from indoor fan',
  },
];

const NonInverterSearchErrorCodeScreen = () => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Search Error Code</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const HelpLabel = React.useCallback(
    () => (
      <TouchableOpacity onPress={() => Linking.openURL('tel:130088324546')}>
        <Text style={[styles.content, styles.link, styles.verticalSpacing]}>
          Need help? Contact us for professional assistance.
        </Text>
      </TouchableOpacity>
    ),
    []
  );

  const ErrorCodeTable = React.useCallback(
    () => (
      <View>
        <View style={[styles.row, styles.tableHeader]}>
          <Text style={[styles.subtitle, styles.flex, styles.columnCenter]}>
            {'Error\nCode'}
          </Text>
          <Text style={[styles.subtitle, styles.flexSub]}>
            Fault Indication
          </Text>
        </View>
        {errorCodes.map((data, index) => (
          <View
            style={[styles.row, styles.tableBody, index % 2 && styles.oddRow]}
            key={data.errorCode}>
            <Text style={[styles.content, styles.flex, styles.columnCenter]}>
              {data.errorCode}
            </Text>
            <Text style={[styles.content, styles.flexSub]}>
              {data.faultIndication}
            </Text>
          </View>
        ))}
      </View>
    ),
    []
  );

  return (
    <SafeAreaView style={[styles.container, styles.flex]}>
      <View style={[styles.contentContainer, styles.flex]}>
        <Text style={[styles.title, styles.verticalSpacing]}>
          Search Result
        </Text>
        <Text style={[styles.content, styles.verticalSpacing]}>
          Choose the error code as displayed
        </Text>
        <ScrollView>
          <ErrorCodeTable />
          <HelpLabel />
        </ScrollView>
      </View>
      <Button
        onPress={() => navigation.goBack()}
        style={styles.button}
        primary
        disabledPrimary>
        Done
      </Button>
    </SafeAreaView>
  );
};

export default NonInverterSearchErrorCodeScreen;
