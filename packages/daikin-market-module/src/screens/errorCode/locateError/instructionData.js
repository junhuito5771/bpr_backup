import React from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { scale, ScaleText, verticalScale } from '@module/utility';
import { Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  instructionContainer: {
    marginVertical: verticalScale(25),
  },
  descriptionContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  descriptionContent: {
    backgroundColor: Colors.onOffBlue,
    paddingHorizontal: scale(30),
    paddingVertical: verticalScale(10),
  },
  flex: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
  },
  instruction: {
    marginVertical: verticalScale(10),
  },
  instructionText: {
    textAlign: 'center',
    fontWeight: '400',
    fontSize: ScaleText(12),
    color: Colors.darkGrey,
  },
  label: {
    fontWeight: '700',
    fontSize: ScaleText(18),
    color: Colors.darkGrey,
    marginVertical: verticalScale(10),
  },
  description: {
    color: Colors.darkGrey,
    fontSize: ScaleText(12),
    marginVertical: verticalScale(6),
  },
  smallDescription: {
    color: Colors.darkGrey,
    fontSize: ScaleText(10),
    marginVertical: verticalScale(6),
  },
  embeddedText: {
    color: Colors.blue,
    fontSize: ScaleText(12),
  },
  remark: {
    color: Colors.blue,
    marginVertical: verticalScale(5),
    marginHorizontal: scale(20),
  },
});

const EmbeddedText = ({ text }) => (
  <Text style={styles.embeddedText}>{text}</Text>
);

EmbeddedText.propTypes = {
  text: PropTypes.string,
};

const instructionsData = [
  {
    key: 'step1',
    instruction: (
      <View style={[styles.instructionContainer, styles.center]}>
        <Image
          style={styles.instruction}
          source={require('../../../shared/assets/errorCode/step1.png')}
        />
        <Image
          style={styles.instruction}
          source={require('../../../shared/assets/errorCode/step1_indicator.png')}
        />
        <Text style={styles.instructionText}>
          {'"00" indication flashes on the\ntemperature display section'}
        </Text>
      </View>
    ),
    description: (
      <View style={[styles.descriptionContainer, styles.flex]}>
        <View style={styles.descriptionContent}>
          <Text style={styles.label}>Step 1</Text>
          <Text style={styles.description}>
            When the <EmbeddedText text="ON TIMER CANCEL" /> button or{' '}
            <EmbeddedText text="OFF TIMER CANCEL" /> button is held down for 5
            seconds, a “00” indication flashes on the temperature display
            section.
          </Text>
        </View>
      </View>
    ),
  },
  {
    key: 'step2',
    instruction: (
      <View style={[styles.instructionContainer, styles.center]}>
        <Image source={require('../../../shared/assets/errorCode/step2.png')} />
      </View>
    ),
    description: (
      <View style={[styles.descriptionContainer, styles.flex]}>
        <View style={styles.descriptionContent}>
          <Text style={styles.label}>Step 2</Text>
          <Text style={styles.description}>
            Press the <EmbeddedText text="ON TIMER CANCEL" /> button or{' '}
            <EmbeddedText text="OFF TIMER CANCEL" /> button repeatedly until a
            continuous beep is produced
          </Text>
          <Text style={styles.smallDescription}>
            *The code indication will change and will notify with a long beep.
          </Text>
          <Text style={styles.smallDescription}>
            *A short beep and two consecutive beeps indicate non-corresponding
            codes.
          </Text>
        </View>
      </View>
    ),
  },
  {
    key: 'step3',
    instruction: (
      <View style={[styles.instructionContainer, styles.center]}>
        <Image source={require('../../../shared/assets/errorCode/step3.png')} />
      </View>
    ),
    description: (
      <View style={[styles.descriptionContainer, styles.flex]}>
        <View style={styles.descriptionContent}>
          <Text style={styles.label}>Step 3</Text>
          <Text style={styles.description}>
            The temperature display sections indicate corresponding codes.
          </Text>
        </View>
      </View>
    ),
  },
  {
    key: 'step4',
    instruction: (
      <View style={[styles.instructionContainer, styles.center]}>
        <Image source={require('../../../shared/assets/errorCode/step4.png')} />
      </View>
    ),
    description: (
      <View style={[styles.descriptionContainer, styles.flex]}>
        <View style={styles.descriptionContent}>
          <Text style={styles.label}>Step 4</Text>
          <Text style={styles.description}>
            To cancel code display, hold on the{' '}
            <EmbeddedText text="ON TIMER CANCEL" /> button or{' '}
            <EmbeddedText text="OFF TIMER" /> button.
          </Text>
          <Text style={styles.description}>
            Hold <EmbeddedText text="CANCEL" /> button down for 5 seconds. The
            code display also cancels itself if the button is not pressed for 1
            minute.
          </Text>
        </View>
      </View>
    ),
    remark: (
      <TouchableOpacity onPress={() => Linking.openURL('tel:130088324546')}>
        <Text style={styles.remark}>
          Need help? Contact us for professional assistance.
        </Text>
      </TouchableOpacity>
    ),
  },
];

export default instructionsData;
