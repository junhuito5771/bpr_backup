import React from 'react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { scale, verticalScale, NavigationService } from '@module/utility';

import {
  Button,
  LocateErrorCarousel,
  Colors,
  HeaderText,
  HeaderBackButton,
} from '@module/daikin-ui';
import instructionsData from './locateError/instructionData';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    backgroundColor: Colors.white,
  },
  note: {
    backgroundColor: Colors.modeSeparatorGrey,
    paddingVertical: verticalScale(10),
    paddingHorizontal: scale(15),
  },
  button: {
    marginHorizontal: scale(15),
    marginVertical: verticalScale(16),
  },
  row: {
    flexDirection: 'row',
  },
});

const LocateErrorCodeScreen = () => {
  const navigation = useNavigation();
  const carouselRef = React.useRef(null);
  const [currentIndex, setCurrentIndex] = React.useState(0);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Locate Error Code</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const handleButtonPress = () => {
    if (currentIndex === instructionsData.length - 1) {
      navigation.goBack();
    } else if (carouselRef) {
      carouselRef.current.snapToNext(false);
    }
  };

  return (
    <SafeAreaView style={[styles.container, styles.flex]}>
      <ScrollView style={styles.flex}>
        <View style={styles.note}>
          <Text>
            Please use your remote controller and follow the steps as follow:
          </Text>
          <Text>
            Remote indicator might differ depend on your remote control model.
          </Text>
        </View>
        <View style={styles.flex}>
          <LocateErrorCarousel
            carouselRef={carouselRef}
            data={instructionsData}
            onSnapToItem={setCurrentIndex}
          />
        </View>
      </ScrollView>
      <Button onPress={handleButtonPress} style={styles.button} primary>
        {currentIndex === instructionsData.length - 1 ? 'Done' : 'Next Step'}
      </Button>
    </SafeAreaView>
  );
};

export default LocateErrorCodeScreen;
