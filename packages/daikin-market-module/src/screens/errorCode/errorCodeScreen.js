import React from 'react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import {
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  scale,
  ScaleText,
  verticalScale,
  NavigationService,
} from '@module/utility';

import { Button, NavDrawerMenu, Colors, HeaderText } from '@module/daikin-ui';

const { width: windowWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  verticalAlign: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    backgroundColor: Colors.white,
  },
  contentContainer: {
    marginHorizontal: scale(15),
    marginVertical: verticalScale(16),
  },
  title: {
    color: Colors.darkGrey,
    fontWeight: '500',
    fontSize: ScaleText(12),
    marginVertical: verticalScale(5),
  },
  subtitle: {
    fontSize: ScaleText(10),
    color: Colors.sectionBorderGrey,
  },
  button: {
    width: windowWidth / 2.2,
    minHeight: verticalScale(45),
    borderColor: Colors.borderlightGray,
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
    marginVertical: verticalScale(15),
    marginHorizontal: scale(3),
  },
  iconButton: {
    minHeight: verticalScale(95),
    paddingHorizontal: scale(15),
    paddingVertical: verticalScale(10),
  },
  textCenter: {
    textAlign: 'center',
  },
  verticalSpacing: {
    marginVertical: verticalScale(8),
  },
  focus: {
    borderColor: Colors.blue,
    backgroundColor: Colors.blue,
  },
  focusText: {
    color: Colors.white,
  },
});

const usageIconStyles = StyleSheet.create({
  button: {
    marginHorizontal: scale(5),
    paddingHorizontal: scale(12),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: verticalScale(30),
    borderColor: Colors.borderlightGray,
    borderRadius: 5,
    borderWidth: 1,
  },
  label: {
    paddingStart: scale(5),
    color: Colors.darkGrey,
    fontSize: ScaleText(12),
  },
});

const UsageIcon = () => (
  <View style={usageIconStyles.button}>
    <Image source={require('../../shared/assets/energy/energy.png')} />
    <Text style={usageIconStyles.label}>Usage</Text>
  </View>
);

const ErrorCodeScreen = () => {
  const navigation = useNavigation();

  const [focusAction, setFocusAction] = React.useState('locateError');
  const [focusInverter, setFocusInverter] = React.useState('inverter');

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Error Code</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
    });
  }, [navigation]);

  const inverterType = React.useMemo(
    () => [
      {
        key: 'inverter',
        label: 'Inverter',
        onPress: () => {},
      },
      {
        key: 'nonInverter',
        label: 'Non - Inverter',
        onPress: () => {},
      },
    ],
    []
  );

  const actionMenu = [
    {
      key: 'locateError',
      icon:
        focusAction === 'locateError'
          ? require('../../shared/assets/icons/locateErrorInverse.png')
          : require('../../shared/assets/icons/locateError.png'),
      title: 'Locate Error Code',
      subtitle: 'How do I locate where the error code is ?',
    },
    {
      key: 'searchError',
      icon:
        focusAction === 'searchError'
          ? require('../../shared/assets/icons/searchErrorInverse.png')
          : require('../../shared/assets/icons/searchError.png'),
      title: 'Search Error Code',
      subtitle: 'What does the error code mean ?',
    },
  ];

  const handleActionPress = item => {
    setFocusAction(focusAction === item.key ? null : item.key);
  };

  const handleInverterPress = item => {
    setFocusInverter(focusInverter === item.key ? null : item.key);
  };

  const renderActionMenu = () =>
    actionMenu.map(item => {
      const isFocus = focusAction === item.key;

      return (
        <TouchableOpacity
          key={item.key}
          style={[styles.button, styles.iconButton, isFocus && styles.focus]}
          onPress={() => handleActionPress(item)}>
          <>
            <Image source={item.icon} />
            <Text style={[styles.title, isFocus && styles.focusText]}>
              {item.title}
            </Text>
            <Text style={[styles.subtitle, isFocus && styles.focusText]}>
              {item.subtitle}
            </Text>
          </>
        </TouchableOpacity>
      );
    });

  const renderInterverMenu = () =>
    inverterType.map(item => {
      const isFocus = focusInverter === item.key;

      return (
        <TouchableOpacity
          key={item.key}
          style={[styles.button, isFocus && styles.focus]}
          onPress={() => handleInverterPress(item)}>
          <Text
            style={[
              styles.label,
              styles.textCenter,
              isFocus && styles.focusText,
            ]}>
            {item.label}
          </Text>
        </TouchableOpacity>
      );
    });

  const handleButtonPress = React.useCallback(() => {
    if (focusAction === 'locateError') {
      navigation.navigate('LocateErrorCode');
    }
    if (focusAction === 'searchError') {
      if (focusInverter === 'inverter') {
        navigation.navigate('InverterSearchErrorCode');
      } else {
        navigation.navigate('NonInverterSearchErrorCode');
      }
    }
  }, [focusAction, focusInverter]);

  return (
    <SafeAreaView style={[styles.container, styles.flex]}>
      <View style={[styles.contentContainer, styles.flex]}>
        <View style={styles.flex}>
          <Text style={styles.title}>
            Please choose your Air Conditioner type
          </Text>
          <View style={styles.verticalAlign}>
            <Text style={styles.subtitle}>Please refer to your</Text>
            <UsageIcon />
            <Text style={styles.subtitle}>to find out your air </Text>
            <Text style={styles.subtitle}>conditioner type.</Text>
          </View>
          <View style={styles.row}>{renderInterverMenu()}</View>
          <Text style={styles.title}>What are you looking for ?</Text>
          <View style={styles.row}>{renderActionMenu()}</View>
        </View>
        <Button
          onPress={handleButtonPress}
          primary
          disabledPrimary
          disabled={!focusAction || !focusInverter}>
          Next
        </Button>
      </View>
    </SafeAreaView>
  );
};

export default ErrorCodeScreen;
