import React from 'react';
import { SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { NavigationService } from '@module/utility';
import { HeaderBackButton, ReminderForm, HeaderText } from '@module/daikin-ui';

import { maintenance, connect } from '@module/redux-marketing';

const ProcessReminderScreen = ({
  route,
  reminders,
  addReminder,
  editReminder,
}) => {
  const navigation = useNavigation();
  const reminderIndex = route.params ? route.params.reminderIndex : null;

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: (
        <HeaderText>
          {reminderIndex !== null ? 'Edit Reminder' : 'Create Reminder'}
        </HeaderText>
      ),
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [reminderIndex]);

  const handleSubmit = form => {
    if (reminderIndex === null) {
      addReminder(form);
    } else {
      editReminder(form, reminderIndex);
    }
    navigation.goBack();
  };

  return (
    <SafeAreaView>
      <ReminderForm
        reminder={reminders[reminderIndex]}
        onSubmit={handleSubmit}
      />
    </SafeAreaView>
  );
};

ProcessReminderScreen.propTypes = {
  route: PropTypes.object,
  reminders: PropTypes.array,
  addReminder: PropTypes.func,
  editReminder: PropTypes.func,
};

const mapStateToProps = ({ maintenance: maintenanceState }) => ({
  reminders: maintenanceState.reminders,
});

const mapDispatchToProps = dispatch => ({
  addReminder: reminder => dispatch(maintenance.addReminder(reminder)),
  editReminder: (reminder, index) =>
    dispatch(maintenance.editReminder(reminder, index)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProcessReminderScreen);
