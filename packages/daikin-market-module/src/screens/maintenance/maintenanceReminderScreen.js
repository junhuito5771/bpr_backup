import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import {
  Alert,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  scale,
  ScaleText,
  verticalScale,
  NavigationService,
} from '@module/utility';

import { maintenance, connect } from '@module/redux-marketing';
import {
  NavDrawerMenu,
  ReminderBox,
  Colors,
  Button,
  HeaderText,
} from '@module/daikin-ui';

import { PROCESS_REMINDER_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  button: {
    marginRight: scale(16),
  },
  deleteButton: {
    marginHorizontal: scale(10),
    marginBottom: verticalScale(22),
  },
  createButton: {
    marginHorizontal: scale(20),
    borderColor: Colors.lightGrey,
  },
  createButtonText: {
    color: Colors.darkGrey,
  },
  separator: {
    height: verticalScale(10),
    backgroundColor: Colors.offWhite,
  },
  header: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: ScaleText(16),
    marginVertical: verticalScale(30),
    color: Colors.darkGrey,
  },
  content: {
    color: Colors.darkGrey,
    marginBottom: verticalScale(10),
    marginHorizontal: scale(20),
    lineHeight: verticalScale(16),
    fontSize: ScaleText(12),
  },
});

const MaintenanceReminderScreen = ({
  reminders,
  init,
  deleteReminders,
  error,
  setCalendarError,
}) => {
  const navigation = useNavigation();
  const [isEditMode, setIsEditMode] = useState(false);
  const [selectedReminders, setSelectedReminders] = useState([]);

  useFocusEffect(() => {
    if (error) {
      Alert.alert(
        'Maintenance Reminder',
        error,
        [
          {
            text: 'OK',
            onPress: () => {
              setCalendarError(null);
            },
          },
        ],
        { cancelable: false }
      );
    }
  }, [error]);

  useFocusEffect(
    React.useCallback(() => {
      init();
    }, [])
  );

  const handleReminderCreate = () => {
    NavigationService.navigate(PROCESS_REMINDER_SCREEN);
  };

  const handleEditPress = () => {
    if (reminders.length) {
      setIsEditMode(!isEditMode);
    }
  };

  const handleRowPress = (reminder, index) => {
    if (isEditMode) {
      setSelectedReminders(
        selectedReminders.includes(reminder)
          ? selectedReminders.filter(data => data !== reminder)
          : [...selectedReminders, reminder]
      );
    } else {
      NavigationService.navigate(PROCESS_REMINDER_SCREEN, {
        reminderIndex: index,
      });
    }
  };

  const handleConfirmDelete = () => {
    setIsEditMode(false);
    deleteReminders(selectedReminders);
  };

  const handleDelete = () => {
    Alert.alert(
      '',
      'Delete reminder?',
      [
        {
          text: 'No',
          style: 'cancel',
        },
        { text: 'Yes', onPress: handleConfirmDelete },
      ],
      { cancelable: false }
    );
  };

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Info &amp; Maintenance</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
      headerRight: () => (
        <View style={styles.row}>
          <TouchableOpacity
            style={styles.button}
            onPress={handleReminderCreate}>
            <Image source={require('../../shared/assets/reminder/plus.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={handleEditPress}>
            <Image
              source={require('../../shared/assets/reminder/delete.png')}
            />
          </TouchableOpacity>
        </View>
      ),
    });
  }, []);

  const Separator = React.useMemo(() => <View style={styles.separator} />, []);

  return (
    <>
      <ScrollView style={[styles.container]}>
        {reminders.length ? (
          reminders.map((reminder, index) => (
            <React.Fragment
              key={`${reminder.unitName}_${reminder.reminderName}_${
                index + 1
              }`}>
              <ReminderBox
                isEditMode={isEditMode}
                reminder={reminder}
                onRowPress={() => handleRowPress(reminder, index)}
              />
              {reminders.length - 1 !== index && Separator}
            </React.Fragment>
          ))
        ) : (
          <View>
            <Text style={styles.header}>No Reminder Found</Text>
            <Text style={styles.content}>
              Pull down to refresh if connected units not displayed.
            </Text>
            <Text style={styles.content}>
              Check that you are connected to the same wireless network that
              your device is connected to.
            </Text>
            <Text style={styles.content}>
              If you need to add your device, press &ldquo;Create
              Reminder&ldquo;.
            </Text>
            <Button
              onPress={handleReminderCreate}
              style={styles.createButton}
              textStyle={styles.createButtonText}>
              Create Reminder
            </Button>
          </View>
        )}
      </ScrollView>
      {isEditMode && (
        <Button onPress={handleDelete} style={styles.deleteButton}>
          Delete Reminder
        </Button>
      )}
    </>
  );
};

MaintenanceReminderScreen.propTypes = {
  reminders: PropTypes.array,
  init: PropTypes.func,
  deleteReminders: PropTypes.func,
  setCalendarError: PropTypes.func,
  error: PropTypes.string,
};

const mapStateToProps = ({ maintenance: maintenanceState }) => ({
  reminders: maintenanceState.reminders,
  error: maintenanceState.calendarError,
});

const mapDispatchToProps = dispatch => ({
  init: () => dispatch(maintenance.init()),
  deleteReminders: reminders =>
    dispatch(maintenance.deleteReminders(reminders)),
  setCalendarError: error => dispatch(maintenance.setCalendarError(error)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MaintenanceReminderScreen);
