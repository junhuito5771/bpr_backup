import React from 'react';
import PropTypes from 'prop-types';
import { connect } from '@module/redux-marketing';

import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { SafeAreaView } from 'react-native-safe-area-context';
import {
  scale,
  verticalScale,
  ScaleText,
  NavigationService,
} from '@module/utility';

import {
  NavDrawerMenu,
  Colors,
  CacheImage,
  HeaderText,
  Text,
} from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    height: '100%',
    paddingVertical: verticalScale(10),
    overflow: 'visible',
  },
  cardContainer: {
    backgroundColor: Colors.white,
    marginHorizontal: scale(20),
    marginVertical: verticalScale(6),
    borderRadius: scale(4),
  },
  cardFooter: {
    flexDirection: 'row',
    alignItems: 'center',
    height: verticalScale(40),
  },
  image: {
    borderTopLeftRadius: scale(4),
    borderTopRightRadius: scale(4),
    height: verticalScale(150),
    resizeMode: 'stretch',
  },
  label: {
    fontSize: ScaleText(12),
    marginStart: scale(20),
    flexGrow: 0.9,
  },
  shadow: {
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 2,
  },
});

const PromotionScreen = ({ promotions }) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Promotions</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
    });
  }, [navigation]);

  const data = React.useMemo(
    () =>
      promotions.map(item => ({
        key: item.id,
        image: { uri: item.imageLink },
        link: item.hyperlink,
        title: item.title,
      })),
    [promotions]
  );

  const renderPromotions = React.useMemo(
    () =>
      data.map(item => (
        <View key={item.key} style={[styles.cardContainer, styles.shadow]}>
          <TouchableOpacity
            onPress={() => {
              NavigationService.navigate('PromotionWeb', {
                link: item.link,
                title: item.title,
              });
            }}
            hitSlop={{
              top: 20,
            }}>
            <>
              <CacheImage
                style={styles.image}
                source={item.image}
                resizeMode="stretch"
              />
              <View style={styles.cardFooter}>
                <Text style={styles.label}>Find Out More</Text>
                <Image
                  source={require('../../shared/assets/general/vector.png')}
                />
              </View>
            </>
          </TouchableOpacity>
        </View>
      )),
    [data]
  );

  return (
    <SafeAreaView>
      <ScrollView style={styles.container}>{renderPromotions}</ScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = ({ marketingHome }) => ({
  promotions: marketingHome.promotions,
});

PromotionScreen.propTypes = {
  promotions: PropTypes.array,
};

export default connect(mapStateToProps, null)(PromotionScreen);
