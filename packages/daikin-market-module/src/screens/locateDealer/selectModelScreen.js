import React from 'react';
import PropTypes from 'prop-types';
import { verticalScale, scale, NavigationService } from '@module/utility';

import { View, StyleSheet, ScrollView, SafeAreaView } from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  HeaderBackButton,
  ImageBoxButton,
  Colors,
  HeaderText,
  Button,
} from '@module/daikin-ui';

import { locateDealer, connect } from '@module/redux-marketing';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  modelContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginHorizontal: scale(10),
    marginBottom: verticalScale(40),
    marginTop: verticalScale(20),
  },
  button: {
    marginHorizontal: scale(10),
    marginVertical: verticalScale(16),
  },
});

const SelectModelScreen = ({ selectedModel, setSelectedModel }) => {
  const navigation = useNavigation();
  const [focusModel, setFocusModel] = React.useState(selectedModel);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Select Model</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const models = React.useMemo(
    () => ({
      FTKM: {
        image: require('../../shared/assets/model/ftmk.png'),
        label: 'FTKM',
      },
      SMARTO: {
        image: require('../../shared/assets/model/smarto.png'),
        label: 'SMARTO',
      },
      FTKU: {
        image: require('../../shared/assets/model/ftku.png'),
        label: 'FTKU',
      },
      FTKF: {
        image: require('../../shared/assets/model/ftkf.png'),
        label: 'FTKF',
      },
      ALL: {
        image: require('../../shared/assets/model/all.png'),
        label: 'ALL',
      },
    }),
    []
  );

  const handleSelectPress = React.useCallback(() => {
    setSelectedModel(focusModel);
    navigation.goBack();
  }, [focusModel]);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.flex}>
        <View style={styles.modelContainer}>
          {Object.keys(models).map(model => (
            <ImageBoxButton
              isSelected={focusModel === model}
              key={model}
              label={models[model].label}
              image={models[model].image}
              onPress={() => {
                setFocusModel(model);
              }}
            />
          ))}
        </View>
      </ScrollView>
      <Button
        style={styles.button}
        disabled={focusModel === null}
        disabledPrimary
        primary
        onPress={handleSelectPress}>
        Select
      </Button>
    </SafeAreaView>
  );
};

SelectModelScreen.propTypes = {
  selectedModel: PropTypes.string,
  setSelectedModel: PropTypes.func,
};

const mapStateToProps = ({ locateDealer: locateDealerState }) => ({
  selectedModel: locateDealerState.selectedModel,
});

const mapDispatchToProps = dispatch => ({
  setSelectedModel: selectedModel => {
    dispatch(locateDealer.setSelectedModel(selectedModel));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectModelScreen);
