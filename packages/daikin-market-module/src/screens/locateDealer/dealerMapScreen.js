import React from 'react';
import { connect, locateDealer } from '@module/redux-marketing';
import PropTypes from 'prop-types';
import {
  MapHelper,
  heightPercentage,
  verticalScale,
  ScaleText,
  StringHelper,
  scale,
  NavigationService,
} from '@module/utility';

import Config from 'react-native-config';
import {
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { HeaderBackButton, HeaderText, Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  mapContainer: {
    height: heightPercentage(40),
  },
  map: {
    flex: 1,
  },
  header: {
    paddingStart: 25,
    height: verticalScale(30),
    justifyContent: 'center',
    backgroundColor: Colors.whiteGrey,
  },
  outletContainer: {
    flex: 1,
  },
  outletCard: {
    flexDirection: 'row',
    minHeight: verticalScale(120),
    margin: scale(12),
    marginVertical: verticalScale(6),
    padding: scale(12),
    borderRadius: 4,
    backgroundColor: Colors.lightWhite,
  },
  outletCardFocus: {
    backgroundColor: Colors.blue,
  },
  cardIndexBox: {
    width: 17,
    alignItems: 'center',
    zIndex: 1,
  },
  cardIndexIcon: {
    width: 17,
    height: 23,
    zIndex: 0,
  },
  cardIndexText: {
    fontSize: 9,
    color: Colors.white,
    position: 'absolute',
    top: 4,
  },
  cardIndexTextFocus: {
    color: Colors.blue,
  },
  cardTitle: {
    flexDirection: 'row',
  },
  cardLabelBox: {
    flexGrow: 1,
    width: '80%',
  },
  cardLabel: {
    fontWeight: '700',
    fontSize: ScaleText(12),
  },
  mapButton: {
    paddingHorizontal: 6,
  },
  cardContent: {
    flex: 1,
    marginHorizontal: scale(12),
  },
  cardPhoneRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: verticalScale(6),
  },
  cardPhone: {
    marginLeft: scale(8),
  },
  cardBodyText: {
    fontSize: ScaleText(12),
    fontWeight: '400',
  },
  focusFont: {
    color: Colors.white,
  },
});

const DealerMapScreen = ({ route, setSearchDealers }) => {
  const { dealers } = route.params;

  const navigation = useNavigation();
  const [focusIndex, setFocusIndex] = React.useState(null);
  const [region, setRegion] = React.useState({
    // will reverse latitude and longtitude passed
    // since it's reversed on Daikin CMS side
    // Malaysia initial lat/long
    title: '',
    latitude: dealers[0] ? parseFloat(dealers[0].longtitude) : 3.140853,
    longitude: dealers[0] ? parseFloat(dealers[0].latitude) : 101.693207,
    latitudeDelta: 0.5,
    longitudeDelta: 0.5,
  });

  useFocusEffect(
    React.useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Locate Dealer</HeaderText>,
        headerLeft: () => <HeaderBackButton navigation={navigation} />,
      });

      return () => setSearchDealers(null);
    }, [navigation])
  );

  const handleCardPress = React.useCallback(
    (dealer, index) => {
      setFocusIndex(index);

      // will reverse latitude and longtitude passed
      // since it's reversed on Daikin CMS side
      setRegion({
        title: dealer.name,
        latitude: parseFloat(dealer.longtitude),
        longitude: parseFloat(dealer.latitude),
        latitudeDelta: 0.01,
        longitudeDelta: 0.01,
      });
    },
    [dealers, focusIndex]
  );

  const handleGoogleMapPress = React.useCallback(
    index => {
      Linking.openURL(
        Platform.OS === 'ios'
          ? `https://www.google.com/maps/dir/?api=1&destination=${dealers[index].longtitude},${dealers[index].latitude}`
          : `geo://?q=${dealers[index].longtitude},${dealers[index].latitude}`
      );
    },
    [dealers]
  );

  const handleWazePress = React.useCallback(
    index => {
      Linking.openURL(
        `https://waze.com/ul?ll=${dealers[index].longtitude},${dealers[index].latitude}&navigate=yes`
      );
    },
    [dealers]
  );

  const renderMapView = React.useMemo(() => {
    const { MapView, Marker } = MapHelper.getMapViewComponent(Config.PROVIDER);

    return Config.PROVIDER === 'GMS' ? (
      <MapView
        style={styles.map}
        region={region}
        onRegionChangeComplete={currentRegion => setRegion(currentRegion)}>
        {focusIndex !== null ? (
          <Marker
            title={region.title}
            coordinate={{
              latitude: region.latitude,
              longitude: region.longitude,
            }}
          />
        ) : (
          <>
            {dealers.map(dealer => (
              <Marker
                title={dealer.name}
                key={dealer.id}
                coordinate={{
                  // will reverse latitude and longtitude passed
                  // since it's reversed on Daikin CMS side
                  latitude: parseFloat(dealer.longtitude),
                  longitude: parseFloat(dealer.latitude),
                }}
              />
            ))}
          </>
        )}
      </MapView>
    ) : (
      <MapView
        camera={{
          target: { latitude: region.latitude, longitude: region.longitude },
          zoom: 10,
        }}>
        {focusIndex !== null ? (
          <Marker
            coordinate={{
              latitude: region.latitude,
              longitude: region.longitude,
            }}
          />
        ) : (
          <></>
        )}
      </MapView>
    );
  }, [focusIndex, dealers]);

  const renderCards = React.useMemo(
    () =>
      dealers.map((d, index) => (
        <TouchableOpacity key={d.id} onPress={() => handleCardPress(d, index)}>
          <View
            style={[
              styles.outletCard,
              focusIndex === index && styles.outletCardFocus,
            ]}>
            <View>
              <View style={styles.cardIndexBox}>
                <Text
                  style={[
                    styles.cardIndexText,
                    focusIndex === index && styles.cardIndexTextFocus,
                  ]}>
                  {index + 1}
                </Text>
              </View>
              <Image
                style={styles.cardIndexIcon}
                source={
                  focusIndex === index
                    ? require('../../shared/assets/icons/dealer.png')
                    : require('../../shared/assets/icons/dealerOff.png')
                }
              />
            </View>
            <View style={styles.cardContent}>
              <View style={styles.cardTitle}>
                <View style={styles.cardLabelBox}>
                  <Text
                    style={[
                      styles.cardLabel,
                      focusIndex === index && styles.focusFont,
                    ]}>
                    {d.name}
                  </Text>
                </View>
                {Config.PROVIDER === 'GMS' && (
                  <TouchableOpacity
                    onPress={() => handleGoogleMapPress(index)}
                    style={styles.mapButton}>
                    <Image
                      source={
                        focusIndex === index
                          ? require('../../shared/assets/icons/googleMap.png')
                          : require('../../shared/assets/icons/googleMapOff.png')
                      }
                    />
                  </TouchableOpacity>
                )}
                <TouchableOpacity
                  onPress={() => handleWazePress(index)}
                  style={styles.mapButton}>
                  <Image
                    source={
                      focusIndex === index
                        ? require('../../shared/assets/icons/waze.png')
                        : require('../../shared/assets/icons/wazeOff.png')
                    }
                  />
                </TouchableOpacity>
              </View>
              <View>
                <Text
                  style={[
                    styles.cardBodyText,
                    focusIndex === index && styles.focusFont,
                  ]}>
                  {d.state}
                </Text>
                <Text
                  style={[
                    styles.cardBodyText,
                    focusIndex === index && styles.focusFont,
                  ]}>
                  {StringHelper.removeDuplicateComma(
                    `Address: ${d.address1}, ${d.address2}, ${d.address3} ${d.postcode}, ${d.city}, ${d.state}`
                  )}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(`tel:${d.telNo}`);
                  }}
                  style={styles.cardPhoneRow}>
                  <Image
                    source={
                      focusIndex === index
                        ? require('../../shared/assets/icons/phone.png')
                        : require('../../shared/assets/icons/phoneOff.png')
                    }
                  />
                  <Text
                    style={[
                      styles.cardBodyText,
                      styles.cardPhone,
                      focusIndex === index && styles.focusFont,
                    ]}>
                    {d.telNo}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )),
    [dealers, focusIndex]
  );

  return (
    <View style={styles.container}>
      <View style={styles.mapContainer}>{renderMapView}</View>
      <View style={styles.header}>
        <Text>Select Your Preferable Outlet</Text>
      </View>
      <ScrollView style={styles.outletContainer}>{renderCards}</ScrollView>
    </View>
  );
};

DealerMapScreen.propTypes = {
  route: PropTypes.object,
  setSearchDealers: PropTypes.func,
};

const mapStateToProps = ({ locateDealer: locateDealerState }) => ({
  dealers: locateDealerState.searchDealers,
});

const mapDispatchToProps = dispatch => ({
  setSearchDealers: dealers => {
    dispatch(locateDealer.setSearchDealers(dealers));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(DealerMapScreen);
