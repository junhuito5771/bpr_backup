import React from 'react';
import PropTypes from 'prop-types';

import { locateDealer, connect, content } from '@module/redux-marketing';
import { verticalScale, scale, NavigationService } from '@module/utility';

import { Alert, SafeAreaView, StyleSheet, View } from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  NavDrawerMenu,
  Colors,
  Dropdown,
  Button,
  HeaderText,
} from '@module/daikin-ui';
import { orderBy } from '@module/utility/src/ObjectHelper';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  dropdownContainer: {
    marginTop: verticalScale(16),
    marginHorizontal: scale(16),
  },
  stickBottom: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: scale(10),
    marginVertical: verticalScale(16),
  },
});

const LocateDealerScreen = ({
  init,
  selectedModel,
  selectedCity,
  selectedState,
  setSelectedCity,
  setSelectedState,
  setError,
  searchLocateDealer,
  cityStateOption,
  searchDealers,
  error,
  isSearching,
  setIsSearching,
}) => {
  const navigation = useNavigation();

  useFocusEffect(
    React.useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Locate Dealer</HeaderText>,
        headerLeft: () => <NavDrawerMenu navigation={navigation} />,
      });
    }, [navigation])
  );

  useFocusEffect(
    React.useCallback(() => {
      init();
    }, [])
  );

  useFocusEffect(
    React.useCallback(() => {
      if (error) {
        Alert.alert('Error', error, [
          { text: 'OK', onPress: () => setError(null) },
        ]);
      }
    }, [error])
  );

  const statesData = React.useMemo(
    () =>
      cityStateOption
        ? orderBy(
            Object.keys(cityStateOption).map(state => ({
              key: state,
              label: state,
              value: state,
            })),
            'label'
          )
        : [],
    [cityStateOption]
  );

  const citiesData = React.useMemo(
    () =>
      selectedState && cityStateOption[selectedState]
        ? orderBy(
            cityStateOption[selectedState].map(city => ({
              key: city,
              label: city,
              value: city,
            })),
            'label'
          )
        : [],
    [selectedState, setSelectedState, cityStateOption]
  );

  const handleStateValueChange = React.useCallback(
    value => {
      setSelectedState(value);
      setSelectedCity(null);
    },
    [selectedState, selectedCity]
  );

  const handleCityValueChange = React.useCallback(
    value => {
      setSelectedCity(value);
    },
    [selectedState, selectedCity]
  );

  const handleSearchPress = React.useCallback(() => {
    searchLocateDealer();
  }, [selectedModel, selectedState, selectedCity, searchDealers]);

  useFocusEffect(
    React.useCallback(() => {
      if (searchDealers && isSearching) {
        if (searchDealers.length) {
          navigation.navigate('DealerMap', { dealers: searchDealers });
        } else {
          setError('No dealers are located.');
        }
        setIsSearching(false);
      }
    }, [searchDealers, isSearching])
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.dropdownContainer}>
        <Dropdown
          navigateTo={() => navigation.navigate('SelectModel')}
          label="Select Models"
          placeholder={selectedModel || 'Select model name'}
        />
        <Dropdown
          value={selectedState}
          label="Select State"
          items={statesData}
          placeholder="Select a state"
          onValueChange={handleStateValueChange}
        />
        <Dropdown
          value={selectedCity}
          label="Select City"
          items={citiesData}
          placeholder="Select a city"
          onValueChange={handleCityValueChange}
        />
      </View>
      <View style={styles.stickBottom}>
        <Button
          primary
          disabled={!(selectedCity && selectedState && selectedModel)}
          disabledPrimary
          onPress={handleSearchPress}>
          Search
        </Button>
      </View>
    </SafeAreaView>
  );
};

LocateDealerScreen.propTypes = {
  init: PropTypes.func,
  selectedModel: PropTypes.string,
  selectedCity: PropTypes.string,
  selectedState: PropTypes.string,
  searchLocateDealer: PropTypes.func,
  setSelectedCity: PropTypes.func,
  setSelectedState: PropTypes.func,
  error: PropTypes.string,
  setError: PropTypes.func,
  cityStateOption: PropTypes.object,
  isSearching: PropTypes.bool,
  setIsSearching: PropTypes.func,
  searchDealers: PropTypes.array,
};

const mapStateToProps = ({
  locateDealer: locateDealerState,
  content: contentState,
}) => {
  const {
    selectedModel,
    selectedState,
    selectedCity,
    cityStateOption,
    searchDealers,
    isSearching,
  } = locateDealerState;
  const { error } = contentState;

  return {
    cityStateOption,
    selectedModel,
    selectedState,
    selectedCity,
    searchDealers,
    error,
    isSearching,
  };
};

const mapDispatchToProps = dispatch => ({
  init: () => {
    dispatch(locateDealer.init());
  },
  setSelectedCity: selectedCity => {
    dispatch(locateDealer.setSelectedCity(selectedCity));
  },
  setSelectedState: selectedState => {
    dispatch(locateDealer.setSelectedState(selectedState));
  },
  searchLocateDealer: () => {
    dispatch(locateDealer.searchLocateDealer());
  },
  setError: error => {
    dispatch(content.setContentError(error));
  },
  setIsSearching: isSearching => {
    dispatch(locateDealer.setIsSearching(isSearching));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LocateDealerScreen);
