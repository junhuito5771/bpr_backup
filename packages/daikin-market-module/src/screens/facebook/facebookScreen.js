import React from 'react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import WebView from 'react-native-webview';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import {
  verticalScale,
  viewportHeight,
  NavigationService,
} from '@module/utility';

import { Colors, NavDrawerMenu, HeaderText } from '@module/daikin-ui';

const styles = StyleSheet.create({
  loadingCenter: {
    paddingTop: verticalScale(30),
    height: viewportHeight,
    justifyContent: 'flex-start',
  },
});

const FacebookScreen = () => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Facebook</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
    });
  }, [navigation]);

  const renderLoading = React.useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  return (
    <WebView
      startInLoadingState
      renderLoading={renderLoading}
      source={{ uri: 'https://www.facebook.com/daikinmy/' }}
    />
  );
};

export default FacebookScreen;
