import { reducers, sagas, marketHelper } from '@module/redux-marketing';

export { default as ContactUsScreen } from './screens/contactUs/ContactUsScreen';
export { default as ErrorCodeScreen } from './screens/errorCode/errorCodeScreen';
export { default as LocateErrorCodeScreen } from './screens/errorCode/locateErrorCodeScreen';
export { default as FacebookScreen } from './screens/facebook/facebookScreen';
export { default as HeatLoadCalculatorScreen } from './screens/heatLoad/heatLoadCalculator';
export { default as RecommendProductScreen } from './screens/heatLoad/recommendProduct';
export { default as WebViewScreen } from './screens/heatLoad/webViewScreen';
export { default as DealerMapScreen } from './screens/locateDealer/dealerMapScreen';
export { default as LocateDealerScreen } from './screens/locateDealer/locateDealerScreen';
export { default as SelectModelScreen } from './screens/locateDealer/selectModelScreen';
export { default as MaintenanceReminderScreen } from './screens/maintenance/maintenanceReminderScreen';
export { default as ProcessReminderScreen } from './screens/maintenance/processReminderScreen';
export { default as ChatbotScreen } from './screens/marketingHome/chatBotScreen';
export { default as MarketingHomeScreen } from './screens/marketingHome/marketingHomeScreen';
export { default as MarketingPromotionScreen } from './screens/marketingHome/marketingPromotionScreen';
export { default as ProductScreen } from './screens/product/main';
export { default as ProductDetailScreen } from './screens/product/productDetails';
export { default as PromotionScreen } from './screens/promotion/promotionScreen';
export { default as PromotionWebScreen } from './screens/promotion/promotionWebScreen';

export * from './navigator';
export * from '@module/redux-marketing';

export { reducers, sagas, marketHelper };

function init() {}

export { init };
