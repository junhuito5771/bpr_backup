import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ErrorCodeScreen from '../screens/errorCode/errorCodeScreen';
import NonInverterSearchErrorScreen from '../screens/errorCode/searchError/NonInverterSearchErrorScreen';
import InverterSearchErrorScreen from '../screens/errorCode/searchError/InverterSearchErrorScreen';
import SelectRemoteScreen from '../screens/errorCode/searchError/SelectRemoteControlScreen';
import LocateErrorCodeScreen from '../screens/errorCode/locateErrorCodeScreen';

const Stack = createStackNavigator();

const ErrorCodeStack = () => (
  <Stack.Navigator>
    <Stack.Screen name="ErrorCode" component={ErrorCodeScreen} />
    <Stack.Screen name="LocateErrorCode" component={LocateErrorCodeScreen} />
    <Stack.Screen
      name="NonInverterSearchErrorCode"
      component={NonInverterSearchErrorScreen}
    />
    <Stack.Screen
      name="InverterSearchErrorCode"
      component={InverterSearchErrorScreen}
    />
    <Stack.Screen name="SelectRemote" component={SelectRemoteScreen} />
  </Stack.Navigator>
);

export default ErrorCodeStack;
