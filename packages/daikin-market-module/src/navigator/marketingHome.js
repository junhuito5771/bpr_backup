import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MarketingHomeScreen from '../screens/marketingHome/marketingHomeScreen';
import ChatBotScreen from '../screens/marketingHome/chatBotScreen';
import MarketingPromotionScreen from '../screens/marketingHome/marketingPromotionScreen';

const Stack = createStackNavigator();

const MarketingHomeStack = () => (
  <Stack.Navigator initialRouteName="MarketingHome">
    <Stack.Screen name="MarketingHome" component={MarketingHomeScreen} />
    <Stack.Screen
      name="MarketingPromotion"
      component={MarketingPromotionScreen}
    />
    <Stack.Screen name="Chatbot" component={ChatBotScreen} />
  </Stack.Navigator>
);

export default MarketingHomeStack;
