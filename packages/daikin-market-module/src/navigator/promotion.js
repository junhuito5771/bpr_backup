import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import PromotionScreen from '../screens/promotion/promotionScreen';
import PromotionWebScreen from '../screens/promotion/promotionWebScreen';

const Stack = createStackNavigator();

const PromotionStack = () => (
  <Stack.Navigator>
    <Stack.Screen name="Promotion" component={PromotionScreen} />
    <Stack.Screen name="PromotionWeb" component={PromotionWebScreen} />
  </Stack.Navigator>
);

export default PromotionStack;
