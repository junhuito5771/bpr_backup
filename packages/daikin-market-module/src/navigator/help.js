import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { HELP_SCREEN } from '../constants/routeNames';
import HelpWebScreen from '../screens/help/helpWeb';

const Stack = createStackNavigator();

const HelpStack = () => (
  <Stack.Navigator>
    <Stack.Screen name={HELP_SCREEN} component={HelpWebScreen} />
  </Stack.Navigator>
);

export default HelpStack;
