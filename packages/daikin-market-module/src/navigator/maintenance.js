import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Colors } from '@module/daikin-ui';
import {
  REMINDER_SCREEN,
  PROCESS_REMINDER_SCREEN,
} from '../constants/routeNames';
import MaintenanceReminderScreen from '../screens/maintenance/maintenanceReminderScreen';
import ProcessReminderScreen from '../screens/maintenance/processReminderScreen';

const Stack = createStackNavigator();

const MaintenanceStack = () => (
  <Stack.Navigator
    screenOptions={{
      cardStyle: {
        backgroundColor: Colors.white,
      },
    }}>
    <Stack.Screen
      name={REMINDER_SCREEN}
      component={MaintenanceReminderScreen}
    />
    <Stack.Screen
      name={PROCESS_REMINDER_SCREEN}
      component={ProcessReminderScreen}
    />
  </Stack.Navigator>
);

export default MaintenanceStack;
