import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HeatLoadCalculatorScreen from '../screens/heatLoad/heatLoadCalculator';
import RecommendProduct from '../screens/heatLoad/recommendProduct';
import WebviewScreen from '../screens/heatLoad/webViewScreen';

const Stack = createStackNavigator();

const HeatLoadCalculatorStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="HeatLoadCalculator"
      component={HeatLoadCalculatorScreen}
    />
    <Stack.Screen name="RecommendProduct" component={RecommendProduct} />
    <Stack.Screen name="WebviewScreen" component={WebviewScreen} />
  </Stack.Navigator>
);

export default HeatLoadCalculatorStack;
