import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import LocateDealerScreen from '../screens/locateDealer/locateDealerScreen';
import SelectModelScreen from '../screens/locateDealer/selectModelScreen';
import DealerMapScreen from '../screens/locateDealer/dealerMapScreen';

const Stack = createStackNavigator();

const LocateDealerStack = () => (
  <Stack.Navigator>
    <Stack.Screen name="LocateDealer" component={LocateDealerScreen} />
    <Stack.Screen name="SelectModel" component={SelectModelScreen} />
    <Stack.Screen name="DealerMap" component={DealerMapScreen} />
  </Stack.Navigator>
);

export default LocateDealerStack;
