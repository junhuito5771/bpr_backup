import { serviceWelcome, connect } from '@module/redux-marketing';

import { MyServiceStack, NewServiceStack } from '@module/service-ticket';
import MarketingStack from '../marketingHome';
import PromotionStack from '../promotion';
import ContactScreen from '../../screens/contactUs/ContactUsScreen';
import LocateDealerStack from '../locateDealer';
import FacebookScreen from '../../screens/facebook/facebookScreen';
import ProductStack from '../product';
import HeatloadStack from '../heatLoad';
import ErrorCodeStack from '../errorCode';
import HelpStack from '../help';

import {
  MARKETING_HOME_SCREEN,
  PROMOTION_SCREEN,
  CONTACT_US_SCREEN,
  LOCATE_DEALER_SCREEN,
  FACEBOOK_SCREEN,
  PRODUCT_SCREEN,
  HEAT_LOAD_CALCULATOR_SCREEN,
  ERROR_CODE_SCREEN,
  SERVICE_LISTING,
  SERVICE_BOOKING,
  HELP_SCREEN,
} from '../../constants/routeNames';

const mapStateToProps = ({ serviceWelcome: welcomeState }) => {
  const { isServiceWelcomed, serviceWelcomeItem } = welcomeState;

  return {
    isServiceWelcomed,
    serviceWelcomeItem,
  };
};

const mapDispatchToProps = dispatch => ({
  initServiceWelcome: () => {
    dispatch(serviceWelcome.initServiceWelcome());
  },
  setIsServiceWelcome: isServiceWelcomed => {
    dispatch(serviceWelcome.setIsServiceWelcome(isServiceWelcomed));
  },
});

const NewServiceStackRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewServiceStack);

const mainDrawer = [
  {
    name: MARKETING_HOME_SCREEN,
    label: 'All',
    component: MarketingStack,
    icon: require('../../shared/assets/sidebar/home.png'),
  },
  {
    name: PROMOTION_SCREEN,
    label: 'Promotion',
    component: PromotionStack,
    icon: require('../../shared/assets/sidebar/promotion.png'),
  },
  {
    name: CONTACT_US_SCREEN,
    label: 'Contact Us',
    component: ContactScreen,
    icon: require('../../shared/assets/sidebar/contact.png'),
  },
  {
    name: LOCATE_DEALER_SCREEN,
    label: 'Where To Buy',
    component: LocateDealerStack,
    icon: require('../../shared/assets/sidebar/location.png'),
  },
  {
    name: FACEBOOK_SCREEN,
    label: 'Daikin Facebook',
    component: FacebookScreen,
    icon: require('../../shared/assets/sidebar/facebook.png'),
  },
  {
    name: PRODUCT_SCREEN,
    label: 'Product',
    component: ProductStack,
    icon: require('../../shared/assets/sidebar/product.png'),
  },
  {
    name: HEAT_LOAD_CALCULATOR_SCREEN,
    label: 'Heat Load Calculator',
    component: HeatloadStack,
    icon: require('../../shared/assets/sidebar/calculator.png'),
  },
  {
    name: SERVICE_BOOKING,
    label: 'Book Service',
    component: NewServiceStackRedux,
    icon: require('../../shared/assets/sidebar/cartIcon.png'),
    iconStyle: { width: 25, height: 25 },
  },
  {
    name: SERVICE_LISTING,
    label: 'My Booking',
    component: MyServiceStack,
    icon: require('../../shared/assets/sidebar/clipboardIcon.png'),
    iconStyle: { width: 25, height: 25 },
  },
];

const homeDrawer = {
  [MARKETING_HOME_SCREEN]: {
    name: MARKETING_HOME_SCREEN,
    label: 'Home',
    childDrawer: mainDrawer,
    icon: require('../../shared/assets/sidebar/home.png'),
  },
  [HELP_SCREEN]: {
    name: HELP_SCREEN,
    label: 'Help',
    component: HelpStack,
    icon: require('../../shared/assets/sidebar/help.png'),
  },
  [ERROR_CODE_SCREEN]: {
    name: ERROR_CODE_SCREEN,
    label: 'Error Code',
    component: ErrorCodeStack,
    icon: require('../../shared/assets/sidebar/error.png'),
  },
};

export default homeDrawer;
