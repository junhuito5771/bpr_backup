import { LoginScreen } from '@module/daikin-auth';
import MarketingStack from '../marketingHome';
import PromotionStack from '../promotion';
import ContactScreen from '../../screens/contactUs/ContactUsScreen';
import LocateDealerStack from '../locateDealer';
import FacebookScreen from '../../screens/facebook/facebookScreen';
import ProductStack from '../product';
import HeatloadStack from '../heatLoad';
import ErrorCodeStack from '../errorCode';

import {
  MARKETING_HOME_SCREEN,
  PROMOTION_SCREEN,
  CONTACT_US_SCREEN,
  LOCATE_DEALER_SCREEN,
  FACEBOOK_SCREEN,
  PRODUCT_SCREEN,
  HEAT_LOAD_CALCULATOR_SCREEN,
  ERROR_CODE_SCREEN,
  SERVICE_LISTING,
  SERVICE_BOOKING,
  HELP_SCREEN,
} from '../../constants/routeNames';

import HelpStack from '../help';

const mainDrawer = {
  [MARKETING_HOME_SCREEN]: {
    name: MARKETING_HOME_SCREEN,
    label: 'Home',
    component: MarketingStack,
    icon: require('../../shared/assets/sidebar/home.png'),
  },
  [PROMOTION_SCREEN]: {
    name: PROMOTION_SCREEN,
    label: 'Promotion',
    component: PromotionStack,
    icon: require('../../shared/assets/sidebar/promotion.png'),
  },
  [PRODUCT_SCREEN]: {
    name: PRODUCT_SCREEN,
    label: 'Product',
    component: ProductStack,
    icon: require('../../shared/assets/sidebar/product.png'),
  },
  [HELP_SCREEN]: {
    name: HELP_SCREEN,
    label: 'Help',
    component: HelpStack,
    icon: require('../../shared/assets/sidebar/help.png'),
  },
  [ERROR_CODE_SCREEN]: {
    name: ERROR_CODE_SCREEN,
    label: 'Error Code',
    component: ErrorCodeStack,
    icon: require('../../shared/assets/sidebar/error.png'),
  },
  [LOCATE_DEALER_SCREEN]: {
    name: LOCATE_DEALER_SCREEN,
    label: 'Where To Buy',
    component: LocateDealerStack,
    icon: require('../../shared/assets/sidebar/location.png'),
  },
  [FACEBOOK_SCREEN]: {
    name: FACEBOOK_SCREEN,
    label: 'Daikin Facebook',
    component: FacebookScreen,
    icon: require('../../shared/assets/sidebar/facebook.png'),
  },
  [HEAT_LOAD_CALCULATOR_SCREEN]: {
    name: HEAT_LOAD_CALCULATOR_SCREEN,
    label: 'Heat Load Calculator',
    component: HeatloadStack,
    icon: require('../../shared/assets/sidebar/calculator.png'),
  },
  [CONTACT_US_SCREEN]: {
    name: CONTACT_US_SCREEN,
    label: 'Contact Us',
    component: ContactScreen,
    icon: require('../../shared/assets/sidebar/contact.png'),
  },
  [SERVICE_BOOKING]: {
    name: SERVICE_BOOKING,
    label: 'Book Service',
    component: LoginScreen,
    icon: require('../../shared/assets/sidebar/cartIcon.png'),
  },
  [SERVICE_LISTING]: {
    name: SERVICE_LISTING,
    label: 'My Booking',
    component: LoginScreen,
    icon: require('../../shared/assets/sidebar/clipboardIcon.png'),
  },
};

export default mainDrawer;
