import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ProductScreen from '../screens/product/main';
import ProductDetailsScreen from '../screens/product/productDetails';

const Stack = createStackNavigator();

const ProductStack = () => (
  <Stack.Navigator>
    <Stack.Screen name="Product" component={ProductScreen} />
    <Stack.Screen name="ProductDetails" component={ProductDetailsScreen} />
  </Stack.Navigator>
);

export default ProductStack;
