export const LOGIN_SCREEN = 'Login';
export const HOME_SCREEN = 'Home';

// Marketing Routes
export const MARKETING_HOME_SCREEN = 'MarketingHome';
export const PROMOTION_SCREEN = 'Promotion';
export const LOCATE_DEALER_SCREEN = 'LocateDealer';
export const FACEBOOK_SCREEN = 'Facebook';
export const PRODUCT_SCREEN = 'Product';
export const PRODUCT_DETAILS_SCREEN = 'ProductDetails';
export const HEAT_LOAD_CALCULATOR_SCREEN = 'HeatLoadCalculator';
export const RECOMMEND_PRODUCT_SCREEN = 'RecommendProduct';
export const ERROR_CODE_SCREEN = 'ErrorCode';
export const WEBVIEW_SCREEN = 'WebviewScreen';
export const CONTACT_US_SCREEN = 'ContactUs';
export const SUBSCRIPTION_SCREEN = 'Subscription';
export const REMINDER_SCREEN = 'VisitorReminder';
export const PROCESS_REMINDER_SCREEN = 'VisitorProcessReminder';
export const LIMIT_USAGE_SCREEN = 'LimitUsage';
export const HELP_SCREEN = 'Help';

export const SERVICE_BOOKING = 'ServiceBooking';
export const SERVICE_LISTING = 'ServiceListing';
