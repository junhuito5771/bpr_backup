const remoteEnums = [
  {
    type: 'dama',
    key: 'DGS01',
    image: require('../shared/assets/remote/DGS01.png'),
    label: 'DGS01',
  },
  {
    type: 'dama',
    key: 'BRC52B65',
    image: require('../shared/assets/remote/BRC52B65.png'),
    label: 'BRC52B65',
  },
  {
    type: 'dama',
    key: 'DSLM8',
    image: require('../shared/assets/remote/DSLM8.png'),
    label: 'DSLM8',
  },
  {
    type: 'dit',
    key: 'ARC480A36',
    image: require('../shared/assets/remote/ARC480A36.png'),
    label: 'ARC480A36',
  },
  {
    type: 'dit',
    key: 'ARC466A19',
    image: require('../shared/assets/remote/ARC466A19.png'),
    label: 'ARC466A19',
  },
  {
    type: 'dit',
    key: 'ARC433B76',
    image: require('../shared/assets/remote/ARC433B76.png'),
    label: 'ARC433B76',
  },
  {
    type: 'dit',
    key: 'ARC433B47',
    image: require('../shared/assets/remote/ARC433B47.png'),
    label: 'ARC433B47',
  },
  {
    type: 'dit',
    key: 'BRC1E63',
    image: require('../shared/assets/remote/BRC1E63.png'),
    label: 'BRC1E63',
  },
];

export default remoteEnums;
