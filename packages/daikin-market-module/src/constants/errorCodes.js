export const damaFirstCodes = [
  '0',
  'A',
  'C',
  'E',
  'F',
  'H',
  'J',
  'L',
  'P',
  'U',
];

export const damaSecondCodes = [
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'A',
  'C',
  'F',
  'H',
  'J',
  'O',
];

export const ditFirstCodes = [
  '0',
  'A',
  'C',
  'E',
  'F',
  'H',
  'J',
  'L',
  'P',
  'U',
  'M',
  '6',
  '7',
  '8',
  '9',
];

export const ditSecondCodes = [
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'A',
  'C',
  'E',
  'F',
  'H',
  'J',
];
