import { hasFieldValue, addError } from './validateHelper';

const deviceConfigValidator = (values) => {
  const errors = {};

  const { name, group } = values;

  addError(errors, 'name', [hasFieldValue(name), 'Unit name is required']);

  addError(errors, 'group', [hasFieldValue(group), 'Unit group is required']);

  return errors;
};

export default deviceConfigValidator;
