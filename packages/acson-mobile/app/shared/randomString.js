import { generateSecureRandom } from 'react-native-securerandom';

/* eslint no-bitwise: 0 */
async function randomString(noOfBits = 16) {
  const randomBytes = await generateSecureRandom(noOfBits);

  return randomBytes.reduce(
    (memo, i) => memo + `0${i.toString(16)}`.slice(-2),
    '',
  );
}

export default randomString;
