import React from 'react';
import { Text, Platform } from 'react-native';
import { Provider, PersistGate } from '@module/redux';
import { CrashReportHelper } from '@module/utility';
import Config from 'react-native-config';
import DropdownAlert from 'react-native-dropdownalert';
import { MenuProvider } from 'react-native-popup-menu';

import {
  ModalService,
  Modal,
  DropDownAlertService,
  Colors,
} from '@module/acson-ui';

import ErrorBoundary from './container/withSagaErrorBoundary';
import { store, persistor } from './modules';
import AppWithNavigation from './navigator';
import PushNotification from './container/pushNotification';

CrashReportHelper.init(Config.BUG_SNAG_ID);

// NOTE: To handle Xiao MI MIUI12 issue, see https://github.com/facebook/react-native/issues/29259 for more details
const originTextRender = Text.render;
Text.render = function render(props, ref) {
  return originTextRender.apply(this, [
    {
      ...props,
      style: [
        Platform.OS === 'android' ? { fontFamily: '' } : null,
        props.style,
      ],
    },
    ref,
  ]);
};

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ErrorBoundary onError={CrashReportHelper.notify}>
        <MenuProvider>
          <AppWithNavigation />
          <Modal ref={ref => ModalService.setModalRef(ref)} />
          <DropdownAlert
            messageNumOfLines={6}
            errorColor={Colors.orange}
            ref={ref => DropDownAlertService.setAlertRef(ref)}
            closeInterval={10000}
            tapToCloseEnabled
            showCancel
          />
          <PushNotification />
        </MenuProvider>
      </ErrorBoundary>
    </PersistGate>
  </Provider>
);

export default App;
