import React, { useEffect } from 'react';
import {
  NavigationContainer,
  getFocusedRouteNameFromRoute,
} from '@react-navigation/native';

import PropTypes from 'prop-types';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Image } from 'react-native';

import { getStore } from '@module/redux';
import { connect, login, userProfile } from '@module/acson-auth';
import { content, welcome } from '@module/acson-market';
import {
  NavigationService,
  NotificationHelper,
  heightPercentage,
  setupApi,
} from '@module/utility';
import { Colors, WelcomeOverlay } from '@module/acson-ui';
import Config from 'react-native-config';

import { getTabNavigators, init as initModules, store } from '../modules';
import MarketStack from './stack';
import SettingStack from './settings';

const Tab = createBottomTabNavigator();
const tabItems = getTabNavigators();

setupApi({
  getTokenParams: () => getStore().getState().user.credential,
  awsClientId: Config.COGNITO_CLIENT_ID,
  awsEndpoint: Config.COGNITO_URL,
  apiEndpoint: Config.API_URL,
  onRefreshedToken: refreshedIdToken => {
    getStore().dispatch(
      userProfile.setIDToken({
        idToken: refreshedIdToken,
        refreshToken: getStore().getState().user.credential.refreshToken,
        lastTokenRefreshedDT: Date.now() / 1000,
      })
    );
  },
});

function Main({
  initConfigAutoLoginIfCan,
  isLogin,
  initContentHelper,
  initWelcome,
  isWelcomed,
  setIsWelcomed,
  welcomeItem,
}) {
  useEffect(() => {
    initModules(Config, store);
    initConfigAutoLoginIfCan({
      apiEndpoint: Config.API_URL,
      cognitoEndpoint: Config.COGNITO_URL,
      cognitoClientId: Config.COGNITO_CLIENT_ID,
      cognitoIdentifyPoolId: Config.COGNITO_POOL_ID,
      cognitoUserPoolID: Config.COGNITO_USER_POOL_ID,
      region: Config.REGION,
      pinpointId: Config.PINPOINT_ID,
      pinpointRegion: Config.PINPOINT_REGION,
    });

    initContentHelper({
      bucketUrl: Config.BUCKET_URL,
      bucketEndpoint: Config.BUCKET_ENDPOINT,
    });

    initWelcome('Acson');
  }, []);

  const handleCloseOverlay = () => {
    setIsWelcomed(true);
  };

  return (
    <>
      <WelcomeOverlay
        visible={!isWelcomed && !!welcomeItem.length}
        data={welcomeItem}
        onClose={handleCloseOverlay}
      />
      <NavigationContainer
        onReady={() => {
          NavigationService.setNavigationReady(true);
          NotificationHelper.handleBackgroundNotification();
        }}
        linking={[`https://${Config.UNI_LINK}`]}
        ref={NavigationService.navigationRef}>
        <Tab.Navigator
          initialRouteName={isLogin ? 'Home' : 'MarketHome'}
          lazy={false}
          headerTitleAllowFontScaling={false}
          headerMode="float"
          tabBarOptions={{
            showLabel: false,
            style: {
              backgroundColor: Colors.white,
              height: heightPercentage(10),
            },
          }}>
          <Tab.Screen
            name="MarketHome"
            component={MarketStack}
            options={{
              tabBarVisible: false,
              tabBarLabel: 'Home',
              // eslint-disable-next-line react/prop-types
              tabBarIcon: ({ focused }) => (
                <Image
                  source={
                    focused
                      ? require('../shared/images/tabbar/home/homeRed.png')
                      : require('../shared/images/tabbar/home/homeGrey.png')
                  }
                />
              ),
            }}
          />
          {isLogin && (
            <>
              {tabItems.map(tabItem =>
                Object.values(tabItem).map(itemProps => (
                  <Tab.Screen
                    {...itemProps}
                    options={optionsProps => {
                      const { route } = optionsProps;

                      const focusedRouteName = getFocusedRouteNameFromRoute(
                        route
                      );

                      return {
                        ...itemProps.options(optionsProps),
                        tabBarVisible:
                          !focusedRouteName || focusedRouteName === route.name,
                      };
                    }}
                  />
                ))
              )}
              <Tab.Screen
                name="Settings"
                component={SettingStack}
                options={optionsProps => {
                  const { route } = optionsProps;

                  const focusedRouteName = getFocusedRouteNameFromRoute(route);

                  return {
                    headerLeft: null,
                    // eslint-disable-next-line react/prop-types
                    tabBarIcon: ({ focused }) => (
                      <Image
                        source={
                          focused
                            ? require('../shared/images/tabbar/settings/settingsRed.png')
                            : require('../shared/images/tabbar/settings/settingsGrey.png')
                        }
                      />
                    ),
                    tabBarVisible:
                      !focusedRouteName || focusedRouteName === route.name,
                  };
                }}
              />
            </>
          )}
        </Tab.Navigator>
      </NavigationContainer>
    </>
  );
}

const mapDispatchToProps = dispatch => ({
  initConfigAutoLoginIfCan: config => {
    dispatch(login.autoLogin(config));
  },
  initContentHelper: config => {
    dispatch(content.init(config));
  },
  initWelcome: appType => {
    dispatch(welcome.init(appType));
  },
  setIsWelcomed: isWelcomed => {
    dispatch(welcome.setIsWelcomed(isWelcomed));
  },
});

const mapStateToProps = ({ login: { isLogin }, welcome: welcomeState }) => ({
  isLogin,
  isWelcomed: welcomeState.isWelcomed,
  welcomeItem: welcomeState.welcomeItem,
});

Main.propTypes = {
  isLogin: PropTypes.bool,
  initContentHelper: PropTypes.func,
  initConfigAutoLoginIfCan: PropTypes.func,
  initWelcome: PropTypes.func,
  isWelcomed: PropTypes.bool,
  welcomeItem: PropTypes.array,
  setIsWelcomed: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
