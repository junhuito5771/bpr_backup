import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import {
  widthPercentage,
  StoreHelper,
  scale,
  verticalScale,
} from '@module/utility';
import { createStackNavigator } from '@react-navigation/stack';

import { Text, Colors } from '@module/acson-ui';
import { getSettingItems, getSettingStacks } from '../modules';
import getScreen from './getScreen';
import { SETTING_SCREEN } from '../constants/routeNames';

import DefaultStackOptions from './defaultStackOptions';

const SettingsStackNav = createStackNavigator();

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
  },
  rowContainer: {
    width: '100%',
    height: verticalScale(55),
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
  },
  row: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: scale(25),
    alignItems: 'center',
    flex: 1,
  },
  touchableRow: {
    flex: 1,
  },
  versionRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: widthPercentage(52),
    alignSelf: 'center',
    paddingVertical: 5,
  },
  versionText: {
    color: 'rgb(74, 74, 74)',
    opacity: 0.4,
  },
  container: { flex: 1 },
});

const settingItems = getSettingItems();
const settingStacks = getSettingStacks();

const AppSettings = () => {
  const [version, setVersion] = React.useState([]);
  const [buildNumber, setBuildNumber] = React.useState([]);

  React.useEffect(() => {
    setVersion(StoreHelper.getCurrentVersion());
    setBuildNumber(StoreHelper.getCurrentBuildNumber());
  }, []);

  return (
    <View style={styles.container}>
      {settingItems.map(settingItemProps => {
        const { key, component: RowComponent } = settingItemProps;
        return <RowComponent key={key} />;
      })}
      <View style={styles.versionRow}>
        <Text small style={styles.versionText}>
          Application Version&nbsp;
        </Text>
        <Text small style={styles.versionText}>
          {version} ({buildNumber})
        </Text>
      </View>
    </View>
  );
};

AppSettings.propTypes = {};

const SettingStack = () => (
  <SettingsStackNav.Navigator screenOptions={DefaultStackOptions}>
    <SettingsStackNav.Screen
      name={SETTING_SCREEN}
      component={AppSettings}
      options={() => ({
        title: 'App Setting',
        headerLeft: null,
      })}
    />

    {getScreen(settingStacks, SettingsStackNav)}
  </SettingsStackNav.Navigator>
);

export default SettingStack;
