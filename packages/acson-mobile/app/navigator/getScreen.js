import React from 'react';

function getScreens(Navigators, Stack) {
  return Navigators.map(navObj =>
    Object.values(navObj).map(navItem => <Stack.Screen {...navItem} />)
  );
}

export default getScreens;
