import React from 'react';
import { ApiService } from '@module/utility';
import { connect } from '@module/redux';
import { serviceWelcome } from '@module/redux-marketing';
import { createStackNavigator } from '@react-navigation/stack';
import { init, MyServiceStack, NewServiceStack } from '@module/service-ticket';
import { getNavigators } from '../modules';
import getScreen from './getScreen';

import DefaultStackOptions from './defaultStackOptions';

const RootStack = createStackNavigator();

const Navigators = getNavigators();

init(
  ApiService.getAuthToken,
  {
    company: 'Acson',
  },
  {
    primary: 'rgb(255, 0, 42)',
  }
);

const mapStateToProps = ({ serviceWelcome: welcomeState, ...rest }) => {
  const { isServiceWelcomed, serviceWelcomeItem } = welcomeState;

  return {
    isServiceWelcomed,
    serviceWelcomeItem,
  };
};

const mapDispatchToProps = dispatch => ({
  initServiceWelcome: appType => {
    dispatch(serviceWelcome.initServiceWelcome(appType));
  },
  setIsServiceWelcome: isServiceWelcomed => {
    dispatch(serviceWelcome.setIsServiceWelcome(isServiceWelcomed));
  },
});

const NewServiceStackRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewServiceStack);

function Main() {
  return (
    <RootStack.Navigator
      initialRouteName="HomeMarketing"
      screenOptions={DefaultStackOptions}>
      {getScreen(Navigators, RootStack)}
      <RootStack.Screen
        name="ServiceListing"
        component={MyServiceStack}
        options={{ headerShown: false }}
      />
      <RootStack.Screen
        name="ServiceBooking"
        component={NewServiceStackRedux}
        options={{ headerShown: false }}
      />
    </RootStack.Navigator>
  );
}

export default Main;
