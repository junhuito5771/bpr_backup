import React from 'react';
import PropTypes from 'prop-types';
import { connect } from '@module/redux';
import RNRestartHandler from 'react-native-restart';

import { ErrorBoundary } from '@module/acson-ui';

const withSagaErrorBoundary = ({ sagaError, clearSagaError, ...restProps }) => {
  const handleOnReload = () => {
    clearSagaError();

    setTimeout(() => RNRestartHandler.Restart(), 100);
  };

  return (
    <ErrorBoundary
      otherError={sagaError}
      {...restProps}
      onReload={handleOnReload}
    />
  );
};

const mapStateToProps = () => ({
  sagaError: ''
});

const mapDispatchToProps = dispatch => ({
  clearSagaError: () => {
    // dispatch(settings.setSagaError(''))
  }
});

withSagaErrorBoundary.propTypes = {
  sagaError: PropTypes.string,
  clearSagaError: PropTypes.func
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withSagaErrorBoundary);
