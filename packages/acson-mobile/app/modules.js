import {
  ConfigureStore,
  combineModuleReducers,
  combineModuleSaga,
} from '@module/redux';
import Config from 'react-native-config';

import * as AcsonAuth from '@module/acson-auth';
import * as AcsonMarket from '@module/acson-market';
import * as AcsonIoT from '@module/iot-dummy';

const modules = [AcsonMarket, AcsonAuth, AcsonIoT];

function onLoad() {
  const sagaList = [];
  let reducerList = {};

  modules.forEach(module => {
    if (module && module.sagas) {
      sagaList.push(module.sagas);
    }

    if (module && module.reducers) {
      reducerList = {
        ...reducerList,
        ...module.reducers,
      };
    }
  });

  combineModuleReducers(reducerList);

  combineModuleSaga(sagaList);
}

onLoad();

function init(config, helpers) {
  const hasIoTModule = modules.some(module => module && module.isIoTModule);
  modules.forEach(module => {
    if (module && module.init) {
      module.init(config, helpers);
    }

    if (module && module.IntegratedModule) {
      module.IntegratedModule.setAllConfig({ isIoTEnabled: hasIoTModule });

      module.IntegratedModule.initExternalModule();
    }
  });
}
init(Config);

function getNavigators() {
  const navigators = [];
  modules.forEach(module => {
    if (module && module.Stack) {
      navigators.push(module.Stack);
    }
  });

  return navigators;
}

function getTabNavigators() {
  const navigators = [];
  modules.forEach(module => {
    if (module && module.Tab) {
      navigators.push(module.Tab);
    }
  });

  return navigators;
}

function getSettingItems() {
  let settingsItems = [];
  modules.forEach(module => {
    if (module && module.settingItems) {
      settingsItems = settingsItems.concat(module.settingItems);
    }
  });

  const sortedSettingsItems = settingsItems.sort((a, b) => a.index - b.index);

  return sortedSettingsItems;
}

function getSettingStacks() {
  const settingsItems = [];
  modules.forEach(module => {
    if (module && module.settingStacks) {
      settingsItems.push(module.settingStacks);
    }
  });

  return settingsItems;
}

export {
  init,
  getNavigators,
  getTabNavigators,
  getSettingItems,
  getSettingStacks,
};
export const { store, persistor } = ConfigureStore();
// eslint-disable-next-line
const dispatch = store.dispatch;

export { dispatch };
