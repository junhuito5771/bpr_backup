import { NavigationActions, StackActions } from 'react-navigation';
import dropDownAlert from 'common/service/dropDownAlert';

let navigator;

function setTopLevelNavigator(navigatorRef) {
  navigator = navigatorRef;
}

function navigate(routeName, params) {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
}

function reset(index, routeName) {
  navigator.dispatch(
    StackActions.reset({
      index,
      actions: [NavigationActions.navigate({ routeName })],
    }),
  );
}

function replace(routeName, params = null) {
  navigator.dispatch(
    StackActions.replace({
      routeName,
      params,
    }),
  );
}

function resetTo(parentRoute, routeName) {
  navigator.dispatch(
    StackActions.reset({
      index: 1,
      actions: [
        NavigationActions.navigate({ routeName: parentRoute }),
        NavigationActions.navigate({ routeName }),
      ],
    }),
  );

  dropDownAlert.close();
}

export default {
  navigate,
  setTopLevelNavigator,
  reset,
  replace,
  resetTo,
};
