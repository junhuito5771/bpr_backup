package my.com.acson.app;

import androidx.annotation.NonNull;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.content.Intent;
import android.content.ComponentName;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AutoStarterModule extends ReactContextBaseJavaModule {

	/***
	 * Xiaomi
	 */
	private final String BRAND_XIAOMI = "xiaomi";
	private final String BRAND_XIAOMI_POCO = "poco";
	private final String BRAND_XIAOMI_REDMI = "redmi";
	private final String PACKAGE_XIAOMI_MAIN = "com.miui.securitycenter";
	private final String PACKAGE_XIAOMI_COMPONENT = "com.miui.permcenter.autostart.AutoStartManagementActivity";

	/***
	 * Letv
	 */
	private final String BRAND_LETV = "letv";
	private final String PACKAGE_LETV_MAIN = "com.letv.android.letvsafe";
	private final String PACKAGE_LETV_COMPONENT = "com.letv.android.letvsafe.AutobootManageActivity";

	/***
	 * ASUS ROG
	 */
	private final String BRAND_ASUS = "asus";
	private final String PACKAGE_ASUS_MAIN = "com.asus.mobilemanager";
	private final String PACKAGE_ASUS_COMPONENT = "com.asus.mobilemanager.powersaver.PowerSaverSettings";
	private final String PACKAGE_ASUS_COMPONENT_FALLBACK = "com.asus.mobilemanager.autostart.AutoStartActivity";

	/***
	 * Honor
	 */
	private final String BRAND_HONOR = "honor";
	private final String PACKAGE_HONOR_MAIN = "com.huawei.systemmanager";
	private final String PACKAGE_HONOR_COMPONENT = "com.huawei.systemmanager.optimize.process.ProtectActivity";

	/***
	 * Huawei
	 */
	private final String BRAND_HUAWEI = "huawei";
	private final String PACKAGE_HUAWEI_MAIN = "com.huawei.systemmanager";
	private final String PACKAGE_HUAWEI_COMPONENT = "com.huawei.systemmanager.optimize.process.ProtectActivity";
	private final String PACKAGE_HUAWEI_COMPONENT_FALLBACK = "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity";

	/**
	 * Oppo
	 */
	private final String BRAND_OPPO = "oppo";
	private final String PACKAGE_OPPO_MAIN = "com.coloros.safecenter";
	private final String PACKAGE_OPPO_FALLBACK = "com.oppo.safe";
	private final String PACKAGE_OPPO_COMPONENT = "com.coloros.safecenter.permission.startup.StartupAppListActivity";
	private final String PACKAGE_OPPO_COMPONENT_FALLBACK = "com.oppo.safe.permission.startup.StartupAppListActivity";
	private final String PACKAGE_OPPO_COMPONENT_FALLBACK_A = "com.coloros.safecenter.startupapp.StartupAppListActivity";

	/**
	 * Vivo
	 */

	private final String BRAND_VIVO = "vivo";
	private final String PACKAGE_VIVO_MAIN = "com.iqoo.secure";
	private final String PACKAGE_VIVO_FALLBACK = "com.vivo.permissionmanager";
	private final String PACKAGE_VIVO_COMPONENT = "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity";
	private final String PACKAGE_VIVO_COMPONENT_FALLBACK = "com.vivo.permissionmanager.activity.BgStartUpManagerActivity";
	private final String PACKAGE_VIVO_COMPONENT_FALLBACK_A = "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager";

	/***
	 * Samsung
	 */
	private final String BRAND_SAMSUNG = "samsung";
	private final String PACKAGE_SAMSUNG_MAIN = "com.samsung.android.lool";
	private final String PACKAGE_SAMSUNG_COMPONENT = "com.samsung.android.sm.ui.battery.BatteryActivity";
	private final String PACKAGE_SAMSUNG_COMPONENT_2 = "com.samsung.android.sm.battery.ui.BatteryActivity";

	private final List<String> brandList = Arrays.asList(BRAND_ASUS, BRAND_HONOR, BRAND_HUAWEI, BRAND_LETV, BRAND_OPPO, BRAND_VIVO, BRAND_XIAOMI, BRAND_XIAOMI_POCO, BRAND_XIAOMI_REDMI, BRAND_SAMSUNG);
	private final List<String> xiaoMiBrand = Arrays.asList(BRAND_XIAOMI_REDMI, BRAND_XIAOMI_POCO, BRAND_XIAOMI);
	private final List<String> packageList = Arrays.asList(PACKAGE_ASUS_MAIN, PACKAGE_XIAOMI_MAIN, PACKAGE_LETV_MAIN, PACKAGE_HONOR_MAIN,
		PACKAGE_OPPO_MAIN, PACKAGE_OPPO_FALLBACK, PACKAGE_VIVO_MAIN, PACKAGE_VIVO_FALLBACK, PACKAGE_HUAWEI_MAIN, PACKAGE_SAMSUNG_MAIN);


	AutoStarterModule(ReactApplicationContext context) {
		super(context);
	}

	@NonNull
	@Override
	public String getName() {
		return "AutoStarterModule";
	}

	private Boolean isCustomAndroid() {
		String brand = Build.BRAND.toLowerCase(Locale.ENGLISH);

		return brandList.contains(brand);
	}

	private Boolean isPackageExists(String targetPackageName) {
		ReactApplicationContext context = getReactApplicationContext();
		PackageManager pm = context.getPackageManager();
		List<ApplicationInfo> packages = pm.getInstalledApplications(0);
		for (ApplicationInfo packageInfo : packages) {
			if(packageInfo.packageName.equals(targetPackageName)) {
				return true;
			}
		}

		return false;
	}

	private boolean startIntent(String packageName, String componentName) {
		try{
			Intent intent = new Intent();
			intent.setComponent(new ComponentName(packageName, componentName));
			getReactApplicationContext().startActivity(intent);

			return true;
		}catch(Exception error) {

		}

		return false;
	}

	private void autoStart(String packageName, List<String> componentList) {
		if(isPackageExists(packageName)) {
			for(String component : componentList) {
				try {
					if(startIntent(packageName, component)) break;
				}catch(Exception error) {

				}
			}
		}
	}

	@Override
	public Map<String, Object> getConstants() {
		final Map<String, Object> constants = new HashMap<>();
		constants.put("IS_CUSTOM_ANDROID", isCustomAndroid());
		return constants;
	}

	@ReactMethod
	public void isAutoStartPermissionAvailable(Promise promise) {
		try {
			ReactApplicationContext context = getReactApplicationContext();
			PackageManager pm = context.getPackageManager();
			List<ApplicationInfo> packages = pm.getInstalledApplications(0);
			for (ApplicationInfo packageInfo : packages) {
				if(packageList.contains(packageInfo.packageName)) {
					promise.resolve(true);
				}
			}

			promise.resolve(false);
		}catch (Exception error) {
			promise.reject("AutostarterError:isAutoStartPermissionAvailable", error.getMessage());
		}
	}

	@ReactMethod
	public void openAutoStartSetting(Promise promise) {
		try {
			String brand = Build.BRAND.toLowerCase(Locale.ENGLISH);

			switch (brand) {
				case BRAND_XIAOMI:
				case BRAND_XIAOMI_POCO:
				case BRAND_XIAOMI_REDMI:
					autoStart(PACKAGE_XIAOMI_MAIN, Arrays.asList(PACKAGE_XIAOMI_COMPONENT));
					break;
				case BRAND_LETV:
					autoStart(PACKAGE_LETV_MAIN, Arrays.asList(PACKAGE_LETV_COMPONENT));
					break;
				case BRAND_SAMSUNG:
					autoStart(PACKAGE_SAMSUNG_MAIN, Arrays.asList(PACKAGE_SAMSUNG_COMPONENT, PACKAGE_SAMSUNG_COMPONENT_2));
					break;
				default:
					promise.resolve(false);
			}

			promise.resolve(false);
		}catch (Exception error) {
			promise.reject("AutostarterError:isAutoStartPermissionAvailable", error.getMessage());
		}
	}
}

