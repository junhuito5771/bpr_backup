package my.com.acson.app;

import android.app.Application;

import android.content.Context;
import com.facebook.react.PackageList;
import com.facebook.hermes.reactexecutor.HermesExecutorFactory;
import com.facebook.react.bridge.JavaScriptExecutorFactory;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import java.lang.reflect.InvocationTargetException;

import com.amazonaws.RNAWSCognitoPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.peel.react.TcpSocketsModule;
import com.pusherman.networkinfo.RNNetworkInfoPackage;
import com.tradle.react.UdpSocketsModule;
import com.rnfs.RNFSPackage;
import com.gaspardbruno.staticsafeareainsets.RNStaticSafeAreaInsetsPackage;
import com.wix.interactable.Interactable;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.bugsnag.BugsnagReactNative;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import io.xogus.reactnative.versioncheck.RNVersionCheckPackage;
import com.emeraldsanto.encryptedstorage.RNEncryptedStoragePackage;
import com.reactnativecommunity.geolocation.GeolocationPackage;
import ca.jaysoo.extradimensions.ExtraDimensionsPackage;

import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      // Packages that cannot be autolinked yet can be added manually here, for example:
      // packages.add(new MyReactNativePackage());
      packages.add(new AsyncStoragePackage());
      packages.add(new RNAWSCognitoPackage());
      packages.add(new TcpSocketsModule());
      packages.add(new RNNetworkInfoPackage());
      packages.add(new UdpSocketsModule());
      packages.add(new RNFSPackage());
      packages.add(new RNStaticSafeAreaInsetsPackage());
      packages.add(new Interactable());
      packages.add(new RNDeviceInfo());
      packages.add(new RNVersionCheckPackage());
      packages.add(BugsnagReactNative.getPackage());
      packages.add(new ReactNativePushNotificationPackage());
      packages.add(new RNEncryptedStoragePackage());
      packages.add(new GeolocationPackage());
      packages.addAll(FlavourPackage.getFlavourPackage());
      packages.add(new PkgManagerPackage());
      packages.add(new ExtraDimensionsPackage());
      packages.add(new AutoStarterPackage());
      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
  }

  /**
   * Loads Flipper in React Native templates. Call this in the onCreate method with something like
   * initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
   *
   * @param context
   * @param reactInstanceManager
   */
  private static void initializeFlipper(
      Context context, ReactInstanceManager reactInstanceManager) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
        Class<?> aClass = Class.forName("com.rndiffapp.ReactNativeFlipper");
        aClass
            .getMethod("initializeFlipper", Context.class, ReactInstanceManager.class)
            .invoke(null, context, reactInstanceManager);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
