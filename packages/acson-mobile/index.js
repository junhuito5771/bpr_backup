import { AppRegistry } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import App from './app/index';
import './shim';

SplashScreen.hide();

AppRegistry.registerComponent('AcsonMalaysia', () => App);
