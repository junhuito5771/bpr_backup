import React from 'react';
import PropTypes from 'prop-types';
import { Text as TextRN, StyleSheet } from 'react-native';

import styles from './styles';

const Text = ({
  mini,
  small,
  medium,
  large,
  bold,
  error,
  success,
  style,
  children,
  color,
  numberOfLines,
}) => {
  const textStyle = StyleSheet.flatten([
    styles.text,
    styles.regular,
    mini && styles.mini,
    small && styles.small,
    medium && styles.medium,
    large && styles.large,
    bold && styles.bold,
    error && styles.error,
    success && styles.success,
    color && { color },
    style,
  ]);

  return (
    <TextRN
      style={textStyle}
      numberOfLines={numberOfLines}
      maxFontSizeMultiplier={1}>
      {children}
    </TextRN>
  );
};

Text.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  mini: PropTypes.bool,
  small: PropTypes.bool,
  medium: PropTypes.bool,
  large: PropTypes.bool,
  bold: PropTypes.bool,
  error: PropTypes.bool,
  success: PropTypes.bool,
  color: PropTypes.string,
  numberOfLines: PropTypes.number,
};

export default Text;
