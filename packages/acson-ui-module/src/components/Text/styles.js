import { StyleSheet } from 'react-native';
import { ScaleText } from '@module/utility';

import Colors from '../../constants/Colors';

export default StyleSheet.create({
  text: {
    backgroundColor: 'transparent',
    color: Colors.black,
  },
  mini: {
    fontSize: ScaleText(11),
  },
  small: {
    fontSize: ScaleText(13),
  },
  medium: {
    fontSize: ScaleText(16),
  },
  large: {
    fontSize: ScaleText(21),
  },
  bold: {
    fontFamily: 'Avenir-Black',
  },
  regular: {
    fontFamily: 'Avenir',
  },
  error: {
    color: Colors.red,
    paddingVertical: 5,
  },
  success: {
    color: Colors.purple,
    paddingVertical: 5,
  },
});
