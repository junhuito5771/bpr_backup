import React from 'react';
import PropTypes from 'prop-types';
import { Modal, SafeAreaView, TouchableOpacity, View } from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import CacheImage from '../CacheImage';
import Text from '../Text';
import styles from './styles';

const Indicator = ({ selected }) => (
  <View style={[styles.indicator, selected && styles.selected]} />
);

const WelcomeOverlay = ({ visible, data, onClose }) => {
  const [currentIndex, setCurrentIndex] = React.useState(0);
  const welcomeItem = data || [];

  const SkipButton = React.memo(
    () => (
      <TouchableOpacity
        hitSlop={{
          right: 10,
          bottom: 10,
          left: 10,
        }}
        onPress={onClose}>
        <Text>Skip</Text>
      </TouchableOpacity>
    ),
    []
  );

  const handlePageSelect = event => {
    if (event.nativeEvent) {
      setCurrentIndex(event.nativeEvent.position);
    }
  };

  // Use react-native default modal instead of 3rd library
  // since will have conflict of displaying viewpager in iOS
  return (
    <Modal style={styles.modal} visible={visible}>
      <SafeAreaView style={styles.flex}>
        <View style={styles.skip}>
          <SkipButton />
        </View>
        <ViewPager
          initialPage={0}
          style={styles.viewpager}
          onPageSelected={handlePageSelect}>
          {welcomeItem.map(item => (
            <View style={styles.page} key={item.id}>
              <CacheImage
                style={styles.image}
                source={{
                  uri: item.imageLink,
                }}
                resizeMode="contain"
              />
              <Text style={styles.center} medium bold>
                {item.title}
              </Text>
              <Text style={styles.description}>{item.description}</Text>
            </View>
          ))}
        </ViewPager>
        <View style={[styles.indicatorBox, styles.row]}>
          {welcomeItem.map((item, index) => (
            <Indicator key={item.id} selected={currentIndex === index} />
          ))}
        </View>
      </SafeAreaView>
    </Modal>
  );
};

Indicator.propTypes = {
  selected: PropTypes.bool,
};

WelcomeOverlay.propTypes = {
  data: PropTypes.array,
};

export default WelcomeOverlay;
