/* eslint-disable no-console */
import React from 'react';
import PropTypes from 'prop-types';

const RowItem = ({
  drag,
  renderItem,
  item,
  keyToIndex,
  itemKey,
  onUnmount,
}) => {
  React.useEffect(() => onUnmount, []);

  const onDrag = () => {
    const hoverComponent = renderItem({
      isActive: true,
      item,
      index: keyToIndex.get(itemKey),
      drag: () =>
        console.log('## attempt to call drag() on hovering component'),
    });
    drag(hoverComponent, itemKey);
  };

  return renderItem({
    isActive: false,
    item,
    index: keyToIndex.get(itemKey),
    drag: onDrag,
  });
};

RowItem.propTypes = {
  drag: PropTypes.func,
  renderItem: PropTypes.func,
  item: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  keyToIndex: PropTypes.object,
  itemKey: PropTypes.string,
  onUnmount: PropTypes.func,
};

export default RowItem;

/* eslint-enable no-console */
