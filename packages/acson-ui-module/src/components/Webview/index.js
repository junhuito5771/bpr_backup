import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';
import { useFocusEffect } from '@react-navigation/native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});

const WebviewScreen = ({ route, navigation }) => {
  const { params } = route;
  const { needRefresh = false, onLoadEnd } = params || {};
  const [isLoading, setIsLoading] = useState(true);

  useFocusEffect(
    useCallback(() => {
      navigation.setOptions({
        title: params.title,
      });
    }, [navigation])
  );

  const renderLoading = () => (
    <View style={styles.container}>
      <ActivityIndicator size="large" />
    </View>
  );

  const getScreenLayout = () => {
    if (isLoading) return { flex: 0 };
    return { flex: 1 };
  };

  return (
    <WebView
      {...(needRefresh && { incognito: true })}
      source={{ uri: params.url }}
      startInLoadingState={isLoading}
      renderLoading={renderLoading}
      onLoadEnd={syntheticEvent => {
        // update component to be aware of loading status
        const { nativeEvent } = syntheticEvent;
        setIsLoading(nativeEvent.loading);
        if (onLoadEnd) onLoadEnd();
      }}
      style={getScreenLayout()}
    />
  );
};

WebviewScreen.propTypes = {
  route: PropTypes.object,
  navigation: PropTypes.object,
};

export default WebviewScreen;
