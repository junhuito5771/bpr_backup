import React, { useState, useEffect } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Collapsible from 'react-native-collapsible';

import Text from '../Text';

import styles from './style';

const COLLAPSIBLE_BUTTONS = [
  require('../../shared/images/general/uncollapse.png'),
  require('../../shared/images/general/collapse.png'),
];

const CollapsibleHeader = ({ title, isCollapsible, onPress }) => (
  <View style={styles.headerContainer}>
    <TouchableOpacity onPress={onPress}>
      <View style={styles.headerWrap}>
        <Text medium>{title}</Text>
        <View style={styles.collapseIcon}>
          <Image
            source={COLLAPSIBLE_BUTTONS[Number(isCollapsible)]}
            style={styles.collapseButton}
          />
        </View>
      </View>
    </TouchableOpacity>
  </View>
);

CollapsibleHeader.propTypes = {
  title: PropTypes.string,
  isCollapsible: PropTypes.bool,
  onPress: PropTypes.func,
};

const CollapsibleContent = ({ isCollapsible, items, onPress }) => (
  <Collapsible collapsed={isCollapsible}>
    {items.map((item, index) => {
      const { name, disallowed } = item;
      const isBeforeLastIndex = index < items.length - 1;

      return (
        <View
          style={[
            styles.card,
            styles.shadow,
            isBeforeLastIndex && styles.cardSpaced,
            disallowed && styles.disallowedCard,
          ]}
          key={name}>
          <TouchableOpacity onPress={() => onPress(item)} style={styles.flex}>
            <View style={styles.cardContent}>
              <Text
                medium
                style={[
                  styles.cardText,
                  disallowed && styles.disallowedCardText,
                ]}>
                {name}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    })}
  </Collapsible>
);

CollapsibleContent.propTypes = {
  isCollapsible: PropTypes.bool,
  items: PropTypes.array,
  onPress: PropTypes.func,
};

const CollapsibleContainer = ({ data, onPressItem }) => {
  const [activeSection, setActiveSection] = useState([]);

  useEffect(() => {
    if (data && data.length > -1) {
      setActiveSection(Array(data.length).fill(false));
    }
  }, [data]);

  const toggleCollapse = index => {
    setActiveSection(prevActiveSection => {
      const curActiveSection = [...prevActiveSection];
      curActiveSection[index] = !prevActiveSection[index];

      return curActiveSection;
    });
  };

  return data.map(({ title, data: items }, index) => (
    <React.Fragment key={title}>
      <CollapsibleHeader
        title={title}
        count={items.length}
        isCollapsible={activeSection[index]}
        onPress={() => toggleCollapse(index)}
      />
      <CollapsibleContent
        items={items}
        sectionIndex={index}
        isCollapsible={activeSection[index]}
        onPress={onPressItem}
      />
    </React.Fragment>
  ));
};

export default CollapsibleContainer;
