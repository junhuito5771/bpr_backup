import { StyleSheet, Platform } from 'react-native';
import { heightPercentage } from '@module/utility';
import Colors from '../../constants/Colors';

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  headerContainer: {
    marginVertical: 10,
  },
  headerWrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  card: {
    borderRadius: 8,
    backgroundColor: Colors.red,
    paddingHorizontal: 20,
    height: heightPercentage(7),
  },
  cardContent: {
    flex: 1,
    justifyContent: 'center',
  },
  disallowedCard: {
    backgroundColor: Colors.white,
    borderColor: Colors.red,
    borderWidth: 1,
  },
  cardSpaced: {
    marginBottom: 20,
  },
  collapseIcon: {
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardText: {
    color: Colors.white,
    fontWeight: Platform.select({ ios: '600', android: '200' }),
  },
  disallowedCardText: {
    color: Colors.red,
    fontWeight: Platform.select({ ios: '600', android: '200' }),
  },
  shadow: {
    elevation: 3,
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
  },
});
