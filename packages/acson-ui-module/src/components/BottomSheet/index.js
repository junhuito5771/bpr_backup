/* eslint-disable no-mixed-operators */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  View,
  PanResponder,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {
  verticalScale,
  viewportHeight,
  isSmallDevice,
  iphoneXS,
} from '@module/utility';

import styles from './styles';

const snapToPoint = (value, point) => {
  Animated.timing(value, {
    toValue: point,
    duration: 250,
    useNativeDriver: false,
  }).start();
};

const calculateHeight = () => {
  const baseHeight = 520;

  const extraMargin = Platform.select({
    ios: iphoneXS ? 0 : 20,
    default: isSmallDevice ? 60 : 0,
  });

  return verticalScale(baseHeight + extraMargin);
};

const BottomSheetGeneral = ({ children, upperContainer, visibleProps }) => {
  const [showMoreOptions, setShowMoreOptions] = React.useState(false);
  const containerHeight = calculateHeight();

  const hiddenMargin = 70;

  const snapPoints = {
    hidden: -containerHeight + hiddenMargin,
    visible: 0,
  };

  const startingPosition = snapPoints.hidden;

  const animatedPosition = React.useRef(new Animated.Value(startingPosition))
    .current;

  const movementValue = gestureState =>
    viewportHeight - gestureState.moveY + snapPoints.hidden;

  const animateMove = toValue => {
    Animated.spring(animatedPosition, {
      toValue,
      tension: 120,
      useNativeDriver: false,
    }).start();
  };

  React.useEffect(() => {
    if (showMoreOptions) {
      visibleProps.setState(true);
      snapToPoint(animatedPosition, snapPoints.visible);
    } else {
      snapToPoint(animatedPosition, snapPoints.hidden);
    }
  }, [showMoreOptions, snapPoints.hidden]);

  React.useEffect(() => {
    setShowMoreOptions(visibleProps.state);
  }, [visibleProps.state]);

  const onMoveShouldSetPanResponder = (_, gestureState) =>
    gestureState.dy >= 3 || gestureState.dy <= -3;

  const panGesture = PanResponder.create({
    onStartShouldSetPanResponder: onMoveShouldSetPanResponder,
    onMoveShouldSetPanResponder,
    onPanResponderMove: (_, gestureState) => {
      const value = snapPoints.visible;
      const toValue = Math.min(value, movementValue(gestureState));
      animateMove(toValue);
    },
    onPanResponderRelease: (_, gestureState) => {
      const slideUp = gestureState.y0 > 400;
      const toValue = startingPosition;

      if (slideUp) {
        setShowMoreOptions(true);
      } else {
        setShowMoreOptions(false);
      }
      animateMove(toValue);
    },
  });

  return (
    <>
      {showMoreOptions && (
        <TouchableWithoutFeedback onPress={() => setShowMoreOptions(false)}>
          <View style={styles.overlayStyle} />
        </TouchableWithoutFeedback>
      )}
      <Animated.View
        style={[
          styles.bottomViewStyle,
          {
            height: containerHeight,
            bottom: animatedPosition,
          },
        ]}>
        <View style={[styles.touchableStyle]} {...panGesture.panHandlers}>
          <View style={styles.oblongStyle} />
          {upperContainer}
        </View>
        {children}
      </Animated.View>
    </>
  );
};

BottomSheetGeneral.propTypes = {
  upperContainer: PropTypes.element,
  children: PropTypes.node,
  visibleProps: PropTypes.object,
};
export default BottomSheetGeneral;
