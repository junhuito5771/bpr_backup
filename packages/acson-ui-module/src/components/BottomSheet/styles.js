import { StyleSheet, Platform } from 'react-native';
import {
  widthPercentage,
  ScaleText,
  heightPercentage,
  viewportWidth,
  viewportHeight,
  iphoneXS
} from '@module/utility';
import Colors from '../../constants/Colors';

const style = StyleSheet.create({
  touchableStyle: {
    alignItems: 'center',
    width: viewportWidth,
    marginBottom: iphoneXS ? 10 : 5,
    borderColor: 'transparent',
    borderWidth: 1,
  },
  bottomViewStyle: {
    alignItems: 'center',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: Colors.white,
    position: 'absolute',
    width: viewportWidth,
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 8,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    elevation: 12,
    borderWidth: Platform.OS === 'android' ? 1 : 0,
    borderColor: Colors.lightCoolGrey,
  },
  oblongStyle: {
    width: widthPercentage(10),
    height: widthPercentage(1),
    borderRadius: 4,
    backgroundColor: Colors.lightCoolGrey,
    marginVertical: 8,
  },
  textStyle: {
    fontSize: ScaleText(15),
    marginTop: 5,
    marginBottom: heightPercentage(3),
  },
  overlayStyle: {
    ...StyleSheet.absoluteFill,
    height: viewportHeight,
    backgroundColor: 'black',
    opacity: 0.3,
  },
  offTextColor: {
    color: Colors.labelGrey,
  },
  columnWrapperStyle: {
    justifyContent: 'space-evenly',
    paddingBottom: 10,
  },
});

export default style;
