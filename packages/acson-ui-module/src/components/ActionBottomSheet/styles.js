import { scale, verticalScale } from '@module/utility';
import { StyleSheet } from 'react-native';
import Colors from '../../constants/Colors';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: Colors.white,
    left: 0,
    right: 0,
    bottom: 0,
  },
  gestureArea: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    paddingBottom: verticalScale(20),
  },
  grabber: {
    width: scale(37),
    borderRadius: 3,
    backgroundColor: Colors.lightCoolGrey,
    alignSelf: 'center',
    marginTop: 12,
    height: 5,
  },
  border: {
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
  },
  shadow: {
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
