import { ScaleText, verticalScale } from '@module/utility';
import PropTypes from 'prop-types';
import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import Colors from '../../constants/Colors';
import ActionBottomSheet from './ActionBottomSheet';

const { width: windowWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  container: {
    justifyContent: 'center',
    flexWrap: 'wrap',
    paddingBottom: verticalScale(20),
  },
  productButton: {
    width: windowWidth / 5.5,
    height: verticalScale(70),
  },
  productLabel: {
    fontWeight: '400',
    fontSize: ScaleText(10),
    color: Colors.titleGrey,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  focus: {
    color: Colors.red,
  },
  imageContainer: {
    width: 46,
    height: 33,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  }
});

const ProductBottomSheet = ({ currentFocus, setOnPress, products }) => {
  const handleOnPress = React.useCallback(
    params => {
      setOnPress(params);
    },
    [setOnPress]
  );

  return (
    <ActionBottomSheet height={verticalScale(200)}>
      <View style={[styles.row, styles.container]}>
        {products.map(p => (
          <TouchableOpacity
            key={`${p.label}_${p.params}`}
            onPress={() => handleOnPress(p.params)}
            style={styles.productButton}>
            <View style={styles.center}>
              <View style={styles.imageContainer}>
                <Image source={p.iconSource} />
              </View>
            </View>
            <Text
              style={[
                styles.center,
                styles.productLabel,
                p.params === currentFocus && styles.focus,
              ]}>
              {p.label}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </ActionBottomSheet>
  );
};

ProductBottomSheet.propTypes = {
  currentFocus: PropTypes.string,
  setOnPress: PropTypes.func,
  products: PropTypes.array,
};

export default ProductBottomSheet;
