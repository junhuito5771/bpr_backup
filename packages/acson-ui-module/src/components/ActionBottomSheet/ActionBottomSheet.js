import { verticalScale } from '@module/utility';
import PropTypes from 'prop-types';
import React from 'react';
import { Animated, Dimensions, PanResponder, View } from 'react-native';
import styles from './styles';

const { height: screenHeight } = Dimensions.get('screen');

const ActionBottomSheet = ({ children, height }) => {
  /* eslint-disable no-mixed-operators */
  const INITIAL_POSITION = height / 2 - verticalScale(5);
  const CLAMP_POSITION = verticalScale(20);
  const useNativeDriver = false;
  const position = React.useRef(new Animated.Value(INITIAL_POSITION));

  // Pan Responder Animation
  const animateMove = toValue => {
    Animated.spring(position.current, {
      toValue,
      tension: 120,
      useNativeDriver,
    }).start();
  };

  const movementValue = gestureState => screenHeight - gestureState.moveY;

  const onMoveShouldSetPanResponder = (_, gestureState) =>
    gestureState.dy >= 10 || gestureState.dy <= 10;

  const onPanResponderMove = (_, gestureState) => {
    const toValue = movementValue(gestureState);
    animateMove(toValue <= height ? height - toValue : CLAMP_POSITION);
  };

  const onPanResponderRelease = (_, gestureState) => {
    const toValue =
      movementValue(gestureState) > INITIAL_POSITION
        ? CLAMP_POSITION
        : INITIAL_POSITION;
    animateMove(toValue);
  };

  const panGesture = PanResponder.create({
    onPanResponderMove,
    onPanResponderRelease,
    onMoveShouldSetPanResponder,
    onStartShouldSetPanResponderCapture: onMoveShouldSetPanResponder,
  });

  const draggableStyle = {
    transform: [{ translateY: position.current || INITIAL_POSITION }],
  };

  return (
    <Animated.View
      style={[
        styles.container,
        styles.border,
        styles.shadow,
        draggableStyle,
        {
          height,
        },
      ]}>
      <View
        style={[styles.gestureArea, styles.border]}
        {...panGesture.panHandlers}>
        <View style={styles.grabber} />
      </View>
      {children}
    </Animated.View>
  );
};

ActionBottomSheet.propTypes = {
  children: PropTypes.node,
  height: PropTypes.number,
};

export default ActionBottomSheet;
