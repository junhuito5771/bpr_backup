import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { heightPercentage } from '@module/utility';

import Divider from '../Divider';
import Colors from '../../constants/Colors';

const CARD = 'Card';
const CARDBUTTON = 'CardButton';
const CARDCONTENT = 'CardContent';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  rightContainer: {
    justifyContent: 'flex-end',
  },
  container: {
    marginHorizontal: 25,
    borderRadius: 25,
    shadowColor: Colors.midLightGrey,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.5,
    shadowRadius: 6.27,
    elevation: 8,
    backgroundColor: Colors.white,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  flex: {
    flex: 1,
  },
  touchableFlex: {
    width: '100%',
    height: '100%',
  },
  inner: {
    flex: 0.72,
    padding: 25,
    marginTop: heightPercentage(8),
    justifyContent: 'flex-end',
  },
  button: {
    flex: 0.28,
  },
});

const Card = ({ children }) => {
  const CardButtons = React.Children.toArray(children).filter(
    cardButtons => cardButtons.type.displayName === CARDBUTTON
  );

  const CardContent = React.Children.toArray(children).filter(
    cardContent => cardContent.type.displayName === CARDCONTENT
  );

  return (
    <View style={[styles.container, styles.row]}>
      {CardContent}
      <Divider />
      <View style={styles.button}>
        {CardButtons.map((cardButton, index) =>
          React.cloneElement(cardButton, {
            hasDivider: cardButton && index !== CardButtons.length - 1,
          })
        )}
      </View>
    </View>
  );
};

Card.displayName = CARD;

Card.Button = ({ onPress, iconSource, hasDivider }) => (
  <>
    <TouchableOpacity style={styles.flex} onPress={onPress}>
      <View style={[styles.flex, styles.center]}>
        <Image source={iconSource} />
      </View>
    </TouchableOpacity>
    {hasDivider && <Divider horizontal />}
  </>
);

Card.Button.displayName = CARDBUTTON;

Card.Button.propTypes = {
  onPress: PropTypes.func,
  iconSource: PropTypes.number,
  hasDivider: PropTypes.bool,
};

Card.Content = ({ children }) => (
  <View style={styles.inner}>{!!children && children}</View>
);

Card.Content.displayName = CARDCONTENT;

Card.Content.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

Card.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default Card;
