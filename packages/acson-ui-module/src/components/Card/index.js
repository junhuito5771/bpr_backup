import React from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import { heightPercentage, SetIndoorTemperature, scale } from '@module/utility';

import Card from './Card';
import ModalService from '../Modal/modalService';

import Colors from '../../constants/Colors';
import Text from '../Text';
import styles from './styles';

const UNIT_CARD_HEIGHT = heightPercentage(14);

const MoveGroupButton = ({ isLoading }) =>
  isLoading ? (
    <ActivityIndicator size="small" color={Colors.white} />
  ) : (
    <Image
      source={require('../../shared/images/general/stack.png')}
      style={{ tintColor: Colors.white }}
    />
  );

MoveGroupButton.propTypes = {
  isLoading: PropTypes.bool,
};

const UnitCard = ({
  status,
  switchState,
  indoorTemp,
  outdoorTemp,
  unitName,
  onPressSwitch,
  onPressCard,
  thingName,
  isFetchUnitsLoading,
  unitLoadingList,
  submitFirmwareUpgrade,
  needUpgrade,
  onPressMoveGroup,
  drag,
  isEdit,
}) => {
  const textColor = () => {
    if (status === 'disconnected') {
      return styles.grey;
    }
    if (switchState === 1) {
      return styles.white;
    }
    return styles.black;
  };

  const indoorTempIcon = () => {
    if (status === 'disconnected') {
      return require('../../shared/images/indicators/indoorTempLightGrey.png');
    }
    if (switchState === 1) {
      return require('../../shared/images/indicators/indoorTempWhite.png');
    }
    return require('../../shared/images/indicators/indoorTempGrey.png');
  };

  const tempIcon = () => {
    if (status === 'disconnected') {
      return require('../../shared/images/indicators/tempLightGrey.png');
    }
    if (switchState === 1) {
      return require('../../shared/images/indicators/tempWhite.png');
    }
    return require('../../shared/images/indicators/tempGrey.png');
  };

  const switchIcon = () => {
    if (status === 'disconnected') {
      return require('../../shared/images/home/disableSwitch.png');
    }
    if (switchState === 1) {
      return require('../../shared/images/home/onSwitch.png');
    }
    return require('../../shared/images/home/offSwitch.png');
  };

  const showFirmwareUpgradeNeedAlert = () => {
    ModalService.show({
      title: 'Firmware update available',
      desc: needUpgrade,
      buttons: [
        {
          title: 'Yes',
          onPress: () => submitFirmwareUpgrade(thingName),
          isRaised: true,
        },
        { title: 'No' },
      ],
    });
  };

  const unitStyle = [
    styles.button,
    styles.shadow,
    styles.marginBottom1,
    switchState === 1 && status === 'connected' && styles.redBackground,
    styles.border,
  ];

  const switchHandler = unitThingName => {
    if (isFetchUnitsLoading) {
      return (
        <ActivityIndicator
          size="small"
          color={Colors.black}
          style={styles.marginRight5}
        />
      );
    }
    if (unitLoadingList[unitThingName]) {
      return (
        <ActivityIndicator
          size="small"
          color={Colors.black}
          style={styles.marginRight5}
        />
      );
    }
    return <Image style={styles.switchIcon} source={switchIcon()} />;
  };

  return (
    <View
      style={{
        height: UNIT_CARD_HEIGHT,
      }}>
      <View style={styles.width}>
        <View style={unitStyle}>
          <TouchableWithoutFeedback
            onPress={onPressCard}
            onLongPress={drag}
            delayLongPress={300}
            disabled={indoorTemp === undefined || indoorTemp === 0}>
            <View style={styles.align}>
              <View>
                <View style={styles.nameContainer}>
                  <Text
                    style={[styles.text, styles.restrictText, textColor()]}
                    numberOfLines={1}>
                    {unitName}
                  </Text>
                  {!isEdit && !!needUpgrade && (
                    <TouchableOpacity
                      style={styles.infoIconContainer}
                      hitSlop={styles.hitSlop}
                      disabled={status === 'disconnected'}
                      onPress={showFirmwareUpgradeNeedAlert}>
                      <Image
                        source={
                          status === 'disconnected'
                            ? require('../../shared/images/general/infoOff.png')
                            : require('../../shared/images/general/info.png')
                        }
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  )}
                </View>
                <View style={[styles.center, styles.marginTop]}>
                  <Image
                    source={indoorTempIcon()}
                    style={styles.tempIcon}
                    resizeMode="stretch"
                  />
                  <Text small style={[styles.marginLeft8, textColor()]}>
                    {SetIndoorTemperature(indoorTemp)}{' '}
                  </Text>
                  {outdoorTemp > 0 && (
                    <>
                      <Image
                        source={tempIcon()}
                        style={styles.outdoorTempIcon}
                      />
                      <Text small style={[styles.marginLeft8, textColor()]}>
                        {`${outdoorTemp > 0 ? outdoorTemp : '-'}°C`}
                      </Text>
                    </>
                  )}
                </View>
              </View>
              {isEdit ? (
                <View style={{ marginRight: scale(10) }}>
                  <TouchableOpacity onPress={onPressMoveGroup}>
                    <Image
                      source={require('../../shared/images/general/stack.png')}
                    />
                  </TouchableOpacity>
                </View>
              ) : (
                <TouchableWithoutFeedback onPress={onPressSwitch}>
                  {switchHandler(thingName)}
                </TouchableWithoutFeedback>
              )}
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </View>
  );
};

UnitCard.propTypes = {
  onPressSwitch: PropTypes.func,
  onPressCard: PropTypes.func,

  status: PropTypes.string,
  switchState: PropTypes.number,
  outdoorTemp: PropTypes.number,
  indoorTemp: PropTypes.number,
  unitName: PropTypes.string,

  isFetchUnitsLoading: PropTypes.bool,
  unitLoadingList: PropTypes.object,
  submitFirmwareUpgrade: PropTypes.func,
  needUpgrade: PropTypes.string,
  onPressMoveGroup: PropTypes.func,
  drag: PropTypes.func,
  thingName: PropTypes.string,
  isEdit: PropTypes.bool,
};

export { UNIT_CARD_HEIGHT, UnitCard };
export default Card;
