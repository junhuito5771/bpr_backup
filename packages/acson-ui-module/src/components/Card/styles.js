import { StyleSheet } from 'react-native';
import {
  heightPercentage,
  widthPercentage,
  isSmallDevice,
  ScaleText,
  scale,
  verticalScale,
} from '@module/utility';

import Colors from '../../constants/Colors';

export default StyleSheet.create({
  text: {
    fontSize: ScaleText(16),
    maxWidth: widthPercentage(40),
  },
  restrictText: {
    fontSize: ScaleText(16),
    maxWidth: widthPercentage(30),
  },
  marginTop: {
    marginTop: 5,
  },
  width: {
    justifyContent: 'center',
    flexDirection: 'row',
    width: widthPercentage(85),
    alignSelf: 'center',
  },
  border: {
    borderColor: Colors.titleGrey,
    borderWidth: 0.5,
    borderRadius: 5,
  },
  align: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '90%',
  },
  button: {
    width: widthPercentage(85),
    height: heightPercentage(12),
    backgroundColor: Colors.lightWhite,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: scale(2),
  },
  regroupBackground: {
    backgroundColor: Colors.darkRed,
    width: scale(50),
    height: verticalScale(60),
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: scale(2),
  },
  red: {
    color: Colors.red,
  },
  grey: {
    color: Colors.linkGrey,
  },
  center: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tempIcon: {
    marginLeft: scale(2),
    overflow: 'visible',
  },
  outdoorTempIcon: {
    marginLeft: isSmallDevice ? scale(10) : scale(20),
    overflow: 'visible',
  },
  marginLeft8: {
    marginLeft: isSmallDevice ? 6 : 8,
  },
  redBackground: {
    backgroundColor: Colors.red,
  },
  white: {
    color: Colors.white,
  },
  padding: {
    paddingLeft: scale(15),
    paddingRight: scale(10),
  },
  marginBottom1: {
    // marginBottom: heightPercentage(1),
  },
  black: {
    color: Colors.black,
  },
  shadow: {
    elevation: 3,
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
  },
  marginRight5: {
    marginRight: scale(15),
  },
  infoIconContainer: {},
  hitSlop: {
    top: 10,
    bottom: 10,
    right: 5,
    left: 5,
  },
  nameContainer: {
    flexDirection: 'row',
    width: scale(130),
    alignItems: 'center',
  },
  switchIcon: {
    resizeMode: 'contain',
    width: scale(130),
  },
});
