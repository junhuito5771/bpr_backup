import React from 'react';
import PropTypes from 'prop-types';
import Accordian from 'react-native-collapsible/Accordion';

const RNAccordian = ({ children, ...props }) => (
  <Accordian {...props}>{children}</Accordian>
);

RNAccordian.propTypes = {
  children: PropTypes.node,
};

export default RNAccordian;
