import React from 'react';
import PropTypes from 'prop-types';
import { connect, getIn } from 'formik';

import Selector from './selector';

const FormikSelector = ({
  name,
  formik,
  validator,
  forwardRef,
  submitOnEditEnd,
  onBlur,
  onSelectorPress,
  selectedValue,
  onChangeText,
  ...extraProps
}) => {
  const error = getIn(formik.errors, name);
  const touch = getIn(formik.touched, name);
  const value = getIn(formik.values, name);

  const errorMsg = touch && error ? error : '';

  React.useEffect(
    () =>
      selectedValue
        ? formik.setFieldValue(name, selectedValue)
        : formik.handleChange(name),
    [selectedValue]
  );

  const handleOnChange = React.useCallback(
    text => {
      onChangeText(text);
      if (validator) {
        formik.setFieldValue(name, validator(text));
      } else {
        formik.handleChange(name);
      }
    },
    [onChangeText]
  );

  const handleOnSelectorPress = React.useCallback(() => {
    onSelectorPress();
  }, [onSelectorPress]);

  return (
    <Selector
      maxFontSizeMultiplier={1}
      ref={forwardRef}
      value={value}
      errorMsg={errorMsg}
      onBlur={() => formik.setFieldTouched(name, true)}
      onChangeText={handleOnChange}
      onSubmitEditing={formik.handleSubmit}
      onSelectorPress={handleOnSelectorPress}
      {...extraProps}
    />
  );
};

FormikSelector.propTypes = {
  name: PropTypes.string,
  formik: PropTypes.object,
  validator: PropTypes.func,
  forwardRef: PropTypes.object,
  submitOnEditEnd: PropTypes.bool,
  initialValue: PropTypes.string,
  onBlur: PropTypes.func,
  onSelectorPress: PropTypes.func,
  onChangeText: PropTypes.func,
  selectedValue: PropTypes.string,
};

export default connect(FormikSelector);
