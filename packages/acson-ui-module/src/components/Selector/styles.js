import { StyleSheet, Platform } from 'react-native';

import Colors from '../../constants/Colors';

export default StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    alignItems: 'center',
    borderBottomColor: Colors.labelGrey,
    paddingVertical: Platform.select({
      ios: 10,
      android: 0,
    }),
  },
  borderBottomFocus: {
    borderBottomColor: Colors.lightBlue,
  },
  borderBottomError: {
    borderBottomColor: Colors.orange,
  },
  borderBottomDisabled: {
    borderBottomColor: Colors.disabledGrey,
  },
  input: {
    padding: 0,
    flex: 1,
  },
  label: {
    color: Colors.labelGrey,
  },
  selected: {
    color: Colors.lightBlue,
  },
  container: {
    width: '100%',
  },
  disabledStyle: {
    opacity: 0.5,
  },
  error: {
    color: Colors.orange,
  },
  messageContainer: {
    paddingVertical: 2,
    minHeight: 30,
  },
  hitslop: {
    top: 10,
    bottom: 10,
    right: 10,
    left: 10,
  },
  icon: {
    paddingRight: 15,
  },
});
