import React, { useState, forwardRef } from 'react';
import {
  View,
  TextInput as TextInputRN,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import Text from '../Text';
import styles from './styles';

const Selector = forwardRef(
  (
    {
      label,
      labelStyle,
      containerStyle,
      inputStyle,
      disabled,
      disabledStyle,
      textContentType,
      error,
      errorMsg,
      onBlur,
      onSelectorPress,
      onChangeText,
      ...extraProps
    },
    ref
  ) => {
    const [isFocus, setIsFocus] = useState(false);
    const hasError = error || !!errorMsg;

    return (
      <View style={StyleSheet.flatten([styles.container, containerStyle])}>
        <Text
          small
          style={StyleSheet.flatten([
            styles.label,
            isFocus && styles.selected,
            disabled && styles.disabledStyle,
            labelStyle,
          ])}>
          {label}
        </Text>
        <View
          style={StyleSheet.flatten([
            styles.inputContainer,
            isFocus && styles.borderBottomFocus,
            hasError && styles.borderBottomError,
            disabled && styles.borderBottomDisabled,
            inputStyle,
          ])}>
          <TextInputRN
            onChangeText={onChangeText}
            allowFontScaling={false}
            maxFontSizeMultiplier={1}
            style={StyleSheet.flatten([
              styles.input,
              disabled && styles.disabledStyle,
              disabled && disabledStyle,
              inputStyle,
            ])}
            ref={ref}
            underlineColorAndroid="transparent"
            editable={!disabled}
            onFocus={() => setIsFocus(true)}
            onBlur={() => {
              if (onBlur) onBlur();
              setIsFocus(false);
            }}
            {...extraProps}
          />
          <TouchableOpacity onPress={onSelectorPress}>
            <Text>Select Groups</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.messageContainer}>
          {hasError && <Text style={[styles.error]}>{errorMsg}</Text>}
        </View>
      </View>
    );
  }
);

Selector.propTypes = {
  label: PropTypes.string,
  labelStyle: PropTypes.object,
  containerStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  disabled: PropTypes.bool,
  disabledStyle: PropTypes.object,
  textContentType: PropTypes.string,
  error: PropTypes.bool,
  errorMsg: PropTypes.string,
  onBlur: PropTypes.func,
  onSelectorPress: PropTypes.func,
  onChangeText: PropTypes.func,
};

export default Selector;
