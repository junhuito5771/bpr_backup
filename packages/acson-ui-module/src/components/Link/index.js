import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import Text from '../Text';
import styles from './styles';

const TouchableText = ({
  children,
  onPress,
  primary,
  style,
  highlighted,
  disabled,
}) => (
  <TouchableOpacity
    onPress={onPress}
    style={style}
    hitSlop={styles.hitslop}
    disabled={disabled}>
    <Text
      small
      style={StyleSheet.flatten([
        styles.text,
        primary && styles.primary,
        highlighted && styles.highlighted,
        disabled && styles.disabled,
      ])}>
      {children}
    </Text>
  </TouchableOpacity>
);

TouchableText.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  onPress: PropTypes.func,
  primary: PropTypes.bool,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  highlighted: PropTypes.bool,
  disabled: PropTypes.bool,
};

export default TouchableText;
