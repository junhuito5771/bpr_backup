import { StyleSheet } from 'react-native';
import Colors from '../../constants/Colors';

export default StyleSheet.create({
  text: {
    color: Colors.labelGrey,
    paddingVertical: 10,
  },
  primary: {
    color: Colors.red,
  },
  highlighted: {
    color: Colors.lightBlue,
  },
  hitslop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
  disabled: {
    color: Colors.labelGrey,
  },
});
