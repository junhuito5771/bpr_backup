import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  horizontal: {
    height: 1,
  },
  vertical: {
    width: 1,
    height: '100%',
  },
});
