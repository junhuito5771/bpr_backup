import React from 'react';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';

import styles from './styles';
import Colors from '../../constants/Colors';

const TRANSPARENT_GRADIENT_BORDER = [
  Colors.white,
  Colors.borderlightGray,
  Colors.white,
];

const Divider = ({ horizontal }) => (
  <LinearGradient
    style={[horizontal && styles.horizontal, !horizontal && styles.vertical]}
    colors={TRANSPARENT_GRADIENT_BORDER}
  />
);

Divider.propTypes = {
  horizontal: PropTypes.bool,
};

Divider.defaultProps = {
  horizontal: false,
};

export default Divider;
