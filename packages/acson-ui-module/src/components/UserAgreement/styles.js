import { StyleSheet, Platform } from 'react-native';

import Colors from '../../constants/Colors';

export default StyleSheet.create({
  scrollView: {
    paddingHorizontal: 25,
    paddingVertical: 25,
  },
  marginBottom: {
    marginBottom: 5,
  },
  paragraph: {
    lineHeight: Platform.OS === 'android' ? 21 : 0,
    textAlign: 'justify',
  },
  largeSpace: {
    marginBottom: 15,
  },
  link: {
    color: Colors.blue,
  },
  title: {
    textAlign: 'center',
    marginBottom: 15,
  },
  row: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
  },
});
