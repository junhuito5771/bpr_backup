import React from 'react';
import { View } from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import PropTypes from 'prop-types';

import Text from '../Text';
import styles from './styles';

const PointParagraph = ({ bulletText, children, hasHyperLink }) => {
  const Container = hasHyperLink ? Hyperlink : React.Fragment;
  const containerProps = hasHyperLink
    ? { linkDefault: true, linkStyle: styles.link }
    : {};

  return (
    <Container {...containerProps}>
      <View style={[styles.marginBottom, styles.row]}>
        <Text small>{bulletText} </Text>
        <Text small style={[styles.flex, styles.paragraph]}>
          {children}
        </Text>
      </View>
    </Container>
  );
};

PointParagraph.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  bulletText: PropTypes.string.isRequired,
  hasHyperLink: PropTypes.bool,
};

const UserAgreement = () => (
  <View style={styles.scrollView}>
    <Text small style={[styles.marginBottom, styles.paragraph]}>
      Please read through these conditions of use carefully before using the
      Software.
    </Text>
    <Text small style={[styles.marginBottom, styles.paragraph]}>
      These terms and conditions (&apos;Terms of Use&apos;) are a binding legal
      agreement among you (&apos;You&apos; or &apos;Your&apos;), Acson Malaysia
      Sales &amp; Service Sdn. Bhd. (129688-D), a company having its registered
      office at Lot 50, Jalan BRP 8/3, Persiaran Bukit Rahman Putra 3, 47000
      Sungai Buloh, Selangor, Malaysia (collectively &apos;Acson&apos;)
      concerning the use of the mobile application known as MY Acson
      (&apos;Software&apos;) and constitutes the complete agreement among Acson
      and You. These Terms of Use shall supersede any other provisions, terms
      and conditions set forth by You, and the rights of the parties shall
      therefore be governed exclusively by the provisions, terms and conditions
      set forth herein.
    </Text>
    <Text small style={[styles.largeSpace, styles.paragraph]}>
      By using the Software, You agree to be bound by the terms and conditions
      of these Terms of Use. If You do not agree with these Terms of Use, You
      may not use the Software in any manner.
    </Text>

    <Text small style={[styles.marginBottom, styles.paragraph]}>
      1. Use of the Software
    </Text>
    <PointParagraph bulletText="a)">
      Subject to the terms and conditions of these Terms of Use, Acson grants
      You a free, non-exclusive and non-transferable license to use the Software
      on devices with suitable OS as designated by Acson for the purpose to
      remotely monitor and control Your Acson equipment.
    </PointParagraph>
    <PointParagraph bulletText="b)">
      These Terms of Use do not grant any right of use in any manner other than
      the above. In particular You are advised that the re-distribution, sale,
      lease, rental, sublicense or export of the Software in any form without
      written authority from Acson is explicitly prohibited. Unless as
      explicitly permitted under these Terms of Use, You shall not copy,
      reproduce, modify, transmit or distribute the Software or any portion
      thereof, or facilitate a third party to do so. You will not submit any
      information on the Software that is unlawful.
    </PointParagraph>

    <PointParagraph bulletText="c)" hasHyperLink>
      In case the Software is downloaded, installed and/or used by You as an
      Apple application, at all times, You will comply with the terms and
      conditions of Apple as applicable from time to time and available on
      http://www.apple.com/legal/terms/ and/or any terms and conditions of third
      parties applicable with respect to the use of the Software.
    </PointParagraph>

    <PointParagraph bulletText="d)" hasHyperLink>
      In case the Software is downloaded, installed and/or used by You as a
      Google Play Store application, at all times, You will comply with the
      terms and conditions of Google Play Store as applicable from time to time
      and available on http://www.google.com/mobile/android/market-tos.html
      and/or any terms and conditions of third parties applicable with respect
      to the use of the Software.
    </PointParagraph>

    <PointParagraph bulletText="e)">
      The Software is to be downloaded, installed and used on Your costs and
      expenses on Your own device in accordance with these Terms of Use.
    </PointParagraph>
    <PointParagraph bulletText="f)">
      Through the Software Acson may distribute advertisements on our own or on
      third parties’ behalf.
    </PointParagraph>

    <Text small style={[styles.marginBottom, styles.paragraph]}>
      2. Account registration
    </Text>
    <PointParagraph bulletText="a)">
      You may need a prior registration of an account for certain functions of
      the Software.
    </PointParagraph>
    <PointParagraph bulletText="b)">
      Acson will consider any use of the Software as usage by the user who
      performed the registration. Acson shall in no circumstances be liable for
      any damages arising out of any abuse or misuse of registered accounts by
      third parties due to Your inappropriate management of Your registered
      account.
    </PointParagraph>

    <Text small style={[styles.marginBottom, styles.paragraph]}>
      3. Copyright
    </Text>
    <PointParagraph bulletText="a)">
      Acson owns or is licensee of all (intellectual) property rights in the
      Software and its content and You acknowledge that You do not acquire any
      rights in respect of such intellectual property rights, except as
      otherwise explicitly provided in these Terms of Use.
    </PointParagraph>

    <Text small style={[styles.marginBottom, styles.paragraph]}>
      4. Trade Name / Trade Marks
    </Text>
    <PointParagraph bulletText="a)">
      All Trade Names and Trade Marks displayed through the Software (if any)
      that belong to Acson now or hereafter is/shall be the sole property of or
      are licensed to Acson or its affiliate(s). Your use of the Software should
      not be construed as granting, by implication or otherwise, any license or
      right to use any Trade Names and Trade Marks displayed through the
      Software (if any).
    </PointParagraph>

    <Text small style={[styles.marginBottom, styles.paragraph]}>
      5. Disclaimer of warranty
    </Text>
    <PointParagraph bulletText="a)">
      Acson has compiled the content of the Software (including translations) to
      the best of its knowledge. The present Software (and its content) is
      however drawn up by way of information only and does not constitute an
      offer binding upon Acson. Acson does not warrant that the Software will be
      error-free. The Software and its content is provided on an &apos;AS
      IS&apos; basis, without warranty of any kind - either express or implied,
      including but not limited to warranty of title and against infringement or
      implied warranties of merchantability or fitness for a particular purpose
      - being made in relation to the completeness, availability, accuracy,
      reliability of the Software or its content and the products and services
      presented therein.
    </PointParagraph>
    <PointParagraph bulletText="b)">
      The Software has inherent limitations including but not limited to
      possible design faults and programming bugs. The entire risk to the
      quality and performance of the Software is borne by You, and it is Your
      responsibility to ensure that it does what You require it to do prior to
      using it for any purpose (other than testing it).
    </PointParagraph>

    <Text small style={[styles.marginBottom, styles.paragraph]}>
      6. Limited liability
    </Text>
    <PointParagraph bulletText="a)">
      When You use the Software You acknowledge and accept that You do so at
      Your sole risk. To the maximum extent permitted by applicable law, Acson
      shall not be liable to You or any other party for any direct, indirect,
      general, special, incidental, punitive and/or consequential damages
      arising out of the use or inability to use the Software and/or the
      interpretation of its content. You agree that under no circumstances You
      shall have any claim against Acson or associated companies whether as
      employee, subcontractor, agent, representative, consultant, installer or
      otherwise for loss, damages, harm, injury, expense, work stoppage, loss of
      business information, business interruption, computer failure or
      malfunction which may be suffered by You or any other party from any cause
      whatsoever, howsoever arising in connection with the use of the Software
      and its content even where Acson were aware or ought to have been aware of
      the potential of such loss.
    </PointParagraph>

    <Text small style={[styles.marginBottom, styles.paragraph]}>
      7. Indemnity
    </Text>
    <PointParagraph bulletText="a)">
      You indemnify Acson and affiliated companies and hold them harmless
      against any claims which may arise from any loss, damages, harm, injury,
      expense, work stoppage, loss of business information, business
      interruption, computer failure or malfunction which may be suffered by You
      or any other party whatsoever as a consequence of any act or omission of
      Acson or affiliated companies, whether negligent or not, arising out of
      Your use of the Software and its content or from any cause whatsoever,
      howsoever arising in connection with the Software.
    </PointParagraph>

    <Text small style={[styles.marginBottom, styles.paragraph]}>
      8. Amendments
    </Text>
    <PointParagraph bulletText="a)">
      Acson reserves the rights to terminate the Software, to release new
      versions of the Software (including changes in functionality or interface)
      that could replace earlier released versions of the Software or to update
      the Software&apos;s content, in whole or part, at any time at Acson&apos;s
      own discretion and without prior announcement.
    </PointParagraph>
    <PointParagraph bulletText="b)">
      Acson reserves the right to modify from time to time these Terms of Use at
      Acson&apos;s own discretion and without prior announcement. You are
      responsible for informing Yourself of these Terms of Use in force at the
      time of access and use of the Software.
    </PointParagraph>
    <Text small style={[styles.marginBottom, styles.paragraph]}>
      9. Data protection
    </Text>
    <PointParagraph bulletText="a)">
      Acson will collect (i) before You being able to use the Software, the IP
      address on the adapter of Your Acson equipment to enable communication
      between the server and the adapter, (ii) in the course of use of the
      Software, operating status of Your Acson equipment, (iii) in the course of
      use of the Software, the name of Your Acson equipment and (iv) in the
      course of use of the Software, the IP address of Your own device to be
      designated and registered with the Software by You (collectively “Your
      Data”).
    </PointParagraph>
    <PointParagraph bulletText="b)">
      Acson only collects Your Data as You have chosen to download and use the
      Software, You have thus demonstrated an interest in its content and,
      accordingly, Your Data could be made available to the members of our
      marketing department or the marketing responsibles of our subsidiaries
      and/or independent distributors and allocated by them to keep You informed
      about the Software or its content, and/or to use Your Data for anonymous
      analysis for marketing and sales purposes, such as to measure the use of
      the Software by country/geographical location. Acson will not use Your
      Data for other purposes.
    </PointParagraph>
    <PointParagraph bulletText="c)">
      Acson may (i) control, process and store Your Data on a server located
      inside or outside the country where You live, and (ii) have Your Data
      controlled, processed and stored by a third-party server provider as
      designated by Acson on a server located inside or outside the country
      where You live for the purpose of outsourcing any portion of the service
      to be provided through the Software; provided, however, that Acson shall
      cause such third-party service provider to take appropriate measures to
      protect Your Data. For the avoidance of doubt, Acson may process or
      arrange the processing of Your Data on a server located in Singapore.
    </PointParagraph>
    <PointParagraph bulletText="d)">
      Except as expressly provided herein, Acson shall not disclose Your Data to
      third parties without Your prior consent. Notwithstanding the foregoing,
      Acson may disclose Your Data to meet any applicable law, regulation, legal
      process or enforceable governmental request.
    </PointParagraph>
    <Text small style={[styles.marginBottom, styles.paragraph]}>
      10. Miscellaneous
    </Text>
    <PointParagraph bulletText="a)">
      The provisions of these Terms of Use shall also apply to and bind Your
      legal successors, administrators, legal representatives, and Your
      permitted assignees.
    </PointParagraph>
    <PointParagraph bulletText="b)">
      Except where explicitly stated otherwise herein, if any provision of these
      Terms of Use is found to be invalid or unenforceable, the invalidity or
      unenforceability of such provision shall not affect the other provisions
      of these Terms of Use, and all provisions not affected by such invalidity
      or unenforceability shall remain in full force and effect. In such cases
      You agree to attempt to substitute for each invalid or unenforceable
      provision a valid or enforceable provision which achieves to the greatest
      extent possible the objectives and intention of the invalid or
      unenforceable provision.
    </PointParagraph>
    <PointParagraph bulletText="c)">
      The validity and interpretation of these Terms of Use will be governed by
      the laws of Malaysia and any claim arising out of these Terms of Use shall
      exclusively be brought to the Malaysia courts.
    </PointParagraph>
  </View>
);

export default UserAgreement;
