import React, {
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import momentDurationFormat from 'moment-duration-format';

import Text from '../Text';
import styles from './styles';

const DEFAULT_DURATION = 60000;
momentDurationFormat(moment);

const Timer = forwardRef(
  (
    { duration, onTimeOut, isAutoRefresh, hideWhenTimeOut, momentFormat },
    ref
  ) => {
    const [timer, setTimer] = useState(duration);

    const momentText =
      moment.duration(timer).asDays() > 1
        ? moment.duration(timer).humanize()
        : moment.duration(timer).format('hh:mm:ss', { trim: 'false' });

    const durationInSecond = duration > 0 ? timer / 1000 : 0;

    const countDownText = momentFormat ? momentText : `(${durationInSecond}s)`;

    useEffect(() => {
      if (timer > 0) {
        const timerListener = setInterval(() => setTimer(timer - 1000), 1000);

        return () => clearInterval(timerListener);
      }
      if (timer <= 0) {
        // If the timer has been timeout
        if (isAutoRefresh) setTimer(duration);
        if (onTimeOut) onTimeOut();
      }

      return () => {};
    }, [timer]);

    useImperativeHandle(ref, () => ({
      resetTimer() {
        setTimer(duration);
      },
    }));

    return (
      <Text style={styles.text}>
        {hideWhenTimeOut && timer <= 0 ? '' : countDownText}
      </Text>
    );
  }
);

Timer.propTypes = {
  duration: PropTypes.number,
  onTimeOut: PropTypes.func,
  isAutoRefresh: PropTypes.bool,
  hideWhenTimeOut: PropTypes.bool,
  momentFormat: PropTypes.bool,
};

Timer.defaultProps = {
  duration: DEFAULT_DURATION,
  hideWhenTimeOut: false,
  momentFormat: false,
};

export default Timer;
