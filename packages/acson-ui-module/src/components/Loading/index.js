import React from 'react';
import { View, ActivityIndicator, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import Text from '../Text';
import styles from './styles';

const Loading = ({ size, color, children, visible, progress, onCancel }) => {
  if (!visible) return <View />;

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <ActivityIndicator size={size} color={color} />
        <View style={styles.descSection}>
          {!!progress && <Text style={styles.text}>{progress}</Text>}
          {!!children && <Text style={styles.text}>{children}</Text>}
        </View>
        <View style={styles.buttonSection}>
          {onCancel && (
            <TouchableOpacity onPress={onCancel} hitSlop={styles.hitslop}>
              <Text style={styles.buttonText}>Cancel</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};

Loading.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
  children: PropTypes.string,
  visible: PropTypes.bool,
  progress: PropTypes.string,
  onCancel: PropTypes.func,
};

export default Loading;
