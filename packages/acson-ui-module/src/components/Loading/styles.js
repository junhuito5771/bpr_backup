import { StyleSheet } from 'react-native';
import Colors from '../../constants/Colors';

export default StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.25)',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  wrapper: {
    borderRadius: 10,
    backgroundColor: Colors.white,
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  descSection: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    color: Colors.titleGrey,
    marginTop: 10,
  },
  buttonSection: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: Colors.red,
  },
  hitslop: {
    top: 20,
    bottom: 20,
    left: 20,
    right: 20,
  },
});
