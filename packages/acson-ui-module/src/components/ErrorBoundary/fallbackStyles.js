import { StyleSheet } from 'react-native';

import Colors from '../../constants/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  content: {
    marginHorizontal: 16,
  },
  title: {
    fontSize: 48,
    fontWeight: '300',
    paddingBottom: 16,
    fontFamily: 'Avenir-Black',
  },
  subtitle: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'Avenir',
  },
  error: {
    paddingVertical: 16,
    fontFamily: 'Avenir',
  },
  button: {
    backgroundColor: Colors.red,
    borderRadius: 50,
    padding: 16,
  },
  buttonText: {
    color: '#fff',
    fontWeight: '600',
    textAlign: 'center',
  },
  bottomContainer: {
    marginHorizontal: 16,
    marginBottom: 20,
  },
  emailLink: {
    color: '#2196f3',
  },
});
