import React from 'react';
import PropTypes from 'prop-types';
import { ScaleText } from '@module/utility';
import {
  StyleSheet,
  Text,
  Modal,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Colors from '../../constants/Colors';

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    zIndex: 9,
    backgroundColor: `${Colors.grey}51`, // Android: 32% opacity
  },
  container: {
    width: '100%',
    height: '100%',
  },
  icon: {
    marginHorizontal: 15,
  },
  text: {
    color: Colors.white,
    fontSize: ScaleText(16),
    lineHeight: ScaleText(22),
    fontWeight: '500',
  },
  iconGroup: {
    flexDirection: 'row',
  },
});

const TutorialOverlay = ({ onClose, visible, currentStep, iconRightAlign }) => (
  <Modal onRequestClose={onClose} visible={visible} transparent>
    <TouchableWithoutFeedback style={styles.container} onPress={onClose}>
      <View style={styles.overlay}>
        <View
          style={[
            styles.iconGroup,
            {
              left: currentStep.position.x,
              top: currentStep.position.y,
            },
          ]}>
          {iconRightAlign || currentStep.iconRightAlign ? (
            <>
              <Text style={styles.text}>{currentStep.description}</Text>
              <View style={styles.icon}>{currentStep.icon}</View>
            </>
          ) : (
            <>
              <View style={styles.icon}>{currentStep.icon}</View>
              <Text style={styles.text}>{currentStep.description}</Text>
            </>
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  </Modal>
);

TutorialOverlay.propTypes = {
  onClose: PropTypes.func,
  visible: PropTypes.bool,
  currentStep: PropTypes.shape({
    position: PropTypes.shape({
      x: PropTypes.number,
      y: PropTypes.number,
    }),
    icon: PropTypes.element,
    description: PropTypes.string,
    iconRightAlign: PropTypes.bool,
  }),
  iconRightAlign: PropTypes.bool,
};

export default TutorialOverlay;
