import React, {
  useState,
  useRef,
  useImperativeHandle,
  forwardRef,
} from 'react';
import {
  Animated,
  View,
  Dimensions,
  StyleSheet,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  PanGestureHandler,
  NativeViewGestureHandler,
  State,
  TapGestureHandler,
} from 'react-native-gesture-handler';
import { iphoneXS } from '@module/utility';

import styles from './styles';

const windowHeight = Dimensions.get('window').height;
const snapRatio = Platform.select({
  ios: iphoneXS ? 0.3 : 0.2,
  android: 0.13,
});
const SNAP_POINTS_FROM_TOP = [windowHeight, windowHeight * snapRatio];

const START = SNAP_POINTS_FROM_TOP[0];
const END = SNAP_POINTS_FROM_TOP[SNAP_POINTS_FROM_TOP.length - 1];

const BottomModal = forwardRef(({ renderContent }, ref) => {
  const masterdrawer = React.createRef();
  const drawer = React.createRef();
  const drawerheader = React.createRef();
  const scroll = React.createRef();

  const [lastSnap, setLastSnap] = useState(START);

  const lastScrollYValue = useRef(0);
  const lastScrollY = useRef(new Animated.Value(0)).current;
  lastScrollY.addListener(({ value }) => {
    lastScrollYValue.current = value;
  });
  const onRegisterLastScroll = useRef(
    Animated.event([{ nativeEvent: { contentOffset: { y: lastScrollY } } }], {
      useNativeDriver: true,
    })
  ).current;

  const dragY = useRef(new Animated.Value(0)).current;
  const onGestureEvent = useRef(
    Animated.event([{ nativeEvent: { translationY: dragY } }], {
      useNativeDriver: true,
    })
  ).current;
  const reverseLastScrollY = Animated.multiply(
    new Animated.Value(-1),
    lastScrollY
  );

  const translateYOffset = useRef(new Animated.Value(START)).current;
  const translateY = Animated.add(
    translateYOffset,
    Animated.add(dragY, reverseLastScrollY)
  ).interpolate({
    inputRange: [END, START],
    outputRange: [END, START],
    extrapolate: 'clamp',
  });

  const handleClose = () => {
    setLastSnap(START);
    Animated.spring(translateYOffset, {
      velocity: 20,
      tension: 86,
      friction: 12,
      toValue: START,
      useNativeDriver: true,
    }).start();
  };

  useImperativeHandle(ref, () => ({
    open: () => {
      setLastSnap(END);
      Animated.spring(translateYOffset, {
        velocity: 20,
        tension: 86,
        friction: 12,
        toValue: END,
        useNativeDriver: true,
      }).start();
    },
    close: handleClose,
  }));

  const onHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.oldState === State.ACTIVE) {
      const { velocityY } = nativeEvent;
      let { translationY } = nativeEvent;
      translationY -= lastScrollYValue.current;
      const dragToss = 0.05;
      const dragTossVelocity = dragToss * velocityY;
      const endOffsetY = lastSnap + translationY + dragTossVelocity;

      let destSnapPoint = SNAP_POINTS_FROM_TOP[0];
      for (let i = 0; i < SNAP_POINTS_FROM_TOP.length; i += 1) {
        const snapPoint = SNAP_POINTS_FROM_TOP[i];
        const distFromSnap = Math.abs(snapPoint - endOffsetY);
        if (distFromSnap < Math.abs(destSnapPoint - endOffsetY)) {
          destSnapPoint = snapPoint;
        }
      }
      setLastSnap(destSnapPoint);
      translateYOffset.extractOffset();
      translateYOffset.setValue(translationY);
      translateYOffset.flattenOffset();
      dragY.setValue(0);
      Animated.spring(translateYOffset, {
        velocity: velocityY,
        tension: 86,
        friction: 12,
        toValue: destSnapPoint,
        useNativeDriver: true,
      }).start();
    }
  };

  const onHeaderHandlerStateChange = ({ nativeEvent }) => {
    if (nativeEvent.oldState === State.BEGAN) {
      lastScrollY.setValue(0);
    }
    onHandlerStateChange({ nativeEvent });
  };

  return (
    <TapGestureHandler
      maxDurationMs={100000}
      ref={masterdrawer}
      maxDeltaY={lastSnap - SNAP_POINTS_FROM_TOP[0]}>
      <View
        style={[styles.drawerContainer, StyleSheet.absoluteFillObject]}
        pointerEvents="box-none">
        {lastSnap !== START && (
          <TouchableWithoutFeedback onPress={handleClose}>
            <Animated.View style={styles.backdrop} />
          </TouchableWithoutFeedback>
        )}
        <Animated.View
          style={[
            StyleSheet.absoluteFillObject,
            {
              transform: [{ translateY }],
            },
          ]}>
          <PanGestureHandler
            ref={drawerheader}
            simultaneousHandlers={[scroll, masterdrawer]}
            shouldCancelWhenOutside={false}
            onGestureEvent={onGestureEvent}
            onHandlerStateChange={onHeaderHandlerStateChange}>
            <Animated.View style={styles.header}>
              <View style={styles.handler} />
            </Animated.View>
          </PanGestureHandler>
          <PanGestureHandler
            ref={drawer}
            simultaneousHandlers={[scroll, masterdrawer]}
            shouldCancelWhenOutside={false}
            onGestureEvent={onGestureEvent}
            onHandlerStateChange={onHandlerStateChange}>
            <Animated.View style={styles.container}>
              <NativeViewGestureHandler
                ref={scroll}
                waitFor={masterdrawer}
                simultaneousHandlers={drawer}>
                <Animated.ScrollView
                  style={styles.scrollView}
                  bounces={false}
                  onScrollBeginDrag={onRegisterLastScroll}
                  scrollEventThrottle={1}>
                  {renderContent()}
                </Animated.ScrollView>
              </NativeViewGestureHandler>
            </Animated.View>
          </PanGestureHandler>
        </Animated.View>
      </View>
    </TapGestureHandler>
  );
});

BottomModal.propTypes = {
  renderContent: PropTypes.func,
};

export default BottomModal;
