import { StyleSheet } from 'react-native';

import Colors from '../../constants/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: 50,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    backgroundColor: Colors.white,
    alignItems: 'center',
  },
  drawerContainer: {
    zIndex: 2,
  },
  scrollView: {
    backgroundColor: Colors.white,
  },
  backdrop: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.7,
    backgroundColor: Colors.black,
  },
  handler: {
    width: 40,
    height: 6,
    backgroundColor: Colors.lightCoolGrey,
    borderRadius: 25,
    marginTop: 10,
  },
});
