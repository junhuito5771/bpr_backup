import React from 'react';
import PropTypes from 'prop-types';
import Switch from 'react-native-switches';

import Colors from '../../constants/Colors';

const PillSwitch = ({ onPress, value }) => (
  <Switch
    shape="pill"
    onChange={onPress}
    value={value}
    showText={false}
    colorSwitchOn={Colors.switchFill}
    colorSwitchOff={Colors.veryLightGrey}
    sliderWidth={38}
    sliderHeight={18}
    buttonSize={13}
    buttonOffsetLeft={2}
    buttonOffsetRight={4}
  />
);

PillSwitch.propTypes = {
  onPress: PropTypes.func,
  value: PropTypes.bool,
};

export default PillSwitch;
