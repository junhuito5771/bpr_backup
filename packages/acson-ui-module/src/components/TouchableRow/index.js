import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const TouchableRow = ({ onPress, children }) => (
  <View style={styles.rowContainer}>
    <TouchableOpacity
      style={styles.touchableRow}
      onPress={onPress}
      disabled={!onPress}>
      <View style={styles.row}>{!!children && children}</View>
    </TouchableOpacity>
  </View>
);

TouchableRow.propTypes = {
  onPress: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

export default TouchableRow;
