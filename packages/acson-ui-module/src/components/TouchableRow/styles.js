import { verticalScale, scale } from '@module/utility';

import { StyleSheet } from 'react-native';
import Colors from '../../constants/Colors';

const styles = StyleSheet.create({
  rowContainer: {
    width: '100%',
    height: verticalScale(55),
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
  },
  row: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: scale(25),
    alignItems: 'center',
    flex: 1,
  },
  touchableRow: {
    flex: 1,
  },
});

export default styles;
