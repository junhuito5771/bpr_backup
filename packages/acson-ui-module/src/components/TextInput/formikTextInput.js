import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect, getIn } from 'formik';

import TextInput from './textinput';

const FormikTextInput = ({
  name,
  formik,
  validator,
  forwardRef,
  submitOnEditEnd,
  onBlur,
  validateOnChange,
  forceValue,
  ...extraProps
}) => {
  const error = getIn(formik.errors, name);
  const touch = getIn(formik.touched, name);
  const value = getIn(formik.values, name);

  const errorMsg = touch && error ? error : '';

  const handleOnChange = text => {
    const fieldValue = validator ? validator(text) : text;
    formik.setFieldValue(name, fieldValue, validateOnChange);
    if (validateOnChange) formik.setFieldTouched(name);
  };

  useEffect(() => {
    if (forceValue) {
      handleOnChange(forceValue);
    }
  }, [forceValue]);

  return (
    <TextInput
      maxFontSizeMultiplier={1}
      ref={forwardRef}
      value={value}
      errorMsg={errorMsg}
      onBlur={() => formik.setFieldTouched(name, true)}
      onChangeText={handleOnChange}
      onSubmitEditing={formik.handleSubmit}
      {...extraProps}
    />
  );
};

FormikTextInput.propTypes = {
  name: PropTypes.string,
  formik: PropTypes.object,
  validator: PropTypes.func,
  forwardRef: PropTypes.object,
  submitOnEditEnd: PropTypes.bool,
  initialValue: PropTypes.string,
  onBlur: PropTypes.func,
  validateOnChange: PropTypes.bool,
  forceValue: PropTypes.string,
};

export default connect(FormikTextInput);
