import React, { useState, forwardRef, useCallback } from 'react';
import {
  View,
  TextInput as TextInputRN,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { DeviceInfo } from '@module/utility';

import Text from '../Text';
import styles from './styles';

const brandsNeedingWorkaround = ['redmi', 'xiaomi', 'poco', 'pocophone'];
const needsXiaomiWorkaround =
  brandsNeedingWorkaround.includes(DeviceInfo.getBrand().toLowerCase()) &&
  Platform.Version > 28;

const EntypoIcon = ({ onPress }) => {
  const [isShow, setIsShow] = useState(false);

  const entypoIconPath = isShow
    ? require('../../shared/images/entypo/viewOn.png')
    : require('../../shared/images/entypo/viewOff.png');

  const toggleEntypo = () => {
    setIsShow(prevIsShow => !prevIsShow);
    if (onPress) onPress();
  };

  return (
    <TouchableOpacity
      style={styles.icon}
      onPress={toggleEntypo}
      hitSlop={styles.hitslop}>
      <Image source={entypoIconPath} />
    </TouchableOpacity>
  );
};

EntypoIcon.propTypes = {
  onPress: PropTypes.func,
};

const TextInput = forwardRef(
  (
    {
      label,
      labelStyle,
      containerStyle,
      inputStyle,
      disabled,
      disabledStyle,
      textContentType,
      error,
      errorMsg,
      onBlur,
      rightComponent,
      onFocus,
      caretHidden,
      ...extraProps
    },
    ref
  ) => {
    const [isFocus, setIsFocus] = useState(false);
    const [isShowPassword, setIsShowPassword] = useState(false);
    const isPasswordInput = textContentType === 'password';
    const isPhoneInput = textContentType === 'telephoneNumber';
    const hasError = error || !!errorMsg;
    const [hackCaretHidden, setHackCaretHidden] = useState(
      needsXiaomiWorkaround ? true : caretHidden
    );

    const handleFocus = useCallback(() => {
      if (needsXiaomiWorkaround) {
        setHackCaretHidden(caretHidden);
      }
      setIsFocus(true);
      if (onFocus) onFocus();
    }, [onFocus, caretHidden]);

    const handleShowPassword = () => {
      setIsShowPassword(prevIsShowPassword => !prevIsShowPassword);
    };

    return (
      <View style={StyleSheet.flatten([styles.container, containerStyle])}>
        <Text
          small
          style={StyleSheet.flatten([
            styles.label,
            isFocus && styles.selected,
            disabled && styles.disabledStyle,
            labelStyle,
          ])}>
          {label}
        </Text>
        <View
          style={StyleSheet.flatten([
            styles.inputContainer,
            isFocus && styles.borderBottomFocus,
            hasError && styles.borderBottomError,
            disabled && styles.borderBottomDisabled,
            inputStyle,
          ])}>
          {isPhoneInput && <Text style={styles.countryCodeContainer}>+60</Text>}
          <TextInputRN
            allowFontScaling={false}
            maxFontSizeMultiplier={1}
            style={StyleSheet.flatten([
              styles.input,
              disabled && styles.disabledStyle,
              disabled && disabledStyle,
              inputStyle,
            ])}
            ref={ref}
            underlineColorAndroid="transparent"
            secureTextEntry={isPasswordInput && !isShowPassword}
            editable={!disabled}
            onFocus={handleFocus}
            onBlur={() => {
              if (onBlur) onBlur();
              setIsFocus(false);
            }}
            caretHidden={hackCaretHidden}
            {...extraProps}
          />
          {isPasswordInput && <EntypoIcon onPress={handleShowPassword} />}
          {React.isValidElement(rightComponent) && rightComponent}
        </View>
        <View style={styles.messageContainer}>
          {hasError && <Text style={[styles.error]}>{errorMsg}</Text>}
        </View>
      </View>
    );
  }
);

TextInput.propTypes = {
  label: PropTypes.string,
  labelStyle: PropTypes.object,
  containerStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  disabled: PropTypes.bool,
  disabledStyle: PropTypes.object,
  textContentType: PropTypes.string,
  error: PropTypes.bool,
  errorMsg: PropTypes.string,
  onBlur: PropTypes.func,
  rightComponent: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.element,
    PropTypes.node,
  ]),
  onFocus: PropTypes.func,
  caretHidden: PropTypes.bool,
};

TextInput.defaultProps = {
  caretHidden: false,
};

export default TextInput;
