import { StyleSheet } from 'react-native';

import Colors from '../../constants/Colors';

export default StyleSheet.create({
  label: {
    marginLeft: 15,
    color: Colors.labelGrey,
  },
  container: {
    flexDirection: 'row',
  },
  wrapper: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
  },
});
