import React from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Image,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import Text from '../Text';

import styles from './styles';

function getCheckboxIconPath(isRound, value) {
  if (isRound) {
    return value
      ? require('../../shared/images/checkbox/roundCheckBoxOn.png')
      : require('../../shared/images/checkbox/roundCheckBoxOff.png');
  }

  return value
    ? require('../../shared/images/checkbox/checkBoxOn.png')
    : require('../../shared/images/checkbox/checkBoxOff.png');
}

const Checkbox = ({ children, value, onPress, containerStyle, round }) => {
  const checkboxIconPath = getCheckboxIconPath(round, value);

  return (
    <View style={StyleSheet.flatten([styles.container, containerStyle])}>
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.wrapper}>
          <Image source={checkboxIconPath} />
          <Text style={styles.label}>{children}</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

Checkbox.propTypes = {
  children: PropTypes.string,
  value: PropTypes.bool,
  onPress: PropTypes.func,
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  round: PropTypes.bool,
};

export default Checkbox;
