import React, {
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const CodeInput = forwardRef(
  ({ onKeyPress, hasError, isFocus, size, ...extraProps }, ref) => (
    <View
      style={StyleSheet.flatten([
        styles.box,
        styles.underline,
        isFocus && styles.focus,
        hasError && styles.error,
        size && { height: size, width: size },
      ])}>
      <TextInput
        ref={ref}
        style={styles.input}
        keyboardType="number-pad"
        maxLength={1}
        onKeyPress={onKeyPress}
        {...extraProps}
      />
    </View>
  )
);

CodeInput.propTypes = {
  onKeyPress: PropTypes.func,
  hasError: PropTypes.bool,
  isFocus: PropTypes.bool,
  size: PropTypes.number,
};

const CodeInputContainer = forwardRef(
  ({ length, onFulfill, onFocus, ...extraProps }, ref) => {
    const inputBoxs = [];
    const inputBoxRef = [];

    const [inputValue, setInputValue] = useState(new Array(length).fill(''));
    const [currentIndex, setCurrentIndex] = useState(0);

    useImperativeHandle(ref, () => ({
      onSubmit() {
        onFulfill(inputValue.toString());
      },
      getValue() {
        return inputValue.join('');
      },
    }));

    const handleFocus = focusIndex => {
      const currentEmptyIndex = inputValue.findIndex(item => !item);
      const newInputValue = inputValue.reduce((acc, cur, index) => {
        acc[index] = cur;
        if (index >= focusIndex) {
          acc[index] = '';
        }

        return acc;
      }, []);

      setInputValue(newInputValue);
      if (currentEmptyIndex > -1) {
        setCurrentIndex(currentEmptyIndex);
        inputBoxRef[currentEmptyIndex].current.focus();
      } else {
        setCurrentIndex(focusIndex);
        inputBoxRef[focusIndex].current.focus();
      }

      if (onFocus) onFocus();
    };

    const handleKeyPress = (event, index) => {
      if (event.nativeEvent.key === 'Backspace') {
        const focusIndex = Math.max(index - 1, 0);

        inputBoxRef[focusIndex].current.focus();
      }
    };

    const handleOnChangeText = (value, index) => {
      if (value) {
        const focusIndex = Math.min(index + 1, length - 1);
        const isLastIndex = index === length - 1;
        inputBoxRef[focusIndex].current.focus();

        setInputValue(prevInputValue => {
          const result = [...prevInputValue];
          result[index] = value;
          if (isLastIndex && onFulfill) onFulfill(result.join(''));

          return result;
        });

        if (isLastIndex) {
          inputBoxRef[length - 1].current.blur();
          setCurrentIndex(-1);
        }
      }
    };

    for (let i = 0; i < length; i += 1) {
      const inputID = `CodeInputBox_${i}`;
      const seperatorID = `CodeInputSeperator_${i}`;

      const inputRef = createRef();
      inputBoxRef.push(inputRef);

      inputBoxs.push(
        <CodeInput
          key={inputID}
          ref={inputRef}
          onKeyPress={event => handleKeyPress(event, i)}
          onChangeText={value => handleOnChangeText(value, i)}
          autoFocus={i === 0}
          isFocus={i === currentIndex}
          onFocus={() => handleFocus(i)}
          value={inputValue[i] ? inputValue[i].toString() : ''}
          {...extraProps}
        />
      );

      if (i < length - 1) {
        inputBoxs.push(
          <View style={styles.seperatorContainer} key={seperatorID}>
            <View key={seperatorID} style={styles.seperator} />
          </View>
        );
      }
    }

    return <View style={styles.container}>{inputBoxs}</View>;
  }
);

CodeInputContainer.propTypes = {
  length: PropTypes.number,
  keyboardType: PropTypes.string,
  onFulfill: PropTypes.func,
  onFocus: PropTypes.func,
};

CodeInputContainer.defaultProps = {
  length: 4,
};

export default CodeInputContainer;
