import { StyleSheet } from 'react-native';
import Colors from '../../constants/Colors';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  box: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    paddingBottom: 5,
  },
  input: {
    textAlign: 'center',
    padding: 0,
    fontSize: 18,
  },
  underline: {
    borderBottomColor: Colors.inputGrey,
    borderBottomWidth: 1,
  },
  focus: {
    borderBottomColor: Colors.lightBlue,
  },
  error: {
    borderBottomColor: Colors.orange,
  },
  seperatorContainer: {
    justifyContent: 'center',
  },
  seperator: {
    backgroundColor: '#D4D4D4',
    height: 2,
    width: 10,
  },
});
