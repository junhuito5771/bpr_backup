let modalRef;

function setModalRef(ref) {
  modalRef = ref;
}

function show({ title = '', desc = '', buttons = [], cancellable = true }) {
  modalRef.show({ title, desc, buttons, cancellable });
}

export default {
  setModalRef,
  show,
};
