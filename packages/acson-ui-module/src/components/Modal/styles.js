import { StyleSheet } from 'react-native';
import { heightPercentage, ScaleText } from '@module//utility';

import Colors from '../../constants/Colors';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    borderRadius: 15,
    paddingTop: heightPercentage(4),
    paddingBottom: heightPercentage(3),
    paddingHorizontal: 20,
    justifyContent: 'space-between',
  },
  flex: {
    flex: 1,
  },
  top: {
    justifyContent: 'center',
  },
  bottom: {
    marginTop: heightPercentage(5),
  },
  spaced: {
    marginBottom: 10,
  },
  buttonHeight: heightPercentage(6),
  descText: {
    lineHeight: 20,
    textAlign: 'center',
    marginVertical: 5,
  },
  textContainer: {
    alignItems: 'center',
  },
  buttonText: {
    fontSize: ScaleText(12),
  },
  titleText: {
    textAlign: 'center',
  },
});
