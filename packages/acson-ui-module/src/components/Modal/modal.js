import React, { useState, forwardRef, useImperativeHandle } from 'react';
import { View, Platform, StyleSheet } from 'react-native';
import ModalRN from 'react-native-modal';

import Text from '../Text';
import Button from '../Button/button';

import styles from './styles';

const renderButtons = (buttons, handleOnPress, cancellable) => {
  if (buttons && buttons.length === 0) {
    return (
      <Button
        key="OK"
        containerStyle={styles.spaced}
        height={styles.buttonHeight}
        onPress={handleOnPress(null, cancellable)}
        textStyle={styles.buttonText}
        raised
        hasBorder>
        OK
      </Button>
    );
  }

  return buttons.map((buttonProps, index) => {
    const { title, onPress, isRaised = false, hasBorder = true } = buttonProps;
    const isLastItem = index === buttons.length - 1;

    return (
      <Button
        key={title}
        containerStyle={StyleSheet.flatten([!isLastItem && styles.spaced])}
        height={styles.buttonHeight}
        onPress={handleOnPress(onPress, cancellable)}
        textStyle={styles.buttonText}
        raised={isRaised}
        hasBorder={hasBorder}>
        {title}
      </Button>
    );
  });
};

const Modal = forwardRef((props, ref) => {
  const [isVisible, setIsVisible] = useState(false);
  const [modalProps, setModalProps] = useState({
    title: '',
    desc: '',
    onSubmit: undefined,
    buttons: [],
    cancellable: true,
  });

  useImperativeHandle(ref, () => ({
    show: modalProp => {
      setIsVisible(true);
      setModalProps(modalProp);
    },
  }));

  const hideModal = () => {
    setIsVisible(false);
  };

  const handleOnPress = (onPress, cancellable) => () => {
    if (modalProps.buttons && onPress) {
      onPress();
    }
    if (cancellable) hideModal();
  };

  return (
    <ModalRN isVisible={isVisible} useNativeDriver={Platform.OS === 'android'}>
      <View style={styles.container}>
        <View style={styles.top}>
          <View style={styles.textContainer}>
            <Text large style={styles.titleText}>
              {modalProps.title}
            </Text>
            <Text small style={styles.descText}>
              {modalProps.desc}
            </Text>
          </View>
        </View>
        <View style={styles.bottom}>
          {renderButtons(
            modalProps.buttons,
            handleOnPress,
            modalProps.cancellable
          )}
        </View>
      </View>
    </ModalRN>
  );
});

export default Modal;
