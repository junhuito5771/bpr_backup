import React from 'react';
import { PropTypes } from 'prop-types';
import { TouchableOpacity, View } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import CacheImage from '../CacheImage';
import Text from '../Text';
import styles from './style';

const MainCarousel = ({
  data,
  loop,
  autoplay,
  itemWidth,
  sliderWidth,
  handlePress,
}) => {
  const carouselRef = React.useRef();
  const [currentIndex, setCurrentIndex] = React.useState(0);

  const handleSnapToItem = React.useCallback(slideIndex => {
    setCurrentIndex(slideIndex);
  }, []);

  // eslint-disable-next-line no-unused-vars
  const renderPagination = React.useMemo(
    () => (
      <Pagination
        dotsLength={data.length}
        activeDotIndex={currentIndex}
        inactiveDotStyle={styles.dot}
        dotStyle={styles.activeDot}
        containerStyle={styles.dotContainer}
        inactiveDotScale={1}
      />
    ),
    [data, currentIndex]
  );

  const handleRenderItem = React.useCallback(
    ({ item }) => (
      <View style={[styles.container, styles.shadow]}>
        <TouchableOpacity onPress={() => handlePress(item)}>
          <CacheImage style={styles.image} source={{ uri: item.imageLink }} />
          <View style={[styles.label]}>
            <Text small style={styles.labelText}>
              {item.title}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    ),
    [data]
  );

  return (
    <>
      <Carousel
        ref={carouselRef}
        data={data}
        onSnapToItem={handleSnapToItem}
        renderItem={handleRenderItem}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
        autoplay={autoplay}
        loop={loop}
      />
      {renderPagination}
    </>
  );
};

MainCarousel.propTypes = {
  data: PropTypes.array,
  sliderWidth: PropTypes.number,
  itemWidth: PropTypes.number,
  loop: PropTypes.bool,
  autoplay: PropTypes.bool,
  handlePress: PropTypes.func,
};

MainCarousel.defaultProps = {
  loop: true,
  autoplay: true,
};

export default MainCarousel;
