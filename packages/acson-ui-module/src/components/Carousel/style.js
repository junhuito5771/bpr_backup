import { verticalScale, scale } from '@module/utility';

import { StyleSheet } from 'react-native';
import Colors from '../../constants/Colors';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    borderRadius: scale(4),
    marginBottom: 4,
  },
  image: {
    width: '100%',
    height: verticalScale(140),
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
  },
  label: {
    flexDirection: 'row',
    alignItems: 'center',
    height: verticalScale(40),
  },
  labelText: {
    marginStart: scale(20),
    flexGrow: 0.8,
  },
  dotContainer: {
    marginBottom: -verticalScale(40),
  },
  dot: {
    borderRadius: 10,
    width: scale(6),
    height: scale(6),
    backgroundColor: Colors.black,
    marginHorizontal: -scale(6),
  },
  activeDot: {
    borderRadius: 10,
    width: scale(12),
    height: scale(6),
    backgroundColor: Colors.white,
    marginHorizontal: -scale(6),
  },
  shadow: {
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    elevation: 2,
  },
});
