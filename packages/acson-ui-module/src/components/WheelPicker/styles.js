import { StyleSheet } from 'react-native';
import { widthPercentage } from '@module/utility';

export default StyleSheet.create({
  container: {
    width: widthPercentage(70),
  },
});
