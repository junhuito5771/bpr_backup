import React from 'react';
import { Platform, View, StyleSheet } from 'react-native';
import { WheelPicker as PickerAndroid } from '@delightfulstudio/react-native-wheel-picker-android';
import PropTypes from 'prop-types';
import { Picker } from '@react-native-community/picker';

import styles from './styles';

const NativePicker = Platform.select({
  ios: Picker,
  android: PickerAndroid,
});

const WheelPicker = props => {
  const { data, ...restProps } = props;
  if (Platform.OS === 'android') {
    const { selectedValue, onValueChange } = restProps;
    const values = data.reduce((acc, { label }) => {
      acc.push(label);
      return acc;
    }, []);
    const selectedItemPosition = data.findIndex(
      ({ value }) => value === selectedValue
    );
    return (
      <View style={{ marginTop: 20 }}>
        <NativePicker
          visibleItemCount={4}
          isCurtain
          selectedItemTextColor="black"
          itemTextColor="lightgrey"
          renderIndicator
          indicatorColor="lightgrey"
          itemTextAlign="center"
          {...restProps}
          data={values}
          selectedItemPosition={selectedItemPosition}
          onItemSelected={({ position }) =>
            onValueChange((data[position] || {}).value)
          }
        />
      </View>
    );
  }

  const pickerIosStyle = StyleSheet.flatten([
    styles.container,
    restProps.style && restProps.style
  ])

  return (
    <Picker {...restProps} style={pickerIosStyle}>
      {data.map(({ label, value }) => (
        <Picker.Item key={label} label={label} value={value} />
      ))}
    </Picker>
  );
};

WheelPicker.propTypes = {
  data: PropTypes.array,
  style: PropTypes.object,
};

export default WheelPicker;
