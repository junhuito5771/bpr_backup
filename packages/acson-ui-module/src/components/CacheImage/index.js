import React from 'react';
import { PropTypes } from 'prop-types';
import FastImage from 'react-native-fast-image';

const CacheImage = ({ style, source, resizeMode, ...props }) => {
  const resizeMethod = {
    cover: FastImage.resizeMode.cover,
    stretch: FastImage.resizeMode.stretch,
    center: FastImage.resizeMode.center,
    contain: FastImage.resizeMode.contain,
  };

  return (
    <FastImage
      style={style}
      source={source}
      resizeMode={resizeMethod[resizeMode]}
      {...props}
    />
  );
};

CacheImage.propTypes = {
  style: PropTypes.object,
  source: PropTypes.object,
  resizeMode: PropTypes.string,
};

CacheImage.defaultProps = {
  resizeMode: 'cover',
};

export default CacheImage;
