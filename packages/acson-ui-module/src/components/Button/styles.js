import { StyleSheet, Platform } from 'react-native';
import Colors from '../../constants/Colors';

const contentFont = Platform.OS === 'android' && { fontFamily: 'Roboto' };

export default StyleSheet.create({
  container: {
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  },
  buttonContainer: {
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  fullWidth: {
    width: '100%',
  },
  shadow: {
    shadowColor: Colors.shadowRed,
    shadowOffset: {
      width: 2,
      height: 4,
    },
    shadowOpacity: 0.5,
    shadowRadius: 4.65,
    elevation: 8,
  },
  borderRed: {
    borderColor: Colors.red,
    borderWidth: 1,
  },
  backgroundRed: {
    backgroundColor: Colors.red,
  },
  disabledContainer: {
    borderColor: Colors.buttonGrey,
    borderWidth: 1,
    backgroundColor: Colors.white,
  },
  backgroundDisabledRaised: {
    backgroundColor: Colors.buttonGrey,
  },
  colorWhite: {
    color: Colors.white,
  },
  colorGrey: {
    color: Colors.buttonGrey,
  },
  text: {
    ...contentFont,
    color: Colors.red,
    fontSize: 20,
  },
  flex: {
    flex: 1,
  },
});
