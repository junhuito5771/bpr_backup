import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { widthPercentage } from '@module/utility';
import Text from '../Text';
import Colors from '../../constants/Colors';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginBottom: widthPercentage(2.7),
  },
  imageStyle: {
    width: widthPercentage(12),
    height: widthPercentage(12),
  },
  textStyle: {
    color: Colors.labelGrey,
    textAlign: 'center',
    width: widthPercentage(20),
  },
});

const IconButton = ({
  src,
  label,
  onPress,
  disabled,
  containerStyle,
  iconStyle,
}) => (
  <View style={[styles.container, containerStyle]}>
    <TouchableOpacity disabled={disabled} {...{ onPress }}>
      <Image
        source={src}
        style={[styles.imageStyle, { opacity: disabled ? 0 : 1 }, iconStyle]}
      />
    </TouchableOpacity>
    <Text mini style={styles.textStyle}>
      {label}
    </Text>
  </View>
);

IconButton.propTypes = {
  src: PropTypes.oneOfType([
    Image.propTypes.source.isRequired,
    PropTypes.string,
  ]),
  label: PropTypes.string,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  containerStyle: PropTypes.object,
  iconStyle: PropTypes.object,
};

export default IconButton;
