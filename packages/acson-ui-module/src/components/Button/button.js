import React from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';
import Colors from '../../constants/Colors';
import Text from '../Text';

const Button = ({
  children,
  raised,
  onPress,
  containerStyle,
  textStyle,
  disabled,
  isLoading,
  hasShadow,
  disabledRaised,
  shadowStyle,
  height,
  width,
  hasBorder,
}) => {
  const isDisabledMode = disabled || isLoading;
  const showShadow = hasShadow && raised && !isDisabledMode;

  return (
    <View
      style={StyleSheet.flatten([
        styles.container,
        hasBorder && styles.borderRed,
        raised && styles.backgroundRed,
        isDisabledMode && styles.disabledContainer,
        isDisabledMode && disabledRaised && styles.backgroundDisabledRaised,
        showShadow && styles.shadow,
        showShadow && shadowStyle,
        height && { height },
        height && { width },
        containerStyle,
      ])}>
      <TouchableOpacity
        onPress={onPress}
        disabled={isDisabledMode}
        style={styles.fullWidth}>
        <View style={StyleSheet.flatten([styles.buttonContainer])}>
          {isLoading ? (
            <ActivityIndicator
              size="small"
              color={disabledRaised ? Colors.white : Colors.grey}
            />
          ) : (
            <Text
              style={StyleSheet.flatten([
                styles.text,
                raised && styles.colorWhite,
                isDisabledMode && styles.colorGrey,
                isDisabledMode && disabledRaised && styles.colorWhite,
                textStyle,
              ])}
              numberOfLines={1}>
              {children}
            </Text>
          )}
        </View>
      </TouchableOpacity>
    </View>
  );
};

Button.propTypes = {
  raised: PropTypes.bool,
  children: PropTypes.string,
  onPress: PropTypes.func,
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  textStyle: PropTypes.object,
  disabled: PropTypes.bool,
  isLoading: PropTypes.bool,
  hasShadow: PropTypes.bool,
  shadowStyle: PropTypes.object,
  disabledRaised: PropTypes.bool,
  height: PropTypes.number,
  width: PropTypes.number,
  hasBorder: PropTypes.bool,
};

Button.defaultProps = {
  hasBorder: true,
};

export default Button;
