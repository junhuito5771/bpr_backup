import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Keyboard,
} from 'react-native';
import { widthPercentage } from '@module/utility';
import ModalService from '../Modal/modalService';

const styles = StyleSheet.create({
  button: {
    width: 50,
    alignItems: 'center',
    marginLeft: widthPercentage(2),
  },
  hitSlop: { top: 20, bottom: 20, left: 50, right: 50 },
});

const BackButton = ({ navigation, basicOnPress }) => {
  const handleOnPress = () => {
    if (basicOnPress) {
      basicOnPress();
    } else {
      const modalProps =
        navigation && navigation.getParam && navigation.getParam('modalProps');
      Keyboard.dismiss();
      if (modalProps) {
        ModalService.show({ ...modalProps });
      } else {
        navigation.goBack();
      }
    }
  };

  return (
    <View style={styles.button}>
      <TouchableOpacity onPress={handleOnPress} hitSlop={styles.hitSlop}>
        <Image source={require('../../shared/images/general/back.png')} />
      </TouchableOpacity>
    </View>
  );
};

BackButton.propTypes = {
  navigation: PropTypes.object,
  basicOnPress: PropTypes.func,
};

export default BackButton;
