import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'formik';

import Button from './button';

const FormikButton = ({ formik, disabled, ...extraProps }) => (
  <Button
    onPress={formik.handleSubmit}
    disabled={!formik.isValid || disabled}
    {...extraProps}
  />
);

FormikButton.propTypes = {
  formik: PropTypes.object,
  disabled: PropTypes.bool,
};

export default connect(FormikButton);
