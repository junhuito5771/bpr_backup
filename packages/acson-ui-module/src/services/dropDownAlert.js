let alert;

function setAlertRef(alertRef) {
  alert = alertRef;
}

function close() {
  alert.closeAction();
}

function show({ type, title, description }) {
  alert.alertWithType(type, title, description);
}

function showSuccessMsg({ title, description }) {
  alert.alertWithType('success', title, description);
}

function showErrorMsg({ title, description }) {
  alert.alertWithType('error', title, description);
}

export default {
  setAlertRef,
  show,
  showSuccessMsg,
  showErrorMsg,
  close,
};
