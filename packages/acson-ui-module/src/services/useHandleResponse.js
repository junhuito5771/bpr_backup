import { useEffect } from 'react';
import dropDownAlertService from './dropDownAlert';

export default ({
  successTitle = 'Success',
  success,
  errorTitle = 'Error',
  error,
  onSuccess,
  onError,
  onLeave,
  showSuccess,
  showError = true,
  clearStatus,
  info = '',
  onInfo,
}) => {
  useEffect(() => {
    if (success) {
      if (showSuccess) {
        dropDownAlertService.showSuccessMsg({
          title: successTitle,
          description: success,
        });
      }

      if (onSuccess) onSuccess();
    }
    if (error) {
      if (showError) {
        dropDownAlertService.showErrorMsg({
          title: errorTitle,
          description: error,
        });
      }

      if (onError) onError();
    }

    if (info) {
      dropDownAlertService.show({
        type: 'custom',
        title: 'Info',
        description: info,
      });

      if (onInfo) onInfo();
    }

    if (clearStatus && (success || error)) clearStatus();
  }, [success, error, info]);

  useEffect(
    () => () => {
      if (onLeave) onLeave();
    },
    []
  );
};
