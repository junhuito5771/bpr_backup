import React, { useEffect, useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Colors } from '@module/daikin-ui';

import LoginScreen from '../screens/Login/login';
import SignUpScreen from '../screens/SignUp/signUp';
import SignUpVerification from '../screens/SignUpVerification/signUpVerification';
import ResetPasswordScreen from '../screens/ResetPassword/resetPassword';
import ForgotPasswordScreen from '../screens/ForgotPassword/forgotPassword';
import PrivacyPolicyScreen from '../screens/PrivacyPolicy/privacyPolicy';

import {
  LOGIN_SCREEN,
  SIGNUP_SCREEN,
  SIGN_UP_VERIFICATION_SCREEN,
  RESET_PASSWORD_SCREEN,
  FORGOT_PASSWORD_SCREEN,
  PRIVACY_POLICY_SCREEN,
  BLUETOOTH_SCREEN,
  WIFI_SCREEN,
  OTHER_MORE_REMOTE_SCREEN,
} from '../shared/constants/routeNames';

import IntegratedModuleHandler from '../integratedModule';

let OtherModeScreen;
let BluetoothScreen;
let WifiScreen;

const AuthStack = createStackNavigator();

// eslint-disable-next-line react/prop-types
export default () => {
  const enableIoT = IntegratedModuleHandler.getIoTEnabled();
  const [, setIsLoaded] = useState(false);

  useEffect(() => {
    if (enableIoT) {
      const daikinIoTModule = require('@daikin-dama/daikin-iot');

      BluetoothScreen = daikinIoTModule.BluetoothModeScreen;
      WifiScreen = daikinIoTModule.WlanModeScreen;
      OtherModeScreen = daikinIoTModule.RemoteControlScreen;
      setIsLoaded(true);
    }
  }, [enableIoT]);

  return (
    <AuthStack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}>
      <AuthStack.Screen name={LOGIN_SCREEN} component={LoginScreen} />
      <AuthStack.Screen
        name={PRIVACY_POLICY_SCREEN}
        component={PrivacyPolicyScreen}
      />
      <AuthStack.Screen name={SIGNUP_SCREEN} component={SignUpScreen} />
      <AuthStack.Screen
        name={SIGN_UP_VERIFICATION_SCREEN}
        component={SignUpVerification}
      />
      <AuthStack.Screen
        name={RESET_PASSWORD_SCREEN}
        component={ResetPasswordScreen}
      />
      <AuthStack.Screen
        name={FORGOT_PASSWORD_SCREEN}
        component={ForgotPasswordScreen}
      />
      {BluetoothScreen !== undefined && (
        <AuthStack.Screen name={BLUETOOTH_SCREEN} component={BluetoothScreen} />
      )}
      {WifiScreen !== undefined && (
        <AuthStack.Screen name={WIFI_SCREEN} component={WifiScreen} />
      )}
      {OtherModeScreen !== undefined && (
        <AuthStack.Screen
          name={OTHER_MORE_REMOTE_SCREEN}
          component={OtherModeScreen}
        />
      )}
    </AuthStack.Navigator>
  );
};
