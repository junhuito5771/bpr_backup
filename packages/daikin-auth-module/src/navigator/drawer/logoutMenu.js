import React from 'react';
import { Alert, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { DrawerActions } from '@react-navigation/native';
import { DrawerItem } from '@react-navigation/drawer';
import { connect, userProfile } from '@module/redux-auth';

import { Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  row: {
    justifyContent: 'center',
    height: 42,
    color: Colors.black,
    width: '100%',
    borderRadius: 0,
    marginHorizontal: 0,
    marginVertical: 0,
  },
});

function logoutMenu({ logout, itemProps, navigation }) {
  const handleOnPress = () => {
    Alert.alert('Log out', 'Are you sure you want to logout?', [
      { text: 'Cancel' },
      {
        text: 'OK',
        onPress: () => {
          navigation.dispatch(DrawerActions.closeDrawer());
          logout();
        },
      },
    ]);
  };
  return (
    <DrawerItem {...itemProps} onPress={handleOnPress} style={styles.row} />
  );
}

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(userProfile.logOut()),
});

logoutMenu.propTypes = {};

export default connect(null, mapDispatchToProps)(logoutMenu);
