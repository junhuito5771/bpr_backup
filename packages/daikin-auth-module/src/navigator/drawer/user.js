import {
  LOGIN_SCREEN,
  LOGOUT_SCREEN,
  USER_PROFILE_SCREEN,
} from '../../shared/constants/routeNames';

import LoginStack from '../stack';
import UserProfileScreen from '../../screens/UserProfile/userProfile';
import LogoutMenu from './logoutMenu';

const Drawer = {
  [LOGIN_SCREEN]: {
    name: LOGIN_SCREEN,
    label: 'Login',
    component: LoginStack,
    icon: require('../../shared/assets/sidebar/login.png'),
  },
  [USER_PROFILE_SCREEN]: {
    name: USER_PROFILE_SCREEN,
    label: 'Profile',
    component: UserProfileScreen,
    icon: require('../../shared/assets/sidebar/profile.png'),
  },
  [LOGOUT_SCREEN]: {
    name: LOGOUT_SCREEN,
    label: 'Logout',
    customComponent: LogoutMenu,
    icon: require('../../shared/assets/sidebar/logout.png'),
  },
};

export default Drawer;
