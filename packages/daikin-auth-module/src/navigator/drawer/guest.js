import { LOGIN_SCREEN } from '../../shared/constants/routeNames';

import LoginStack from '../stack';

const Drawer = {
  [LOGIN_SCREEN]: {
    name: LOGIN_SCREEN,
    label: 'Login',
    component: LoginStack,
    icon: require('../../shared/assets/sidebar/login.png'),
  },
};

export default Drawer;
