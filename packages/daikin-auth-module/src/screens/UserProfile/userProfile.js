import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  StyleSheet,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  BackHandler,
} from 'react-native';
import { withFormik } from 'formik';
import {
  widthPercentage,
  heightPercentage,
  NavigationService,
  phoneNoPreValidator,
  generalInputPreValidator,
} from '@module/utility';
import { connect, userProfile } from '@module/redux-auth';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  FormikTextInput,
  Text,
  HeaderButton,
  NavDrawerMenu,
  Colors,
  HeaderText,
} from '@module/daikin-ui';

import userProfileValidator from './validator';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
  },
  flex: {
    flex: 1,
  },
  marginTop: {
    marginTop: 20,
  },

  textLabel: {
    marginBottom: 5,
    paddingLeft: 2,
  }
});

const renderNavigationOptions = (route, navigation) => {
  const { params } = route;

  if (params && params.saveButtonParams) {
    const { saveButtonParams } = params;
    return {
      title: <HeaderText>Profile</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
      headerRight: () => (
        <HeaderButton {...saveButtonParams}>Save</HeaderButton>
      ),
    };
  }

  return {};
};

const UserProfileForm = ({
  handleSubmit,
  isLoading,
  success,
  error,
  clearUserStatus,
  isValid,
  handleReset,
  route,
}) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(route, navigation)
    );
  }, [navigation]);

  useFocusEffect(React.useCallback(() => handleReset, []));

  useEffect(() => {
    if (success) {
      Alert.alert('Success', 'User profile update successfully');
      clearUserStatus();
    }
  }, [success, isLoading]);

  useEffect(() => clearUserStatus, []);

  useEffect(() => {
    navigation.setParams({
      isEditing: isValid,
      screen: 'user profile',
      saveButtonParams: {
        disabled: !isValid,
        isLoading,
        onPress: () => {
          Keyboard.dismiss();
          handleSubmit();
        },
      },
    });

    let backListener;

    if (isValid) {
      backListener = BackHandler.addEventListener('hardwareBackPress', () => {
        Alert.alert(
          'Are you sure?',
          'You have unsaved changes on your user profile. Do you want to leave before saving?',
          [
            {
              text: 'No',
            },
            {
              text: 'Yes',
              onPress: () => {
                navigation.goBack();
              },
            },
          ]
        );

        return true;
      });
    }

    return () => {
      if (backListener) backListener.remove();
      BackHandler.removeEventListener('hardwareBackPress', () => { });
      // navigation.setParams({});
    };
  }, [isValid, isLoading]);

  const inputOne = useRef(null);
  const inputTwo = useRef(null);

  return (
    <KeyboardAvoidingView
      behavior={Platform.select({
        ios: 'padding',
        default: null,
      })}
      style={styles.flex}
      keyboardVerticalOffset={Platform.select({
        ios: 70,
        default: 0,
      })}>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <SafeAreaView style={styles.flex}>
          <View style={styles.container}>
            <Text style={styles.textLabel}>Name</Text>
            <FormikTextInput
              name="name"
              placeholder="Full Name *"
              textContentType="name"
              keyboardType="default"
              returnKeyType="next"
              hasSpaced
              autoCapitalize="none"
              onSubmitEditing={() => inputTwo.current.focus()}
              validator={generalInputPreValidator}
              forwardRef={inputOne}
            />
            <Text style={styles.textLabel}>Email</Text>
            <FormikTextInput
              name="email"
              placeholder="Email *"
              textContentType="emailAddress"
              keyboardType="email-address"
              returnKeyType="next"
              hasSpaced
              autoCapitalize="none"
              disabled
            />
            <Text style={styles.textLabel}>Phone</Text>
            <FormikTextInput
              name="phoneNo"
              placeholder="Phone Number"
              textContentType="telephoneNumber"
              keyboardType="numeric"
              returnKeyType="next"
              hasSpaced
              autoCapitalize="none"
              forwardRef={inputTwo}
              maxLength={11}
              validator={phoneNoPreValidator}
            />
            {!!error && <Text error>{error}</Text>}
          </View>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

UserProfileForm.propTypes = {
  handleSubmit: PropTypes.func,
  clearUserStatus: PropTypes.func,
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  isValid: PropTypes.bool,
  route: PropTypes.object,
  handleReset: PropTypes.func,
};

const formikUserProfile = withFormik({
  enableReinitialize: true,
  validate: userProfileValidator,
  mapPropsToValues: ({ name, email, phoneNo }) => ({ name, email, phoneNo }),
  handleSubmit: (payload, bag) => {
    bag.props.submitUserParams(payload);
  },
})(UserProfileForm);

const mapStateToProps = ({
  user: {
    profile: { name, email, phoneNo },
    success,
    error,
    isLoading,
  },
}) => ({
  isLoading,
  success,
  error,
  name,
  email,
  phoneNo,
});

const mapDispatchToProps = dispatch => ({
  submitUserParams: userProfileParams => {
    dispatch(userProfile.submitUserParams(userProfileParams));
  },
  clearUserStatus: () => {
    dispatch(userProfile.clearUserStatus());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(formikUserProfile);
