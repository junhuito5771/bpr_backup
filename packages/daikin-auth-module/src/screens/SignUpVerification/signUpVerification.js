import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { connect, signUp } from '@module/redux-auth';
import {
  widthPercentage,
  heightPercentage,
  NavigationService,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  Button,
  Text,
  CountdownTimer,
  Colors,
  TextInput,
  HeaderText,
  HeaderBackButton,
} from '@module/daikin-ui';

import { HOME_SCREEN, LOGIN_SCREEN } from '../../shared/constants/routeNames';

const styles = StyleSheet.create({
  container: {
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(9),
  },
  wrapper: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
  },
  text: {
    color: Colors.lightWarmGrey,
    marginTop: 2,
    marginBottom: heightPercentage(9),
  },
  linkContainer: {
    marginTop: heightPercentage(3),
  },
  link: {
    fontSize: 12,
  },
  codeContainer: {
    marginTop: 30,
    justifyContent: 'space-between',
  },
  codeInput: {
    borderWidth: 1,
    borderRadius: 3,
    marginLeft: 0,
    marginRight: 0,
  },
  timer: {
    marginTop: 5,
    color: Colors.blue,
  },
  grey: {
    color: Colors.lightGrey,
  },
});

const Confirmation = ({
  email,
  submitConfirmParams,
  confirmIsLoading,
  confirmSuccess,
  confirmError,
  clearMessage,
  onResendCode,
  resendSuccess,
  resendError,
  clearResendCodeMsg,
  isResendLoading,
  route,
  clearConfirmMsg,
}) => {
  const [code, setCode] = React.useState('');

  const userPw = route.params && route.params.password;
  const isContinueWithLogin = route.params && route.params.continueWithLogin;

  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Verification</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  React.useEffect(() => {
    if (confirmSuccess) {
      Alert.alert('Success!', confirmSuccess, [
        {
          text: 'OK',
          onPress: () => {
            if (isContinueWithLogin) {
              NavigationService.navigate(HOME_SCREEN);
            } else {
              NavigationService.navigate(LOGIN_SCREEN);
            }
          },
        },
      ]);
    }
    return () => clearConfirmMsg();
  }, [confirmSuccess]);

  React.useEffect(() => clearMessage, []);

  const resendCode = () => {
    onResendCode();
  };

  return (
    <TouchableWithoutFeedback
      style={styles.wrapper}
      onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.center}>
          <Text regular>Verification code has been sent to</Text>
          <Text regular bold>
            {email || 'your email address'}
          </Text>
        </View>
        <TextInput
          autoFocus={false}
          keyboardType="numeric"
          maxLength={6}
          style={styles.codeContainer}
          onChangeText={value => setCode(value)}
        />
        <Text mini style={styles.text}>
          Enter the code to verify.
        </Text>
        <Button
          hasSpaced
          secondary
          disabled={code === '' || code.length < 6}
          onPress={() =>
            submitConfirmParams({ code, userPw, isContinueWithLogin })
          }
          isLoading={confirmIsLoading}>
          Confirm
        </Button>
        {!!confirmError && <Text error>{confirmError}</Text>}
        <CountdownTimer
          linkText="Resend verification code"
          onPress={resendCode}
          loading={isResendLoading}
          success={resendSuccess}
          error={resendError}
          postAction={clearResendCodeMsg}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

Confirmation.propTypes = {
  submitConfirmParams: PropTypes.func.isRequired,
  clearMessage: PropTypes.func.isRequired,
  clearConfirmMsg: PropTypes.func.isRequired,
  onResendCode: PropTypes.func.isRequired,
  clearResendCodeMsg: PropTypes.func.isRequired,
  route: PropTypes.objectOf(PropTypes.object).isRequired,
  email: PropTypes.string.isRequired,
  confirmIsLoading: PropTypes.bool.isRequired,
  confirmSuccess: PropTypes.string.isRequired,
  confirmError: PropTypes.string.isRequired,
  resendSuccess: PropTypes.string.isRequired,
  resendError: PropTypes.string.isRequired,
  isResendLoading: PropTypes.bool.isRequired,
};

const mapStateToProps = ({
  signUp: {
    confirmIsLoading,
    confirmSuccess,
    confirmError,
    signUpParams,
    resendSuccess,
    resendError,
    isResendLoading,
  },
}) => ({
  email: signUpParams.email,
  confirmIsLoading,
  confirmSuccess,
  confirmError,
  resendSuccess,
  resendError,
  isResendLoading,
});

const mapDispatchToProps = dispatch => ({
  submitConfirmParams: confirmParams => {
    dispatch(
      signUp.submitConfirmParams(confirmParams, NavigationService, LOGIN_SCREEN)
    );
  },
  clearMessage: () => {
    dispatch(signUp.clearConfirmErrorMsg());
  },
  clearResendCodeMsg: () => {
    dispatch(signUp.clearResendCodeMsg());
  },
  onResendCode: () => {
    dispatch(signUp.resendVerificationCode());
  },
  clearConfirmMsg: () => {
    dispatch(signUp.clearConfirmSuccessMsg());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Confirmation);
