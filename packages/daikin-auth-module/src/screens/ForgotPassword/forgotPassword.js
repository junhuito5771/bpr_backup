import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { Formik } from 'formik';
import PropTypes from 'prop-types';

import { connect, forgotPassword } from '@module/redux-auth';
import {
  widthPercentage,
  heightPercentage,
  NavigationService,
  passwordPreValidator,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  Text,
  FormikTextInput,
  HeaderText,
  FormikButton,
  Colors,
  HeaderBackButton,
} from '@module/daikin-ui';

import { RESET_PASSWORD_SCREEN } from '../../shared/constants/routeNames';
import forgotPasswordValidator from './validator';

const styles = StyleSheet.create({
  container: {
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
  },
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  text: {
    marginBottom: heightPercentage(10),
    color: Colors.lightWarmGrey,
  },
});

const ForgotPassword = ({
  isLoading,
  submitForgetPasswordParams,
  clearForgetPasswordStatus,
  error,
  success,
}) => {
  const [forgotPasswordParams, setForgotPasswordParams] = useState({});
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Forgot Password</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  useEffect(() => {
    if (success) {
      navigation.navigate(RESET_PASSWORD_SCREEN, {
        email: forgotPasswordParams,
      });
    }
  }, [success, error]);

  useEffect(() => clearForgetPasswordStatus, []);

  const handleForgetPasswordParams = email => {
    setForgotPasswordParams(email);
    submitForgetPasswordParams(email);
  };
  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <View style={[styles.flex, styles.container]}>
          <Formik
            initialValues={{ email: '' }}
            onSubmit={handleForgetPasswordParams}
            validate={forgotPasswordValidator}>
            <View style={styles.flex}>
              <FormikTextInput
                name="email"
                placeholder="Email"
                textContentType="emailAddress"
                keyboardType="email-address"
                returnKeyType="next"
                autoCapitalize="none"
                validator={passwordPreValidator}
                submitOnEditEnd
                hasSpaced
              />
              <Text style={styles.text} small>
                Don&apos;t worry. Resetting your password is easy, just tell us
                the email address you registered with GO DAIKIN.
              </Text>
              <FormikButton isLoading={isLoading}>Reset password</FormikButton>
              {!!error && <Text error>{error}</Text>}
            </View>
          </Formik>
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

ForgotPassword.propTypes = {
  submitForgetPasswordParams: PropTypes.func.isRequired,
  clearForgetPasswordStatus: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  success: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
};

const mapStateToProps = ({
  forgotPassword: { isLoading, success, error },
}) => ({
  isLoading,
  success,
  error,
});

const mapDispatchToProps = dispatch => ({
  submitForgetPasswordParams: forgotPasswordParams => {
    dispatch(forgotPassword.submitForgotPasswordParams(forgotPasswordParams));
  },
  clearForgetPasswordStatus: () => {
    dispatch(forgotPassword.clearForgotPasswordStatus());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
