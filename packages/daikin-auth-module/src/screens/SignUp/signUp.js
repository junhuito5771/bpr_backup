import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { Formik } from 'formik';
import {
  widthPercentage,
  heightPercentage,
  emailPreValidator,
  passwordPreValidator,
  phoneNoPreValidator,
  generalInputPreValidator,
  NavigationService,
} from '@module/utility';
import { connect, signUp } from '@module/redux-auth';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  FormikTextInput,
  FormikButton,
  Text,
  HeaderText,
  HeaderBackButton,
  Colors,
} from '@module/daikin-ui';

import { SIGN_UP_VERIFICATION_SCREEN } from '../../shared/constants/routeNames';
import signUpValidator from './validator';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
    justifyContent: 'space-between',
  },
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  marginTop: {
    marginTop: 20,
  },
});

const SignUp = ({
  submitSignUpParams,
  isLoading,
  success,
  error,
  clearMessage,
}) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Sign Up</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  useEffect(() => {
    if (success) {
      NavigationService.navigate(SIGN_UP_VERIFICATION_SCREEN);
    }
    return () => clearMessage();
  }, [success]);

  const inputTwo = useRef(null);
  const inputThree = useRef(null);
  const inputFour = useRef(null);
  const inputFive = useRef(null);

  return (
    <KeyboardAwareScrollView
      // eslint-disable-next-line react/jsx-boolean-value
      enableOnAndroid={true}
      extraScrollHeight={50}
      behavior={Platform.select({
        ios: 'padding',
        default: null,
      })}
      style={styles.flex}
      keyboardVerticalOffset={Platform.select({
        ios: 70,
        default: 0,
      })}>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <SafeAreaView style={styles.flex}>
          <ScrollView contentContainerStyle={styles.container}>
            <Formik
              initialValues={{
                email: '',
                password: '',
                passwordConfirmation: '',
                fullName: '',
                phoneNumber: '',
              }}
              onSubmit={submitSignUpParams}
              validate={signUpValidator}>
              <View>
                <FormikTextInput
                  name="email"
                  placeholder="Email *"
                  textContentType="emailAddress"
                  keyboardType="email-address"
                  returnKeyType="next"
                  hasSpaced
                  autoCapitalize="none"
                  onSubmitEditing={() => inputTwo.current.focus()}
                  validator={emailPreValidator}
                />
                <FormikTextInput
                  name="password"
                  placeholder="Password *"
                  textContentType="password"
                  returnKeyType="next"
                  hasSpaced
                  autoCapitalize="none"
                  forwardRef={inputTwo}
                  validator={passwordPreValidator}
                />
                <FormikTextInput
                  name="passwordConfirmation"
                  placeholder="Password Confirmation *"
                  textContentType="password"
                  returnKeyType="next"
                  hasSpaced
                  autoCapitalize="none"
                  onSubmitEditing={() => inputFour.current.focus()}
                  forwardRef={inputThree}
                  validator={passwordPreValidator}
                  validateOnChange
                />
                <FormikTextInput
                  name="fullName"
                  placeholder="Full Name *"
                  textContentType="name"
                  keyboardType="default"
                  returnKeyType="next"
                  hasSpaced
                  autoCapitalize="none"
                  onSubmitEditing={() => inputFive.current.focus()}
                  forwardRef={inputFour}
                  validator={generalInputPreValidator}
                />
                <FormikTextInput
                  name="phoneNumber"
                  placeholder=" Phone Number *"
                  textContentType="telephoneNumber"
                  keyboardType="numeric"
                  returnKeyType="next"
                  hasSpaced
                  autoCapitalize="none"
                  forwardRef={inputFive}
                  submitOnEditEnd
                  maxLength={11}
                  validator={phoneNoPreValidator}
                />
                <FormikButton
                  secondary
                  isLoading={isLoading}
                  style={styles.marginTop}>
                  Sign Up
                </FormikButton>
                {!!error && <Text error>{error}</Text>}
              </View>
            </Formik>
          </ScrollView>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAwareScrollView>
  );
};

SignUp.propTypes = {
  submitSignUpParams: PropTypes.func.isRequired,
  clearMessage: PropTypes.func.isRequired,

  isLoading: PropTypes.bool.isRequired,
  success: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
};

const mapStateToProps = ({ signUp: { isLoading, success, error } }) => ({
  isLoading,
  success,
  error,
});

const mapDispatchToProps = dispatch => ({
  submitSignUpParams: signUpParams => {
    dispatch(signUp.submitSignUpParams(signUpParams));
  },
  clearMessage: () => {
    dispatch(signUp.clearSignUpMsg());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
