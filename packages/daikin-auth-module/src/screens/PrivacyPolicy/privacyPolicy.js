import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Linking,
  TouchableOpacity,
  SafeAreaView,
  Alert,
} from 'react-native';
import {
  widthPercentage,
  heightPercentage,
  verticalScale,
  scale,
  NavigationService,
} from '@module/utility';
import WebView from 'react-native-webview';
import { connect } from '@module/redux-auth';
// eslint-disable-next-line import/no-unresolved
import { privacyPolicy } from '@module/redux-marketing';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  HeaderBackButton,
  HeaderText,
  Colors,
  Button,
  Checkbox,
} from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
    justifyContent: 'space-between',
  },
  content: {
    marginVertical: verticalScale(10),
    marginHorizontal: scale(10),
    borderWidth: 1,
    borderColor: Colors.blue,
  },
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  marginTop: {
    marginTop: 20,
  },
  button: {
    marginHorizontal: scale(10),
    marginVertical: verticalScale(16),
  },
  hyperlink: {
    color: Colors.blue,
    textDecorationLine: 'underline',
  },
  checkRow: {
    flexDirection: 'row',
    marginHorizontal: scale(10),
  },
  checkbox: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: verticalScale(5),
    marginHorizontal: scale(10),
    borderColor: Colors.sectionBorderGrey,
    borderRadius: 2,
    height: 20,
    width: 20,
    borderWidth: 1.5,
  },
  checkboxChecked: {
    backgroundColor: Colors.dayBlue,
    borderWidth: 0,
  },
  error: {
    paddingTop: verticalScale(30),
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

const ErrorMessage = () => (
  <View style={styles.error}>
    <Text>An error has occurred, please try again later</Text>
  </View>
);

const PrivacyPolicy = ({ init, content }) => {
  const navigation = useNavigation();
  const webviewRef = React.useRef(null);
  const [isEulaViewed, setIsEulaViewed] = React.useState(false);
  const [isEulaLoad, setIsEulaLoad] = React.useState(false);
  const [isAgreed, setIsAgreed] = React.useState(false);

  useFocusEffect(
    React.useCallback(() => {
      if (init) init();
    }, [])
  );

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Confirmation</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const onEndReached = ({ layoutMeasurement, contentOffset, contentSize }) =>
    isEulaLoad &&
    Math.round(layoutMeasurement.height + contentOffset.y, 2) >=
      Math.round(contentSize.height, 2);

  const handleEulaScroll = ({ nativeEvent }) => {
    if (onEndReached(nativeEvent)) {
      setIsEulaViewed(true);
    }
  };

  const handleAgreementPress = () => {
    setIsAgreed(!isAgreed);
  };

  const handleAlertConfirm = () => {
    Alert.alert(
      'Agreement Confirmation',
      content
        ? 'Please scroll down to privacy policy bottom and confirm the eula agreement to confirm the agreement'
        : 'Please confirm you have network connection in order to view the agreement'
    );
  };

  const handleConfirmAgreement = () => {
    navigation.replace('SignUp');
  };

  return (
    <SafeAreaView style={styles.flex}>
      <View style={[styles.flex, styles.content]}>
        {content && (
          <WebView
            ref={webviewRef}
            source={{ uri: content ? content.hyperlink : null }}
            onShouldStartLoadWithRequest={event => {
              if (event.url !== content.hyperlink) {
                webviewRef.current.stopLoading();
                Linking.openURL(event.url);
                return false;
              }
              return true;
            }}
            onScroll={handleEulaScroll}
            renderError={() => <ErrorMessage />}
            onLoadEnd={() => setIsEulaLoad(true)}
          />
        )}
      </View>
      <View style={styles.checkRow}>
        {/* <TouchableOpacity onPress={handleAgreementPress}>
          {isAgreed ? (
            <View style={[styles.checkbox, styles.checkboxChecked]}>
              <Image
                source={require('../../shared/assets/general/check.png')}
              />
            </View>
          ) : (
            <View style={styles.checkbox} />
          )}
        </TouchableOpacity> */}
        <Checkbox value={isAgreed} onPress={handleAgreementPress} />
        <Text>
          I have read and accept the &nbsp;
          <Text
            style={styles.hyperlink}
            onPress={() => Linking.openURL(content.hyperlink)}>
            End User License Agreement
          </Text>
          &nbsp;and&nbsp;
          <Text
            style={styles.hyperlink}
            onPress={() =>
              Linking.openURL('http://www.daikinmalaysia.com/privacy-policy')
            }>
            Privacy Policy
          </Text>
          &nbsp;
        </Text>
      </View>
      <TouchableOpacity
        onPress={(!isEulaViewed || !isAgreed) && handleAlertConfirm}>
        <Button
          onPress={handleConfirmAgreement}
          disabled={!isEulaViewed || !isAgreed}
          style={styles.button}>
          Sign Up
        </Button>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

PrivacyPolicy.propTypes = {
  init: PropTypes.func,
  content: PropTypes.object,
};

PrivacyPolicy.defaultProps = {
  init: () => {},
  content: {},
};

const mapStateToProps = ({ privacyPolicy: privacyPolicyState }) => ({
  content: privacyPolicyState.content,
});

const mapDispatchToProps = dispatch => ({
  init: () => {
    dispatch(privacyPolicy.init());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicy);
