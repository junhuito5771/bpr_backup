import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  SafeAreaView,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Linking,
} from 'react-native';
import { Formik } from 'formik';
import SplashScreen from 'react-native-splash-screen';
import {
  widthPercentage,
  heightPercentage,
  emailPreValidator,
  passwordPreValidator,
  NavigationService,
} from '@module/utility';
import { connect, login, signUp, userProfile } from '@module/redux-auth';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  Text,
  FormikTextInput,
  Link as LinkText,
  Button,
  FormikButton,
  FormikCheckbox,
  HeaderText,
  Colors,
  NavDrawerMenu,
} from '@module/daikin-ui';

import loginValidator from './validator';
import {
  FORGOT_PASSWORD_SCREEN,
  SIGN_UP_VERIFICATION_SCREEN,
  HOME_SCREEN,
  PRIVACY_POLICY_SCREEN,
} from '../../shared/constants/routeNames';

import { LoginModeControl } from '../../integratedModule';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
    justifyContent: 'space-between',
  },
  button: {
    borderWidth: 1,
    borderColor: Colors.lightCoolGrey,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  topSection: {
    flex: 0.9,
  },
  bottomSection: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 15,
  },
  videoEN: {
    paddingTop: heightPercentage(8),
  },
  videoBM: {
    paddingTop: heightPercentage(2),
  },
  videoButton: {
    height: 40,
    alignItems: 'center',
  },
});

const Login = ({
  submitLoginParams,
  isLoading,
  clearStatus,
  profileEmail,
  clearErrorCode,
  errorCode,
  resendVerification,
  error,
  success,
}) => {
  const inputTwo = useRef(null);
  const formRef = useRef();
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Log In</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
    });
  }, [navigation]);

  useEffect(() => {
    SplashScreen.hide();

    return clearStatus;
  }, []);

  useEffect(() => {
    if (errorCode === 'UserNotConfirmedException') {
      clearStatus();
      Alert.alert(
        'Account not verified!',
        "We've sent you a new verification code, please verify your account before logging in.",
        [
          {
            text: 'OK',
            onPress: () => {
              resendVerification();
              navigation.navigate(SIGN_UP_VERIFICATION_SCREEN, {
                password: formRef.current.state.values.password,
                continueWithLogin: true,
              });
            },
          },
        ]
      );
    }
    return () => clearErrorCode();
  }, [errorCode]);

  useEffect(() => {
    if (success) {
      navigation.navigate(HOME_SCREEN);
      clearStatus();
    }
  }, [success]);

  const handleSignUpPress = () => {
    NavigationService.navigate(PRIVACY_POLICY_SCREEN);
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <View style={styles.container}>
          <Formik
            ref={formRef}
            enableReinitialize
            initialValues={{
              email: profileEmail,
              password: '',
              isRemember: true,
            }}
            onSubmit={submitLoginParams}
            validate={loginValidator}>
            <View style={styles.topSection}>
              <FormikTextInput
                name="email"
                placeholder="Email"
                textContentType="emailAddress"
                keyboardType="email-address"
                returnKeyType="next"
                hasSpaced
                autoCapitalize="none"
                validator={emailPreValidator}
                onSubmitEditing={() => inputTwo.current.focus()}
              />
              <FormikTextInput
                name="password"
                placeholder="Password"
                textContentType="password"
                returnKeyType="send"
                autoCapitalize="none"
                hasSpaced
                validator={passwordPreValidator}
                submitOnEditEnd
                forwardRef={inputTwo}
              />
              <View style={styles.row}>
                <FormikCheckbox name="isRemember">Remember me</FormikCheckbox>
                <LinkText
                  onPress={() => navigation.navigate(FORGOT_PASSWORD_SCREEN)}>
                  Forgot password
                </LinkText>
              </View>
              <FormikButton hasSpaced isLoading={isLoading}>
                Log in
              </FormikButton>
              <Button primary onPress={handleSignUpPress}>
                Sign up
              </Button>
              {!!error && <Text error>{error}</Text>}

              <View style={styles.videoEN}>
                <Text>How to Setup GO DAIKIN</Text>
                <Button
                  style={styles.videoButton}
                  onPress={() => {
                    Linking.openURL(
                      'https://www.youtube.com/watch?v=R9cKEFpPXUo'
                    );
                  }}>
                  Click to watch video
                </Button>
                <Text style={styles.videoBM}>Cara Pemasangan GO DAIKIN</Text>
                <Button
                  style={styles.videoButton}
                  onPress={() => {
                    Linking.openURL(
                      'https://www.youtube.com/watch?v=p9UPMgaXl4M'
                    );
                  }}>
                  Klik untuk menonton video
                </Button>
              </View>
            </View>
          </Formik>
          {!!LoginModeControl && <LoginModeControl navigation={navigation} />}
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

Login.propTypes = {
  submitLoginParams: PropTypes.func.isRequired,
  clearStatus: PropTypes.func.isRequired,
  clearErrorCode: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  errorCode: PropTypes.string.isRequired,
  resendVerification: PropTypes.func.isRequired,
  profileEmail: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
  success: PropTypes.string.isRequired,
  // bleSuccess: PropTypes.string.isRequired,
  // bleError: PropTypes.string.isRequired,
  // isScanning: PropTypes.bool.isRequired,
  // setBleError: PropTypes.func.isRequired,
  // discoverWLANUnits: PropTypes.func.isRequired,
  // wlanSuccess: PropTypes.string.isRequired,
  // wlanError: PropTypes.string.isRequired,
  // isWLANScanning: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = ({
  login: { isLoading, errorCode, error, success },
  signUp: { confirmSuccess },
  user: {
    profile: { email },
  },
  // bleMode: { success: bleSuccess, error: bleError, isLoading: isScanning },
  // wlanMode: {
  //   success: wlanSuccess,
  //   error: wlanError,
  //   isLoading: isWLANScanning,
  // },
}) => ({
  error,
  success,
  errorCode,
  isLoading,
  confirmSuccess,
  profileEmail: email,
  // bleSuccess,
  // bleError,
  // isScanning,
  // wlanSuccess,
  // wlanError,
  // isWLANScanning,
});

const mapDispatchToProps = dispatch => ({
  submitLoginParams: loginParams => {
    dispatch(login.submitLoginParams(loginParams));
  },
  clearStatus: () => {
    dispatch(login.clearLoginStatus());
    dispatch(userProfile.clearUserStatus());
    // dispatch(bleMode.clearBleStatus());
    // dispatch(wlanMode.clearStatus());
  },
  resendVerification: () => dispatch(signUp.resendVerificationCode()),
  clearErrorCode: () => dispatch(login.clearErrorCode()),
  // setBleError: message => {
  //   dispatch(
  //     bleMode.setBleStatus({
  //       messageType: 'error',
  //       message,
  //     })
  //   );
  // },
  // discoverWLANUnits: () => {
  //   dispatch(
  //     wlanMode.discoverUnits({
  //       setBroadcast: Platform.select({
  //         ios: true,
  //         android: false,
  //       }),
  //     })
  //   );
  // },
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

// export default withBleManager({
//   maxScanTimeOut: 10,
//   canDuplicate: false,
// })(connectedLogin);
