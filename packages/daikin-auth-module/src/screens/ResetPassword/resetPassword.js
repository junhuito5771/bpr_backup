import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { Formik } from 'formik';
import { connect, resetPassword, forgotPassword } from '@module/redux-auth';
import {
  widthPercentage,
  heightPercentage,
  passwordPreValidator,
  NavigationService,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  FormikButton,
  Text,
  FormikTextInput,
  CountdownTimer,
  Colors,
  HeaderText,
  HeaderBackButton,
} from '@module/daikin-ui';

import { LOGIN_SCREEN } from '../../shared/constants/routeNames';
import resetPasswordValidator from './validator';

const styles = StyleSheet.create({
  container: {
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(9),
  },
  center: {
    alignItems: 'center',
  },
  text: {
    color: Colors.lightWarmGrey,
    marginBottom: heightPercentage(5),
  },
  linkContainer: {
    marginTop: heightPercentage(3),
  },
  link: {
    fontSize: 12,
  },
  codeContainer: {
    marginTop: 30,
  },
  codeInput: {
    borderWidth: 1,
    borderRadius: 3,
    marginLeft: 8,
    marginRight: 8,
  },
  timer: {
    marginTop: 5,
    color: Colors.blue,
  },
  grey: {
    color: Colors.lightGrey,
  },
});

const ResetPassword = ({
  route,
  submitResetPasswordParams,
  clearResetPasswordStatus,
  submitForgetPasswordParams,
  isLoading,
  success,
  error,
  isResendLoading,
  resendSuccess,
  resendError,
  clearForgetPasswordStatus,
}) => {
  const { email } = route.params.email;
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Reset Password</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  React.useEffect(() => {
    if (success) {
      Alert.alert('Success', 'Password reset successfully');
      NavigationService.navigate(LOGIN_SCREEN);
      clearResetPasswordStatus();
    }
    clearResetPasswordStatus();
  }, [success]);

  const resendVerication = () => {
    submitForgetPasswordParams({ email });
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.center}>
          <Text regular>Verification code has been sent to</Text>
          <Text regular bold>
            {email || 'your email address'}
          </Text>
        </View>
        <Formik
          initialValues={{
            email,
            password: '',
            passwordConfirmation: '',
            code: '',
          }}
          onSubmit={submitResetPasswordParams}
          validate={resetPasswordValidator}>
          <View>
            <FormikTextInput
              name="code"
              maxLength={6}
              autoFocus={false}
              keyboardType="numeric"
              style={styles.codeContainer}
            />
            <Text mini style={styles.text}>
              Enter the code to verify.
            </Text>
            <FormikTextInput
              name="password"
              placeholder="Password"
              textContentType="password"
              returnKeyType="next"
              autoCapitalize="none"
              hasSpaced
              validator={passwordPreValidator}
            />
            <FormikTextInput
              name="passwordConfirmation"
              placeholder="Confirm password"
              textContentType="password"
              returnKeyType="send"
              autoCapitalize="none"
              hasSpaced
              validator={passwordPreValidator}
              validateOnChange
            />
            <FormikButton hasSpaced secondary isLoading={isLoading}>
              Confirm
            </FormikButton>
            {!!error && (
              <Text mini error>
                {error}
              </Text>
            )}
          </View>
        </Formik>
        <CountdownTimer
          linkText="Resend verification code"
          onPress={resendVerication}
          loading={isResendLoading}
          success={resendSuccess}
          error={resendError}
          postAction={clearForgetPasswordStatus}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

ResetPassword.propTypes = {
  route: PropTypes.object,
  submitResetPasswordParams: PropTypes.func,
  clearResetPasswordStatus: PropTypes.func,
  submitForgetPasswordParams: PropTypes.func,
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  isResendLoading: PropTypes.bool,
  resendSuccess: PropTypes.string,
  resendError: PropTypes.string,
  clearForgetPasswordStatus: PropTypes.func,
};

const mapStateToProps = ({
  resetPassword: { isLoading, success, error },
  forgotPassword: {
    isLoading: isResendLoading,
    success: resendSuccess,
    error: resendError,
  },
}) => ({
  isLoading,
  success,
  error,
  isResendLoading,
  resendSuccess,
  resendError,
});

const mapDispatchToProps = dispatch => ({
  submitResetPasswordParams: resetPasswordParams => {
    dispatch(resetPassword.submitResetPasswordParams(resetPasswordParams));
  },
  submitForgetPasswordParams: forgotPasswordParams => {
    dispatch(forgotPassword.submitForgotPasswordParams(forgotPasswordParams));
  },
  clearResetPasswordStatus: () => {
    dispatch(resetPassword.clearResetPasswordStatus());
  },
  clearForgetPasswordStatus: () => {
    dispatch(forgotPassword.clearForgotPasswordStatus());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
