export const USERAGREEMENT_SCREEN = 'UserAgreement';
export const LOGIN_SCREEN = 'Login';
export const SIGNUP_SCREEN = 'SignUp';
export const SIGN_UP_VERIFICATION_SCREEN = 'SignUpVerification';
export const FORGOT_PASSWORD_SCREEN = 'ForgotPassword';
export const RESET_PASSWORD_SCREEN = 'ResetPassword';
export const LOGOUT_SCREEN = 'Logout';
export const HOME_SCREEN = 'Home';
export const PRIVACY_POLICY_SCREEN = 'PrivacyPolicy';
export const USER_PROFILE_SCREEN = 'UserProfile';

export const WIFI_SCREEN = 'Wifi';
export const BLUETOOTH_SCREEN = 'Bluetooth';
export const OTHER_MORE_REMOTE_SCREEN = 'OtherModeRemote';
