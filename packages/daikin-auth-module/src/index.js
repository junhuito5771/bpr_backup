import { configuration } from '@module/redux-auth';

export { default as LoginScreen } from './screens/Login/login';
export { default as SignUpScreen } from './screens/SignUp/signUp';
export { default as SignUpVerification } from './screens/SignUpVerification/signUpVerification';
export { default as ResetPasswordScreen } from './screens/ResetPassword/resetPassword';
export { default as ForgotPasswordScreen } from './screens/ForgotPassword/forgotPassword';

export * from './navigator';
export * from '@module/redux-auth';

export { default as IntegratedModule } from './integratedModule';

function init(config) {
  configuration.init({
    apiEndpoint: config.API_URL,
    cognitoEndpoint: config.COGNITO_URL,
    cognitoClientId: config.COGNITO_CLIENT_ID,
    cognitoIdentifyPoolId: config.COGNITO_POOL_ID,
    cognitoUserPoolID: config.COGNITO_USER_POOL_ID,
    region: config.REGION,
    pinpointId: config.PINPOINT_ID,
    pinpointRegion: config.PINPOINT_REGION,
  });
}

export { init };
