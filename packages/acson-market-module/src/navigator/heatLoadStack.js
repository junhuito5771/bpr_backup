import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import RecommendProduct from '../screens/heatLoadCalculator/recommendProduct';
import ProductDetails from '../screens/heatLoadCalculator/productDetails';
import AddWindow from '../screens/heatLoadCalculator/addWindow';
import HeatLoadCalculatorScreen from '../screens/heatLoadCalculator/heatLoadCalculator';
import headerOptions from './headerStyle';

import {
  HEAT_LOAD_CALCULATOR_SCREEN,
  ADD_WINDOW_SCREEN,
  RECOMMEND_PRODUCT_SCREEN,
  PRODUCT_DETAILS_SCREEN,
} from '../constants/routeNames';

const HeatloadStack = createStackNavigator();

export default () => (
  <HeatloadStack.Navigator screenOptions={headerOptions}>
    <HeatloadStack.Screen
      name={HEAT_LOAD_CALCULATOR_SCREEN}
      component={HeatLoadCalculatorScreen}
    />
    <HeatloadStack.Screen
      name={ADD_WINDOW_SCREEN}
      component={AddWindow}
    />
    <HeatloadStack.Screen
      name={RECOMMEND_PRODUCT_SCREEN}
      component={RecommendProduct}
    />
    <HeatloadStack.Screen
      name={PRODUCT_DETAILS_SCREEN}
      component={ProductDetails}
    />
  </HeatloadStack.Navigator>
);