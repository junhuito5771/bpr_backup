import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import { ScaleText, heightPercentage } from '@module/utility';
import { Colors, BackButton, ModalService } from '@module/acson-ui';


const headerOptions = ({ navigation }) => ({
  headerStyle: {
    height: heightPercentage(10),
    elevation: 5,
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
    },
  },
  headerTitleStyle: {
    fontFamily: 'Avenir-Black',
    color: Colors.blueGrey,
    fontWeight: '700',
    fontSize: ScaleText(18),
  },
  headerTitleAllowFontScaling: false,
  headerTitleContainerStyle: {
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardStyle: {
    backgroundColor: 'white',
  },
  headerTitleAlign: 'center',
  headerShown: true,
  title: 'Heat Load Calculator',
  headerLeft: () =>
    navigation.canGoBack() && <BackButton navigation={navigation} />,
  headerRight: () => (
    <TouchableOpacity
      onPress={() => {
        ModalService.show({
          title: 'Disclaimer',
          desc:
            'This calculator is provided for a quick and approximate check of heat loads. It should not be considered as a replacement for expert advice or a precise and detailed heat load calculation. It is advised to reconfirm with your air conditioner contractors again before you place your order. We accept no responsibility or liability resulting or howsoever arising from its use.',
          buttons: [{ title: 'Ok' }],
        });
      }}
      style={{ marginRight: 13 }}>
      <Image source={require('../shared/images/info.png')} />
    </TouchableOpacity>
  )
});

export default headerOptions;
