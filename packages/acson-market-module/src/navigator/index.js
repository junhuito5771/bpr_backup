import React from 'react';
// import { createStackNavigator, TransitionSpecs } from 'react-navigation-stack';

import { BackButton } from '@module/acson-ui';

// import AuthStack from 'iot/navigator/auth';
import PromotionScreen from '../screens/promotionScreen';
import WebviewScreen from '../screens/webViewScreen';
import MarketingHomeScreen from '../screens/marketingHome/marketingHomeScreen';
import HeatLoadCalculatorStack from './heatLoadStack';

import {
  HOME_MARKETING,
  PROMOTION_SCREEN,
  WEBVIEW_SCREEN,
  HEAT_LOAD_CALCULATOR_SCREEN,
} from '../constants/routeNames';

const MarketingStack = {
  [HOME_MARKETING]: {
    name: HOME_MARKETING,
    component: MarketingHomeScreen,
    options: () => ({
      title: 'Home',
    }),
  },
  [PROMOTION_SCREEN]: {
    name: PROMOTION_SCREEN,
    component: PromotionScreen,
    options: () => ({
      title: 'Promotion',
    }),
  },
  [WEBVIEW_SCREEN]: {
    name: WEBVIEW_SCREEN,
    component: WebviewScreen,
    options: ({ route, navigation }) => {
      let title = route && route.params && route.params.title;

      if (title && title.length > 20) {
        title = `${title.slice(0, 20)}...`;
      }
      return {
        headerLeft: () => <BackButton navigation={navigation} />,
        title,
      };
    },
  },
  [HEAT_LOAD_CALCULATOR_SCREEN]: {
    name: HEAT_LOAD_CALCULATOR_SCREEN,
    component: HeatLoadCalculatorStack,
    options: () => ({
      headerShown: false,
    }),
  },
};

/*
const MarketingStack = createStackNavigator(
  {
    HomeMarketing: {
      screen: MarketingHomeStack,
      navigationOptions: () => ({
        headerShown: false,
      }),
    },
    Auth: {
      screen: AuthStack,
      navigationOptions: ({ navigation }) => ({
        headerShown: navigation.state.routes.length === 1,
        title: '',
        headerLeft: () => <BackButton navigation={navigation} />,
      }),
    },
    WebviewScreen: {
      screen: WebviewScreen,
    },
    HeatLoadCalculator: {
      screen: HeatLoadCalculatorStack,
      navigationOptions: ({ navigation }) => ({
        headerShown: navigation.state.routes.length === 1,
        title: 'Heat Load Calculator',
        headerLeft: () => <BackButton navigation={navigation} />,
        headerRight: () => (
          <TouchableOpacity
            onPress={() => {
              ModalService.show({
                title: 'Disclaimer',
                desc:
                  'This calculator is provided for a quick and approximate check of heat loads. It should not be considered as a replacement for expert advice or a precise and detailed heat load calculation. It is advised to reconfirm with your air conditioner contractors again before you place your order. We accept no responsibility or liability resulting or howsoever arising from its use.',
                buttons: [{ title: 'Ok' }],
              });
            }}
            style={{ marginRight: 13 }}>
            <Image source={require('marketing/shared/images/info.png')} />
          </TouchableOpacity>
        ),
      }),
    },
    Promotion: {
      screen: PromotionScreen,
    },
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        height: heightPercentage(10),
        elevation: 5,
        shadowOpacity: 0.2,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
        },
      },
      headerTitleStyle: {
        fontFamily: 'Avenir-Black',
        color: Colors.blueGrey,
        fontWeight: '700',
        fontSize: ScaleText(17),
      },
      headerTitleAllowFontScaling: false,
      headerTitleContainerStyle: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
      },
      cardStyle: {
        backgroundColor: 'white',
      },
      headerTitleAlign: 'center',
      transitionSpec: {
        open: Platform.select({
          ios: TransitionSpecs.TransitionIOSSpec,
          android: TransitionSpecs.FadeInFromBottomAndroidSpec,
        }),
        close: Platform.select({
          ios: TransitionSpecs.TransitionIOSSpec,
          android: TransitionSpecs.FadeOutToBottomAndroidSpec,
        }),
      },
    },
    headerMode: 'float',
  }
);
*/
export default MarketingStack;
