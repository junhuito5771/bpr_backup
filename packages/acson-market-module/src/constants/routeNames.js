export const HOME_MARKETING = 'HomeMarketing';
export const WEBVIEW_SCREEN = 'WebviewScreen';
export const LOCATE_DEALER = 'LocateDealer';
export const HEAT_LOAD_CALCULATOR_SCREEN = 'HeatLoadCalculator';
export const ADD_WINDOW_SCREEN = 'AddWindow';
export const RECOMMEND_PRODUCT_SCREEN = 'RecommendProduct';
export const PRODUCT_DETAILS_SCREEN = 'ProductDetails';
export const PROMOTION_SCREEN = 'Promotion';

export const HOME_SCREEN = 'Home';
export const LOGIN_SCREEN = 'Login';
export const USAGE_LIST_SCREEN = 'UsageList';

export const SERVICE_BOOKING = 'ServiceBooking';
export const SERVICE_LISTING = 'ServiceListing';
