import React from 'react';
import PropTypes from 'prop-types';
import { connect } from '@module/redux-marketing';
import { CacheImage, BackButton, Colors, Text } from '@module/acson-ui';

import {
  View,
  Image,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  ScaleText,
  scale,
  verticalScale,
  NavigationService,
} from '@module/utility';

import { WEBVIEW_SCREEN } from '../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    height: '100%',
    paddingVertical: verticalScale(10),
    overflow: 'visible',
  },
  cardContainer: {
    backgroundColor: Colors.white,
    marginHorizontal: scale(20),
    marginVertical: verticalScale(6),
    borderRadius: scale(4),
  },
  cardFooter: {
    flexDirection: 'row',
    alignItems: 'center',
    height: verticalScale(40),
  },
  image: {
    borderTopLeftRadius: scale(4),
    borderTopRightRadius: scale(4),
    height: verticalScale(150),
    resizeMode: 'stretch',
  },
  label: {
    fontSize: ScaleText(12),
    marginStart: scale(20),
    flexGrow: 0.9,
  },
  shadow: {
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 2,
  },
});

const PromotionScreen = ({ promotions }) => {
  const renderItem = item => (
    <View style={[styles.cardContainer, styles.shadow]}>
      <Image style={styles.image} source={{ uri: item.item.imageLink }} />
      <TouchableOpacity
        onPress={() => {
          NavigationService.navigate(WEBVIEW_SCREEN, {
            title: item.item.title,
            url: item.item.hyperlink,
          });
        }}>
        <View style={styles.cardFooter}>
          <Text style={styles.label}>Find Out More</Text>
          <CacheImage
            source={require('../shared/images/promotion/right_arrow.png')}
          />
        </View>
      </TouchableOpacity>
    </View>
  );

  return (
    <View>
      <FlatList data={promotions} renderItem={item => renderItem(item)} />
    </View>
  );
};

PromotionScreen.navigationOptions = ({ navigation }) => ({
  headerLeft: () => <BackButton navigation={navigation} />,
});

PromotionScreen.propTypes = {
  promotions: PropTypes.array,
};

const mapStateToProps = ({ marketingHome: marketingHomeState }) => ({
  promotions: marketingHomeState.promotions,
});

export default connect(mapStateToProps, null)(PromotionScreen);
