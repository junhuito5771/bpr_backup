import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TextInput,
  StyleSheet,
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Keyboard,
  Alert,
  Image,
} from 'react-native';
import { connect, heatLoadCalculator } from '@module/redux-marketing';

import {
  NavigationService,
  decimalValidator,
  integerValidator,
} from '@module/utility';

import { Button, Text } from '@module/acson-ui';
import {
  RECOMMEND_PRODUCT_SCREEN,
  ADD_WINDOW_SCREEN,
} from '../../constants/routeNames';
import styles from './styles';

const HeatLoadCalculatorScreen = ({
  room,
  windows,
  setRooms,
  setWindows,
  initialize,
  noOfPeople,
  calculateBTU,
  setNoOfPeople,
  setMeasurementScale,
}) => {
  const [isFeet, setIsFeet] = useState(1);
  const [extraData, setExtraData] = useState(0);

  useEffect(() => {
    initialize('Acson');
  }, []);

  const handleMesurementBtn = value => {
    setIsFeet(1 - value);
    setMeasurementScale(1 - value);
  };

  const onChangeText = (value, type) => {
    switch (type) {
      case 'roomWidth':
        setRooms({ width: decimalValidator(value), length: room.length });
        break;
      case 'roomLength':
        setRooms({ width: room.width, length: decimalValidator(value) });
        break;
      default:
        setNoOfPeople(integerValidator(value));
        break;
    }
  };

  const handleCalculate = () => {
    const allEmpty = !room.width || !room.length || !noOfPeople;
    if (allEmpty) {
      Alert.alert('Error', 'Please insert all input');
    } else {
      setWindows(windows);
      calculateBTU();
      NavigationService.navigate(RECOMMEND_PRODUCT_SCREEN);
    }
  };

  const renderItem = (item, index) => (
    <TouchableWithoutFeedback>
      <View
        style={[styles.rowContainer, styles.marginTop10, styles.windowBorder]}>
        <View style={styles.columnContainer}>
          <Text>
            {item.length} x {item.height}
          </Text>
          <Text>{item.direction ? 'West / East' : 'North / South'}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            Alert.alert(
              '',
              'Are you sure want to delete ?',
              [
                {
                  text: 'Cancel',
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => {
                    windows.splice(index, 1);
                    setExtraData(extraData + 1);
                  },
                },
              ],
              { cancelable: false }
            );
          }}>
          <Image source={require('../../shared/images/delete.png')} />
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );

  return (
    <TouchableWithoutFeedback
      onPress={() => Keyboard.dismiss()}
    >
      <View style={styles.container}>
        <View style={styles.margin20}>
          <Text bold>Heat Load Calculator</Text>

          <View style={styles.marginTop20}>
            <Text>Measurement Scale*</Text>
          </View>
          <View style={[styles.rowContainer, styles.marginTop10, styles.rowSpace]}>
            <TouchableWithoutFeedback
              onPress={() => {
                handleMesurementBtn(isFeet);
              }}
              disabled={isFeet}>
              <View
                style={StyleSheet.flatten([
                  styles.measurementBtn,
                  styles.passiveMeasurementBtn,
                  isFeet && styles.activeMeasurementBtn,
                ])}>
                <Text
                  style={StyleSheet.flatten([
                    isFeet && styles.measurementText,
                  ])}>
                  Feet
                </Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                handleMesurementBtn(isFeet);
              }}
              disabled={!isFeet}>
              <View
                style={StyleSheet.flatten([
                  styles.measurementBtn,
                  styles.passiveMeasurementBtn,
                  !isFeet && styles.activeMeasurementBtn,
                ])}>
                <Text
                  style={StyleSheet.flatten([
                    !isFeet && styles.measurementText,
                  ])}>
                  Meter
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <View style={[styles.rowContainer, styles.marginTop20]}>
            <View style={styles.columnContainer}>
              <Text>Room Width*</Text>
              <View
                style={styles.inputContainer}>
                <TextInput
                  style={styles.textInput}
                  value={room.width === 0 ? '' : `${room.width}`}
                  onChangeText={text => onChangeText(text, 'roomWidth')}
                  keyboardType="numeric"
                />
              </View>
            </View>
            <View style={styles.xLabel}>
              <Text bold>x</Text>
            </View>
            <View style={styles.columnContainer}>
              <Text>Room Length*</Text>
              <View
                style={styles.inputContainer}>
                <TextInput
                  style={styles.textInput}
                  value={room.length === 0 ? '' : `${room.length}`}
                  onChangeText={text => onChangeText(text, 'roomLength')}
                  keyboardType="numeric"
                />
              </View>
            </View>
          </View>

          <View style={styles.marginTop20}>
            <Text>No Of People*</Text>
            <View
              style={StyleSheet.flatten([
                styles.inputContainer,
                styles.fullWidth,
              ])}>
              <TextInput
                style={styles.textInput}
                value={noOfPeople === 0 ? '' : `${noOfPeople}`}
                onChangeText={text => onChangeText(text)}
                keyboardType="numeric"
              />
            </View>
          </View>
        </View>

        <View style={styles.margin20}>
          <TouchableWithoutFeedback
            onPress={() => {
              NavigationService.navigate(ADD_WINDOW_SCREEN);
            }}>
            <View style={styles.addWindowContainer}>
              <Text medium style={styles.titleLabel}>
                Add Window
              </Text>
              <Image source={require('../../shared/images/add.png')} />
            </View>
          </TouchableWithoutFeedback>

          <FlatList
            data={windows}
            extraData={extraData}
            renderItem={({ item, index }) => renderItem(item, index)}
            keyExtractor={(item, index) => item + index}
            style={styles.flatListHeight}
          />
        </View>

        <View style={[styles.margin20, styles.bottomBtn]}>
          <Button raised onPress={() => handleCalculate()}>
            Calculate
          </Button>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const mapStateToProps = ({ heatLoadCalculator: heatLoadCalculatorState }) => {
  const { room, noOfPeople, windows } = heatLoadCalculatorState;
  return {
    room,
    noOfPeople,
    windows,
  };
};

const mapDispatchToProps = dispatch => ({
  initialize: appType => {
    dispatch(heatLoadCalculator.init(appType));
  },
  setMeasurementScale: measurementScale => {
    dispatch(heatLoadCalculator.setMeasurementScale(measurementScale));
  },
  setRooms: room => {
    dispatch(heatLoadCalculator.setRooms(room));
  },
  setWindows: windows => {
    dispatch(heatLoadCalculator.setWindows(windows));
  },
  setNoOfPeople: noOfPeople => {
    dispatch(heatLoadCalculator.setNoOfPeople(noOfPeople));
  },
  calculateBTU: () => {
    dispatch(heatLoadCalculator.calculateBTU());
  },
});

HeatLoadCalculatorScreen.propTypes = {
  setRooms: PropTypes.func,
  initialize: PropTypes.func,
  setWindows: PropTypes.func,
  setNoOfPeople: PropTypes.func,
  calculateBTU: PropTypes.func,
  setMeasurementScale: PropTypes.func,
  room: PropTypes.object,
  noOfPeople: PropTypes.number,
  windows: PropTypes.array,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeatLoadCalculatorScreen);
