import { StyleSheet } from 'react-native';
import {
  scale,
  verticalScale,
  ScaleText,
  viewportWidth
} from '@module/utility';
import { Colors } from '@module/acson-ui';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  margin20: {
    margin: 20,
  },
  padding20: {
    padding: 20,
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  measurementBtn: {
    borderBottomWidth: 2,
    width: scale(153),
    height: verticalScale(40),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    borderColor: Colors.stoneGrey,
  },
  measurementBtnInSheet: {
    borderBottomWidth: 2,
    width: scale(160),
    height: verticalScale(40),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    borderColor: Colors.stoneGrey,
  },
  inputContainer: {
    borderBottomColor: Colors.stoneGrey,
    borderBottomWidth: 2,
    height: scale(35),
  },
  textInput: {
    textAlignVertical: 'bottom',
    flex: 1,
    padding: 0,
    paddingBottom: 5,
  },
  fullContainer: {
    height: verticalScale(35),
  },
  activeMeasurementBtn: {
    borderColor: Colors.cerulean,
    borderWidth: 2,
  },
  passiveMeasurementBtn: {
    borderWidth: 2,
  },
  measurementText: {
    color: Colors.black,
  },
  columnContainer: {
    flex: 1,
  },
  xLabel: {
    marginTop: verticalScale(10),
    marginHorizontal: 15,
  },
  leftMargin: {
    marginLeft: 15,
  },
  fullWidth: {
    width: scale(310),
  },
  window: {
    backgroundColor: Colors.weeklyTimerGroupHeaderGrey,
    height: verticalScale(25),
    justifyContent: 'center',
    paddingLeft: 10,
  },
  bottomBtn: {
    position: 'absolute',
    bottom: 15,
    alignSelf: 'center',
    width: scale(310),
  },
  marginTop10: {
    marginTop: 10,
  },
  marginTop20: {
    marginTop: 20,
  },
  productTypeWrap: {
    borderColor: Colors.stoneGrey,
    minWidth: scale(80),
    maxWidth: scale(150),
    borderWidth: 1,
    borderRadius: 22,
    padding: 8,
    paddingHorizontal: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  hitSlop: {
    top: 20,
    bottom: 20,
    left: 20,
    right: 20,
  },
  flatlistContainer: {
    height: verticalScale(450),
    width: scale(325),
  },
  flatListHeight: {
    height: verticalScale(110),
  },
  popupContainer: {
    width: scale(120),
  },
  productContainer: {
    height: verticalScale(150),
    width: scale(300),
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: Colors.stoneGrey,
    borderRadius: 12,
    marginBottom: verticalScale(20),
  },
  productImage: {
    height: verticalScale(100),
    width: scale(200),
  },
  addWindowContainer: {
    backgroundColor: Colors.lightGrey,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 30,
    padding: 10,
    borderRadius: 22,
  },
  renderNoItem: {
    marginTop: verticalScale(150),
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleTotalBTU: {
    fontSize: ScaleText(14),
    fontWeight: '700',
  },
  titleLabel: {
    color: Colors.titleGrey,
  },
  totalBTULabel: {
    fontSize: ScaleText(16),
    color: Colors.titleGrey,
  },
  windowDirectionLabel: {
    fontSize: ScaleText(17),
  },
  windowBorder: {
    borderWidth: 1,
    padding: 5,
    paddingHorizontal: 20,
    borderRadius: 18,
    borderColor: Colors.stoneGrey,
  },
  windowBottomWrap: {
    height: verticalScale(95),
    paddingBottom: 13,
  },
  bottomSheetWrap: {
    width: viewportWidth,
    marginTop: 5,
    paddingHorizontal: 10,
  },
  flexRow: {
    flexDirection: 'row',
  },
  selectContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: Colors.stoneGrey,
    paddingVertical: 10,
    paddingRight: 10,
  },
  productLabel: {
    textAlign: 'center',
  },
});
