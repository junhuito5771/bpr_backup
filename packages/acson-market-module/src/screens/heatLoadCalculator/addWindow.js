import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect, heatLoadCalculator } from '@module/redux-marketing';

import {
  View,
  Alert,
  Image,
  Keyboard,
  TextInput,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import { scale, decimalValidator } from '@module/utility';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';

import { Colors, Text, Button } from '@module/acson-ui';

import styles from './styles';

const triggerStyles = {
  OptionTouchableComponent: TouchableWithoutFeedback,
  triggerWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: Colors.stoneGrey,
    paddingVertical: 10,
    paddingRight: 10,
  },
  triggerTouchable: {
    hitSlop: { top: 30, bottom: 30, left: 30, right: 30 },
  },
};

const optionsStyles = {
  optionsContainer: {
    width: scale(320),
    padding: 5,
  },
};

const AddWindow = ({ windows, setWindows, navigation }) => {
  const { Popover } = renderers;
  const [isNorthSouth, setIsNorthSouth] = useState(null);
  const [windowHeight, setWindowHeight] = useState(0);
  const [windowLength, setWindowLength] = useState(0);

  const onChangeText = (value, type) => {
    if (type === 'windowLength') setWindowLength(decimalValidator(value));
    else {
      setWindowHeight(decimalValidator(value));
    }
  };

  const handleOnConfirm = () => {
    if (windowHeight && windowLength) {
      setWindows([
        ...windows,
        {
          length: windowHeight,
          height: windowLength,
          direction: isNorthSouth,
        },
      ]);
      setWindowHeight(0);
      setWindowLength(0);
      navigation.goBack();
    } else Alert.alert('Error', 'Input is not valid');
  };

  const renderWindowLocation = () => {
    if (isNorthSouth === null) return 'Select Window Location';

    return isNorthSouth ? 'East / West' : 'North / South';
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.container}>
        <View style={styles.padding20}>
          <Text>Window Direction</Text>
          <Menu
            renderer={Popover}
            rendererProps={{
              placement: 'bottom',
            }}>
            <MenuTrigger customStyles={triggerStyles}>
              <Text style={styles.windowDirectionLabel}>
                {renderWindowLocation()}
              </Text>
              <Image source={require('../../shared/images/dropdown.png')} />
            </MenuTrigger>
            <MenuOptions customStyles={optionsStyles}>
              <MenuOption
                onSelect={() => setIsNorthSouth(false)}
                text="North / South"
              />
              <MenuOption
                onSelect={() => setIsNorthSouth(true)}
                text="East / West"
              />
            </MenuOptions>
          </Menu>
        </View>
        <View style={[styles.rowContainer, styles.padding20]}>
          <View style={styles.columnContainer}>
            <Text>Window Height*</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.textInput}
                value={windowHeight === 0 ? '' : `${windowHeight}`}
                onChangeText={text => onChangeText(text)}
                keyboardType="numeric"
              />
            </View>
          </View>
          <View style={styles.xLabel}>
            <Text bold>x</Text>
          </View>
          <View style={styles.columnContainer}>
            <Text>Window Length*</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.textInput}
                value={windowLength === 0 ? '' : `${windowLength}`}
                onChangeText={text =>
                  onChangeText(text, 'windowLength')
                }
                keyboardType="numeric"
              />
            </View>
          </View>
        </View>
        <View style={[styles.margin20, styles.bottomBtn]}>
          <Button
            raised
            disabled={isNorthSouth === null || !windowLength || !windowHeight}
            onPress={() => handleOnConfirm()}>
            Add
          </Button>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const mapStateToProps = ({ heatLoadCalculator: heatLoadCalculatorState }) => {
  const { room, noOfPeople, windows } = heatLoadCalculatorState;
  return {
    room,
    noOfPeople,
    windows,
  };
};

const mapDispatchToProps = dispatch => ({
  setWindows: windows => {
    dispatch(heatLoadCalculator.setWindows(windows));
  },
});

AddWindow.propTypes = {
  windows: PropTypes.array,
  setWindows: PropTypes.func,
  navigation: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddWindow);
