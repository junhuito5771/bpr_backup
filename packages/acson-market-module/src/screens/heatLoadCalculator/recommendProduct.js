import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { heatLoadCalculator, connect } from '@module/redux-marketing';

import {
  View,
  Alert,
  Image,
  FlatList,
  Platform,
  TextInput,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  viewportWidth,
  scale,
  NavigationService,
  decimalValidator,
  integerValidator,
} from '@module/utility';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';

import {
  CacheImage,
  Colors,
  Text,
  Button,
  BackButton,
  BottomSheetGeneral,
} from '@module/acson-ui';

import { PRODUCT_DETAILS_SCREEN } from '../../constants/routeNames';
import styles from './styles';

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const triggerStyles = {
  OptionTouchableComponent: TouchableWithoutFeedback,
  triggerWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 2,
    borderColor: Colors.stoneGrey,
    paddingVertical: 10,
    paddingRight: 10,
  },
  triggerTouchable: {
    hitSlop: { top: 30, bottom: 30, left: 30, right: 30 },
  },
};

const optionsStyles = {
  optionsContainer: {
    width: scale(320),
    padding: 5,
  },
};

const RecommendProduct = ({
  room,
  windows,
  setRooms,
  totalBTU,
  setWindows,
  allProduct,
  noOfPeople,
  calculateBTU,
  setNoOfPeople,
  measurementScale,
  setMeasurementScale,
}) => {
  const [selectProductType, setSelectProductType] = useState('All');
  const [allFilteredProduct, setAllFilteredProduct] = useState([]);
  const [wallMountedProduct, setWallMountedProduct] = useState([]);
  const [floorStandingProduct, setFloorStandingProduct] = useState([]);
  const [ceilingConcealedProduct, setCeilingConcealedProduct] = useState([]);
  const [isNorthSouth, setIsNorthSouth] = useState(null);
  const [isFeet, setIsFeet] = useState(measurementScale);
  const [currentPage, setCurrentPage] = React.useState(0);
  const [extraData, setExtraData] = useState(0);
  const [windowHeight, setWindowHeight] = useState(0);
  const [windowLength, setWindowLength] = useState(0);
  const [dropBottomSheet, setdropBottomSheet] = useState(false);

  const scrollRef = useRef(null);
  const { Popover } = renderers;

  function splitProduct(data) {
    let wallMountedList = [];
    let floorStandingList = [];
    let ceilingConcealedList = [];

    data.forEach(value => {
      if (value.Category === 'Wall Mounted') {
        wallMountedList.push(value);
      } else if (value.Category === 'Floor Standing') {
        floorStandingList.push(value);
      } else if (value.Category === 'Ceiling Concealed') {
        ceilingConcealedList.push(value);
      }
    });

    wallMountedList = wallMountedList.filter(
      k =>
        Number(k.RatedCapacity) >= totalBTU &&
        Number(k.RatedCapacity) <= totalBTU + 5000
    );

    floorStandingList = floorStandingList.filter(
      k =>
        Number(k.RatedCapacity) >= totalBTU &&
        Number(k.RatedCapacity) <= totalBTU + 5000
    );

    ceilingConcealedList = ceilingConcealedList.filter(
      k =>
        Number(k.RatedCapacity) >= totalBTU &&
        Number(k.RatedCapacity) <= totalBTU + 5000
    );

    setWallMountedProduct(wallMountedList);
    setFloorStandingProduct(floorStandingList);
    setCeilingConcealedProduct(ceilingConcealedList);

    const filteredProduct = [].concat(
      wallMountedList,
      floorStandingList,
      ceilingConcealedList
    );
    setAllFilteredProduct(filteredProduct);
  }

  useEffect(() => {
    splitProduct(allProduct);
  }, [totalBTU]);

  const handleMesurementBtn = value => {
    setIsFeet(1 - value);
    setMeasurementScale(1 - value);
  };

  const renderNoItem = () => (
    <View style={styles.renderNoItem}>
      <Text>Sorry, there are no products matching your search. </Text>
    </View>
  );

  const renderItem = item => (
    <TouchableOpacity
      style={styles.productContainer}
      onPress={() => {
        NavigationService.navigate(PRODUCT_DETAILS_SCREEN, {
          productUrl: item.ProductUrlLink,
        });
      }}>
      <CacheImage
        source={{ uri: item.ProductImageLink }}
        style={styles.productImage}
        resizeMode="contain"
      />
      <Text style={styles.productLabel} medium>{item.Series}</Text>
    </TouchableOpacity>
  );

  const UpperRender = () => (
    <View style={styles.center}>
      <Text style={styles.titleTotalBTU}>Recommended Cooling Capacity</Text>
      <Text style={styles.totalBTULabel}>
        {numberWithCommas(totalBTU)} BTU/HR
      </Text>
    </View>
  );

  const renderProduct = () => {
    switch (selectProductType) {
      case 'Wall Mounted':
        return wallMountedProduct;
      case 'Floor Standing':
        return floorStandingProduct;
      case 'Ceiling Concealed':
        return ceilingConcealedProduct;
      default:
        return allFilteredProduct;
    }
  };

  const renderWindowLocation = () => {
    if (isNorthSouth === null) return 'Select Window Location';

    return isNorthSouth ? 'East / West' : 'North / South';
  };

  const handleScroll = event => {
    setCurrentPage(event.nativeEvent.contentOffset.x);
    if (
      event.nativeEvent.contentOffset.x > viewportWidth - 2.5 &&
      Platform.OS === 'android'
    ) {
      scrollRef.current.scrollToEnd();
    }
  };

  const onChangeText = (value, type) => {
    switch (type) {
      case 'roomWidth':
        setRooms({ width: decimalValidator(value), length: room.length });
        break;
      case 'roomLength':
        setRooms({ width: room.width, length: decimalValidator(value) });
        break;
      case 'windowHeight':
        setWindowHeight(decimalValidator(value));
        break;
      case 'windowLength':
        setWindowLength(decimalValidator(value));
        break;
      default:
        setNoOfPeople(integerValidator(value));
        break;
    }
  };

  const handleCalculate = () => {
    const allEmpty = !room.width || !room.length || !noOfPeople;
    if (allEmpty) {
      Alert.alert('Error', 'Please insert all input');
    } else {
      setdropBottomSheet(false);
      calculateBTU();
    }
  };

  const renderWindowItem = (item, index) => (
    <TouchableWithoutFeedback>
      <View
        style={[styles.rowContainer, styles.marginTop10, styles.windowBorder]}>
        <View style={styles.columnContainer}>
          <Text>
            {item.length} x {item.height}
          </Text>
          <Text>{item.direction ? 'West / East' : 'North / South'}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            Alert.alert(
              '',
              'Are you sure want to delete ?',
              [
                {
                  text: 'Cancel',
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => {
                    windows.splice(index, 1);
                    setExtraData(extraData + 1);
                  },
                },
              ],
              { cancelable: false }
            );
          }}>
          <Image source={require('../../shared/images/delete.png')} />
        </TouchableOpacity>
      </View>
    </TouchableWithoutFeedback>
  );

  const handleOnConfirm = () => {
    if (windowHeight && windowLength && isNorthSouth !== null) {
      setWindows([
        ...windows,
        {
          length: windowHeight,
          height: windowLength,
          direction: isNorthSouth,
        },
      ]);
      setWindowHeight(0);
      setWindowLength(0);
      setIsNorthSouth(null);
      scrollRef.current.scrollTo({ x: 0 });
    } else Alert.alert('Error', 'Input is not valid');
  };

  return (
    <>
      <View style={styles.margin20}>
        <View style={styles.rowContainer}>
          <Text> Recommended Product </Text>
          <Menu renderer={Popover} rendererProps={{ placement: 'bottom' }}>
            <MenuTrigger
              customStyles={{ triggerWrapper: styles.productTypeWrap }}>
              <Text>{selectProductType} </Text>
              <Image source={require('../../shared/images/dropdown.png')} />
            </MenuTrigger>
            <MenuOptions style={styles.popupContainer}>
              <MenuOption
                onSelect={() => setSelectProductType('All')}
                text="All"
              />
              <MenuOption
                onSelect={() => setSelectProductType('Wall Mounted')}
                text="Wall Mounted"
              />
              <MenuOption
                onSelect={() => setSelectProductType('Floor Standing')}
                text="Floor Standing"
              />
              <MenuOption
                onSelect={() => setSelectProductType('Ceiling Concealed')}
                text="Ceiling Concealed"
              />
            </MenuOptions>
          </Menu>
        </View>

        <View style={styles.marginTop20}>
          <FlatList
            data={renderProduct()}
            extraData={totalBTU}
            renderItem={({ item }) => renderItem(item)}
            ListEmptyComponent={renderNoItem}
            keyExtractor={item => item.Oid}
            style={styles.flatlistContainer}
          />
        </View>
      </View>

      <BottomSheetGeneral
        visibleProps={{ state: dropBottomSheet, setState: setdropBottomSheet }}
        upperContainer={currentPage === 0 && <UpperRender />}>
        <ScrollView
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          ref={scrollRef}
          onScroll={e => handleScroll(e)}
          scrollEnabled={false}>
          <View
            style={[{ backgroundColor: 'transparent' }, styles.bottomSheetWrap]}
          >
            <Text bold>Heat Load Calculator</Text>
            <Text>Measurement Scale*</Text>
            <View style={styles.rowContainer}>
              <TouchableWithoutFeedback
                onPress={() => {
                  handleMesurementBtn(isFeet);
                }}
                disabled={isFeet}>
                <View
                  style={StyleSheet.flatten([
                    styles.measurementBtnInSheet,
                    styles.passiveMeasurementBtn,
                    isFeet && styles.activeMeasurementBtn,
                  ])}>
                  <Text
                    style={StyleSheet.flatten([
                      isFeet && styles.measurementText,
                    ])}>
                    Feet
                  </Text>
                </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback
                onPress={() => {
                  handleMesurementBtn(isFeet);
                }}
                disabled={!isFeet}>
                <View
                  style={StyleSheet.flatten([
                    styles.measurementBtnInSheet,
                    styles.passiveMeasurementBtn,
                    !isFeet && styles.activeMeasurementBtn,
                  ])}>
                  <Text
                    style={StyleSheet.flatten([
                      !isFeet && styles.measurementText,
                    ])}>
                    Meter
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>

            <View style={styles.rowContainer}>
              <View style={styles.columnContainer}>
                <Text>Room Width*</Text>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.textInput}
                    value={`${room.width}`}
                    onChangeText={text => onChangeText(text, 'roomWidth')}
                    keyboardType="numeric"
                  />
                </View>
              </View>
              <View style={styles.xLabel}>
                <Text bold>x</Text>
              </View>
              <View style={styles.columnContainer}>
                <Text>Room Length*</Text>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.textInput}
                    value={`${room.length}`}
                    onChangeText={text => onChangeText(text, 'roomLength')}
                    keyboardType="numeric"
                  />
                </View>
              </View>
            </View>

            <View style={styles.marginTop10}>
              <Text>No of people*</Text>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.textInput}
                  value={`${noOfPeople}`}
                  onChangeText={text => onChangeText(text)}
                  keyboardType="numeric"
                />
              </View>
            </View>
            <View style={styles.margin20}>
              <TouchableWithoutFeedback
                onPress={() => {
                  scrollRef.current.scrollToEnd();
                }}>
                <View style={styles.addWindowContainer}>
                  <Text medium style={{ color: Colors.titleGrey }}>
                    Add Window
                  </Text>
                  <Image source={require('../../shared/images/add.png')} />
                </View>
              </TouchableWithoutFeedback>
            </View>
            <View style={styles.windowBottomWrap}>
              <FlatList
                data={windows}
                extraData={extraData}
                renderItem={({ item, index }) => renderWindowItem(item, index)}
                keyExtractor={(item, index) => item + index}
              />
            </View>

            <View style={styles.bottomBtn}>
              <Button raised onPress={() => handleCalculate()}>
                Calculate
              </Button>
            </View>
          </View>
          <View style={styles.bottomSheetWrap}>
            <View style={styles.flexRow}>
              <BackButton
                basicOnPress={() => scrollRef.current.scrollTo({ x: 0 })}
              />
              <Text bold>Add Window</Text>
            </View>
            <View style={styles.padding20}>
              <Text>Window Direction</Text>
              <Menu
                renderer={Popover}
                rendererProps={{
                  placement: 'bottom',
                }}>
                <MenuTrigger customStyles={triggerStyles}>
                  <Text style={styles.windowDirectionLabel}>
                    {renderWindowLocation()}
                  </Text>
                  <Image source={require('../../shared/images/dropdown.png')} />
                </MenuTrigger>
                <MenuOptions customStyles={optionsStyles}>
                  <MenuOption
                    onSelect={() => setIsNorthSouth(false)}
                    text="North / South"
                  />
                  <MenuOption
                    onSelect={() => setIsNorthSouth(true)}
                    text="East / West"
                  />
                </MenuOptions>
              </Menu>
            </View>
            <View style={[styles.rowContainer, styles.padding20]}>
              <View style={styles.columnContainer}>
                <Text>Window Height*</Text>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.textInput}
                    value={windowHeight === 0 ? '' : `${windowHeight}`}
                    onChangeText={text => onChangeText(text, 'windowHeight')}
                    keyboardType="numeric"
                  />
                </View>
              </View>
              <View style={styles.xLabel}>
                <Text bold>x</Text>
              </View>
              <View style={styles.columnContainer}>
                <Text>Window Length*</Text>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.textInput}
                    value={windowLength === 0 ? '' : `${windowLength}`}
                    onChangeText={text => onChangeText(text, 'windowLength')}
                    keyboardType="numeric"
                  />
                </View>
              </View>
            </View>
            <View style={[styles.margin20, styles.bottomBtn]}>
              <Button
                raised
                disabled={
                  isNorthSouth === null || !windowLength || !windowHeight
                }
                onPress={() => handleOnConfirm()}>
                Add
              </Button>
            </View>
          </View>
        </ScrollView>
      </BottomSheetGeneral>
    </>
  );
};

const mapStateToProps = ({ heatLoadCalculator: heatLoadCalculatorState }) => {
  const {
    room,
    noOfPeople,
    totalBTU,
    windows,
    measurementScale,
    allProduct,
  } = heatLoadCalculatorState;
  return {
    room,
    noOfPeople,
    totalBTU,
    windows,
    measurementScale,
    allProduct,
  };
};

const mapDispatchToProps = dispatch => ({
  setMeasurementScale: measurementScale => {
    dispatch(heatLoadCalculator.setMeasurementScale(measurementScale));
  },
  setRooms: room => {
    dispatch(heatLoadCalculator.setRooms(room));
  },
  setNoOfPeople: noOfPeople => {
    dispatch(heatLoadCalculator.setNoOfPeople(noOfPeople));
  },
  calculateBTU: () => {
    dispatch(heatLoadCalculator.calculateBTU());
  },
  setWindows: windows => {
    dispatch(heatLoadCalculator.setWindows(windows));
  },
});

RecommendProduct.propTypes = {
  setMeasurementScale: PropTypes.func,
  setRooms: PropTypes.func,
  setNoOfPeople: PropTypes.func,
  setWindows: PropTypes.func,
  calculateBTU: PropTypes.func,
  room: PropTypes.object,
  noOfPeople: PropTypes.number,
  totalBTU: PropTypes.number,
  measurementScale: PropTypes.number,
  allProduct: PropTypes.array,
  windows: PropTypes.array,
};

export default connect(mapStateToProps, mapDispatchToProps)(RecommendProduct);
