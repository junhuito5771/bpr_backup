import React from 'react';
import PropTypes from 'prop-types';

import {
  connect,
  heatLoadCalculator,
  marketingHome,
  setConfigure,
} from '@module/redux-marketing';
import { useIsFocused } from '@react-navigation/native';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import Modal from 'react-native-modal';

import {
  Colors,
  Text,
  ProductBottomSheet,
  MainCarousel,
} from '@module/acson-ui';
import LinearGradient from 'react-native-linear-gradient';
import {
  scale,
  ScaleText,
  verticalScale,
  widthPercentage,
  viewportWidth,
  NavigationService,
  LocationHelper,
} from '@module/utility';
import WebView from 'react-native-webview';
import Config from 'react-native-config';
import {
  PERMISSIONS,
  RESULTS,
  request,
  check,
  openSettings,
} from 'react-native-permissions';

import {
  HOME_SCREEN,
  LOGIN_SCREEN,
  USAGE_LIST_SCREEN,
  WEBVIEW_SCREEN,
  HEAT_LOAD_CALCULATOR_SCREEN,
  PROMOTION_SCREEN,
  SERVICE_BOOKING,
  SERVICE_LISTING,
} from '../../constants/routeNames';
import productItems from './constants/products';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  containerWrap: {
    flex: 1,
    paddingBottom: verticalScale(100),
  },
  row: {
    flexDirection: 'row',
  },
  backButton: {
    width: 50,
    alignItems: 'center',
    marginLeft: widthPercentage(2),
  },
  hitSlop: { top: 20, bottom: 20, left: 50, right: 50 },
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  container: {
    paddingVertical: verticalScale(10),
  },
  optionContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  moreOptionContainer: {
    paddingTop: verticalScale(50),
    paddingBottom: verticalScale(40),
    paddingHorizontal: scale(10),
    flexDirection: 'row',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: Colors.white,
  },
  carouselContainer: {
    marginVertical: verticalScale(10),
    alignItems: 'center',
    overflow: 'visible',
  },
  scrollContainer: {
    marginBottom: verticalScale(90),
  },
  webContainer: {
    height: '100%',
  },
  loadingCenter: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  background: {
    position: 'absolute',
    width: '100%',
    resizeMode: 'stretch',
  },
  title: {
    marginVertical: verticalScale(10),
    marginHorizontal: scale(10),
    fontWeight: '600',
    fontSize: ScaleText(18),
    color: Colors.lightRed,
  },
  iconBox: {
    backgroundColor: Colors.white,
    minHeight: verticalScale(100),
    width: viewportWidth / 4,
    marginVertical: verticalScale(10),
    marginHorizontal: scale(10),
    paddingVertical: verticalScale(10),
    paddingHorizontal: scale(10),
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  margin: {
    alignSelf: 'center',
    marginVertical: verticalScale(5),
  },
  optionLabel: {
    fontSize: ScaleText(10),
    fontWeight: '700',
    textAlign: 'center',
  },
  shadow: {
    elevation: 3,
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
  },
  subtitleRow: {
    width: scale(310), // same width as carousel
  },
  subtitle: {
    flex: 1,
    color: Colors.white,
    marginVertical: verticalScale(5),
    marginHorizontal: scale(5),
  },
  right: {
    justifyContent: 'flex-end',
  },
});

const MarketingHomeScreen = ({
  isLogin,
  navigation,
  clearStatusHeatLoadCalculator,
  init,
  promotions,
}) => {
  const isFocused = useIsFocused();
  const [modalOpen, setModalOpen] = React.useState(false);
  const [productParam, setProductParam] = React.useState('');

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: productParam ? 'Product' : 'Home',
      headerLeft:
        productParam &&
        (() => (
          <TouchableOpacity
            style={styles.backButton}
            hitSlop={styles.hitSlop}
            onPress={() => setProductParam('')}>
            <Image source={require('../../shared/images/back.png')} />
          </TouchableOpacity>
        )),
    });
  }, [productParam]);

  React.useEffect(() => {
    SplashScreen.hide();
    setConfigure({
      bucketUrl: Config.BUCKET_URL,
      bucketEndpoint: Config.BUCKET_ENDPOINT,
    });
    init('Acson');
  }, []);

  React.useEffect(() => {
    if (isFocused) clearStatusHeatLoadCalculator();
  }, [isFocused]);

  // React.useEffect(() => {
  //   if (isLogin) NavigationService.navigate(HOME_MARKETING);
  // }, [isLogin]);

  const handleOnIotNavigate = React.useCallback(
    routeName => {
      NavigationService.navigate(isLogin ? routeName : LOGIN_SCREEN);
    },
    [isLogin]
  );

  const checkLocationPermission = async () => {
    const checkPermission = Platform.select({
      ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
      android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    });

    const checkPermissionStatus = await check(checkPermission);
    const locationReqNeeded = checkPermissionStatus !== RESULTS.GRANTED;
    const permissionStatus = await request(checkPermission);

    const isLocationGranted =
      !locationReqNeeded || permissionStatus === RESULTS.GRANTED;

    Platform.select({
      ios: () => {
        if (!isLocationGranted) {
          Alert.alert(
            `No Location Access`,
            "To locate dealer, change permission in your device's app settings to allow Acson app access to Location.",
            [
              { text: 'Cancel', style: 'cancel' },
              { text: 'Settings', onPress: openSettings },
            ]
          );
        }
      },
      android: async () => {
        if (isLocationGranted) {
          await LocationHelper.enableLocationIfNeeded();
        }
      },
    })();
  };

  const moreMenuOptions = React.useMemo(
    () => [
      {
        icon: (
          <Image
            source={require('../../shared/images/home/About.png')}
            style={{ height: 27, width: 29 }}
          />
        ),
        label: 'About Us',
        onPress: () => {
          NavigationService.navigate(WEBVIEW_SCREEN, {
            title: 'About Us',
            url: `${Config.ACSON_URL}/about-us`,
          });
        },
      },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/Location.png')}
            style={{ height: 30, width: 30 }}
          />
        ),
        label: 'Locate Dealer',
        onPress: () => {
          checkLocationPermission();
          NavigationService.navigate(WEBVIEW_SCREEN, {
            title: 'Locate Dealer',
            url: `${Config.ACSON_URL}/find/dealers`,
          });
        },
      },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/Help.png')}
            style={{ tintColor: 'rgb(255, 0, 42)', height: 25, width: 25 }}
          />
        ),
        label: 'Help',
        onPress: () => {
          NavigationService.navigate(WEBVIEW_SCREEN, {
            title: 'Help',
            url:
              'http://doc.aircontrol.acson.com.my/CMS/XPOCMS_AcsonPromotionLink/24/Content.html',
          });
        },
      },
    ],
    []
  );

  const renderLoading = React.useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.grey} />
      </View>
    ),
    []
  );

  const handleOnPress = React.useCallback(
    params => {
      setProductParam(params === productParam ? '' : params);
    },
    [productParam]
  );

  const menuOptions = React.useMemo(
    () => [
      {
        icon: (
          <Image
            source={require('../../shared/images/home/RemoteControl.png')}
            style={{ height: 40, width: 40 }}
          />
        ),
        label: 'Remote Control',
        onPress: () => handleOnIotNavigate(HOME_SCREEN),
      },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/Usage.png')}
            style={{ height: 40, width: 40 }}
          />
        ),
        label: 'Energy Consumption Usage',
        onPress: () => handleOnIotNavigate(USAGE_LIST_SCREEN),
      },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/Calculator.png')}
            style={{ height: 40, width: 40 }}
          />
        ),
        label: 'Heat Load Calculator',
        onPress: () => {
          NavigationService.navigate(HEAT_LOAD_CALCULATOR_SCREEN);
        },
      },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/Service.png')}
            style={{ height: 27, width: 29 }}
          />
        ),
        label: 'Service / Installation Request',
        onPress: () => {
          NavigationService.navigate(isLogin ? SERVICE_BOOKING : LOGIN_SCREEN);
        },
        disabled: false,
      },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/Booking.png')}
            style={{ height: 30, width: 30 }}
          />
        ),
        label: 'My Booking',
        onPress: () => {
          NavigationService.navigate(isLogin ? SERVICE_LISTING : LOGIN_SCREEN);
        },
        disabled: false,
      },
      // {
      //  icon: <Image source={require('../../shared/images/home/Status.png')} />,
      //  label: 'E-Warranty',
      //  onPress: () => {},
      //  disabled: true,
      // },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/ContactUs.png')}
            style={{ height: 40, width: 40 }}
          />
        ),
        label: 'Contact Us',
        onPress: () => {
          NavigationService.navigate(WEBVIEW_SCREEN, {
            title: 'Contact Us',
            url: `${Config.ACSON_URL}/contactus`,
          });
        },
      },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/Facebook.png')}
            style={{ height: 27, width: 29 }}
          />
        ),
        label: 'Facebook',
        onPress: () => {
          NavigationService.navigate(WEBVIEW_SCREEN, {
            title: 'Facebook',
            url: 'https://www.facebook.com/AcsonMalaysia',
          });
        },
      },
      {
        icon: (
          <Image
            source={require('../../shared/images/home/Other.png')}
            style={{ height: 40, width: 40 }}
          />
        ),
        label: 'More',
        onPress: () => {
          setModalOpen(true);
        },
      },
    ],
    [isLogin]
  );

  const renderMenuOptions = React.useMemo(
    () =>
      menuOptions.map(option => (
        <TouchableOpacity
          disabled={option.disabled}
          style={[styles.iconBox, styles.shadow]}
          key={option.label}
          onPress={option.onPress}>
          <View>
            <View style={styles.margin}>{option.icon}</View>
            <Text style={[styles.margin, styles.optionLabel]}>
              {option.label}
            </Text>
          </View>
        </TouchableOpacity>
      )),
    [menuOptions]
  );

  const handleModalPress = optionPress => {
    setModalOpen(false);
    optionPress();
  };

  const renderMoreOptionMenu = React.useMemo(
    () =>
      moreMenuOptions.map(option => (
        <TouchableOpacity
          disabled={option.disabled}
          style={[styles.iconBox, styles.shadow]}
          key={option.label}
          onPress={() => handleModalPress(option.onPress)}>
          <View>
            <View style={[styles.margin]}>{option.icon}</View>
            <Text style={[styles.margin, styles.optionLabel]}>
              {option.label}
            </Text>
          </View>
        </TouchableOpacity>
      )),
    [menuOptions]
  );
  const handlePress = React.useCallback(
    item => {
      NavigationService.navigate(WEBVIEW_SCREEN, {
        title: item.title,
        url: item.hyperlink,
      });
    },
    [navigation, promotions]
  );

  const renderCarousel = React.useMemo(
    () => (
      <>
        <View style={[styles.row, styles.flex, styles.subtitleRow]}>
          <Text style={[styles.subtitle, styles.left]}>Promotion</Text>
          <TouchableOpacity
            onPress={() => {
              NavigationService.navigate(PROMOTION_SCREEN);
            }}>
            <Text style={[styles.subtitle, styles.right]}>View All</Text>
          </TouchableOpacity>
        </View>
        <MainCarousel
          data={promotions}
          sliderWidth={scale(310)}
          itemWidth={scale(300)}
          handlePress={handlePress}
        />
      </>
    ),
    [promotions]
  );

  return (
    <>
      {productParam ? (
        <WebView
          renderLoading={renderLoading}
          style={styles.webContainer}
          startInLoadingState
          source={{ uri: `${Config.ACSON_URL}/${productParam}` }}
        />
      ) : (
        <LinearGradient
          colors={['#DE3434', '#AF2221']}
          style={[styles.flex, styles.container]}>
          <Image
            style={styles.background}
            source={require('../../shared/images/home/background.png')}
          />
          <View style={styles.containerWrap}>
            <ScrollView style={styles.flex}>
              <Text style={styles.title}>Explore Acson</Text>
              <Modal
                useNativeDriver={Platform.OS === 'android'}
                hasBackdrop
                onBackdropPress={() => {
                  setModalOpen(false);
                }}
                onBackButtonPress={() => {
                  setModalOpen(false);
                }}
                style={styles.modal}
                isVisible={modalOpen}>
                <View style={styles.moreOptionContainer}>
                  {renderMoreOptionMenu}
                </View>
              </Modal>
              <View style={styles.optionContainer}>{renderMenuOptions}</View>
              <View style={[styles.carouselContainer, styles.flex]}>
                {renderCarousel}
              </View>
            </ScrollView>
          </View>
        </LinearGradient>
      )}
      <ProductBottomSheet
        currentFocus={productParam}
        setOnPress={handleOnPress}
        products={productItems}
      />
    </>
  );
};

MarketingHomeScreen.propTypes = {
  isLogin: PropTypes.bool,
  clearStatusHeatLoadCalculator: PropTypes.func,
  init: PropTypes.func,
  promotions: PropTypes.array,
  navigation: PropTypes.object,
};

const mapStateToProps = ({
  login: loginState,
  marketingHome: marketingHomeState,
}) => {
  const { isLogin } = loginState;
  const { promotions } = marketingHomeState;
  return {
    isLogin,
    promotions,
  };
};

const mapDispatchToProps = dispatch => ({
  clearStatusHeatLoadCalculator: () => {
    dispatch(heatLoadCalculator.clearStatus());
  },
  init: appType => {
    dispatch(marketingHome.init(appType));
  },
});

MarketingHomeScreen.propTypes = {
  navigation: PropTypes.object,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarketingHomeScreen);
