const products = [
  {
    iconSource: require('../../../shared/images/product/wallMount.png'),
    label: 'Wall Mounted',
    params: 'wall-mounted',
  },
  {
    iconSource: require('../../../shared/images/product/moveo.png'),
    label: 'Moveo',
    params: 'portable-air-conditioner-moveo',
  },
  {
    iconSource: require('../../../shared/images/product/ceilingCassette.png'),
    label: 'Ceiling Cassette',
    params: 'ceiling-cassette',
  },
  {
    iconSource: require('../../../shared/images/product/floorStand.png'),
    label: 'Floor Standing',
    params: 'floor-standing',
  },
  {
    iconSource: require('../../../shared/images/product/airCurtain.png'),
    label: 'Air Curtain',
    params: 'air-curtain',
  },
  {
    iconSource: require('../../../shared/images/product/purifier.png'),
    label: 'Air Purifier',
    params: 'air-purifier',
  },
  {
    iconSource: require('../../../shared/images/product/diffuser.png'),
    label: 'Aroma Diffuser',
    params: 'aroma-diffuser',
  },
  {
    iconSource: require('../../../shared/images/product/antiVoc.png'),
    label: 'Anti VOC & Bacterial',
    params: 'anti-voc-spray',
  },
  {
    iconSource: require('../../../shared/images/product/ceilingConcealed.png'),
    label: 'Ceiling Concealed',
    params: 'ceiling-concealed',
  },
  {
    iconSource: require('../../../shared/images/product/avr.png'),
    label: 'AVR',
    params: 'avr',
  },
];

export default products;
