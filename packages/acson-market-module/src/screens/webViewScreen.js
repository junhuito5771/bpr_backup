import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});

const WebviewScreen = ({ route }) => {
  const { params } = route;
  const [isLoading, setIsLoading] = useState(true);

  const renderLoading = () => (
    <View style={styles.container}>
      <ActivityIndicator size="large" />
    </View>
  );

  const getScreenLayout = () => {
    if (isLoading) return { flex: 0 };
    return { flex: 1 };
  };

  return (
    <WebView
      source={{ uri: params.url }}
      startInLoadingState={isLoading}
      renderLoading={renderLoading}
      onLoadEnd={syntheticEvent => {
        // update component to be aware of loading status
        const { nativeEvent } = syntheticEvent;
        setIsLoading(nativeEvent.loading);
      }}
      style={getScreenLayout()}
    />
  );
};

WebviewScreen.propTypes = {
  route: PropTypes.object,
};

export default WebviewScreen;
