import React, { PureComponent } from 'react';
import {
  View,
  requireNativeComponent,
  findNodeHandle,
  StyleSheet,
  NativeModules,
} from 'react-native';
import PropTypes from 'prop-types';

const EventThrottleMs = 500;

const HMSCameraManager = NativeModules.RNHMSCameraModule || {
  stubbed: true,
  Type: {
    back: 1,
  },
  AutoFocus: {
    on: 1,
  },
  FlashMode: {
    off: 1,
  },
  WhiteBalance: {},
  BarCodeType: {},
};

export default class HMSCamera extends PureComponent {
  constructor(props) {
    super(props);
    this.lastEvents = {};
    this.lastEventsTimes = {};
    this.cameraRef = null;
    this.cameraHandle = null;
  }

  static Constants = {
    BarCodeType: HMSCameraManager.BarCodeType,
  };

  onObjectDetected = callback => ({ nativeEvent }) => {
    const { type } = nativeEvent;
    if (
      this.lastEvents[type] &&
      this.lastEventsTimes[type] &&
      JSON.stringify(nativeEvent) === this.lastEvents[type] &&
      new Date() - this.lastEventsTimes[type] < EventThrottleMs
    ) {
      return;
    }

    if (callback) {
      callback(nativeEvent);
      this.lastEventsTimes[type] = new Date();
      this.lastEvents[type] = JSON.stringify(nativeEvent);
    }
  };

  onMountError = ({ nativeEvent }) => {
    if (this.props.onMountError) {
      this.props.onMountError(nativeEvent);
    }
  };

  onCameraReady = () => {
    if (this.props.onCameraReady) {
      this.props.onCameraReady();
    }
  };

  setReference = ref => {
    if (ref) {
      this.cameraRef = ref;
      this.cameraHandle = findNodeHandle(ref);
    } else {
      this.cameraRef = null;
      this.cameraHandle = null;
    }
  };

  render() {
    const { style, children, ...cameraProps } = this.props;

    const newCameraProps = {
      ...cameraProps,
      barCodeScannerEnabled: !!cameraProps.onBarCodeRead,
    };

    return (
      <View style={style}>
        <RNHMSCamera
          {...newCameraProps}
          style={StyleSheet.absoluteFill}
          ref={this.setReference}
          onMountError={this.onMountError}
          onCameraReady={this.onCameraReady}
          onBarCodeRead={this.onObjectDetected(this.props.onBarCodeRead)}
        />
        {!!children && children}
      </View>
    );
  }
}

HMSCamera.propTypes = {
  style: PropTypes.any,
  onBarCodeRead: PropTypes.func,
  children: PropTypes.any,
};

const RNHMSCamera = requireNativeComponent('RNHMSCameraView', HMSCamera, {
  nativeOnly: {
    barCodeScannerEnabled: true,
    onBarCodeRead: true,
    onCameraReady: true,
    onTouch: true,
    onLayout: true,
    onMountError: true,
    onSubjectAreaChanged: true,
    renderToHardwareTextureAndroid: true,
    testID: true,
  },
});

HMSCamera.defaultProps = {
  autoFocus: HMSCameraManager.AutoFocus.on,
  exposure: -1,
  whiteBalance: HMSCameraManager.WhiteBalance.auto,
};
