package com.huawei.hms.rn.camera;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RNHMSCameraManager extends ViewGroupManager<RNHMSCameraView> {

	private static final String REACT_CLASS = "RNHMSCameraView";
	public enum Events {
		EVENT_CAMERA_READY("onCameraReady"),
		EVENT_ON_MOUNT_ERROR("onMountError"),
		EVENT_ON_BAR_CODE_READ("onBarCodeRead"),
		EVENT_ON_BARCODES_DETECTED("onGoogleVisionBarcodesDetected"),
		EVENT_ON_PICTURE_TAKEN("onPictureTaken"),
		EVENT_ON_PICTURE_SAVED("onPictureSaved"),
		EVENT_ON_RECORDING_START("onRecordingStart"),
		EVENT_ON_RECORDING_END("onRecordingEnd"),
		EVENT_ON_TOUCH("onTouch");


		private final String mName;

		Events(final String name) {
			mName = name;
		}

		@Override
		public String toString() {
			return mName;
		}
	}
	@NonNull
	@Override
	public String getName() {
		return REACT_CLASS;
	}

	@NonNull
	@Override
	protected RNHMSCameraView createViewInstance(@NonNull ThemedReactContext reactContext) {
		return new RNHMSCameraView(reactContext);
	}

	@Override
	public void onDropViewInstance(@NonNull RNHMSCameraView view) {
		view.onHostDestroy();
		super.onDropViewInstance(view);
	}

	@Override
	@Nullable
	public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
		MapBuilder.Builder<String, Object> builder = MapBuilder.builder();
		for (Events event : Events.values()) {
			builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
		}
		return builder.build();
	}

	@ReactProp(name = "barCodeTypes")
	public void setBarCodeTypes(RNHMSCameraView view, ReadableArray barCodeTypes) {
		if (barCodeTypes == null) {
			return;
		}
		List<String> result = new ArrayList<>(barCodeTypes.size());
		for (int i = 0; i < barCodeTypes.size(); i++) {
			result.add(barCodeTypes.getString(i));
		}
		view.setBarCodeTypes(result);
	}

	@ReactProp(name = "barCodeScannerEnabled")
	public void setBarCodeScanning(RNHMSCameraView view, boolean barCodeScannerEnabled) {
		view.setShouldScanBarCodes(barCodeScannerEnabled);
	}

	@ReactProp(name = "autoFocus")
	public void setAutoFocus(RNHMSCameraView view, boolean autoFocus) {
		view.setAutoFocus(autoFocus);
	}

	@ReactProp(name = "exposure")
	public void setExposureCompensation(RNHMSCameraView view, float exposure){
		view.setExposureCompensation(exposure);
	}

	@ReactProp(name = "whiteBalance")
	public void setWhiteBalance(RNHMSCameraView view, int whiteBalance) {
		view.setWhiteBalance(whiteBalance);
	}

	/**---limit scan area addition---**/
	@ReactProp(name = "rectOfInterest")
	public void setRectOfInterest(RNHMSCameraView view, ReadableMap coordinates) {
		if(coordinates != null){
			float x = (float) coordinates.getDouble("x");
			float y = (float) coordinates.getDouble("y");
			float width = (float) coordinates.getDouble("width");
			float height = (float) coordinates.getDouble("height");
			view.setRectOfInterest(x, y, width, height);
		}
	}

	@ReactProp(name = "cameraViewDimensions")
	public void setCameraViewDimensions(RNHMSCameraView view, ReadableMap dimensions) {
		if(dimensions != null){
			int cameraViewWidth = (int) dimensions.getDouble("width");
			int cameraViewHeight = (int) dimensions.getDouble("height");
			view.setCameraViewDimensions(cameraViewWidth, cameraViewHeight);
		}
	}
	/**---limit scan area addition---**/
}
