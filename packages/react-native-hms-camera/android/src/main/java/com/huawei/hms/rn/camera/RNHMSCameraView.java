package com.huawei.hms.rn.camera;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Build;
import android.view.View;
import android.graphics.Color;
import com.android.CameraView;
import androidx.core.content.ContextCompat;

import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.uimanager.ThemedReactContext;
import com.google.zxing.Result;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.huawei.hms.rn.camera.tasks.BarCodeScannerAsyncTask;
import com.huawei.hms.rn.camera.tasks.BarCodeScannerAsyncTaskDelegate;

import java.io.ByteArrayOutputStream;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;


public class RNHMSCameraView extends CameraView implements LifecycleEventListener, BarCodeScannerAsyncTaskDelegate {

	private final ThemedReactContext mThemedReactContext;
	private final Activity mActivty;

	private int mPaddingX;
	private int mPaddingY;
	private boolean mIsPaused = false;
	private boolean mIsNew = true;

	private boolean mShouldScanBarCodes = false;
	private MultiFormatReader mMultiFormatReader;
	private List<String> mBarCodeTypes = null;

	// Concurrency lock for scanners to avoid flooding the runtime
	public volatile boolean barCodeScannerTaskLock = false;

	// Limit Android Scan Area
	private boolean mLimitScanArea = false;
	private float mScanAreaX = 0.0f;
	private float mScanAreaY = 0.0f;
	private float mScanAreaWidth = 0.0f;
	private float mScanAreaHeight = 0.0f;
	private int mCameraViewWidth = 0;
	private int mCameraViewHeight = 0;

	public RNHMSCameraView(ThemedReactContext themedReactContext) {
		super(themedReactContext, true);
		mThemedReactContext = themedReactContext;
		themedReactContext.addLifecycleEventListener(this);

		mActivty = themedReactContext.getCurrentActivity();

		addCallback(new Callback() {
			@Override
			public void onCameraOpened(CameraView cameraView) {
				RNHMSCameraViewHelper.emitCameraReadyEvent(cameraView);
			}

			@Override
			public void onCameraClosed(CameraView cameraView) {
				super.onCameraClosed(cameraView);
			}

			@Override
			public void onFramePreview(CameraView cameraView, byte[] data, int width, int height, int orientation) {
				boolean willCallBarCodeTask = mShouldScanBarCodes && !barCodeScannerTaskLock && cameraView instanceof BarCodeScannerAsyncTaskDelegate;
				if (willCallBarCodeTask) {
					barCodeScannerTaskLock = true;
					BarCodeScannerAsyncTaskDelegate delegate = (BarCodeScannerAsyncTaskDelegate) cameraView;
					new BarCodeScannerAsyncTask(delegate, mMultiFormatReader, data, width, height, mLimitScanArea, mScanAreaX, mScanAreaY, mScanAreaWidth, mScanAreaHeight, mCameraViewWidth, mCameraViewHeight, getAspectRatio().toFloat()).execute();
				}
			}

			@Override
			public void onMountError(CameraView cameraView) {
				super.onMountError(cameraView);
			}
		});

	}

	private boolean hasCameraPermissions() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			int result = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
			return result == PackageManager.PERMISSION_GRANTED;
		} else {
			return true;
		}
	}

	/**
	 * Initialize the barcode decoder.
	 * Supports all iOS codes except [code138, code39mod43, itf14]
	 * Additionally supports [codabar, code128, maxicode, rss14, rssexpanded, upc_a, upc_ean]
	 */
	private void initBarcodeReader() {
		mMultiFormatReader = new MultiFormatReader();
		EnumMap<DecodeHintType, Object> hints = new EnumMap<>(DecodeHintType.class);
		EnumSet<BarcodeFormat> decodeFormats = EnumSet.noneOf(BarcodeFormat.class);

		if (mBarCodeTypes != null) {
			for (String code : mBarCodeTypes) {
				String formatString = (String) RNHMSCameraModule.VALID_BARCODE_TYPES.get(code);
				if (formatString != null) {
					decodeFormats.add(BarcodeFormat.valueOf(formatString));
				}
			}
		}

		hints.put(DecodeHintType.POSSIBLE_FORMATS, decodeFormats);
		mMultiFormatReader.setHints(hints);
	}

	public void setBarCodeTypes(List<String> barCodeTypes) {
		mBarCodeTypes = barCodeTypes;
		initBarcodeReader();
	}


	public void setShouldScanBarCodes(boolean shouldScanBarCodes) {
		if (shouldScanBarCodes && mMultiFormatReader == null) {
			initBarcodeReader();
		}
		this.mShouldScanBarCodes = shouldScanBarCodes;
		setScanning(mShouldScanBarCodes);
	}


	@Override
	public void onBarCodeRead(Result barCode, int width, int height, byte[] imageData) {
		if (!mShouldScanBarCodes) {
			return;
		}

		RNHMSCameraViewHelper.emitBarCodeReadEvent(this, barCode, width, height);
	}

	public void onBarCodeScanningTaskCompleted() {
		barCodeScannerTaskLock = false;
	}

	// Limit Scan Area
	public void setRectOfInterest(float x, float y, float width, float height) {
		this.mLimitScanArea = true;
		this.mScanAreaX = x;
		this.mScanAreaY = y;
		this.mScanAreaWidth = width;
		this.mScanAreaHeight = height;
	}

	public void setCameraViewDimensions(int width, int height) {
		this.mCameraViewWidth = width;
		this.mCameraViewHeight = height;
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		View preview = getView();
		if (null == preview || getAspectRatio() == null) {
			return;
		}
		float width = right - left;
		float height = bottom - top;
		float ratio = getAspectRatio().toFloat();
		int orientation = getResources().getConfiguration().orientation;
		int correctHeight;
		int correctWidth;
		this.setBackgroundColor(Color.BLACK);
		if (orientation == android.content.res.Configuration.ORIENTATION_LANDSCAPE) {
			if (ratio * height < width) {
				correctHeight = (int) (width / ratio);
				correctWidth = (int) width;
			} else {
				correctWidth = (int) (height * ratio);
				correctHeight = (int) height;
			}
		} else {
			if (ratio * width > height) {
				correctHeight = (int) (width * ratio);
				correctWidth = (int) width;
			} else {
				correctWidth = (int) (height / ratio);
				correctHeight = (int) height;
			}
		}
		int paddingX = (int) ((width - correctWidth) / 2);
		int paddingY = (int) ((height - correctHeight) / 2);
		mPaddingX = paddingX;
		mPaddingY = paddingY;
		preview.layout(paddingX, paddingY, correctWidth + paddingX, correctHeight + paddingY);
	}

	@SuppressLint("all")
	@Override
	public void requestLayout() {
		// React handles this for us, so we don't need to call super.requestLayout();
	}

	@Override
	public void onHostResume() {
		if(hasCameraPermissions()) {
			mBgHandler.post(new Runnable() {
				@Override
				public void run() {
					if ((mIsPaused && !isCameraOpened()) || mIsNew) {
						mIsPaused = false;
						mIsNew = false;
						start();
					}
				}
			});
		} else {
			RNHMSCameraViewHelper.emitMountErrorEvent(this, "Camera permissions not granted - component could not be rendered.");
		}
	}

	@Override
	public void onHostPause() {
		if (!mIsPaused && isCameraOpened()) {
			mIsPaused = true;
			stop();
		}
	}

	@Override
	public void onHostDestroy() {
		mThemedReactContext.removeLifecycleEventListener(this);
	}
}
