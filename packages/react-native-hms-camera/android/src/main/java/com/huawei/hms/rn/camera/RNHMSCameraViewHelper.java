package com.huawei.hms.rn.camera;

import android.view.ViewGroup;

import com.facebook.react.bridge.ReactContext;
import com.facebook.react.uimanager.UIManagerModule;
import com.huawei.hms.rn.camera.events.CameraReadyEvent;
import com.huawei.hms.rn.camera.events.CameraMountErrorEvent;
import com.huawei.hms.rn.camera.events.BarCodeReadEvent;

import com.google.zxing.Result;

public class RNHMSCameraViewHelper {

	public static void emitCameraReadyEvent(final ViewGroup view) {

		final ReactContext reactContext = (ReactContext) view.getContext();
		reactContext.runOnNativeModulesQueueThread(new Runnable() {
			@Override
			public void run() {
				CameraReadyEvent event = CameraReadyEvent.obtain(view.getId());
				reactContext.getNativeModule(UIManagerModule.class).getEventDispatcher().dispatchEvent(event);
			}
		});
	}

	public static void emitMountErrorEvent(final ViewGroup view, final String error) {

		final ReactContext reactContext = (ReactContext) view.getContext();
		reactContext.runOnNativeModulesQueueThread(new Runnable() {
			@Override
			public void run() {
				CameraMountErrorEvent event = CameraMountErrorEvent.obtain(view.getId(), error);
				reactContext.getNativeModule(UIManagerModule.class).getEventDispatcher().dispatchEvent(event);
			}
		});
	}

	public static void emitBarCodeReadEvent(final ViewGroup view, final Result barCode, final int width, final int height) {
		final ReactContext reactContext = (ReactContext) view.getContext();
		reactContext.runOnNativeModulesQueueThread(new Runnable() {
			@Override
			public void run() {
				BarCodeReadEvent event = BarCodeReadEvent.obtain(view.getId(), barCode, width,  height);
				reactContext.getNativeModule(UIManagerModule.class).getEventDispatcher().dispatchEvent(event);
			}
		});
	}
}
