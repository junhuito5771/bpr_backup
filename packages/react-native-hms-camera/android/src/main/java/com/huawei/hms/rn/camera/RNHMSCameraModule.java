package com.huawei.hms.rn.camera;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.uimanager.NativeViewHierarchyManager;
import com.facebook.react.uimanager.UIBlock;
import com.facebook.react.uimanager.UIManagerModule;

import com.google.zxing.BarcodeFormat;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Collections;
import java.util.Map;

public class RNHMSCameraModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public RNHMSCameraModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "RNHMSCameraModule";
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        return Collections.unmodifiableMap(new HashMap<String, Object>() {
            {
                put("WhiteBalance", getWhiteBalanceConstants());
                put("AutoFocus", getAutoFocusConstants());
                put("BarCodeType", getBarCodeConstants());
            }});
    }

    public static final Map<String, Object> VALID_BARCODE_TYPES =
      Collections.unmodifiableMap(new HashMap<String, Object>() {
          {
              put("aztec", BarcodeFormat.AZTEC.toString());
              put("ean13", BarcodeFormat.EAN_13.toString());
              put("ean8", BarcodeFormat.EAN_8.toString());
              put("qr", BarcodeFormat.QR_CODE.toString());
              put("pdf417", BarcodeFormat.PDF_417.toString());
              put("upc_e", BarcodeFormat.UPC_E.toString());
              put("datamatrix", BarcodeFormat.DATA_MATRIX.toString());
              put("code39", BarcodeFormat.CODE_39.toString());
              put("code93", BarcodeFormat.CODE_93.toString());
              put("interleaved2of5", BarcodeFormat.ITF.toString());
              put("codabar", BarcodeFormat.CODABAR.toString());
              put("code128", BarcodeFormat.CODE_128.toString());
              put("maxicode", BarcodeFormat.MAXICODE.toString());
              put("rss14", BarcodeFormat.RSS_14.toString());
              put("rssexpanded", BarcodeFormat.RSS_EXPANDED.toString());
              put("upc_a", BarcodeFormat.UPC_A.toString());
              put("upc_ean", BarcodeFormat.UPC_EAN_EXTENSION.toString());
          }
      });

    private Map<String, Object> getWhiteBalanceConstants() {
        return Collections.unmodifiableMap(new HashMap<String, Object>() {
            {
                put("auto", Constants.WB_AUTO);
                put("cloudy", Constants.WB_CLOUDY);
                put("sunny", Constants.WB_SUNNY);
                put("shadow", Constants.WB_SHADOW);
                put("fluorescent", Constants.WB_FLUORESCENT);
                put("incandescent", Constants.WB_INCANDESCENT);
            }
        });
    }

    private Map<String, Object> getAutoFocusConstants() {
        return Collections.unmodifiableMap(new HashMap<String, Object>() {
            {
                put("on", true);
                put("off", false);
            }
        });
    }

    private Map<String, Object> getBarCodeConstants() {
        return VALID_BARCODE_TYPES;
    }

    @ReactMethod
    public void pausePreview(final int viewTag) {
        final ReactApplicationContext context = getReactApplicationContext();
        UIManagerModule uiManager = context.getNativeModule(UIManagerModule.class);
        uiManager.addUIBlock(new UIBlock() {
            @Override
            public void execute(NativeViewHierarchyManager nativeViewHierarchyManager) {
                final RNHMSCameraView cameraView;

                try {
                    cameraView = (RNHMSCameraView) nativeViewHierarchyManager.resolveView(viewTag);
                    if (cameraView.isCameraOpened()) {
                        cameraView.pausePreview();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @ReactMethod
    public void resumePreview(final int viewTag) {
        final ReactApplicationContext context = getReactApplicationContext();
        UIManagerModule uiManager = context.getNativeModule(UIManagerModule.class);
        uiManager.addUIBlock(new UIBlock() {
            @Override
            public void execute(NativeViewHierarchyManager nativeViewHierarchyManager) {
                final RNHMSCameraView cameraView;

                try {
                    cameraView = (RNHMSCameraView) nativeViewHierarchyManager.resolveView(viewTag);
                    if (cameraView.isCameraOpened()) {
                        cameraView.resumePreview();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
