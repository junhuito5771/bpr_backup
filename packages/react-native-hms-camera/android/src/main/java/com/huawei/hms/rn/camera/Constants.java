package com.huawei.hms.rn.camera;

public interface Constants {
	int LANDSCAPE_90 = 90;
	int LANDSCAPE_270 = 270;

	int WB_AUTO = 0;
	int WB_CLOUDY = 1;
	int WB_SUNNY = 2;
	int WB_SHADOW = 3;
	int WB_FLUORESCENT = 4;
	int WB_INCANDESCENT = 5;
}
