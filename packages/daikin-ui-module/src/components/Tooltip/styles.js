import { StyleSheet } from 'react-native';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#F5FCFF',
    ...StyleSheet.absoluteFillObject,
  },
  mainView: {
    flex: 1,
    flexDirection: 'row',
  },
  talkBubble: {
    backgroundColor: 'transparent',
    position: 'absolute',
    zIndex: 2, // <- zIndex here
    flex: 1,
    right: 20,
  },
  talkBubbleSquare: {
    width: 300,
    backgroundColor: Colors.white,
    borderRadius: 5,
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  talkBubbleTriangle: {
    position: 'absolute',
    bottom: -20,
    right: 0,
    width: 0,
    height: 0,
    borderLeftWidth: 30,
    borderRightWidth: 0,
    borderTopWidth: 30,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderTopColor: Colors.white,
  },
  talkBubbleMessage: {
    color: '#626262',
    // marginTop: 40,
    // marginLeft: 20,
    // marginRight: 20,
  },
  anotherView: {
    backgroundColor: '#CCCCCC',
    width: 340,
    height: 300,
    position: 'absolute',
    zIndex: 1, // <- zIndex here
  },
});

/*
export default StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    opacity: 0,
    backgroundColor: 'transparent',
    zIndex: 500,
  },
  tooltip: {
    backgroundColor: 'transparent',
    position: 'absolute',
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 2,
    shadowOpacity: 0.8,
  },
  content: {
    borderRadius: 4,
    padding: 8,
    backgroundColor: '#fff',
    overflow: 'hidden',
  },
  arrow: {
    position: 'absolute',
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'transparent',
  },
}); */
