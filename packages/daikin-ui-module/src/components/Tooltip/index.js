import React from 'react';
import { View, Modal, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const Tooltip = ({ showTooltip, onClose, children, position }) => (
  <Modal transparent visible={showTooltip} onRequestClose={onClose}>
    <TouchableWithoutFeedback onPress={onClose}>
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: 'transparent',
            position: 'absolute',
            zIndex: 2, // <- zIndex here
            flex: 1,
            left: position.x,
            top: position.y,
          }}>
          <View style={styles.talkBubbleSquare}>
            {children}
            {/* <Text style={styles.talkBubbleMessage}>
              Put whatever you want here. This is just a test message
            </Text> */}
          </View>
          <View style={styles.talkBubbleTriangle} />
        </View>
      </View>
    </TouchableWithoutFeedback>
  </Modal>
);

Tooltip.propTypes = {
  showTooltip: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  position: PropTypes.objectOf(PropTypes.object).isRequired,
};

Tooltip.defaultProps = {
  showTooltip: false,
};

export default Tooltip;
