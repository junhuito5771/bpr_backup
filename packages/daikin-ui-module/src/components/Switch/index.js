import React from 'react';
import PropTypes from 'prop-types';
import Switch from 'react-native-switches';

import Colors from '../../shared/constants/color';

const PillSwitch = ({ onPress, value }) => (
  <Switch
    shape="pill"
    onChange={onPress}
    value={value}
    showText={false}
    colorSwitchOn={Colors.switchFill}
    colorSwitchOff={Colors.veryLightGrey}
    sliderWidth={35}
    sliderHeight={20}
    buttonSize={17}
    buttonOffsetLeft={0.5}
    buttonOffsetRight={2}
  />
);

PillSwitch.propTypes = {
  onPress: PropTypes.func.isRequired,
  value: PropTypes.bool,
};

PillSwitch.defaultProps = {
  value: false,
};

export default PillSwitch;
