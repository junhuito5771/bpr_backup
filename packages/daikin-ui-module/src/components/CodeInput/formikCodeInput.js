import React from 'react';
import { connect } from 'formik';
import PropTypes from 'prop-types';

import CodeInput from 'react-native-confirmation-code-input';

const FormikCodeInput = ({ name, formik, ...extraProps }) => {
  const handleOnFullfill = value => formik.setFieldValue(name, value);

  return <CodeInput onFulfill={handleOnFullfill} {...extraProps} />;
};

FormikCodeInput.propTypes = {
  name: PropTypes.string.isRequired,
  formik: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default connect(FormikCodeInput);
