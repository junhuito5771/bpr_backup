import React, { useState } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';

const ReminderBox = ({ reminder, isEditMode, onRowPress }) => {
  const [isChecked, setIsChecked] = useState(false);

  const handleOnReminderPress = React.useCallback(() => {
    if (isEditMode) {
      setIsChecked(!isChecked);
    }
    onRowPress();
  }, [isEditMode, isChecked]);

  const renderReminderBox = React.useCallback(
    () => (
      <>
        <View style={styles.reminderHeader}>
          <Text style={styles.reminderLabel}>{reminder.reminderName}</Text>
        </View>
        <TouchableOpacity
          style={[styles.reminderContent, styles.row]}
          onPress={handleOnReminderPress}>
          {isEditMode && (
            <TouchableOpacity onPress={handleOnReminderPress}>
              {isChecked ? (
                <View style={[styles.checkbox, styles.checkboxChecked]}>
                  <Image
                    source={require('../../shared/assets/general/check.png')}
                  />
                </View>
              ) : (
                <View style={styles.checkbox} />
              )}
            </TouchableOpacity>
          )}
          <View style={styles.center}>
            <Text style={styles.reminderTitle}>Air Filter Cleaning</Text>
            <Text style={styles.reminderValue}>{reminder.nextFilter}</Text>
            <Text style={styles.reminderTitle}>Regular AC Cleaning</Text>
            <Text style={styles.reminderValue}>{reminder.nextService}</Text>
          </View>
          <View style={[styles.reminderArrow, styles.center]}>
            <Image source={require('../../shared/assets/reminder/arrow.png')} />
          </View>
        </TouchableOpacity>
      </>
    ),
    [reminder, isEditMode, handleOnReminderPress, isChecked]
  );

  return (
    <View style={styles.unitContainer}>
      <View style={[styles.row, styles.unitHeader]}>
        <Image
          style={styles.unitIcon}
          source={require('../../shared/assets/aircond/0.png')}
        />
        <Text style={styles.unitLabel}>{reminder.unitName}</Text>
      </View>
      {renderReminderBox()}
    </View>
  );
};

ReminderBox.propTypes = {
  reminder: PropTypes.objectOf(PropTypes.object).isRequired,
  isEditMode: PropTypes.bool,
  onRowPress: PropTypes.func.isRequired,
};

ReminderBox.defaultProps = {
  isEditMode: false,
};

export default ReminderBox;
