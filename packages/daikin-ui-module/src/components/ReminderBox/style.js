import { StyleSheet } from 'react-native';
import { scale, ScaleText, verticalScale } from '@module/utility';
import Colors from '../../shared/constants/color';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  center: {
    justifyContent: 'center',
  },
  unitContainer: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: Colors.whiteGrey,
  },
  unitHeader: {
    height: verticalScale(60),
    alignItems: 'center',
  },
  unitIcon: {
    marginStart: scale(20),
    marginEnd: scale(10),
  },
  unitLabel: {
    fontSize: ScaleText(16),
    color: Colors.darkGrey,
  },
  reminderHeader: {
    height: verticalScale(30),
    justifyContent: 'center',
    paddingHorizontal: scale(20),
    backgroundColor: Colors.modeSeparatorGrey,
  },
  reminderLabel: {
    color: Colors.sectionHeaderTitleGrey,
    fontWeight: '500',
  },
  reminderContent: {
    paddingHorizontal: scale(20),
    paddingBottom: verticalScale(10),
  },
  reminderTitle: {
    color: Colors.darkGrey,
    marginTop: verticalScale(10),
  },
  reminderValue: {
    color: Colors.darkGrey,
    marginTop: verticalScale(5),
  },
  reminderArrow: {
    flex: 1,
    alignItems: 'flex-end',
  },
  checkbox: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: verticalScale(15),
    marginRight: scale(20),
    borderColor: Colors.sectionBorderGrey,
    borderRadius: 2,
    height: 20,
    width: 20,
    borderWidth: 1.5,
  },
  checkboxChecked: {
    backgroundColor: Colors.dayBlue,
    borderWidth: 0,
  },
});

export default styles;
