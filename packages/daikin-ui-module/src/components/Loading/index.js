import React from 'react';
import { View, ActivityIndicator, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import { Text } from '../Text';
import styles from './styles';

import Colors from '../../shared/constants/color';

const Loading = ({ size, color, children, visible, progress, onCancel }) => {
  if (!visible) return <View />;

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <ActivityIndicator size={size} color={color} />
        <View style={styles.descSection}>
          {!!progress && <Text style={styles.text}>{progress}</Text>}
          {!!children && <Text style={styles.text}>{children}</Text>}
        </View>
        {!!onCancel && (
          <View style={styles.buttonSection} hitSlop={styles.hitSlop}>
            <TouchableOpacity onPress={onCancel}>
              <Text style={styles.buttonText}>Cancel</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
};

Loading.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string,
  children: PropTypes.string.isRequired,
  visible: PropTypes.bool,
  progress: PropTypes.string,
  onCancel: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};

Loading.defaultProps = {
  color: Colors.lightGrey,
  visible: false,
  progress: '',
  size: 'small',
  onCancel: false,
};

export default Loading;
