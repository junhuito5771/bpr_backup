import { StyleSheet } from 'react-native';
import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.25)',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 20,
  },
  wrapper: {
    borderRadius: 10,
    backgroundColor: Colors.white,
    width: 200,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  descSection: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    color: Colors.lightGrey,
    marginTop: 10,
  },
  buttonSection: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: Colors.blue,
  },
  hitslop: {
    top: 20,
    bottom: 20,
    left: 20,
    right: 20,
  },
});
