import { StyleSheet } from 'react-native';

import Colors from '../../shared/constants/color';
import { HANDLE_HEIGHT } from './constants';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: HANDLE_HEIGHT,
    alignItems: 'center',
  },
  drawerContainer: {
    zIndex: 10,
    position: 'absolute',
    overflow: 'hidden',
    height: '100%',
    width: '100%',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  scrollView: {
    backgroundColor: Colors.white,
    flexGrow: 1,
  },
  backdrop: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.7,
    backgroundColor: Colors.black,
  },
  handler: {
    width: 40,
    height: 6,
    backgroundColor: Colors.lightCoolGrey,
    borderRadius: 25,
    marginTop: 10,
  },
});
