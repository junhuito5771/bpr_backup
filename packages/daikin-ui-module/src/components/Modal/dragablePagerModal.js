import React, {
  useState,
  useImperativeHandle,
  forwardRef,
  useRef,
  createRef,
  useEffect,
} from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import ViewPager from '@react-native-community/viewpager';
import { SAFE_VIEWPORT_HEIGHT, SAFE_AREA_INSET_TOP } from '@module/utility';

import DragableModal from './dragable';
import useHeaderHeight from '../../services/useHeaderHeight';
import { TAB_ITEM_HEIGHT, HANDLE_HEIGHT } from './constants';

function calculateSnapPoints(children, setSnapPoints, headerHeight) {
  const groups = React.Children.toArray(children)[0];

  const generalItems = groups.props.children[0].props.children;
  const specialItems = groups.props.children[1]
    ? groups.props.children[1].props.children
    : [];

  const nrGenItems = generalItems.filter(item => !!item).length;
  const nrSpcItems = specialItems.filter(item => !!item).length;
  const nrGenRows = Math.ceil(nrGenItems / 5);
  const nrSpecRows = Math.ceil(nrSpcItems / 5);

  const realHeaderHeight = headerHeight - SAFE_AREA_INSET_TOP;

  const speFeatHeader = nrSpecRows > 0 ? 30 : 0;

  const nrRows = nrGenRows + nrSpecRows;
  const rowHeight = Math.max((nrRows - 1) * TAB_ITEM_HEIGHT, TAB_ITEM_HEIGHT);
  const hiddenHeight = rowHeight + speFeatHeader;

  const closedHeight =
    SAFE_VIEWPORT_HEIGHT - realHeaderHeight - TAB_ITEM_HEIGHT - HANDLE_HEIGHT;

  const openHeight = closedHeight - hiddenHeight;

  setSnapPoints([closedHeight, openHeight]);
}

const DraggablePagerModal = forwardRef(({ children, router, canDrag }, ref) => {
  const [pages, setPage] = useState([]);
  const [snapPoints, setSnapPoints] = useState(undefined);
  const headerHeight = useHeaderHeight();

  const modalRef = useRef();
  const pagerRef = createRef();

  useEffect(() => {
    if (pages && pages.length === 0) {
      calculateSnapPoints(children, setSnapPoints, headerHeight);
    }
  }, [children]);

  const handleClose = () => {
    calculateSnapPoints(children, setSnapPoints, headerHeight);
    pagerRef.current.setPage(0);
    setPage([]);

    requestAnimationFrame(() => {
      modalRef.current.close();
    });
  };

  const handleBack = () => {
    calculateSnapPoints(children, setSnapPoints, headerHeight);
    const curIndex = Math.max(pages.length - 1, 0);
    pagerRef.current.setPage(curIndex);
    setPage([]);
    requestAnimationFrame(() => {
      if (canDrag) {
        modalRef.current.open();
      } else {
        modalRef.current.close();
      }
    });
  };

  useImperativeHandle(ref, () => ({
    ...modalRef.current,
    navigate: (routeName, params) => {
      const newScreen = router(routeName);
      setPage([{ Component: newScreen, props: params }]);
      setSnapPoints(params.snapPoints);
      const pagerRefLocal = pagerRef.current;
      requestAnimationFrame(() => {
        modalRef.current.open();
        pagerRefLocal.setPage(1);
      });
    },
    setPages: newPages => setPage(newPages),
    setCurPageIndex: pageIndex => {
      const pagerRefLocal = pagerRef.current;
      requestAnimationFrame(() => {
        pagerRefLocal.setPage(pageIndex);
      });
    },
  }));

  return (
    <DragableModal
      {...{ onClose: handleClose, ref: modalRef, snapPoints, canDrag }}>
      <ViewPager
        ref={pagerRef}
        initialPage={0}
        style={{ flex: 1 }}
        scrollEnabled={pages.length > 0}>
        <View key="0">{children}</View>
        {pages.map((page, index) => {
          const pageIndex = (index + 1).toString();
          const { Component = View, props } = page;

          return (
            <View key={pageIndex}>
              <Component
                {...props}
                onClose={() => modalRef.current.close()}
                onBack={handleBack}
              />
            </View>
          );
        })}
      </ViewPager>
    </DragableModal>
  );
});

DraggablePagerModal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  pages: PropTypes.arrayOf(PropTypes.object),
  snapPoints: PropTypes.arrayOf(PropTypes.number),
  router: PropTypes.func.isRequired,
  canDrag: PropTypes.bool,
};

DraggablePagerModal.defaultProps = {
  pages: [],
  snapPoints: [],
  canDrag: false,
};

export default DraggablePagerModal;
