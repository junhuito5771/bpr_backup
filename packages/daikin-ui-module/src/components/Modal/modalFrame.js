import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  Platform,
  UIManager,
  findNodeHandle,
} from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import { iphoneXS, SAFE_VIEWPORT_HEIGHT, verticalScale } from '@module/utility';
import { HEADER_HEIGHT, TAB_ITEM_HEIGHT, HANDLE_HEIGHT } from './constants';

import { Text } from '../Text';
import BackButton from '../Button/backButton';
import InfoButton from './infoIcon';
import Tooltip from '../Tooltip';
import styles from './modalFrameStyle';

const ModalFrame = ({
  title,
  subTitle,
  children,
  onClose,
  onBack,
  toolTipMsgComp,
}) => {
  const infoViewRef = React.useRef(null);
  const [showTooltip, setShowTooltip] = useState(false);
  const [position, setPosition] = useState({ x: 0, y: 0 });
  const DEFAULT_SNAP_POINTS =
    SAFE_VIEWPORT_HEIGHT - TAB_ITEM_HEIGHT - HEADER_HEIGHT - HANDLE_HEIGHT;

  React.useEffect(() => {
    if (infoViewRef.current && position.x && position.y) {
      UIManager.measure(
        findNodeHandle(infoViewRef.current),
        (x, y, w, h, px, py) => {
          setPosition({ x: px - 300, y: py - verticalScale(150) });
        }
      );
    }
  }, [position]);

  const calcPosition = x => {
    const value = x / 2;
    if (x >= 600 && x < 700) {
      return value + 100;
    }
    if (x >= 700) {
      return value + 130;
    }
    return value - 40;
  };
  return (
    <View style={styles.modalContent}>
      <View style={styles.modalHeader}>
        <View style={styles.modalLeftHeader}>
          {onBack && (
            <BackButton
              containerStyle={styles.backButton}
              onPress={onBack}
              style={styles.backButton}
            />
          )}
          <Text medium>{title}</Text>
          {!!subTitle && (
            <Text style={styles.modalSubTitleText}>{subTitle}</Text>
          )}
        </View>
        <View
          onLayout={() => {
            setPosition({
              x: 40,
              y: Platform.select({
                ios: iphoneXS ? 265 : 242,
                android: calcPosition(DEFAULT_SNAP_POINTS),
              }),
            });
          }}>
          <Tooltip
            position={position}
            showTooltip={showTooltip}
            onClose={() => setShowTooltip(false)}>
            {toolTipMsgComp}
          </Tooltip>
        </View>
        <View>
          {toolTipMsgComp && (
            <>
              <View
                style={styles.infoRef}
                ref={infoViewRef}
                collapsable={false}
              />
              <InfoButton
                containerStyle={styles.infoButton}
                onPress={() => setShowTooltip(true)}
              />
            </>
          )}
        </View>
      </View>
      {children}
      <LinearGradient
        colors={['#FFFFFF', '#D3D3D3', '#FFFFFF']}
        style={styles.topBorder}
      />
      <TouchableOpacity style={styles.modalButton} onPress={onClose}>
        <View style={styles.center}>
          <Text medium>Close</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

ModalFrame.propTypes = {
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  onBack: PropTypes.func.isRequired,
  toolTipMsgComp: PropTypes.node.isRequired,
};

export default ModalFrame;
