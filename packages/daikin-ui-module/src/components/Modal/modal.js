import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import ModalRN from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';

import { Text } from '../Text';
import TouchableFeedbackIcon from '../TouchableFeedbackIcon';
import Colors from '../../shared/constants/color';

const styles = StyleSheet.create({
  alignBottom: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modal: {
    backgroundColor: Colors.white,
    height: 200,
  },
  modalTitle: {
    flex: 0.2,
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: Colors.lightCoolGrey,
  },
  modalTitleText: {
    textAlign: 'center',
  },
  modalContent: {
    flex: 0.6,
  },
  modalButton: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectionTitle: {
    alignItems: 'center',
    paddingTop: 10,
  },
  selectionContent: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    flex: 1,
  },
  flex: {
    flex: 1,
  },
  iconContainer: {
    justifyContent: 'flex-end',
    height: 25,
    width: 25,
  },
  topBorder: {
    height: 0.5,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Modal = ({
  selectedValue,
  isVisible,
  onClose,
  options,
  title,
  optionMap,
  onPressOption,
  children,
}) => {
  const [currentOption, setCurrentOption] = useState(selectedValue);

  if (selectedValue !== currentOption) {
    setCurrentOption(selectedValue);
  }

  const setSelectedOption = option => {
    if (title === 'Fan speed') {
      if (
        option.value.match(/\d+/) !== null &&
        currentOption.match(/\d+/) !== null
      ) {
        return option.value.match(/\d+/) <= currentOption.match(/\d+/);
      }
      return option.value === currentOption;
    }
    return option.value === currentOption;
  };

  return (
    <ModalRN
      isVisible={isVisible}
      style={styles.alignBottom}
      coverScreen
      hasBackdrop
      backdropColor={Colors.modalBackdrop}
      onBackdropPress={() => onClose(currentOption)}>
      <View style={styles.modal}>
        <View style={styles.modalTitle}>
          <Text medium>{title}</Text>
        </View>
        {React.isValidElement(children) ? (
          children
        ) : (
          <View style={styles.modalContent}>
            <View style={styles.selectionTitle}>
              <Text
                style={styles.modalTitleText}
                medium
                color={Colors.blue}>{`--- ${
                (optionMap[currentOption] || {}).label || ''
              } ---`}</Text>
            </View>
            <View style={styles.selectionContent}>
              {options.map(option => (
                <TouchableFeedbackIcon
                  isSelected={setSelectedOption(option)}
                  key={option.label}
                  icons={option.icons}
                  onPress={() => {
                    onPressOption(option.value);
                  }}
                />
              ))}
            </View>
          </View>
        )}
        <LinearGradient
          colors={['#FFFFFF', '#D3D3D3', '#FFFFFF']}
          style={styles.topBorder}
        />
        <TouchableOpacity style={styles.modalButton} onPress={onClose}>
          <View style={styles.center}>
            <Text medium>Close</Text>
          </View>
        </TouchableOpacity>
      </View>
    </ModalRN>
  );
};

Modal.propTypes = {
  isVisible: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  onClose: PropTypes.func.isRequired,
  onPressOption: PropTypes.func.isRequired,
  selectedValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired,
  title: PropTypes.string.isRequired,
  optionMap: PropTypes.objectOf(PropTypes.object).isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

Modal.defaultProps = {
  isVisible: false,
};

export default Modal;
