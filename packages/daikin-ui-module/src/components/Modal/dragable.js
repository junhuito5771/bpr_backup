import React, {
  useState,
  useRef,
  useImperativeHandle,
  forwardRef,
  useEffect,
} from 'react';
import {
  Animated,
  View,
  SafeAreaView,
  Dimensions,
  StyleSheet,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  PanGestureHandler,
  NativeViewGestureHandler,
  State,
  TapGestureHandler,
} from 'react-native-gesture-handler';
import { SAFE_VIEWPORT_HEIGHT } from '@module/utility';

import { HEADER_HEIGHT, TAB_ITEM_HEIGHT, HANDLE_HEIGHT } from './constants';

import styles from './draggableStyles';

const windowHeight = Dimensions.get('window').height;
const snapRatio = Platform.select({
  ios: 0.65,
  android: windowHeight > 750 ? 0.45 : 0.65,
});

// Please note that 55 current is the header height
// 50 is the tab item height
const DEFAULT_SNAP_POINTS = [
  SAFE_VIEWPORT_HEIGHT - TAB_ITEM_HEIGHT - HEADER_HEIGHT - HANDLE_HEIGHT,
  (SAFE_VIEWPORT_HEIGHT - TAB_ITEM_HEIGHT - HEADER_HEIGHT) * snapRatio,
];

const BottomModal = forwardRef(
  ({ children, onClose, snapPoints, canDrag }, ref) => {
    const masterdrawer = React.createRef();
    const drawer = React.createRef();
    const drawerheader = React.createRef();
    const scroll = React.createRef();

    const snapPointStart = snapPoints[0];
    const snapPointEnd = snapPoints[snapPoints.length - 1];

    const [lastSnap, setLastSnap] = useState(snapPointStart);

    const lastScrollYValue = useRef(0);
    const lastScrollY = useRef(new Animated.Value(0)).current;
    lastScrollY.addListener(({ value }) => {
      lastScrollYValue.current = value;
    });
    const onRegisterLastScroll = useRef(
      Animated.event([{ nativeEvent: { contentOffset: { y: lastScrollY } } }], {
        useNativeDriver: true,
      })
    ).current;

    const dragY = useRef(new Animated.Value(0)).current;
    const onGestureEvent = useRef(
      Animated.event([{ nativeEvent: { translationY: dragY } }], {
        useNativeDriver: true,
      })
    ).current;
    const reverseLastScrollY = Animated.multiply(
      new Animated.Value(-1),
      lastScrollY
    );

    const translateYOffset = useRef(new Animated.Value(snapPointStart)).current;
    const translateY = Animated.add(
      translateYOffset,
      Animated.add(dragY, reverseLastScrollY)
    ).interpolate({
      inputRange: [snapPointEnd, snapPointStart],
      outputRange: [snapPointEnd, snapPointStart],
      extrapolate: 'clamp',
    });

    const handleClose = () => {
      // if (onClose) onClose();
      setLastSnap(snapPointStart);
      Animated.spring(translateYOffset, {
        velocity: 20,
        tension: 86,
        friction: 12,
        toValue: snapPointStart,
        useNativeDriver: true,
      }).start();
    };

    useEffect(() => {
      if (lastSnap === snapPointStart && onClose) {
        onClose();
      }
    }, [lastSnap]);

    useImperativeHandle(ref, () => ({
      open: () => {
        setLastSnap(snapPointEnd);
        Animated.spring(translateYOffset, {
          velocity: 20,
          tension: 86,
          friction: 12,
          toValue: snapPointEnd,
          useNativeDriver: true,
        }).start();
      },
      close: handleClose,
      reset: () => {
        setLastSnap(snapPointStart);
      },
      adjustHeight: adjustedHeight => {
        const targetSnapPoint =
          (SAFE_VIEWPORT_HEIGHT - adjustedHeight - 80) * snapRatio;
        Animated.spring(translateYOffset, {
          velocity: 20,
          tension: 86,
          friction: 12,
          toValue: targetSnapPoint,
          useNativeDriver: true,
        }).start();
      },
    }));

    const onHandlerStateChange = ({ nativeEvent }) => {
      if (nativeEvent.oldState === State.ACTIVE) {
        const { velocityY } = nativeEvent;
        let { translationY } = nativeEvent;
        translationY -= lastScrollYValue.current;
        const dragToss = 0.05;
        const dragTossVelocity = dragToss * velocityY;
        const endOffsetY = lastSnap + translationY + dragTossVelocity;

        let destSnapPoint = snapPointStart;
        for (let i = 0; i < snapPoints.length; i += 1) {
          const snapPoint = snapPoints[i];
          const distFromSnap = Math.abs(snapPoint - endOffsetY);
          if (distFromSnap < Math.abs(destSnapPoint - endOffsetY)) {
            destSnapPoint = snapPoint;
          }
        }
        setLastSnap(destSnapPoint);
        translateYOffset.extractOffset();
        translateYOffset.setValue(translationY);
        translateYOffset.flattenOffset();
        dragY.setValue(0);
        Animated.spring(translateYOffset, {
          velocity: velocityY,
          tension: 86,
          friction: 12,
          toValue: destSnapPoint,
          useNativeDriver: true,
        }).start();
      }
    };

    const onHeaderHandlerStateChange = ({ nativeEvent }) => {
      if (nativeEvent.oldState === State.BEGAN) {
        lastScrollY.setValue(0);
      }
      onHandlerStateChange({ nativeEvent });
    };

    return (
      <TapGestureHandler
        maxDurationMs={100000}
        ref={masterdrawer}
        maxDeltaY={lastSnap - snapPointStart}>
        <SafeAreaView style={styles.drawerContainer} pointerEvents="box-none">
          {lastSnap !== snapPointStart && (
            <TouchableWithoutFeedback onPress={handleClose}>
              <Animated.View style={styles.backdrop} />
            </TouchableWithoutFeedback>
          )}
          <Animated.View
            style={[
              StyleSheet.absoluteFillObject,
              {
                transform: [{ translateY }],
              },
            ]}>
            <PanGestureHandler
              enabled={canDrag}
              ref={drawerheader}
              simultaneousHandlers={[scroll, masterdrawer]}
              shouldCancelWhenOutside={false}
              onGestureEvent={onGestureEvent}
              onHandlerStateChange={onHeaderHandlerStateChange}>
              <Animated.View style={styles.header}>
                {canDrag && <View style={styles.handler} />}
              </Animated.View>
            </PanGestureHandler>
            <PanGestureHandler
              enabled={canDrag}
              ref={drawer}
              simultaneousHandlers={[scroll, masterdrawer]}
              shouldCancelWhenOutside={false}
              onGestureEvent={onGestureEvent}
              onHandlerStateChange={onHandlerStateChange}>
              <Animated.View style={styles.container}>
                <NativeViewGestureHandler
                  ref={scroll}
                  waitFor={masterdrawer}
                  simultaneousHandlers={drawer}>
                  <Animated.ScrollView
                    contentContainerStyle={styles.scrollView}
                    bounces={false}
                    onScrollBeginDrag={onRegisterLastScroll}
                    scrollEventThrottle={1}>
                    {children}
                  </Animated.ScrollView>
                </NativeViewGestureHandler>
              </Animated.View>
            </PanGestureHandler>
          </Animated.View>
        </SafeAreaView>
      </TapGestureHandler>
    );
  }
);

BottomModal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  onClose: PropTypes.func.isRequired,
  snapPoints: PropTypes.arrayOf(PropTypes.number),
  canDrag: PropTypes.bool,
};

BottomModal.defaultProps = {
  snapPoints: DEFAULT_SNAP_POINTS,
  canDrag: false,
};

export default BottomModal;
