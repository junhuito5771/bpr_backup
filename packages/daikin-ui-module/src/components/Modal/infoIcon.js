import React, { forwardRef } from 'react';
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  InfoIconBorder: {
    borderColor: 'transparent',
    borderWidth: 1,
  },
});

const InfoIcon = forwardRef(({ onPress, containerStyle }, ref) => (
  <View style={[styles.container, containerStyle]}>
    <TouchableOpacity onPress={onPress} ref={ref} style={styles.InfoIconBorder}>
      <Image source={require('../../shared/assets/general/remoteInfo.png')} />
    </TouchableOpacity>
  </View>
));

InfoIcon.propTypes = {
  onPress: PropTypes.func.isRequired,
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default InfoIcon;
