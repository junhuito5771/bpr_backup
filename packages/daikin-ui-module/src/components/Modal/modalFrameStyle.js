import { StyleSheet, Platform } from 'react-native';

import { heightPercentage } from '@module/utility';
import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  modal: {
    backgroundColor: Colors.white,
    height: 200,
  },
  modalTitle: {
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: Colors.lightCoolGrey,
  },
  backButton: {
    marginRight: 10,
  },
  modalTitleText: {
    textAlign: 'center',
  },
  modalSubTitleText: {
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    marginLeft: 10,
  },
  modalContent: {
    flex: 1,
  },
  modalButton: {
    height: heightPercentage(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalHeader: {
    paddingVertical: Platform.select({
      ios: heightPercentage(1.5),
      android: heightPercentage(1),
    }),
    paddingHorizontal: 20,
    alignItems: 'center',
    backgroundColor: Colors.sectionHeaderGrey,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalLeftHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  topBorder: {
    height: Platform.select({ ios: 0.5, android: 1, default: 1 }),
  },
  infoButton: {
    alignItems: 'center',
    right: 8,
  },
});
