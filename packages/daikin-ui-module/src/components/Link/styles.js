import { StyleSheet } from 'react-native';
import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  isLeft: {
    alignSelf: 'flex-start',
  },
  isRight: {
    alignSelf: 'flex-end',
  },
  isCenter: {
    alignSelf: 'center',
  },
  blue: {
    color: Colors.blue,
  },
});
