import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import { Text } from '../Text';
import styles from './styles';

import Colors from '../../shared/constants/color';

const Link = ({
  style,
  textStyle,
  children,
  onPress,
  isLeft,
  isRight,
  isCenter,
  color,
  disabled,
}) => {
  const containerStyle = StyleSheet.flatten([
    isRight && styles.isRight,
    isLeft && styles.isLeft,
    isCenter && styles.isCenter,
    style,
  ]);
  const linkStyle = StyleSheet.flatten([
    styles.blue,
    color && { color },
    textStyle,
  ]);

  return (
    <TouchableOpacity
      onPress={onPress}
      style={containerStyle}
      disabled={disabled}>
      <Text style={linkStyle}>{children}</Text>
    </TouchableOpacity>
  );
};

Link.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  onPress: PropTypes.func.isRequired,
  isLeft: PropTypes.bool,
  isRight: PropTypes.bool,
  isCenter: PropTypes.bool,
  disabled: PropTypes.bool,
  color: PropTypes.string,
  textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

Link.defaultProps = {
  style: {},
  isLeft: false,
  isRight: false,
  isCenter: false,
  disabled: false,
  textStyle: {},
  color: Colors.blue,
};

export default Link;
