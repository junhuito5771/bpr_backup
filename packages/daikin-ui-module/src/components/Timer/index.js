import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import momentDurationFormat from 'moment-duration-format';

import { Text } from '../Text';

momentDurationFormat(moment);

const Timer = ({ duration, onTimeOut, style, isAutoRefresh }) => {
  const [countDown, setCountDown] = useState(duration);

  const countDownText =
    moment.duration(countDown).asDays() > 1
      ? moment.duration(countDown).humanize()
      : moment.duration(countDown).format('hh:mm:ss', { trim: 'false' });

  useEffect(() => {
    if (duration > 0) {
      const intervalID = setInterval(() => {
        setCountDown(prevCountDown => {
          const expCountdown = prevCountDown - 1000;

          if (expCountdown === 0) {
            if (onTimeOut) onTimeOut();
            if (isAutoRefresh) return duration;
            clearInterval(intervalID);
          }

          return expCountdown;
        });
      }, 1000);
      return () => clearInterval(intervalID);
    }

    return () => {};
  }, []);

  return <Text style={style}>{countDownText}</Text>;
};

Timer.propTypes = {
  duration: PropTypes.number,
  onTimeOut: PropTypes.func.isRequired,
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  isAutoRefresh: PropTypes.bool,
};

Timer.defaultProps = {
  style: {},
  isAutoRefresh: false,
  duration: 10000,
};

export default Timer;
