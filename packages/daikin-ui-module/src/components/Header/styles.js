import { StyleSheet } from 'react-native';
import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lightCoolGrey,
    paddingHorizontal: 10,
    paddingVertical: 15,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  text: {
    fontSize: 18,
    paddingLeft: 5,
    color: Colors.darkGrey,
  },
  marginLeft: {
    marginLeft: 15,
  },
  button: {
    width: 50,
  },
  paddingLeft: {
    paddingLeft: 15,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
