import React from 'react';
import { StatusBar, SafeAreaView, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Colors from '../../shared/constants/color';

import { Text } from '../Text';
import styles from './styles';

const Header = ({ onPress, icon, text, customTextStyles, disabled }) => (
  <SafeAreaView>
    <StatusBar backgroundColor={Colors.black} />
    <View style={styles.container}>
      <View style={styles.row}>
        <View style={styles.button}>
          <TouchableOpacity
            onPress={onPress}
            style={styles.marginLeft}
            disabled={disabled}
            hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}>
            {icon && icon}
          </TouchableOpacity>
        </View>
        <Text
          regular
          style={[styles.text, icon && styles.paddingLeft, customTextStyles]}>
          {text}
        </Text>
      </View>
    </View>
  </SafeAreaView>
);

Header.propTypes = {
  onPress: PropTypes.func.isRequired,
  icon: PropTypes.element.isRequired,
  customTextStyles: PropTypes.objectOf(PropTypes.object),
  text: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
};

Header.defaultProps = {
  customTextStyles: {},
};

export default Header;
