import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Linking,
} from 'react-native';
import PropTypes from 'prop-types';
import Hyperlink from 'react-native-hyperlink';

import styles from './fallbackStyles';

const FallBackComponent = ({ onReload }) => (
  <SafeAreaView style={styles.container}>
    <ScrollView contentContainerStyle={styles.scrollView}>
      <View style={styles.content}>
        <Text style={styles.title}>Oops!</Text>
        <Hyperlink
          linkStyle={styles.emailLink}
          onPress={url => Linking.openURL(url)}
          linkText={url =>
            url.includes('mailto:') ? url.replace('mailto:', '') : url
          }>
          <Text style={styles.subtitle}>
            We&apos;re sorry that you have to see this. There seems to be an
            error. Feel free to tap on the button below to reload the app. If
            this keeps happening, please reach out to us at
            mailto:customer_service@daikin.com.my
          </Text>
        </Hyperlink>
      </View>
    </ScrollView>
    <View style={styles.bottomContainer}>
      <TouchableOpacity style={styles.button} onPress={onReload}>
        <Text style={styles.buttonText}>Reload</Text>
      </TouchableOpacity>
    </View>
  </SafeAreaView>
);

FallBackComponent.propTypes = {
  onReload: PropTypes.func.isRequired,
};

export default FallBackComponent;
