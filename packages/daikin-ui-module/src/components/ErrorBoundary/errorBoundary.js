import React from 'react';
import PropTypes from 'prop-types';
import RNRestart from 'react-native-restart';
import SplashScreen from 'react-native-splash-screen';

import DefaultFallbackComponent from './fallback';

export default class ErrorBoundary extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { error: null, hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { error, hasError: true };
  }

  static getDerivedStateFromProps(props) {
    if (props.otherError) {
      return { error: new Error(props.otherError), hasError: true };
    }

    return null;
  }

  componentDidMount() {
    const { hasError } = this.state;
    // Hide the splash screen if error happens at the initial render
    if (hasError) {
      SplashScreen.hide();
    }
  }

  componentDidCatch(error, errorInfo) {
    const { onError } = this.props;
    if (onError) {
      onError.call(this, error, errorInfo.componentStack);
    }
  }

  handleReload = () => {
    const { onReload } = this.props;
    if (onReload) {
      onReload();
    }
  };

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      const { FallbackComponent } = this.props;
      const { error } = this.state;
      // You can render any custom fallback UI
      return <FallbackComponent error={error} onReload={this.handleReload} />;
    }

    return children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  FallbackComponent: PropTypes.oneOfType([PropTypes.element, PropTypes.func]),
  onError: PropTypes.func.isRequired,
  onReload: PropTypes.func,
  otherError: PropTypes.string,
};

ErrorBoundary.defaultProps = {
  FallbackComponent: DefaultFallbackComponent,
  onReload: () => RNRestart.Restart(),
  otherError: '',
};
