export const inquiryType = [
  {
    label: 'Sales',
    value: 'Sales',
  },
  {
    label: 'Service and Warranty',
    value: 'Service and Warranty',
  },
  {
    label: 'Spare Parts',
    value: 'Spare Parts',
  },
  {
    label: 'Career',
    value: 'Career',
  },
];

export const intervalType = [
  {
    label: 'Weekly',
    value: {
      frequency: 'weekly',
      interval: 1,
    },
  },
  {
    label: 'Monthly',
    value: {
      frequency: 'monthly',
      interval: 1,
    },
  },
  {
    label: 'Quarterly',
    value: {
      frequency: 'monthly',
      interval: 3,
    },
  },
  {
    label: 'Half-yearly',
    value: {
      frequency: 'monthly',
      interval: 6,
    },
  },
  {
    label: 'Yearly',
    value: {
      frequency: 'yearly',
      interval: 1,
    },
  },
];
