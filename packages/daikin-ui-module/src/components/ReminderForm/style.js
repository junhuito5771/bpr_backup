import { StyleSheet } from 'react-native';
import { scale, ScaleText, verticalScale } from '@module/utility';
import Colors from '../../shared/constants/color';

const styles = StyleSheet.create({
  sectionHeader: {
    height: verticalScale(30),
    backgroundColor: Colors.modeSeparatorGrey,
    justifyContent: 'center',
  },
  sectionContainer: {
    paddingHorizontal: scale(10),
    paddingVertical: verticalScale(10),
  },
  sectionTitle: {
    color: Colors.sectionHeaderTitleGrey,
    fontWeight: '400',
    paddingHorizontal: scale(10),
  },
  stickBottom: {
    paddingBottom: verticalScale(22),
    paddingHorizontal: scale(10),
  },
  reminderTitle: {
    color: Colors.darkGrey,
    fontWeight: '700',
    fontSize: ScaleText(14),
    marginVertical: verticalScale(15),
  },
  inputLabel: {
    fontSize: ScaleText(12),
    fontWeight: '500',
    paddingBottom: verticalScale(5),
    color: Colors.black,
  },
  reminderLabel: {
    fontWeight: '400',
    fontSize: ScaleText(14),
    color: Colors.darkGrey,
  },
  flex: {
    flex: 1,
  },
  row: {
    marginVertical: verticalScale(15),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default styles;
