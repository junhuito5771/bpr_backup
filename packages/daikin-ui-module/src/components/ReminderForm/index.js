import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import {
  getCurrentDateInCalendarFormat,
  getCalendarDateAfterInterval,
} from '@module/utility';
import ToggleSwitch from 'toggle-switch-react-native';

import { intervalType } from './constants';
import Colors from '../../shared/constants/color';
import FormikButton from '../Button/formikButton';
import FormikTextInput from '../TextInput/formikTextInput';
import TextInput from '../TextInput';
import { FormikDropdown } from '../Dropdown';
import CalendarPicker from '../CalendarPicker';
import styles from './style';

const ReminderForm = ({ reminder, isGuestReminder, onSubmit, validator }) => {
  const currentDate = getCurrentDateInCalendarFormat();

  const initialValues = reminder
    ? {
        ...reminder,
        nextFilter: getCalendarDateAfterInterval(
          reminder.nextFilter,
          reminder.filterInterval
        ),
        nextService: getCalendarDateAfterInterval(
          reminder.nextService,
          reminder.serviceInterval
        ),
      }
    : {
        unitName: '',
        purchaseDate: currentDate,
        reminderName: '',
        filterInterval: intervalType[0].value,
        nextFilter: currentDate,
        serviceInterval: intervalType[0].value,
        nextService: currentDate,
        setReminder: false,
      };
  const reminderInputRef = React.useRef(null);
  const [switchEnabled, setSwitchEnabled] = React.useState(
    initialValues.setReminder
  );

  const handleSubmit = form => {
    onSubmit(form);
  };

  const renderAircondInfo = () => (
    <>
      <View style={styles.sectionHeader}>
        <Text style={styles.sectionTitle}>Aircond Info</Text>
      </View>
      <View style={styles.sectionContainer}>
        {isGuestReminder ? (
          <>
            <Text style={styles.inputLabel}>Unit Name</Text>
            <FormikTextInput
              name="unitName"
              placeholder="Unit Name"
              textContentType="none"
              keyboardType="default"
              returnKeyType="next"
              hasSpaced
              onSubmitEditing={() => reminderInputRef.current.focus()}
            />
            <CalendarPicker name="purchaseDate" label="Purchase Date" />
          </>
        ) : (
          <>
            <FormikDropdown name="unitName" label="Unit Name" items={[]} />
            <Text style={styles.inputLabel}>Purchase Date</Text>
            <TextInput disabled />
          </>
        )}
      </View>
    </>
  );

  const renderAircondMaintenance = setFieldValue => (
    <>
      <View style={styles.sectionHeader}>
        <Text style={styles.sectionTitle}>AC Maintenance Reminder</Text>
      </View>
      <View style={styles.sectionContainer}>
        <View>
          <Text style={styles.inputLabel}>Reminder Name</Text>
          <FormikTextInput
            name="reminderName"
            placeholder="Reminder Name"
            textContentType="none"
            keyboardType="default"
            returnKeyType="next"
            hasSpaced
            forwardRef={reminderInputRef}
            onSubmitEditing={() => {}}
          />
        </View>
        <Text style={styles.reminderTitle}>AC Filter Cleaning</Text>
        <FormikDropdown
          name="filterInterval"
          label="Maintenance Interval"
          items={intervalType}
        />
        <CalendarPicker name="nextFilter" label="Next Reminder" />
        <Text style={styles.reminderTitle}>Regular AC Cleaning</Text>
        <FormikDropdown
          name="serviceInterval"
          label="Maintenance Interval"
          items={intervalType}
        />
        <CalendarPicker name="nextService" label="Next Reminder" />
        <View style={[styles.row, styles.flex]}>
          <Text style={[styles.reminderLabel]}>Reminder</Text>
          <ToggleSwitch
            isOn={switchEnabled}
            onColor={Colors.green}
            offColor={Colors.lightGrey}
            onToggle={() => {
              setFieldValue('setReminder', !switchEnabled);
              setSwitchEnabled(!switchEnabled);
            }}
          />
        </View>
      </View>
    </>
  );

  return (
    <ScrollView style={styles.container}>
      <Formik
        enableReinitialize
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validate={validator}>
        {({ setFieldValue }) => (
          <>
            {renderAircondInfo()}
            {renderAircondMaintenance(setFieldValue)}
            <View style={styles.stickBottom}>
              <FormikButton primary disabledPrimary>
                Save
              </FormikButton>
            </View>
          </>
        )}
      </Formik>
    </ScrollView>
  );
};

ReminderForm.propTypes = {
  reminder: PropTypes.objectOf(PropTypes.object),
  isGuestReminder: PropTypes.bool,
  onSubmit: PropTypes.func,
  validator: PropTypes.func.isRequired,
};

ReminderForm.defaultProps = {
  reminder: null,
  isGuestReminder: true,
  onSubmit: () => {},
};

export default ReminderForm;
