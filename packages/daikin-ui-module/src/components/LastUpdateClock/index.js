import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import { heightPercentage } from '@module/utility';
import { Text } from '../Text';
import Colors from '../../shared/constants/color';

const styles = {
  center: {
    alignSelf: 'center',
  },
  updateText: {
    marginVertical: heightPercentage(0.5),
    color: Colors.lightWarmGrey,
  },
};

const LastUpdateClock = ({ time }) => (
  <View style={styles.center}>
    <Text mini style={styles.updateText}>{`--- Last updated at ${moment(
      time
    ).format('HH:mm')} ---`}</Text>
  </View>
);

LastUpdateClock.propTypes = {
  time: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

export default LastUpdateClock;
