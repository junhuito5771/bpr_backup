import React, { useState } from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import DatePicker from 'react-native-date-picker';
import Colors from '../../shared/constants/color';

import { Text } from '../Text';
import styles from './styles';

const TimerPicker = ({ isVisible, onClose }) => {
  const [time, setTime] = useState({ date: new Date() });

  return (
    <Modal
      isVisible={isVisible}
      style={styles.alignBottom}
      coverScreen
      hasBackdrop
      backdropColor={Colors.modalBackdrop}
      onBackdropPress={onClose}>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={onClose} hitSlop={styles.hitslop}>
            <Text medium bold style={styles.buttonText}>
              Done
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.center}>
          <DatePicker
            locale="fr"
            mode="time"
            date={time.date}
            onDateChange={date => setTime({ date })}
          />
        </View>
      </View>
    </Modal>
  );
};

TimerPicker.propTypes = {
  isVisible: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
};

TimerPicker.defaultProps = {
  isVisible: false,
};

export default TimerPicker;
