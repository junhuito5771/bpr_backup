import { StyleSheet } from 'react-native';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  alignBottom: {
    margin: 0,
  },
  header: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'flex-end',
    shadowColor: Colors.black,
    borderBottomColor: Colors.borderlightGray,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  buttonText: {
    color: Colors.blue,
    marginRight: 5,
  },
  hitslop: {
    top: 20,
    bottom: 20,
    right: 20,
    left: 20,
  },
});
