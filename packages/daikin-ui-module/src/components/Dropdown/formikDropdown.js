import * as React from 'react';
import { getIn, connect } from 'formik';
import PropTypes from 'prop-types';

import { StyleSheet } from 'react-native';
import { ScaleText, verticalScale } from '@module/utility';
import Colors from '../../shared/constants/color';
import Dropdown from './dropdown';

const styles = StyleSheet.create({
  label: {
    fontSize: ScaleText(12),
    fontWeight: '500',
    paddingBottom: verticalScale(5),
    color: Colors.darkGrey,
  },
});

const FormikDropdown = ({
  formik: { values, setFieldValue },
  name,
  label,
  items,
  onValueChange,
  placeholder,
  disabled,
  navigateTo,
}) => {
  const value = getIn(values, name);

  const handleChange = React.useCallback(
    v => {
      setFieldValue(name, v);
      onValueChange(v);
    },
    [value]
  );

  return (
    <Dropdown
      labelStyle={styles.label}
      label={label}
      items={items}
      onValueChange={handleChange}
      placeholder={placeholder}
      disabled={disabled}
      navigateTo={navigateTo}
    />
  );
};

FormikDropdown.propTypes = {
  formik: PropTypes.objectOf(PropTypes.object).isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object),
  onValueChange: PropTypes.func,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  navigateTo: PropTypes.func,
};

FormikDropdown.defaultProps = {
  label: '',
  items: [],
  placeholder: '',
  disabled: false,
  navigateTo: null,
  onValueChange: () => {},
};

export default connect(FormikDropdown);
