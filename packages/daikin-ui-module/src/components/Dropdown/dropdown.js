import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import RNPickerSelect from 'react-native-picker-select';

import Colors from '../../shared/constants/color';

import styles from './style';

const pickerCss = StyleSheet.create({
  inputIOS: {
    height: 45,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.lightCoolGrey,
    color: Colors.darkGrey,
    paddingLeft: 6,
    paddingRight: 6,
  },
  inputAndroid: {
    height: 45,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.lightCoolGrey,
    color: Colors.black,
    paddingLeft: 6,
    paddingRight: 6,
  },
  placeholder: {
    color: Colors.darkGrey,
  },
  iconContainer: {
    color: Colors.darkGrey,
    height: '100%',
    justifyContent: 'center',
    right: 18,
  },
});

const Dropdown = ({
  label,
  items,
  onValueChange,
  placeholder,
  disabled,
  navigateTo,
  labelStyle,
}) => {
  const placeholderObj = placeholder
    ? {
        label: placeholder,
        value: null,
        color: '#000',
      }
    : {};

  return (
    <View style={styles.container}>
      <Text style={[styles.label, labelStyle]}>{label}</Text>
      {navigateTo ? (
        <TouchableWithoutFeedback onPress={navigateTo}>
          <View style={[pickerCss.inputIOS, styles.picker]}>
            <Text style={styles.pickerText}>{placeholder}</Text>
            <View style={styles.pickerIcon}>
              <Image
                source={require('../../shared/assets/general/dropdown-dark.png')}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      ) : (
        <RNPickerSelect
          useNativeAndroidPickerStyle={false}
          style={pickerCss}
          placeholderTextColor={Colors.black}
          onValueChange={onValueChange}
          items={items}
          placeholder={placeholderObj}
          disabled={disabled}
          Icon={() => (
            <Image
              source={require('../../shared/assets/general/dropdown-dark.png')}
            />
          )}
        />
      )}
    </View>
  );
};

Dropdown.propTypes = {
  label: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object),
  onValueChange: PropTypes.func,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  navigateTo: PropTypes.func,
  labelStyle: PropTypes.objectOf(PropTypes.object).isRequired,
};

Dropdown.defaultProps = {
  label: '',
  items: [],
  placeholder: '',
  disabled: false,
  navigateTo: null,
  onValueChange: () => {},
};

export default Dropdown;
