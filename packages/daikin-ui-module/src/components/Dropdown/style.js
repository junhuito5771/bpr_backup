import { StyleSheet } from 'react-native';
import { ScaleText, verticalScale } from '@module/utility';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  container: {
    marginVertical: verticalScale(8),
  },
  label: {
    fontSize: ScaleText(12),
    fontWeight: '500',
    paddingBottom: verticalScale(5),
    color: Colors.black,
  },
  picker: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pickerText: {
    color: Colors.black,
  },
  pickerIcon: {
    flex: 1,
    alignItems: 'flex-end',
    right: 12,
  },
  error: {
    color: Colors.red,
    fontSize: 12,
    paddingVertical: 2,
  },
});
