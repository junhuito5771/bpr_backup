import React, { useEffect, useState, useRef, memo } from 'react';
import PropTypes from 'prop-types';

import { Text } from '../Text';
import styles from './styles';

function renderCond(prevProps, nextProps) {
  return prevProps.countDown === nextProps.countDown;
}

function getTimeText(time) {
  if (time < 0) return '';

  return ` (${time})`;
}

const CountdownText = memo(({ countDown, onFinish, style, desc }) => {
  const [time, setTime] = useState(countDown);
  const timerIntervalHandler = useRef();

  useEffect(() => {
    timerIntervalHandler.current = setInterval(() => {
      setTime(prevTime => prevTime - 1);
    }, 1000);

    return () => {
      if (timerIntervalHandler.current) {
        clearInterval(timerIntervalHandler.current);
      }
    };
  }, [countDown]);

  useEffect(() => {
    if (time < 0) {
      clearInterval(timerIntervalHandler.current);
    }

    if (time === 0 && onFinish) {
      onFinish();
    }
  }, [time]);

  return (
    <Text style={[styles.text, style]}>{`${desc}${getTimeText(time)}`}</Text>
  );
}, renderCond);

CountdownText.propTypes = {
  countDown: PropTypes.number,
  onFinish: PropTypes.func.isRequired,
  style: PropTypes.objectOf(PropTypes.object).isRequired,
  desc: PropTypes.string.isRequired,
};

CountdownText.defaultProps = {
  countDown: 3,
};

export default CountdownText;
