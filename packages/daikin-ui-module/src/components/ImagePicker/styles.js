import { StyleSheet } from 'react-native';
import { ScaleText, verticalScale, scale } from '@module/utility';
import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.offWhite,
    borderRadius: 4,
    borderColor: Colors.stoneGrey,
    borderWidth: 1,
    marginVertical: verticalScale(13),
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: verticalScale(13),
  },
  label1: {
    fontSize: ScaleText(12),
    fontWeight: '700',
    color: Colors.darkGrey,
  },
  label2: {
    fontSize: ScaleText(10),
  },
  closeButton: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: 5,
    right: 5,
    backgroundColor: Colors.white,
    borderRadius: 100,
    width: scale(24),
    height: scale(24),
    zIndex: 1,
  },
  attachmentImage: {
    height: verticalScale(122),
    width: scale(157),
    borderRadius: 4,
    marginEnd: scale(13),
  },
  image: {
    flex: 1,
    height: '100%',
    width: '100%',
    borderRadius: 4,
  },
  error: {
    color: Colors.red,
    fontSize: 12,
    paddingVertical: 2,
  },
});
