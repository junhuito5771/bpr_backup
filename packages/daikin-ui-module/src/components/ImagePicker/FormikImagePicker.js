import React from 'react';
import {
  Alert,
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect, getIn } from 'formik';
import ImagePicker from 'react-native-image-picker';
import PropTypes from 'prop-types';

import styles from './styles';

const FormikImagePicker = ({
  formik: { values, setFieldValue, setFieldTouched, errors, touched },
  name,
}) => {
  const error = getIn(errors, name);
  const touch = getIn(touched, name);
  const value = getIn(values, name);
  const isInvalid = error && touch;

  const handleOnPress = React.useCallback(() => {
    ImagePicker.launchImageLibrary(
      {
        noData: false,
        mediaType: 'mixed',
      },
      response => {
        if (response.error) {
          Alert.alert('Upload error', response.error);
        } else if (!response.didCancel) {
          const source = {
            uri:
              Platform.OS === 'android'
                ? response.uri
                : response.uri.replace('file://', ''),
            type: response.type,
            name: response.fileName,
          };

          let isDuplicate = false;
          // eslint-disable-next-line no-restricted-syntax
          for (const val of value) {
            if (val.uri === source.uri) {
              isDuplicate = true;
              break;
            }
          }

          if (isDuplicate) {
            Alert.alert('Duplicate File', 'File has already been uploaded');
          } else {
            const newValue = [...value, source];
            setFieldValue(name, newValue, true);
          }
        }

        setFieldTouched(name, true, false);
      }
    );
  }, [values]);

  const handleRemove = React.useCallback(
    uri => {
      const newValue = [...value].filter(data => data.uri !== uri);
      setFieldValue(name, newValue, true);
    },
    [values]
  );

  return (
    <>
      {!!value.length && (
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {value.map((data, index) => (
            <View key={`${data}_${index + 1}`} style={styles.attachmentImage}>
              <TouchableOpacity
                onPress={() => handleRemove(data.uri)}
                style={styles.closeButton}>
                <Image
                  source={require('../../shared/assets/general/close.png')}
                />
              </TouchableOpacity>
              <Image source={{ uri: data.uri }} style={styles.image} />
            </View>
          ))}
        </ScrollView>
      )}
      <TouchableOpacity style={styles.container} onPress={handleOnPress}>
        <View style={styles.textContainer}>
          <Text style={styles.label1}>Upload image/video</Text>
          <Text style={styles.label2}>Total file limit not more than 10MB</Text>
        </View>
      </TouchableOpacity>
      {!!isInvalid && <Text style={styles.error}>{error}</Text>}
    </>
  );
};

FormikImagePicker.propTypes = {
  formik: PropTypes.shape({
    setFieldValue: PropTypes.func,
    setFieldTouched: PropTypes.func,
    validateForm: PropTypes.func,
    values: PropTypes.objectOf(PropTypes.object),
    errors: PropTypes.objectOf(PropTypes.object),
    touched: PropTypes.objectOf(PropTypes.object),
  }).isRequired,
  name: PropTypes.string.isRequired,
};

export default connect(FormikImagePicker);
