import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Svg, { G, Path, Defs } from 'react-native-svg';
import { widthPercentage } from '@module/utility';

import styles from './styles';

const DottedLine = ({ style }) => {
  const containerStyle = StyleSheet.flatten([styles.dottedLine, style]);

  return (
    <View style={containerStyle}>
      <Svg width={widthPercentage(80)} height={10}>
        <G>
          <Path
            d="M4.5 4.093H340"
            stroke="#9B9B9B"
            strokeWidth={0.5}
            strokeLinecap="square"
            strokeDasharray="2 7"
          />
        </G>
        <Defs />
      </Svg>
    </View>
  );
};

DottedLine.propTypes = {
  style: PropTypes.objectOf(PropTypes.object),
};

DottedLine.defaultProps = {
  style: {},
};

export default DottedLine;
