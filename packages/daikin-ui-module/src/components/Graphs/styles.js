import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  dottedLine: {
    position: 'absolute',
    top: 45,
    left: 10,
  },
});
