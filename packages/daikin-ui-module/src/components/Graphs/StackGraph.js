import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { widthPercentage, heightPercentage } from '@module/utility';

import Colors from '../../shared/constants/color';
import { Text } from '../Text';

const styles = StyleSheet.create({
  bottomLabel: {
    position: 'absolute',
    top: 70,
    left: widthPercentage(32),
  },
  label: {
    position: 'absolute',
    top: 5,
    left: widthPercentage(30),
  },
  bottomLabelText: {
    fontSize: 10,
    color: Colors.turquoise,
  },
  labelText: {
    fontSize: 10,
    color: Colors.pictonBlue,
  },
  hourLabelTemp: {
    position: 'absolute',
    bottom: heightPercentage(3),
    right: 0,
  },
  hourLabelText: {
    fontSize: 11,
    color: Colors.lightWarmGrey,
  },
});

const labels = {
  daily: 'Hour',
  weekly: 'Day',
  monthly: 'Date',
  yearly: 'Month',
};

const StackGraph = props => {
  const { data, selectedPeriod, containerStyle, renderLine } = props;

  return (
    <View style={containerStyle}>
      <View style={styles.hourLabelTemp}>
        <Text style={styles.hourLabelText}>{labels[selectedPeriod]}</Text>
      </View>
      {renderLine(data, props)}
    </View>
  );
};

StackGraph.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  selectedPeriod: PropTypes.string.isRequired,
  renderLine: PropTypes.func,
  containerStyle: PropTypes.objectOf(PropTypes.object),
};

StackGraph.defaultProps = {
  data: [],
  renderLine: () => {},
  containerStyle: {},
};

export default StackGraph;
