import { StyleSheet } from 'react-native';
import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  wrapper: {
    flexDirection: 'row',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  topBorder: {
    height: 0.5,
  },
  linearGradient: {
    width: 0.5,
  },
  inner: {
    padding: 2,
  },
  label: {
    fontSize: 12,
    marginTop: 5,
    color: Colors.tabBarLabelGrey,
  },
  imageIcon: {
    padding: 5,
  },
  touchable: {
    width: '100%',
    height: '100%',
  },
});
