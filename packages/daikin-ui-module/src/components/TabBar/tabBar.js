import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { viewportWidth } from '@module/utility';
import Colors from '../../shared/constants/color';

import { Text } from '../Text';

import styles from './styles';

const TRANSPARENT_GRADIENT_BORDER = [
  Colors.white,
  Colors.borderlightGray,
  Colors.white,
];

const TabBar = ({ style, children }) => {
  const wraperStyle = StyleSheet.flatten([styles.flex, styles.wrapper]);
  const flattenChildren = React.Children.toArray(children);

  const renderItems = items =>
    items.map((item, index) => {
      const propDetails = {
        key: item.key,
        hasRightDivider: index !== items.length - 1,
        width:
          items.length > 0
            ? viewportWidth / Math.min(5.5, items.length)
            : viewportWidth,
        onPress: () => {
          if (item.props.onPress) item.props.onPress();
        },
      };

      if (React.isValidElement(item.props.children)) {
        const newProps = {
          ...propDetails,
          children: item.props.children,
        };
        return React.cloneElement(item, newProps);
      }

      return React.cloneElement(item, propDetails);
    });

  return (
    <View style={style}>
      <LinearGradient
        colors={TRANSPARENT_GRADIENT_BORDER}
        style={styles.topBorder}
      />
      <ScrollView horizontal scrollEnabled={flattenChildren.length > 5}>
        <View style={wraperStyle}>{renderItems(flattenChildren)}</View>
      </ScrollView>
    </View>
  );
};

TabBar.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
};

TabBar.defaultProps = {
  style: {},
};

TabBar.Item = ({
  imgSource,
  onPress,
  hasRightDivider,
  width,
  label,
  children,
}) => {
  const itemStyle = StyleSheet.flatten([styles.center, { width }]);

  return (
    <>
      <View style={itemStyle}>
        <TouchableOpacity
          style={[styles.center, styles.touchable]}
          activeOpacity={0.5}
          onPress={onPress}>
          {React.isValidElement(children) ? (
            children
          ) : (
            <View style={[styles.center, styles.inner]}>
              <Image
                style={styles.imageIcon}
                source={imgSource}
                resizeMode="cover"
              />
              {!!label && <Text style={styles.label}>{label}</Text>}
            </View>
          )}
        </TouchableOpacity>
      </View>
      {hasRightDivider && (
        <LinearGradient
          colors={TRANSPARENT_GRADIENT_BORDER}
          style={styles.linearGradient}
        />
      )}
    </>
  );
};

TabBar.displayName = 'TabBar';
TabBar.Item.displayName = 'TabBarItem';

TabBar.Item.propTypes = {
  imgSource: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired,
  hasRightDivider: PropTypes.bool,
  width: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
};

TabBar.Item.defaultProps = {
  hasRightDivider: false,
};

export default TabBar;
