import { StyleSheet, Platform } from 'react-native';
import { TAB_ITEM_HEIGHT } from '../../Modal/constants';
import Colors from '../../../shared/constants/color';

export default StyleSheet.create({
  container: {},
  flex: {
    flex: 1,
  },
  groupHeader: {
    backgroundColor: Colors.sectionHeaderGrey,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  groupHeaderTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.sectionHeaderTitleGrey,
  },
  groupContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
  },
  itemContainer: {
    height: TAB_ITEM_HEIGHT,
    width: 20,
  },
  fullTouchable: {
    height: '100%',
    width: '100%',
  },
  iconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  linearGradient: {
    width: 0.5,
  },
  topBorder: {
    height: Platform.select({ ios: 0.5, android: 1, default: 1 }),
  },
});
