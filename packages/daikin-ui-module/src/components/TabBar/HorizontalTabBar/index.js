import React, { createContext, useContext } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { viewportWidth } from '@module/utility';

import { Text } from '../../Text';
import Colors from '../../../shared/constants/color';
import styles from './style';

const TRANSPARENT_GRADIENT_BORDER = [
  Colors.white,
  Colors.borderlightGray,
  Colors.white,
];

const TabBarContext = createContext();

const TabBar = ({ style, children, context }) => (
  <TabBarContext.Provider value={context}>
    <View style={[styles.flex, style]}>
      <LinearGradient
        colors={TRANSPARENT_GRADIENT_BORDER}
        style={styles.topBorder}
      />
      {children}
    </View>
  </TabBarContext.Provider>
);

TabBar.propTypes = {
  style: PropTypes.object,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  context: PropTypes.object,
};

TabBar.displayName = 'TabBar';

const renderTabItems = (items, nrItemPerRow, isFixedWidth) => {
  const totalSpacing = 0.5 * items.length;
  const dividerNr = isFixedWidth
    ? nrItemPerRow
    : Math.min(nrItemPerRow, items.length);

  return items.map((item, index) => {
    const propDetails = {
      key: item.key,
      nrItemPerRow,
      nrItems: items.length,
      hasRightDivider: (index + 1) % nrItemPerRow !== 0,
      width:
        items.length > 0
          ? (viewportWidth - totalSpacing) / dividerNr
          : viewportWidth,
      onPress: () => {
        if (item.props.onPress) item.props.onPress();
      },
    };

    if (React.isValidElement(item.props.children)) {
      const newProps = {
        ...propDetails,
        children: item.props.children,
      };
      return React.cloneElement(item, newProps);
    }

    return React.cloneElement(item, propDetails);
  });
};

TabBar.Group = ({
  title,
  children,
  sectionStyles,
  nrItemPerRow,
  isFixedWidth,
  canRenderHandler,
}) => {
  const flattenChildren = React.Children.toArray(children);

  if (canRenderHandler(children)) {
    return (
      <View>
        {title && (
          <View style={[styles.groupHeader, sectionStyles]}>
            <Text style={styles.groupHeaderTitle}>{title}</Text>
          </View>
        )}
        <View style={styles.groupContainer}>
          {renderTabItems(flattenChildren, nrItemPerRow, isFixedWidth)}
        </View>
      </View>
    );
  }

  return null;
};

TabBar.Group.defaultProps = {
  nrItemPerRow: 5,
  canRenderHandler: () => true,
};

TabBar.Group.propTypes = {
  title: PropTypes.string,
  nrItemPerRow: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
  sectionStyles: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.arrayOf(PropTypes.object),
  ]),
  isFixedWidth: PropTypes.bool,
  canRenderHandler: PropTypes.func,
};

TabBar.Icon = ({ source, containerStyle, style, ...extraProps }) => (
  <View style={[styles.iconContainer, containerStyle]}>
    <Image style={style} source={source} {...extraProps} />
  </View>
);

TabBar.Icon.displayName = 'TabBarIcon';

TabBar.Icon.propTypes = {
  width: PropTypes.number,
  containerStyle: PropTypes.object,
  source: PropTypes.number,
  style: PropTypes.object,
};

TabBar.Item = ({
  children,
  containerStyles,
  width,
  onPress,
  hasRightDivider,
  icon,
  iconStyle,
}) => (
  <>
    <View style={[styles.itemContainer, containerStyles, { width }]}>
      <TouchableOpacity style={styles.fullTouchable} onPress={onPress}>
        {!!icon && <TabBar.Icon source={icon} style={iconStyle} />}
        {typeof children === 'function' ? children() : children}
      </TouchableOpacity>
    </View>
    {hasRightDivider && (
      <LinearGradient
        colors={TRANSPARENT_GRADIENT_BORDER}
        style={styles.linearGradient}
      />
    )}
  </>
);

TabBar.displayName = 'TabBar';
TabBar.Item.displayName = 'TabBarItem';

TabBar.Item.propTypes = {
  onPress: PropTypes.func,
  hasRightDivider: PropTypes.bool,
  width: PropTypes.number,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  containerStyles: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.arrayOf(PropTypes.object),
  ]),
  icon: PropTypes.number,
  iconStyle: PropTypes.object,
};

TabBar.Model = ({ component }) => {
  const { modelRef } = useContext(TabBarContext);

  modelRef.current.setPage([component]);
};

TabBar.Model.displayName = 'TabBarModel';

export default TabBar;
