import { StyleSheet } from 'react-native';
import { ScaleText } from '@module/utility';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  text: {
    backgroundColor: 'transparent',
    color: Colors.black,
  },
  mini: {
    fontSize: ScaleText(11),
  },
  small: {
    fontSize: ScaleText(13),
  },
  medium: {
    fontSize: ScaleText(16),
  },
  large: {
    fontSize: ScaleText(24),
  },
  bold: {
    fontFamily: 'Roboto-Bold',
  },
  light: {
    fontFamily: 'Roboto-Light',
  },
  regular: {
    fontFamily: 'Roboto',
  },
  error: {
    color: Colors.red,
    paddingVertical: 5,
  },
  success: {
    color: Colors.blue,
    paddingVertical: 5,
  },
  headerText: {
    fontFamily: 'Roboto',
    fontSize: ScaleText(16),
    fontWeight: '400',
  },
});
