import { StyleSheet, Platform } from 'react-native';
import Colors from '../../../shared/constants/color';

const contentFont = Platform.OS === 'android' && { fontFamily: 'Roboto' };

export default StyleSheet.create({
  textStyle: {
    ...contentFont,
    color: Colors.black,
    fontWeight: '400',
    fontSize: 16,
    margin: 0,
    marginLeft: 10,
    height: 40,
    lineHeight: 40,
  },
});
