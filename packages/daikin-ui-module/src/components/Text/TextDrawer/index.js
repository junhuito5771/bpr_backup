import React from 'react';
import PropTypes from 'prop-types';
import { Text as TextRN } from 'react-native';
import styles from './styles';

const Text = ({ children }) => (
  <TextRN maxFontSizeMultiplier={1} style={styles.textStyle}>
    {children}
  </TextRN>
);

Text.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.number,
  ]).isRequired,
};

export default Text;
