import React from 'react';
import PropTypes from 'prop-types';
import { Text as TextRN, StyleSheet } from 'react-native';

import styles from './styles';

const Text = ({
  mini,
  small,
  medium,
  large,
  bold,
  light,
  error,
  success,
  style,
  children,
  color,
  numberOfLines,
  fullText
}) => {
  const textStyle = StyleSheet.flatten([
    styles.text,
    styles.regular,
    mini && styles.mini,
    small && styles.small,
    medium && styles.medium,
    large && styles.large,
    light && styles.light,
    bold && styles.bold,
    error && styles.error,
    success && styles.success,
    color && { color },
    style,
  ]);

  const isFullText = () => {
    return fullText ? 0 : numberOfLines
  }

  return (
    <TextRN
      style={textStyle}
      numberOfLines={fullText ? 0 : numberOfLines}
      maxFontSizeMultiplier={1}
      allowFontScaling={false}>
      {children}
    </TextRN>
  );
};

Text.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.number,
  ]).isRequired,
  mini: PropTypes.bool,
  small: PropTypes.bool,
  medium: PropTypes.bool,
  large: PropTypes.bool,
  bold: PropTypes.bool,
  error: PropTypes.bool,
  success: PropTypes.bool,
  color: PropTypes.string,
  numberOfLines: PropTypes.number,
  light: PropTypes.bool,
  fullText: PropTypes.bool,
};

Text.defaultProps = {
  style: {},
  mini: false,
  small: false,
  medium: false,
  large: false,
  bold: false,
  error: false,
  success: false,
  numberOfLines: 5,
  light: false,
  color: '',
  fullText: false
};

export default Text;
