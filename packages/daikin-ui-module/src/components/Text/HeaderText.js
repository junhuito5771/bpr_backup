import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

import styles from './styles';

const HeaderText = ({ style, children }) => (
  <Text style={[style, styles.headerText]} maxFontSizeMultiplier={1}>
    {children}
  </Text>
);

HeaderText.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
};

HeaderText.defaultProps = {
  style: {},
};

export default HeaderText;
