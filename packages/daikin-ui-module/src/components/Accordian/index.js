import React from 'react';
import PropTypes from 'prop-types';
import Accordian from 'react-native-collapsible/Accordion';

const RNAccordian = ({ children, ...props }) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Accordian {...props}>{children}</Accordian>
);

RNAccordian.propTypes = {
  children: PropTypes.node.isRequired,
};

export default RNAccordian;
