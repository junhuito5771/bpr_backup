import { StyleSheet } from 'react-native';
import { verticalScale, scale } from '@module/utility';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    borderRadius: scale(4),
    marginBottom: 4,
  },
  image: {
    height: verticalScale(150),
    borderTopLeftRadius: scale(4),
    borderTopRightRadius: scale(4),
  },
  label: {
    flexDirection: 'row',
    alignItems: 'center',
    height: verticalScale(40),
  },
  labelText: {
    marginStart: scale(20),
    flexGrow: 0.8,
  },
  dotContainer: {
    marginBottom: -verticalScale(30),
  },
  dot: {
    borderRadius: 10,
    width: scale(6),
    height: scale(6),
    backgroundColor: Colors.lightWarmGrey,
    marginHorizontal: -scale(6),
  },
  activeDot: {
    borderRadius: 10,
    width: scale(12),
    height: scale(6),
    backgroundColor: Colors.blue,
    marginHorizontal: -scale(6),
  },
  shadow: {
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.25,
    elevation: 2,
  },
});
