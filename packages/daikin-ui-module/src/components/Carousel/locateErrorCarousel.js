import React from 'react';
import PropTypes from 'prop-types';
import { Dimensions, ScrollView, StyleSheet } from 'react-native';
import { scale } from '@module/utility';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import Colors from '../../shared/constants/color';

const { width: windowWidth } = Dimensions.get('window');

const styles = StyleSheet.create({
  dotContainer: {
    backgroundColor: Colors.onOffBlue,
  },
  dot: {
    borderRadius: 10,
    width: scale(6),
    height: scale(6),
    backgroundColor: Colors.lightWarmGrey,
    marginHorizontal: -scale(6),
  },
  activeDot: {
    borderRadius: 10,
    width: scale(12),
    height: scale(6),
    backgroundColor: Colors.blue,
    marginHorizontal: -scale(6),
  },
});

const LocateErrorCarousel = ({ carouselRef, data, onSnapToItem }) => {
  const [currentIndex, setCurrentIndex] = React.useState(0);

  const handleSnapToItem = React.useCallback(slideIndex => {
    setCurrentIndex(slideIndex);
    onSnapToItem(slideIndex);
  }, []);

  const renderPagination = React.useMemo(
    () => (
      <>
        <Pagination
          dotsLength={data.length}
          activeDotIndex={currentIndex}
          inactiveDotStyle={styles.dot}
          dotStyle={styles.activeDot}
          containerStyle={styles.dotContainer}
          inactiveDotScale={1}
        />
        {data[currentIndex].remark && data[currentIndex].remark}
      </>
    ),
    [data, currentIndex]
  );

  const handleRenderItem = React.useCallback(
    ({ item }) => (
      <>
        {item.instruction}
        {item.description}
      </>
    ),
    [data]
  );

  return (
    <ScrollView>
      <Carousel
        ref={carouselRef}
        data={data}
        onSnapToItem={handleSnapToItem}
        renderItem={handleRenderItem}
        sliderWidth={windowWidth}
        itemWidth={windowWidth}
        scrollEnabled={false}
      />
      {renderPagination}
    </ScrollView>
  );
};

LocateErrorCarousel.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  carouselRef: PropTypes.objectOf(PropTypes.object).isRequired,
  onSnapToItem: PropTypes.func.isRequired,
};

export default LocateErrorCarousel;
