import React, { useEffect } from 'react';
import { StyleSheet, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect, getIn } from 'formik';
import { verticalScale } from '@module/utility';
import Colors from '../../shared/constants/color';

import TextInput from './textInput';

const styles = StyleSheet.create({
  label: {
    fontSize: 12,
    fontWeight: '500',
    paddingBottom: verticalScale(5),
    color: Colors.darkGrey,
  },
});

const FormikTextInput = ({
  label,
  name,
  formik,
  validator,
  forwardRef,
  submitOnEditEnd,
  validateOnChange,
  forceValue,
  ...extraProps
}) => {
  const error = getIn(formik.errors, name);
  const touch = getIn(formik.touched, name);
  const value = getIn(formik.values, name);

  const errorMsg = touch && error ? error : '';

  const handleSetFieldValue = text => {
    formik.setFieldValue(name, validator(text));
    if (validateOnChange) formik.setFieldTouched(name, true);
  };

  const handleOnChange = validator
    ? text => handleSetFieldValue(text)
    : formik.handleChange(name);

  useEffect(() => {
    if (forceValue) {
      handleOnChange(forceValue);
    }
  }, [forceValue]);

  return (
    <>
      {!!label && <Text style={styles.label}>{label}</Text>}
      <TextInput
        ref={forwardRef}
        value={value}
        errorMsg={errorMsg}
        onBlur={formik.handleBlur(name)}
        onChangeText={handleOnChange}
        onSubmitEditing={formik.handleSubmit}
        {...extraProps}
      />
    </>
  );
};

FormikTextInput.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  formik: PropTypes.object.isRequired,
  validator: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  forwardRef: PropTypes.any,
  submitOnEditEnd: PropTypes.bool,
  initialValue: PropTypes.string,
  validateOnChange: PropTypes.bool,
  forceValue: PropTypes.string,
};

FormikTextInput.defaultProps = {
  label: '',
  submitOnEditEnd: false,
  initialValue: '',
  forceValue: '',
  name: '',
  validateOnChange: false,
  forwardRef: () => {},
  validator: false,
};

export default connect(FormikTextInput);
