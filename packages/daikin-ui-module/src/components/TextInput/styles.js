import { StyleSheet, Platform } from 'react-native';

import Colors from '../../shared/constants/color';
import { ScaleText } from '@module/utility';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    paddingBottom: 18,
  },
  textinput: {
    height: 40,
    flex: 1,
  },
  hasSpaced: {
    paddingBottom: 18,
  },
  errorMsg: {
    color: Colors.red,
    fontSize: 12,
    paddingVertical: 2,
  },
  inner: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.lightCoolGrey,
    paddingHorizontal: 10,
  },
  center: {
    justifyContent: 'center',
  },
  countryCode: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  number: {
    paddingBottom: Platform.OS === 'ios' ? 0 : 1, // ORIGINAL 1 : -4
    paddingLeft: 5,
	fontSize: Platform.OS === 'ios' ? ScaleText(12) : ScaleText(13),
	paddingTop: Platform.OS === 'ios' ? 1 : 0,
  },
  disabled: {
    color: Colors.lightGrey,
  },
  hitSlop: {
    top: 10,
    right: 10,
    left: 10,
    bottom: 10,
  },
});
