import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TextInput as TextInputRN,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { DeviceInfo } from '@module/utility';

import { Text } from '../Text';
import styles from './styles';

const brandsNeedingWorkaround = ['redmi', 'xiaomi', 'poco', 'pocophone'];
const needsXiaomiWorkaround =
  brandsNeedingWorkaround.includes(DeviceInfo.getBrand().toLowerCase()) &&
  Platform.Version > 28;

const EntypoIcon = ({ onPress }) => {
  const [isShow, setIsShow] = useState(false);

  const entypoIconPath = isShow
    ? require('../../shared/assets/entypo/viewOn.png')
    : require('../../shared/assets/entypo/viewOff.png');

  const toggleEntypo = () => {
    setIsShow(prevIsShow => !prevIsShow);
    if (onPress) onPress();
  };

  return (
    <TouchableOpacity
      style={styles.center}
      onPress={toggleEntypo}
      hitSlop={styles.hitSlop}>
      <Image source={entypoIconPath} />
    </TouchableOpacity>
  );
};

EntypoIcon.propTypes = {
  onPress: PropTypes.func.isRequired,
};

const TextInput = React.forwardRef(
  (
    {
      style,
      innerStyle,
      textInputstyle,
      hasSpaced,
      leftIcon,
      textContentType,
      errorMsg,
      disabled,
      onFocus,
      rightComponent,
      caretHidden,
      ...extraProps
    },
    ref
  ) => {
    const [isShowPassword, setisShowPassword] = useState(false);
    const [hackCaretHidden, setHackCaretHidden] = useState(
      needsXiaomiWorkaround ? true : caretHidden
    );

    const containerStyle = StyleSheet.flatten([
      styles.container,
      // Use error message padding when there is error message
      errorMsg && hasSpaced && styles.hasSpaced,
      style,
    ]);
    const wrapperStyle = StyleSheet.flatten([styles.inner, innerStyle]);
    const textInputStyle = StyleSheet.flatten([
      styles.textinput,
      disabled && styles.disabled,
      textInputstyle,
    ]);

    const isPasswordInput = textContentType === 'password';
    const isPhoneNumberInput = textContentType === 'telephoneNumber';

    const errorTextStyle = StyleSheet.flatten([styles.errorMsg]);

    const handleShowPassword = () => {
      setisShowPassword(prevIsShowPassword => !prevIsShowPassword);
    };

    const handleFocus = useCallback(() => {
      if (needsXiaomiWorkaround) {
        setHackCaretHidden(caretHidden);
      }
      if (onFocus) onFocus();
    }, [onFocus, caretHidden]);

    return (
      <View style={containerStyle}>
        <View style={wrapperStyle}>
          {isPhoneNumberInput && (
            <View style={styles.countryCode}>
              <Image
                source={require('../../shared/assets/general/malaysia.jpg')}
              />
              <Text small style={styles.number}>
                +60
              </Text>
            </View>
          )}
          <TextInputRN
            maxFontSizeMultiplier={1}
            ref={ref}
            style={textInputStyle}
            textContentType={textContentType}
            secureTextEntry={isPasswordInput && !isShowPassword}
            editable={!disabled}
            onFocus={handleFocus}
            caretHidden={hackCaretHidden}
            {...extraProps}
          />
          {isPasswordInput && <EntypoIcon onPress={handleShowPassword} />}
          {React.isValidElement(rightComponent) && rightComponent}
        </View>
        {!!errorMsg && <Text style={errorTextStyle}>{errorMsg}</Text>}
      </View>
    );
  }
);

TextInput.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  innerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  textInputstyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  leftIcon: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  textContentType: PropTypes.string,
  hasSpaced: PropTypes.bool,
  errorMsg: PropTypes.string,
  disabled: PropTypes.bool,
  onFocus: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  rightComponent: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.element,
    PropTypes.node,
  ]),
  caretHidden: PropTypes.bool,
};

TextInput.defaultProps = {
  style: {},
  innerStyle: {},
  textInputstyle: {},
  caretHidden: false,
  leftIcon: [],
  textContentType: '',
  hasSpaced: false,
  errorMsg: '',
  disabled: false,
  rightComponent: null,
  onFocus: false,
};

export default TextInput;
