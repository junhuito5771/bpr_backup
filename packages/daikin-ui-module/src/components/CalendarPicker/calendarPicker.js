import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Image, TouchableWithoutFeedback, View } from 'react-native';
import { getIn, connect } from 'formik';
import { getCurrentDateInCalendarFormat } from '@module/utility';

import { Text } from '../Text';
import CalendarModal from './calendarModal';
import styles from './style';

const CalendarPicker = ({ formik: { values, setFieldValue }, name, label }) => {
  const value = getIn(values, name);

  const currentDate = getCurrentDateInCalendarFormat();

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleOpenCalendar = React.useCallback(
    date => {
      if (date && isModalOpen) {
        setFieldValue(name, date);
      }
      setIsModalOpen(!isModalOpen);
    },
    [isModalOpen, value]
  );

  return (
    <>
      <Text style={styles.label}>{label}</Text>
      <TouchableWithoutFeedback onPress={handleOpenCalendar}>
        <View style={[styles.row, styles.border]}>
          <Text style={styles.value}>{value || currentDate}</Text>
          <Image
            style={styles.icon}
            source={require('../../shared/assets/general/date.png')}
          />
        </View>
      </TouchableWithoutFeedback>
      <CalendarModal isVisible={isModalOpen} onClose={handleOpenCalendar} />
    </>
  );
};

CalendarPicker.propTypes = {
  formik: PropTypes.objectOf(PropTypes.object).isRequired,
  name: PropTypes.string,
  label: PropTypes.string,
};

CalendarPicker.defaultProps = {
  name: '',
  label: '',
};

export default connect(CalendarPicker);
