import React, { useState } from 'react';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import { Calendar } from 'react-native-calendars';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { verticalScale, scale, ScaleText } from '@module/utility';
import Colors from '../../shared/constants/color';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  content: {
    backgroundColor: Colors.white,
    paddingBottom: verticalScale(40),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    marginHorizontal: scale(25),
    marginVertical: verticalScale(20),
  },
  label: {
    flex: 1,
    fontWeight: '700',
    fontSize: ScaleText(14),
    color: Colors.black,
  },
  buttonText: {
    color: Colors.blue,
    fontSize: ScaleText(14),
  },
});

const markedStyles = {
  container: {
    borderRadius: 0,
    backgroundColor: Colors.dayBlue,
  },
  text: {
    color: Colors.white,
  },
};

const CalendarModal = ({ isVisible, onClose }) => {
  const date = new Date();
  const [markedDates, setMarkedDates] = useState({
    [`${date.getFullYear()}-${date.getMonth() + 1}-${
      date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
    }`]: {
      selected: true,
      customStyles: markedStyles,
    },
  });

  const handleSelectDate = React.useCallback(() => {
    // Pass selected date back to picker
    const selectedDate = Object.keys(markedDates).length
      ? Object.keys(markedDates)[0]
      : null;
    onClose(selectedDate);
  }, [markedDates]);

  const handleOnClose = React.useCallback(() => {
    onClose();
  }, [onClose]);

  const handleDayPress = React.useCallback(
    day => {
      setMarkedDates({
        [day.dateString]: {
          selected: true,
          customStyles: markedStyles,
        },
      });
    },
    [markedDates]
  );

  return (
    <Modal
      style={styles.container}
      hasBackdrop
      coverScreen
      isVisible={isVisible}
      onBackdropPress={handleOnClose}
      onBackButtonPress={handleOnClose}
      useNativeDriver={Platform.OS === 'android'}>
      <View style={styles.content}>
        <View style={[styles.row, styles.header]}>
          <Text style={styles.label}>Select Date</Text>
          <TouchableOpacity onPress={handleSelectDate}>
            <Text style={styles.buttonText}>Next</Text>
          </TouchableOpacity>
        </View>
        <Calendar
          hideExtraDays
          firstDay={1}
          markingType="custom"
          onDayPress={handleDayPress}
          markedDates={markedDates}
          minDate={new Date()}
        />
      </View>
    </Modal>
  );
};

CalendarModal.propTypes = {
  isVisible: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
};

CalendarModal.defaultProps = {
  isVisible: false,
};

export default CalendarModal;
