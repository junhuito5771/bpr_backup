import { StyleSheet } from 'react-native';
import { ScaleText, scale, verticalScale } from '@module/utility';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    fontSize: ScaleText(12),
    fontWeight: '500',
    paddingBottom: verticalScale(5),
    color: Colors.darkGrey,
  },
  value: {
    flexGrow: 1,
  },
  icon: {
    right: scale(6),
  },
  border: {
    height: 45,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.lightCoolGrey,
    color: Colors.black,
    paddingLeft: 6,
    paddingRight: 6,
  },
});
