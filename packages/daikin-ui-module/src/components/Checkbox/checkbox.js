import React from 'react';
import { TouchableWithoutFeedback, View, Image } from 'react-native';
import PropTypes from 'prop-types';

import { Text } from '../Text';
import styles from './styles';

const Checkbox = ({ children, value, onPress, disabled }) => {
  const checkboxIconPath = value
    ? require('../../shared/assets/general/checkBoxOn.png')
    : require('../../shared/assets/general/checkBoxOff.png');

  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={onPress} disabled={disabled}>
        <View style={styles.wrapper}>
          <Image source={checkboxIconPath} />
          <Text style={styles.label}>{children}</Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

Checkbox.propTypes = {
  children: PropTypes.string,
  value: PropTypes.bool.isRequired,
  onPress: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Checkbox.defaultProps = {
  disabled: false,
};

export default Checkbox;
