import { StyleSheet } from 'react-native';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  label: {
    marginLeft: 15,
    color: Colors.midLightGrey,
  },
  container: {
    flexDirection: 'row',
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
