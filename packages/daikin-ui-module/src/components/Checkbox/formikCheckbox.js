import React from 'react';
import PropTypes from 'prop-types';
import { connect, getIn } from 'formik';

import Checkbox from './checkbox';

const FormikCheckbox = ({ name, formik, ...extraProps }) => {
  const value = getIn(formik.values, name);
  const handleOnPress = () => formik.setFieldValue(name, !value);

  return <Checkbox onPress={handleOnPress} value={value} {...extraProps} />;
};

FormikCheckbox.propTypes = {
  name: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  formik: PropTypes.object.isRequired,
};

export default connect(FormikCheckbox);
