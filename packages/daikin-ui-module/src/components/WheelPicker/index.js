import React from 'react';
import { Picker, Platform, StyleSheet } from 'react-native';
import { WheelPicker as PickerAndroid } from '@delightfulstudio/react-native-wheel-picker-android';
import PropTypes from 'prop-types';

import styles from './styles';

const WheelPicker = ({
  data,
  selectedItemPosition,
  onValueChange,
  style,
  ...restProps
}) => {
  if (Platform.OS === 'android') {
    return (
      <PickerAndroid
        visibleItemCount={7}
        isCurtain
        isCurved
        selectedItemTextColor="black"
        itemTextColor="lightgrey"
        renderIndicator
        indicatorColor="lightgrey"
        itemTextAlign="center"
        data={data}
        selectedItemPosition={selectedItemPosition}
        onItemSelected={({ position }) => onValueChange(0, position)}
        style={StyleSheet.flatten([styles.wheelPicker, style])}
        {...restProps}
      />
    );
  }

  return (
    <Picker
      selectedValue={selectedItemPosition}
      onValueChange={onValueChange}
      style={StyleSheet.flatten([styles.iosWheelPicker, style])}
      {...restProps}>
      {data.map((label, index) => (
        <Picker.Item key={label} label={label} value={index} />
      ))}
    </Picker>
  );
};

WheelPicker.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  selectedItemPosition: PropTypes.number.isRequired,
  onValueChange: PropTypes.func.isRequired,
};

WheelPicker.defaultProps = {
  style: {},
};

export default WheelPicker;
