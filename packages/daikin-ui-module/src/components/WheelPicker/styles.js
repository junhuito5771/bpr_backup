import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wheelPicker: {
    height: 250,
    width: 200,
  },
});
