import React from 'react';
import PropTypes from 'prop-types';
import { connect, userProfile } from '@module/redux';
import { Image, View } from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';

import styles from './styles';

const Drawer = ({ isLogin, logOut, ...props }) => (
  <DrawerContentScrollView {...props}>
    <View style={styles.logoContainer}>
      <Image
        source={require('common/shared/images/logo/daikin.png')}
        style={styles.logo}
      />
    </View>
    <DrawerItemList {...props} />
  </DrawerContentScrollView>
);

Drawer.propTypes = {
  isLogin: PropTypes.bool,
  logOut: PropTypes.func,
};

const mapStateToProps = ({ login: loginState }) => {
  const { isLogin } = loginState;

  return { isLogin };
};

const mapDispatchToProps = dispatch => ({
  logOut: () => {
    dispatch(userProfile.logOut());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);
