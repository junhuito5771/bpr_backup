/* eslint-disable react/prop-types */
import React from 'react';
import { View, Image, Alert } from 'react-native';

import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import {
  CommonActions,
  DrawerActions,
  useLinkBuilder,
} from '@react-navigation/native';
import { DrawerMenuIcon, DrawerMenuText, DrawerDropDownMenu } from '../Menu';

import styles from './styles';

function convertRouteToObj(routes) {
  let routeInObj = {};

  routes.forEach((route, index) => {
    routeInObj = {
      ...routeInObj,
      [route.name]: { ...route, index },
    };
  });

  return routeInObj;
}

function Content(props) {
  const {
    state,
    navigation,
    descriptors,
    activeTintColor,
    inactiveTintColor,
    activeBackgroundColor,
    inactiveBackgroundColor,
    itemStyle,
    labelStyle,
    items,
  } = props;

  const { routes, history } = state;

  const previousRouteData =
    history && history.length > 0 ? history[history.length - 2] : undefined;

  const previousRoute =
    previousRouteData !== undefined &&
    routes.find(route => route.key === previousRouteData.key);

  const isEditing =
    previousRoute !== undefined &&
    previousRoute.params &&
    previousRoute.params.isEditing;

  const previousRouteName =
    previousRoute !== undefined &&
    previousRoute.params &&
    previousRoute.params.screen;

  const routeKeyMap = convertRouteToObj(routes);

  const buildLink = useLinkBuilder();
  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.logoContainer}>
        <Image
          source={require('../../../shared/assets/logo/daikin.png')}
          style={styles.logo}
        />
      </View>
      {items.map(menuItem => {
        if (menuItem.customComponent) {
          const CustomComponent = menuItem.customComponent;
          const itemProps = {
            ...menuItem,
            ...props,
            icon: () => (
              <DrawerMenuIcon
                icon={menuItem.icon}
                {...(menuItem.iconStyle !== undefined && {
                  style: menuItem.iconStyle,
                })}
              />
            ),
            label: () => <DrawerMenuText label={menuItem.label} />,
            key: menuItem.name,
          };

          return (
            <CustomComponent
              key={menuItem.name}
              itemProps={itemProps}
              navigation={navigation}
            />
          );
        }

        if (menuItem.childDrawer) {
          return (
            <DrawerDropDownMenu
              {...props}
              {...menuItem}
              isEditing={isEditing}
              previousRouteName={previousRouteName}
              items={menuItem.childDrawer}
              key="DropDownMenu"
            />
          );
        }
        const route = routeKeyMap[menuItem.name];
        if (!route) return null;

        const focused = route.index === state.index;
        const { title, drawerLabel, drawerIcon } = descriptors[
          route.key
        ].options;
        const label =
          // eslint-disable-next-line no-nested-ternary
          drawerLabel !== undefined
            ? drawerLabel
            : title !== undefined
            ? title
            : route.name;

        return (
          <DrawerItem
            focused={focused}
            key={route.key}
            label={label}
            style={[styles.row, itemStyle, focused && styles.isFocused]}
            icon={drawerIcon}
            activeTintColor={activeTintColor}
            inactiveTintColor={inactiveTintColor}
            activeBackgroundColor={activeBackgroundColor}
            inactiveBackgroundColor={inactiveBackgroundColor}
            labelStyle={labelStyle}
            to={buildLink(route.name, route.params)}
            onPress={() => {
              if (isEditing) {
                Alert.alert(
                  'Are you sure?',
                  `You have unsaved changes on your ${previousRouteName}. Do you want to leave before saving?`,
                  [
                    {
                      text: 'No',
                      onPress: () => {
                        navigation.dispatch({
                          ...DrawerActions.closeDrawer(),
                          target: state.key,
                        });
                      },
                    },
                    {
                      text: 'Yes',
                      onPress: () => {
                        navigation.dispatch({
                          ...CommonActions.navigate(route.name),
                          target: state.key,
                        });
                      },
                    },
                  ]
                );
              } else {
                navigation.dispatch({
                  ...(focused
                    ? DrawerActions.closeDrawer()
                    : CommonActions.navigate(route.name)),
                  target: state.key,
                });
              }
            }}
          />
        );
      })}
    </DrawerContentScrollView>
  );
}

export default Content;
/* eslint-enable react/prop-types */
