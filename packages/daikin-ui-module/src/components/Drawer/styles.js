import { Platform, StyleSheet } from 'react-native';
import { verticalScale } from '@module/utility';
import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  logoContainer: {
    paddingHorizontal: 40,
    paddingTop: Platform.OS === 'ios' ? 0 : verticalScale(50),
    paddingBottom: 30,
  },
  logo: {
    width: 200,
    height: 60,
  },
  text: {
    fontWeight: '400',
    fontSize: 16,
    margin: 0,
    marginLeft: 10,
  },
  row: {
    justifyContent: 'center',
    height: 40,
    color: Colors.black,
    width: '100%',
    borderRadius: 0,
    marginHorizontal: 0,
    marginVertical: 0,
  },
  icon: {
    flexBasis: 1,
    left: 15,
    alignItems: 'center',
  },
  opaque: {
    opacity: 1,
  },
  nestedDrawer: {
    paddingStart: 20,
    backgroundColor: Colors.sailBlue,
  },
  nestedTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  dropdownIcon: {
    marginLeft: 'auto',
  },
  isFocused: {
    backgroundColor: Colors.babyBlue,
  },
});
