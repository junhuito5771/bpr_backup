import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image } from 'react-native';

import styles from './styles';

const DrawerMenu = ({ onPress }) => (
  <View style={styles.button}>
    <TouchableOpacity
      onPress={onPress}
      style={styles.marginLeft}
      hitSlop={styles.hitSlop}>
      <Image source={require('../../../shared/assets/menu/menu.png')} />
    </TouchableOpacity>
  </View>
);

DrawerMenu.propTypes = {
  onPress: PropTypes.func.isRequired,
};

export default DrawerMenu;
