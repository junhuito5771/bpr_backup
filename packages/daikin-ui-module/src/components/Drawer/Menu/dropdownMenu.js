import React, { useState } from 'react';
import { View, StyleSheet, Image, Alert } from 'react-native';
import PropTypes from 'prop-types';

import { DrawerItem } from '@react-navigation/drawer';
import { CommonActions, DrawerActions } from '@react-navigation/native';
import DrawerMenuIcon from './menuIcon';
import DrawerMenuText from './menuText';

import Colors from '../../../shared/constants/color';

const styles = StyleSheet.create({
  row: {
    justifyContent: 'center',
    height: 40,
    color: Colors.black,
    width: '100%',
    borderRadius: 0,
    marginHorizontal: 0,
    marginVertical: 0,
  },
  nestedTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  dropdownIcon: {
    marginLeft: 'auto',
  },
  nestedDrawer: {
    paddingStart: 20,
    backgroundColor: Colors.sailBlue,
  },
});

export default function DropDownDrawerMenu(props) {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  const {
    navigation,
    state,
    items,
    icon,
    label,
    isEditing,
    previousRouteName,
  } = props;
  return (
    <>
      <DrawerItem
        style={[styles.row]}
        label={() => (
          <View style={styles.nestedTitle}>
            <DrawerMenuText label={label} />
            <View style={styles.dropdownIcon}>
              <Image
                source={
                  isDropdownOpen
                    ? require('../../../shared/assets/menu/dropdownOn.png')
                    : require('../../../shared/assets/menu/dropdown.png')
                }
              />
            </View>
          </View>
        )}
        icon={() => <DrawerMenuIcon icon={icon} />}
        onPress={() => {
          setIsDropdownOpen(!isDropdownOpen);
        }}
      />
      <View style={styles.nestedDrawer}>
        {isDropdownOpen &&
          items.map(item => {
            const focused = state.routeNames[state.index] === item.name;

            const itemProps = {
              ...item,
              ...props,
              focused,
              icon: () => (
                <DrawerMenuIcon
                  icon={item.icon}
                  {...(item.iconStyle !== undefined && {
                    style: item.iconStyle,
                  })}
                />
              ),
              label: () => <DrawerMenuText label={item.label} />,
              key: item.name,
              onPress: () => {
                if (isEditing) {
                  Alert.alert(
                    'Are you sure?',
                    `You have unsaved changes on your ${previousRouteName}. Do you want to leave before saving?`,
                    [
                      {
                        text: 'No',
                        onPress: () => {
                          navigation.dispatch({
                            ...DrawerActions.closeDrawer(),
                            target: state.key,
                          });
                        },
                      },
                      {
                        text: 'Yes',
                        onPress: () => {
                          navigation.dispatch({
                            ...CommonActions.navigate(item.name),
                            target: state.key,
                          });
                        },
                      },
                    ]
                  );
                } else {
                  navigation.dispatch({
                    ...(focused
                      ? DrawerActions.closeDrawer()
                      : CommonActions.navigate(item.name)),
                    target: state.key,
                  });
                }
              },
            };

            return <DrawerItem {...itemProps} style={[styles.row]} />;
          })}
      </View>
    </>
  );
}

DropDownDrawerMenu.propTypes = {
  navigation: PropTypes.object,
  state: PropTypes.object,
  items: PropTypes.array,
  icon: PropTypes.any,
  label: PropTypes.string,
  isEditing: PropTypes.bool,
  previousRouteName: PropTypes.string,
};
