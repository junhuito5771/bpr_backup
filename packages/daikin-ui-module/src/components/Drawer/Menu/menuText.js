import React from 'react';
import { StyleSheet } from 'react-native';

import { Text } from '../../Text';

const styles = StyleSheet.create({
  drawerLabel: {
    paddingLeft: 10,
  },
});

export default function MenuText({ label }) {
  return <Text style={styles.drawerLabel}>{label}</Text>;
}
