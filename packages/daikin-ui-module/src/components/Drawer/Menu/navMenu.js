import React from 'react';
import PropTypes from 'prop-types';
import { DrawerActions } from '@react-navigation/native';

import Menu from './menu';

const NavDrawerMenu = ({ navigation }) => (
  <Menu
    onPress={() => {
      navigation.dispatch(DrawerActions.toggleDrawer());
    }}
  />
);

NavDrawerMenu.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  navigation: PropTypes.object.isRequired,
};

export default NavDrawerMenu;
