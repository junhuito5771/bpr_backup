import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  marginLeft: {
    marginLeft: 15,
  },
  button: {
    width: 50,
    alignItems: 'center',
  },
  paddingLeft: {
    paddingLeft: 20,
  },
  hitSlop: { top: 20, bottom: 20, left: 50, right: 50 },
});
