import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  icon: {
    left: 15,
    alignItems: 'center',
    justifyContent: 'center',
    height: 20,
    width: 20,
  },
  iconImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  }
});

export default function menuIcon({ icon, style }) {
  return (
    <View style={[styles.icon]}>
      <Image source={icon} style={style || styles.iconImage} />
    </View>
  );
}

menuIcon.propTypes = {
  style: PropTypes.object,
};
