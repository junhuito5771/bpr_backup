import React, { useState, useEffect } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Collapsible from 'react-native-collapsible';

import { Text } from '../Text';

import styles from './styles';

const COLLAPSIBLE_BUTTONS = [
  require('../../shared/assets/general/uncollapse.png'),
  require('../../shared/assets/general/collapse.png'),
];

const CollapsibleHeader = ({ title, count, isCollapsible, onPress }) => (
  <View style={[styles.header, styles.rowContainer]}>
    <TouchableOpacity
      onPress={onPress}
      style={[styles.flex, styles.justifyCenter]}>
      <View style={[styles.flexRow, styles.alignCenter]}>
        <Image
          source={COLLAPSIBLE_BUTTONS[Number(isCollapsible)]}
          style={styles.collapseButton}
        />
        <View style={[styles.flexRow, styles.alignCenter]}>
          <Text medium>{title}</Text>
          <Text small> ({count})</Text>
        </View>
      </View>
    </TouchableOpacity>
  </View>
);

CollapsibleHeader.propTypes = {
  title: PropTypes.string.isRequired,
  count: PropTypes.number,
  isCollapsible: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
};

CollapsibleHeader.defaultProps = {
  isCollapsible: false,
  count: 0,
};

const CollapsibleContent = ({ isCollapsible, items, onPress, getIconPath }) => (
  <Collapsible collapsed={isCollapsible}>
    {items.map(item => {
      const { name, logo, thingName } = item;

      return (
        <View style={[styles.content, styles.rowContainer]} key={thingName}>
          <TouchableOpacity
            onPress={() => onPress(item)}
            style={[styles.flex, styles.justifyCenter]}>
            <View style={[styles.flexRow, styles.alignCenter]}>
              <Image
                source={getIconPath(logo)}
                style={styles.logoIcon}
                resizeMode="contain"
              />
              <Text medium>{name}</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    })}
  </Collapsible>
);

CollapsibleContent.propTypes = {
  items: PropTypes.arrayOf([PropTypes.object]).isRequired,
  isCollapsible: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  getIconPath: PropTypes.func.isRequired,
};

CollapsibleContent.defaultProps = {
  isCollapsible: false,
};

const CollapsibleContainer = ({ data, onPressItem, getIconPath }) => {
  const [activeSection, setActiveSection] = useState([]);

  useEffect(() => {
    if (data && data.length > -1) {
      setActiveSection(Array(data.length).fill(false));
    }
  }, [data]);

  const toggleCollapse = index => {
    setActiveSection(prevActiveSection => {
      const curActiveSection = [...prevActiveSection];
      curActiveSection[index] = !prevActiveSection[index];

      return curActiveSection;
    });
  };

  return data.map(({ title, data: items }, index) => (
    <React.Fragment key={title}>
      <CollapsibleHeader
        title={title}
        count={items.length}
        isCollapsible={activeSection[index]}
        onPress={() => toggleCollapse(index)}
      />
      <CollapsibleContent
        items={items}
        sectionIndex={index}
        isCollapsible={activeSection[index]}
        onPress={onPressItem}
        getIconPath={getIconPath}
      />
    </React.Fragment>
  ));
};

CollapsibleContainer.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  getIconPath: PropTypes.func,
  onPressItem: PropTypes.func,
};

CollapsibleContainer.defaultProps = {
  data: [],
};

export default CollapsibleContainer;
