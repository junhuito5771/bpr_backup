import { StyleSheet, Platform } from 'react-native';
import {
  heightPercentage,
  widthPercentage,
  isSmallDevice,
} from '@module/utility';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  flexRow: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
  },
  header: {
    backgroundColor: Colors.lightCoolGrey,
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  content: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
  },
  headerText: {},
  rowContainer: {
    height: isSmallDevice
      ? heightPercentage(10)
      : Platform.select({
          ios: heightPercentage(8),
          android: heightPercentage(10),
        }),
    paddingHorizontal: widthPercentage(5),
  },
  alignCenter: {
    alignItems: 'center',
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  spacebetween: {
    justifyContent: 'space-between',
  },
  collapseButton: {
    marginRight: 10,
    height: 10,
  },
  logoIcon: {
    marginRight: 15,
    minWidth: 56,
  },
});
