import { StyleSheet, Platform } from 'react-native';
import Colors from '../../shared/constants/color';

const contentFont = Platform.OS === 'android' && { fontFamily: 'Roboto' };

export default StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: Colors.blue,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 40,
    marginVertical: 5,
  },
  blueContainer: {
    backgroundColor: Colors.blue,
  },
  blueOutline: {
    borderWidth: 1,
    borderColor: Colors.blue,
    backgroundColor: Colors.white,
  },
  borderDisabled: {
    borderColor: Colors.lightCoolGrey,
  },
  textContainer: {
    ...contentFont,
    color: Colors.blue,
    fontWeight: 'bold',
  },
  white: {
    color: Colors.white,
  },
  blue: {
    color: Colors.blue,
  },
  disabled: {
    color: Colors.lightGrey,
  },
  disableBackground: {
    backgroundColor: Colors.lightGrey,
  },
});
