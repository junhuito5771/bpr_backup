import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
  },
  hitSlop: { top: 20, bottom: 20, left: 50, right: 50 },
});

const BackButton = ({ onPress, disabled, containerStyle }) => (
  <View style={[styles.button, containerStyle]}>
    <TouchableOpacity
      onPress={onPress}
      hitSlop={styles.hitSlop}
      disabled={disabled}>
      <Image source={require('../../shared/assets/general/back.png')} />
    </TouchableOpacity>
  </View>
);

BackButton.propTypes = {
  containerStyle: PropTypes.object,
  onPress: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

BackButton.defaultProps = {
  containerStyle: {},
  disabled: false,
};

export default BackButton;
