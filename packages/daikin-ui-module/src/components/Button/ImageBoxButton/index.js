import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import styles from './style';

const ImageBoxButton = ({ label, image, onPress, isSelected }) => {
  const handlePress = React.useCallback(() => {
    onPress();
  }, []);

  return (
    <TouchableOpacity onPress={handlePress}>
      <View style={[styles.container, isSelected && styles.selected]}>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={image} />
        </View>
        <Text style={[styles.label, isSelected && styles.selectedLabel]}>
          {label}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

ImageBoxButton.propTypes = {
  label: PropTypes.string,
  image: PropTypes.number,
  onPress: PropTypes.func,
  isSelected: PropTypes.bool,
};

export default ImageBoxButton;
