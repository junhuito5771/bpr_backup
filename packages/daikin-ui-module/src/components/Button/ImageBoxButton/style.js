import { Dimensions, StyleSheet } from 'react-native';
import { ScaleText, verticalScale, scale } from '@module/utility';

import Colors from '../../../shared/constants/color';

const { width: windowWidth } = Dimensions.get('window');

export default StyleSheet.create({
  default: {
    backgroundColor: Colors.lightWhite,
    color: Colors.black,
  },
  container: {
    height: verticalScale(188),
    width: windowWidth / 2.4,
    alignItems: 'center',
    justifyContent: 'center',
    margin: scale(7),
    borderRadius: 4,
    backgroundColor: Colors.lightWhite,
  },
  selected: {
    backgroundColor: Colors.blue,
  },
  imageContainer: {
    height: verticalScale(150),
  },
  label: {
    color: Colors.black,
    fontWeight: '700',
    fontSize: ScaleText(14),
  },
  selectedLabel: {
    color: Colors.white,
  },
});
