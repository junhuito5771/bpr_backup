import React from 'react';
import {
  ActivityIndicator,
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import Colors from '../../shared/constants/color';
import styles from './styles';

const Button = ({
  style,
  textStyle,
  children,
  disabled,
  onPress,
  primary,
  disabledPrimary,
  secondary,
  isLoading,
}) => {
  const isDisabledMode = disabled || isLoading;
  const containerStyles = StyleSheet.flatten([
    styles.container,
    primary && styles.blueContainer,
    secondary && styles.blueOutline,
    isDisabledMode && styles.borderDisabled,
    isDisabledMode && disabledPrimary && styles.disableBackground,
    style,
  ]);

  const textStyles = StyleSheet.flatten([
    styles.textContainer,
    primary && styles.white,
    secondary && styles.blue,
    isDisabledMode && styles.disabled,
    isDisabledMode && disabledPrimary && styles.white,
    textStyle,
  ]);

  return (
    <TouchableOpacity onPress={onPress} disabled={isDisabledMode}>
      <View style={containerStyles}>
        {isLoading ? (
          <ActivityIndicator size="small" color={Colors.grey} />
        ) : (
          <Text
            style={textStyles}
            maxFontSizeMultiplier={1}
            allowFontScaling={false}>
            {children}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  onPress: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  isLoading: PropTypes.bool,
  disabledPrimary: PropTypes.bool,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

Button.defaultProps = {
  disabled: false,
  primary: false,
  secondary: false,
  isLoading: false,
  disabledPrimary: false,
  style: {},
  textStyle: {},
};

export default Button;
