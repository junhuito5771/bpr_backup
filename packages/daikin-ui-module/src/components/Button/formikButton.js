import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'formik';

import Button from './button';

const FormikButton = ({ formik, hasValidation, disabled, ...extraProps }) => (
  <Button
    onPress={formik.handleSubmit}
    disabled={!formik.isValid || disabled}
    {...extraProps}
  />
);

FormikButton.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  formik: PropTypes.object.isRequired,
  hasValidation: PropTypes.bool,
  disabled: PropTypes.bool,
};

FormikButton.defaultProps = {
  hasValidation: false,
  disabled: false,
};

export default connect(FormikButton);
