import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet, Image } from 'react-native';

const styles = StyleSheet.create({
  marginRight: {
    marginRight: 25,
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
});

const HeaderIconButton = ({ onPress, src }) => (
  <TouchableOpacity
    style={styles.marginRight}
    hitSlop={styles.hitSlop}
    onPress={onPress}>
    <Image source={src} />
  </TouchableOpacity>
);

HeaderIconButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  src: Image.propTypes.source.isRequired,
};

export default HeaderIconButton;
