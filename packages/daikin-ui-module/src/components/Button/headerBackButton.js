import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';

import BackButton from './backButton';

const styles = StyleSheet.create({
  container: {
    width: 50,
  },
});

const HeaderBackButton = ({ navigation, disabled, onPress }) => (
  <BackButton
    containerStyle={styles.container}
    onPress={onPress || (() => navigation.goBack())}
    disabled={disabled}
  />
);

HeaderBackButton.propTypes = {
  navigation: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  onPress: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};

HeaderBackButton.defaultProps = {
  disabled: false,
  onPress: false,
};

export default HeaderBackButton;
