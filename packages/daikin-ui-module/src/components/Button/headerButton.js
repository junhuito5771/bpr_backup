import React from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import { ScaleText, widthPercentage } from '@module/utility';

import { Text } from '../Text';
import Colors from '../../shared/constants/color';

const styles = StyleSheet.create({
  saveButton: {
    paddingHorizontal: widthPercentage(3),
    alignItems: 'center',
  },
  saveButtonHitSlop: {
    top: 20,
    bottom: 20,
    left: 50,
    right: 50,
  },
  saveButtonText: {
    color: Colors.black,
    fontWeight: '400',
    fontSize: ScaleText(16),
  },
  marginRight: {
    marginRight: 15,
  },
  disabled: {
    color: Colors.grey,
  },
});

const HeaderButton = ({ onPress, disabled, isLoading, children }) => {
  const textStyles = StyleSheet.flatten([
    styles.saveButtonText,
    disabled && styles.disabled,
  ]);

  return (
    <View style={[styles.saveButton, styles.marginRight]}>
      <TouchableOpacity
        disabled={disabled}
        onPress={onPress}
        hitSlop={styles.saveButtonHitSlop}>
        {isLoading ? (
          <ActivityIndicator size="small" color={Colors.grey} />
        ) : (
          <Text style={textStyles}>{children}</Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

HeaderButton.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  onPress: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  isLoading: PropTypes.bool,
};

HeaderButton.defaultProps = {
  disabled: false,
  isLoading: false,
};

export default HeaderButton;
