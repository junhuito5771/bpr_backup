import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet, View } from 'react-native';
import { widthPercentage } from '@module/utility';

import { Text } from '../Text';

const styles = StyleSheet.create({
  errorButtonContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 4,
  },
  errorButtonStyle: {
    borderWidth: 1,
    borderRadius: 12,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: widthPercentage(8),
    paddingRight: widthPercentage(8),
  },
});

const ErrorButton = ({ onPress, errorCode, color }) => (
  <View style={styles.errorButtonContainer}>
    <TouchableOpacity
      onPress={onPress}
      style={[styles.errorButtonStyle, { borderColor: color }]}>
      <Text style={{ color }}>{`Error ${errorCode}`}</Text>
    </TouchableOpacity>
  </View>
);

ErrorButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  errorCode: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
};

export default ErrorButton;
