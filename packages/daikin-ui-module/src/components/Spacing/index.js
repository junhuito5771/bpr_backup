import React from 'react';
import { PropTypes } from 'prop-types';
import { View } from 'react-native';
import { verticalScale } from '@module/utility';

const Spacing = ({ unit }) => <View style={{ paddingBottom: unit }} />;

Spacing.propTypes = {
  unit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

Spacing.defaultProps = {
  unit: verticalScale(12),
};

export default Spacing;
