import { StyleSheet } from 'react-native';
import { heightPercentage } from '@module/utility';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  center: {
    alignItems: 'center',
  },
  linkContainer: {
    marginRight: 5,
  },
  link: {
    fontSize: 12,
  },
  timer: {
    marginTop: 5,
    color: Colors.blue,
  },
  grey: {
    color: Colors.lightGrey,
  },
  row: {
    marginTop: heightPercentage(3),
    flexDirection: 'row',
    alignItems: 'center',
  },
  messageContainer: {
    height: heightPercentage(6),
  },
});
