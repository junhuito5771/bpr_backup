import React from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator, Animated } from 'react-native';

import { Text } from '../Text';
import LinkText from '../Link';
import Colors from '../../shared/constants/color';

import styles from './styles';

const COUNT_DOWN_TIME = 60;

const CountDownMessage = ({ error, success, postAction }) => {
  const [fadeAnim, setFadeAnim] = React.useState(new Animated.Value(1));

  React.useEffect(() => {
    if (success || error) {
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 6000,
        useNativeDriver: true,
      }).start();
      setTimeout(() => {
        postAction();
        setFadeAnim(new Animated.Value(1));
      }, 7000);
    }
  }, [success, error]);

  return (
    <View style={styles.messageContainer}>
      <Animated.View style={[{ opacity: fadeAnim }]}>
        {!!success && <Text success>{success}</Text>}
        {!!error && <Text error>{error}</Text>}
      </Animated.View>
    </View>
  );
};

CountDownMessage.propTypes = {
  success: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
  postAction: PropTypes.func.isRequired,
};

const CountdownTimer = ({
  linkText,
  onPress,
  loading,
  error,
  success,
  postAction,
}) => {
  const [timer, setTimer] = React.useState(COUNT_DOWN_TIME);
  const [linkDisabled, setLinkDisabled] = React.useState(true);
  const timerRef = React.useRef();

  React.useEffect(() => {
    if (timer === 0) {
      clearInterval(timerRef.current);
      setLinkDisabled(false);
    } else {
      timerRef.current = setInterval(() => setTimer(timer - 1), 1000);
    }
    return () => {
      clearInterval(timerRef.current);
    };
  }, [timer]);

  const handleOnPress = () => {
    if (onPress) {
      onPress();
    }
    setTimer(COUNT_DOWN_TIME);
    setLinkDisabled(true);
  };

  return (
    <View style={styles.center}>
      <CountDownMessage
        success={success}
        error={error}
        postAction={postAction}
      />
      <View style={styles.row}>
        <LinkText
          style={styles.linkContainer}
          textStyle={[styles.link, linkDisabled && styles.grey]}
          disabled={linkDisabled}
          onPress={handleOnPress}>
          {linkText}
        </LinkText>
        {loading && <ActivityIndicator size="small" color={Colors.grey} />}
      </View>
      <Text small style={styles.timer}>
        {timer.toString()}s
      </Text>
    </View>
  );
};

CountdownTimer.propTypes = {
  onPress: PropTypes.func.isRequired,
  linkText: PropTypes.string.isRequired,
  loading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  postAction: PropTypes.func.isRequired,
};

CountdownTimer.defaultProps = {
  loading: false,
  success: '',
  error: '',
};

export default CountdownTimer;
