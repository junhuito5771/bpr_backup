import React from 'react';
import PropTypes from 'prop-types';
import { connect, getIn } from 'formik';

import Selector from './selector';

const FormikSelector = ({
  name,
  formik,
  validator,
  forwardRef,
  submitOnEditEnd,
  onSelectorPress,
  selectedValue,
  onChangeText,
  ...extraProps
}) => {
  const error = getIn(formik.errors, name);
  const touch = getIn(formik.touched, name);
  const value = getIn(formik.values, name);

  const errorMsg = touch && error ? error : '';

  React.useEffect(
    () =>
      selectedValue
        ? formik.setFieldValue(name, selectedValue)
        : formik.handleChange(name),
    [selectedValue]
  );

  const handleOnChange = React.useCallback(
    text => {
      onChangeText(text);
      if (validator) {
        formik.setFieldValue(name, validator(text));
      } else {
        formik.handleChange(name);
      }
    },
    [onChangeText]
  );

  const handleOnSelectorPress = React.useCallback(() => {
    onSelectorPress();
  }, [onSelectorPress]);

  return (
    <Selector
      ref={forwardRef}
      value={value}
      onSelectorPress={handleOnSelectorPress}
      errorMsg={errorMsg}
      onBlur={formik.handleBlur(name)}
      onChangeText={handleOnChange}
      onSubmitEditing={formik.handleSubmit}
      {...extraProps}
    />
  );
};

FormikSelector.propTypes = {
  name: PropTypes.string.isRequired,
  formik: PropTypes.objectOf(PropTypes.object).isRequired,
  validator: PropTypes.func.isRequired,
  forwardRef: PropTypes.objectOf(PropTypes.object).isRequired,
  submitOnEditEnd: PropTypes.bool,
  initialValue: PropTypes.string.isRequired,
  onSelectorPress: PropTypes.func.isRequired,
  onChangeText: PropTypes.func.isRequired,
  selectedValue: PropTypes.string.isRequired,
};

FormikSelector.defaultProps = {
  submitOnEditEnd: false,
};

export default connect(FormikSelector);
