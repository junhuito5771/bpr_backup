import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TextInput as TextInputRN,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import { Text } from '../Text';
import styles from './styles';

const Selector = React.forwardRef(
  (
    {
      style,
      innerStyle,
      textInputstyle,
      hasSpaced,
      leftIcon,
      textContentType,
      errorMsg,
      disabled,
      onSelectorPress,
      onChangeText,
      ...extraProps
    },
    ref
  ) => {
    const containerStyle = StyleSheet.flatten([
      styles.container,
      // Use error message padding when there is error message
      !errorMsg && hasSpaced && styles.hasSpaced,
      style,
    ]);
    const wrapperStyle = StyleSheet.flatten([styles.inner, innerStyle]);
    const textInputStyle = StyleSheet.flatten([
      styles.textinput,
      disabled && styles.disabled,
      textInputstyle,
    ]);

    const errorTextStyle = StyleSheet.flatten([styles.errorMsg]);

    return (
      <View style={containerStyle}>
        <View style={wrapperStyle}>
          <TextInputRN
            onChangeText={onChangeText}
            maxFontSizeMultiplier={1}
            ref={ref}
            style={textInputStyle}
            textContentType={textContentType}
            editable={!disabled}
            onFocus={() => {
              if (extraProps.onFocus) {
                extraProps.onFocus();
              }
            }}
            {...extraProps}
          />
          <TouchableOpacity onPress={onSelectorPress}>
            <Text>Select Groups</Text>
          </TouchableOpacity>
        </View>
        {!!errorMsg && <Text style={errorTextStyle}>{errorMsg}</Text>}
      </View>
    );
  }
);

Selector.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  innerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  textInputstyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array,
  ]),
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  leftIcon: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  textContentType: PropTypes.string.isRequired,
  hasSpaced: PropTypes.bool,
  errorMsg: PropTypes.string,
  disabled: PropTypes.bool,
  onSelectorPress: PropTypes.func.isRequired,
  onChangeText: PropTypes.func.isRequired,
};

Selector.defaultProps = {
  style: {},
  innerStyle: {},
  textInputstyle: {},
  leftIcon: [],
  hasSpaced: false,
  errorMsg: '',
  disabled: false,
};

export default Selector;
