import { StyleSheet, Platform } from 'react-native';

import Colors from '../../shared/constants/color';

export default StyleSheet.create({
  container: {
    flexDirection: 'column',
  },
  textinput: {
    height: 40,
    flex: 1,
  },
  hasSpaced: {
    paddingBottom: 18,
  },
  errorMsg: {
    color: Colors.red,
    fontSize: 12,
    paddingVertical: 2,
  },
  inner: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.lightCoolGrey,
    paddingHorizontal: 10,
  },
  center: {
    justifyContent: 'center',
  },
  number: {
    paddingBottom: Platform.OS === 'ios' ? 1 : -4,
    paddingLeft: 5,
  },
  disabled: {
    color: Colors.lightGrey,
  },
});
