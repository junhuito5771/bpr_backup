import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image } from 'react-native';

import styles from './styles';

const TouchableFeedbackIcon = ({
  isSelected,
  icons,
  onPress,
  containerStyle,
}) => {
  const icon = isSelected ? icons[1] : icons[0];

  return (
    <View style={[styles.container, containerStyle]}>
      <TouchableOpacity
        opacity={0.5}
        onPress={onPress}
        hitSlop={styles.hitslop}>
        <Image source={icon} />
      </TouchableOpacity>
    </View>
  );
};

TouchableFeedbackIcon.propTypes = {
  isSelected: PropTypes.bool,
  icons: PropTypes.arrayOf(PropTypes.object),
  onPress: PropTypes.func.isRequired,
  containerStyle: PropTypes.objectOf(PropTypes.object),
};

TouchableFeedbackIcon.defaultProps = {
  isSelected: false,
  icons: [],
  containerStyle: {},
};

export default TouchableFeedbackIcon;
