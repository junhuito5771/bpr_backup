import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, View, Image, Text } from 'react-native';
import styles from './style';

const GridBox = ({ label, icon, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <View style={[styles.container, styles.border]}>
      <View style={styles.iconContainer}>
        <Image source={icon} />
      </View>
      <View style={styles.labelContainer}>
        <Text style={styles.label}>{label}</Text>
      </View>
    </View>
  </TouchableOpacity>
);

GridBox.propTypes = {
  label: PropTypes.string,
  icon: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired,
};

GridBox.defaultProps = {
  label: '',
};

export default GridBox;
