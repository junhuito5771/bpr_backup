import { Dimensions, StyleSheet } from 'react-native';
import { scale, verticalScale, ScaleText } from '@module/utility';

import Colors from '../../shared/constants/color';

const windowWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    height: verticalScale(80),
    width: windowWidth * 0.27,
    margin: scale(5),
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  border: {
    borderRadius: 4,
    elevation: 5,
    shadowOpacity: 0.1,
    shadowRadius: 2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
  },
  iconContainer: {
    alignItems: 'center',
    height: verticalScale(25),
  },
  labelContainer: {
    marginHorizontal: scale(5),
  },
  label: {
    marginTop: verticalScale(10),
    fontSize: ScaleText(10),
    textAlign: 'center',
  },
});
