import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
});

export default function TouchableIcon({ onPress, source }) {
  return (
    <TouchableOpacity
      style={styles.center}
      hitSlop={styles.hitSlop}
      onPress={onPress}>
      <Image source={source} />
    </TouchableOpacity>
  );
}

TouchableIcon.propTypes = {
  onPress: PropTypes.func.isRequired,
  source: PropTypes.number.isRequired,
};
