import { scale, verticalScale } from '@module/utility';
import { StyleSheet } from 'react-native';
import { Colors } from '../../shared/constants';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  modal: {
    backgroundColor: Colors.white,
  },
  page: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  skip: {
    alignItems: 'flex-end',
    marginHorizontal: scale(20),
    marginVertical: verticalScale(20),
  },
  image: {
    height: verticalScale(250),
    width: '90%',
    marginBottom: verticalScale(25),
  },
  viewpager: {
    flex: 8,
  },
  indicatorBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  indicator: {
    marginHorizontal: 5,
    width: scale(30),
    height: 8,
    borderRadius: 10,
    backgroundColor: Colors.lightCoolGrey,
  },
  selected: {
    backgroundColor: Colors.blue,
  },
  description: {
    marginVertical: verticalScale(10),
    marginHorizontal: scale(20),
    color: Colors.darkGrey,
    textAlign: 'center',
  },
});

export default styles;
