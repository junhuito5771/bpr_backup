export const aircondIcons = {
  // 0: require('../assets/aircond/0.png'),
  // 1: require('../assets/aircond/1.png'),
  2: require('../assets/aircond/2.png'),
  // 3: require('../assets/aircond/3.png'),
  // 4: require('../assets/aircond/4.png'),
  5: require('../assets/aircond/5.png'),
  // 6: require('../assets/aircond/6.png'),
  // 7: require('../assets/aircond/7.png'),
  // 8: require('../assets/aircond/8.png'),
  // 9: require('../assets/aircond/9.png'),
  // 10: require('../assets/aircond/10.png'),
  // 11: require('../assets/aircond/11.png'),
  12: require('../assets/aircond/12.png'),
  // 13: require('../assets/aircond/13.png'),
  // 14: require('../assets/aircond/14.png'),
  // 15: require('../assets/aircond/15.png'),
  // 16: require('../assets/aircond/16.png'),
  // 17: require('../assets/aircond/17.png'),
  // 18: require('../assets/aircond/18.png'),
  // 19: require('../assets/aircond/19.png'),
  // 20: require('../assets/aircond/20.png'),
  // 21: require('../assets/aircond/21.png'),
  22: require('../assets/aircond/22.png'),
  23: require('../assets/aircond/23.png'),
  // 24: require('../assets/aircond/24.png'),
  25: require('../assets/aircond/25.png'),
  // 26: require('../assets/aircond/26.png'),
  27: require('../assets/aircond/27.png'),
  // 28: require('../assets/aircond/28.png'),
  // 29: require('../assets/aircond/29.png'),
  30: require('../assets/aircond/30.png'),
  // 31: require('../assets/aircond/31.png'),
};

export const bleIcon = require('../assets/general/ble.png');

const getACIconKey = logo => logo && logo.replace(/\.[^/.]+$/, '');

export default (key, defaultIcon = aircondIcons[2]) => {
  const acKey = getACIconKey(key);
  if (acKey) {
    const icon = aircondIcons[acKey];

    if (icon) return icon;
  }

  return defaultIcon;
};
