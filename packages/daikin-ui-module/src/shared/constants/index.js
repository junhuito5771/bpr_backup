// eslint-disable-next-line import/prefer-default-export
export { default as Colors } from './color';
export {
  default as getACIconFromPath,
  aircondIcons,
  bleIcon,
} from './getACIconFromPath';
