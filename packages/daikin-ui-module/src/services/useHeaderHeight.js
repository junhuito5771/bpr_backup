let headerHeight;

export default () => headerHeight;

const setHeaderHeight = value => {
  headerHeight = value;
};

export { setHeaderHeight };
