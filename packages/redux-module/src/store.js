import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import { CrashReportHelper } from '@module/utility';

import getRootReducer from './reducers';
import rootSaga from './sagas';

let store;
let rehydrationComplete;
let rehydrationFailed;
let sagaRunner;

const SET_SAGA_ERROR = 'SET_SAGA_ERROR';

const setSagaError = error => ({
  type: SET_SAGA_ERROR,
  error,
});

const rehydrationPromise = new Promise((resolve, reject) => {
  rehydrationComplete = resolve;
  rehydrationFailed = reject;
});
const sagaMiddleware = createSagaMiddleware({
  onError: error => {
    if (store) {
      store.dispatch(setSagaError(error.message));
    }

    CrashReportHelper.notify(new Error(error.message));
  },
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  keyPrefix: '',
  whitelist: ['userAgreement', 'units', 'settings', 'editUnits'],
  blackList: ['user'],
};

export default function configureStore() {
  const middleware = [sagaMiddleware];
  const persistedReducer = persistReducer(persistConfig, getRootReducer());
  if (__DEV__) {
    middleware.push(logger);
  }
  if (__DEV__ && module.hot && sagaRunner) {
    sagaRunner.cancel();
  }

  if (store === undefined) {
    store = createStore(
      persistedReducer,
      composeWithDevTools(applyMiddleware(...middleware))
    );
  } else if (__DEV__ && module.hot) {
    module.hot.accept('./reducers', () => {
      // This fetch the new state of the above reducers.
      const nextRootReducer = require('./reducers').default;
      store.replaceReducer(persistReducer(persistConfig, nextRootReducer()));
    });
  }

  const persistor = persistStore(store, null, () => {
    // this will be invoked after rehydration is complete
    rehydrationComplete();
  });

  sagaRunner = sagaMiddleware.run(rootSaga);

  return { store, persistor };
}

export function getStore() {
  return store;
}

export function rehydration() {
  return rehydrationPromise;
}
