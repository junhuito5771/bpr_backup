import { fork, all, call } from 'redux-saga/effects';

let rootArr = [];

export function combineModuleSaga(list) {
  rootArr = list;
}

export default function* rootSaga() {
  yield all(
    rootArr.map(saga =>
      fork(function* execute() {
        yield call(saga);
      })
    )
  );
}
