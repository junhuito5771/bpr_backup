import { combineReducers } from 'redux';

let rootReducer;

function combineModuleReducers(reducers) {
  rootReducer = combineReducers({
    ...reducers,
  });
}

function getRootReducer() {
  return rootReducer;
}

export default getRootReducer;

export { combineModuleReducers };
