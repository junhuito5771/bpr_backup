import { Provider, connect } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import ConfigureStore, { rehydration, getStore } from './store';
import getRootReducer, { combineModuleReducers } from './reducers';
import { combineModuleSaga } from './sagas';
import '../shim';

export { Provider, connect, PersistGate };

// // Export Store
export { ConfigureStore, rehydration, getStore };

// eslint-disable-next-line import/prefer-default-export
export { combineModuleReducers, getRootReducer, combineModuleSaga };
