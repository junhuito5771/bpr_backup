let IoTEnabled = false;

// Component
// eslint-disable-next-line import/no-mutable-exports
let LoginModeControl = null;

function setAllConfig({ isIoTEnabled }) {
  IoTEnabled = isIoTEnabled;
}

function getAllConfig() {
  return { IoTEnabled };
}

function getIoTEnabled() {
  return IoTEnabled;
}

function initExternalModule() {
  // eslint-disable-next-line import/no-extraneous-dependencies
  const iotmodule = require('@daikin-dama/acson-iot');

  LoginModeControl = iotmodule.LoginModeControl;
}

export default {
  setAllConfig,
  getAllConfig,
  getIoTEnabled,
  initExternalModule,
};

export { LoginModeControl };
