import React, { useState, useRef } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  ActivityIndicator,
  TextInput,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  heightPercentage,
  widthPercentage,
  NavigationService,
} from '@module/utility';
import { connect, signUp } from '@module/redux-auth';

import {
  Text,
  Button,
  TouchableText as LinkText,
  Timer,
  Colors,
  useHandleResponse,
} from '@module/acson-ui';
import { LOGIN_SCREEN, HOME_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    paddingVertical: heightPercentage(5),
    paddingHorizontal: widthPercentage(6),
  },
  flex: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    justifyContent: 'flex-end',
  },
  codeContainer: {
    marginTop: 30,
    marginBottom: 40,
    justifyContent: 'space-between',
  },
  codeInput: {
    marginLeft: 0,
    marginRight: 0,
    borderBottomColor: Colors.labelGrey,
    borderBottomWidth: 1,
  },
  link: {
    alignSelf: 'center',
  },
  enableLink: {
    color: Colors.lightBlue,
  },
  label: {
    color: Colors.labelGrey,
  },
  codeInputDescText: {
    paddingVertical: 15,
  },
  countDownText: {
    color: Colors.lightBlue,
  },
  codeInputContainer: {
    paddingVertical: heightPercentage(5),
  },
  resendContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading: {
    marginLeft: 1,
  },
  countDownContainer: {
    paddingVertical: 15,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

const SignUpVerificationScreen = ({
  email,
  confirmIsLoading,
  confirmSuccess,
  confirmError,
  resendSuccess,
  resendError,
  isResendLoading,
  submitConfirmParams,
  onResendCode,
  clearConfirmMsg,
  clearResendCodeMsg,
  route,
}) => {
  const [canResend, setCanResend] = useState(false);
  const [canSubmit, setCanSubmit] = useState(false);
  const [verifyCode, setVerifyCode] = useState(null);

  const timerRef = useRef();
  const { password: userPw } = route.params || {};
  const { continueWithLogin: isContinueWithLogin } = route.params || {};

  const handleOnPressResend = () => {
    timerRef.current.resetTimer();
    setCanResend(false);
    onResendCode();
  };

  useHandleResponse({
    success: confirmSuccess,
    error: confirmError,
    onSuccess: () => {
      if (isContinueWithLogin) {
        NavigationService.navigate(HOME_SCREEN);
      } else {
        NavigationService.navigate(LOGIN_SCREEN);
      }
    },
    clearStatus: clearConfirmMsg,
    showSuccess: true,
  });

  useHandleResponse({
    success: resendSuccess,
    error: resendError,
    clearStatus: clearResendCodeMsg,
  });

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <KeyboardAvoidingView
          style={styles.flex}
          behavior="padding"
          enabled
          keyboardVerticalOffset={heightPercentage(10)}>
          <View style={[styles.flex, styles.container]}>
            <View style={styles.center}>
              <Text small style={styles.label}>
                Verification code have been sent to
              </Text>
              <Text medium>{email || 'email'}</Text>
            </View>
            <View style={styles.codeInputContainer}>
              <TextInput
                style={styles.codeInput}
                keyboardType="number-pad"
                maxLength={6}
                onChangeText={code => {
                  setVerifyCode(code);
                  if (code.length === 6) {
                    setCanSubmit(true);
                    submitConfirmParams({
                      code,
                      userPw,
                      isContinueWithLogin,
                    });
                  } else {
                    setCanSubmit(false);
                  }
                }}
                onFocus={() => setCanSubmit(false)}
              />
              <Text style={[styles.codeInputDescText, styles.label]}>
                Enter the code to verify
              </Text>
            </View>
            <View style={[styles.flex, styles.bottom]}>
              <Button
                containerStyle={styles.buttonSpaced}
                raised
                onPress={() =>
                  submitConfirmParams({
                    code: verifyCode,
                    userPw,
                    isContinueWithLogin,
                  })
                }
                isLoading={confirmIsLoading}
                disabled={!canSubmit}>
                Confirm
              </Button>
              <View style={styles.resendContainer}>
                <LinkText
                  style={[styles.link, styles.label]}
                  disabled={!canResend}
                  highlighted
                  onPress={handleOnPressResend}
                  isLoading={isResendLoading}>
                  Resend verification code{' '}
                </LinkText>
                {isResendLoading ? (
                  <ActivityIndicator
                    size="small"
                    color={Colors.grey}
                    style={styles.loading}
                  />
                ) : (
                  <Timer
                    hideWhenTimeOut
                    onTimeOut={() => setCanResend(true)}
                    ref={timerRef}
                  />
                )}
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

SignUpVerificationScreen.propTypes = {
  email: PropTypes.string,
  confirmIsLoading: PropTypes.bool,
  confirmSuccess: PropTypes.string,
  confirmError: PropTypes.string,
  resendSuccess: PropTypes.string,
  resendError: PropTypes.string,
  isResendLoading: PropTypes.bool,
  submitConfirmParams: PropTypes.func,
  onResendCode: PropTypes.func,
  clearConfirmMsg: PropTypes.func,
  clearResendCodeMsg: PropTypes.func,
  route: PropTypes.object,
};

const mapStateToProps = ({
  signUp: {
    confirmIsLoading,
    confirmSuccess,
    confirmError,
    signUpParams,
    resendSuccess,
    resendError,
    isResendLoading,
  },
}) => ({
  email: signUpParams.email,
  confirmIsLoading,
  confirmSuccess,
  confirmError,
  resendSuccess,
  resendError,
  isResendLoading,
});

const mapDispatchToProps = dispatch => ({
  submitConfirmParams: confirmParams => {
    dispatch(
      signUp.submitConfirmParams(confirmParams, NavigationService, LOGIN_SCREEN)
    );
  },
  clearResendCodeMsg: () => {
    dispatch(signUp.clearResendCodeMsg());
  },
  onResendCode: () => {
    dispatch(signUp.clearResendCodeMsg());
    dispatch(signUp.resendVerificationCode());
  },
  clearConfirmMsg: () => {
    dispatch(signUp.clearConfirmErrorMsg());
    dispatch(signUp.clearConfirmSuccessMsg());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpVerificationScreen);
