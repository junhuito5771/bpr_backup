import React, { useState, useRef } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Keyboard,
  TouchableWithoutFeedback,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import { withFormik } from 'formik';
import {
  heightPercentage,
  widthPercentage,
  passwordPreValidator,
  NavigationService,
} from '@module/utility';
import { connect, resetPassword, forgotPassword } from '@module/redux-auth';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import {
  Text,
  TouchableText as LinkText,
  Timer,
  FormikTextInput,
  FormikButton,
  Colors,
  useHandleResponse,
} from '@module/acson-ui';

import resetPasswordValidator from './validator';

import { LOGIN_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    paddingTop: heightPercentage(5),
    paddingBottom: heightPercentage(2),
    paddingHorizontal: widthPercentage(6),
  },
  flex: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    justifyContent: 'flex-end',
  },
  codeContainer: {
    marginTop: 30,
    marginBottom: 40,
    justifyContent: 'space-between',
  },
  codeInput: {
    marginLeft: 0,
    marginRight: 0,
    borderBottomColor: Colors.inputGrey,
    borderBottomWidth: 1,
  },
  link: {
    alignSelf: 'center',
  },
  enableLink: {
    color: Colors.lightBlue,
  },
  label: {
    color: Colors.labelGrey,
  },
  codeInputContainer: {
    paddingTop: heightPercentage(5),
    flex: 1,
  },
  resendContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loading: {
    marginLeft: 1,
  },
  flexGrow: {
    flexGrow: 1,
  },
  countDownContainer: {
    paddingBottom: 15,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

const ResetPasswordScreen = ({
  navigation,
  clearResetPasswordStatus,
  submitForgetPasswordParams,
  isLoading,
  success,
  error,
  isResendLoading,
  resendSuccess,
  resendError,
  clearForgetPasswordStatus,
  route,
}) => {
  const [canResend, setCanResend] = useState(false);
  const timerRef = useRef();
  const codeInputRef = useRef();
  const passwordInputRef = useRef();
  const passwordConfirmInputRef = useRef();

  const { email } = route.params;

  const handleOnPressResend = () => {
    timerRef.current.resetTimer();
    setCanResend(false);
    clearForgetPasswordStatus();
    submitForgetPasswordParams(email);
  };

  useHandleResponse({
    success,
    error,
    onSuccess: () => NavigationService.navigate(LOGIN_SCREEN),
    onLeave: clearResetPasswordStatus,
    showSuccess: true,
  });

  useHandleResponse({
    success: resendSuccess,
    error: resendError,
    onLeave: clearForgetPasswordStatus,
    showSuccess: true,
  });

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <KeyboardAwareScrollView
          enableOnAndroid
          contentContainerStyle={styles.flexGrow}
          extraScrollHeight={40}>
          <View style={[styles.flex, styles.container]}>
            <View style={styles.center}>
              <Text small style={styles.label}>
                Verification code have been sent to
              </Text>
              <Text medium>{email || 'email'}</Text>
            </View>
            <View style={styles.codeInputContainer}>
              <FormikTextInput
                maxLength={6}
                name="code"
                keyboardType="numeric"
                returnKeyType="next"
                forwardRef={codeInputRef}
                onSubmitEditing={() => passwordInputRef.current.focus()}
              />
              <View style={styles.countDownContainer}>
                <Text style={styles.label}>Enter the code to verify</Text>
              </View>
              <FormikTextInput
                name="password"
                label="Password *"
                textContentType="password"
                forwardRef={passwordInputRef}
                returnKeyType="next"
                autoCapitalize="none"
                validator={passwordPreValidator}
                onSubmitEditing={() => passwordConfirmInputRef.current.focus()}
              />
              <FormikTextInput
                name="passwordConfirmation"
                label="Confirm password *"
                textContentType="password"
                forwardRef={passwordConfirmInputRef}
                returnKeyType="send"
                autoCapitalize="none"
                validator={passwordPreValidator}
                submitOnEditEnd
                validateOnChange
              />
              <View style={[styles.flex, styles.bottom]}>
                <FormikButton
                  containerStyle={styles.buttonSpaced}
                  raised
                  isLoading={isLoading}>
                  Confirm
                </FormikButton>
                <View style={styles.resendContainer}>
                  <LinkText
                    style={[styles.link, styles.label]}
                    disabled={!canResend}
                    highlighted
                    onPress={handleOnPressResend}
                    isLoading={isResendLoading}>
                    Resend verification code{' '}
                  </LinkText>
                  {isResendLoading ? (
                    <ActivityIndicator
                      size="small"
                      color={Colors.grey}
                      style={styles.loading}
                    />
                  ) : (
                    <Timer
                      hideWhenTimeOut
                      onTimeOut={() => setCanResend(true)}
                      ref={timerRef}
                    />
                  )}
                </View>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

ResetPasswordScreen.propTypes = {
  navigation: PropTypes.object,
  clearResetPasswordStatus: PropTypes.func,
  submitForgetPasswordParams: PropTypes.func,
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  isResendLoading: PropTypes.bool,
  resendSuccess: PropTypes.string,
  resendError: PropTypes.string,
  clearForgetPasswordStatus: PropTypes.func,
  route: PropTypes.object,
};

const mapStateToProps = ({
  resetPassword: { isLoading, success, error },
  forgotPassword: {
    isLoading: isResendLoading,
    success: resendSuccess,
    error: resendError,
  },
}) => ({
  isLoading,
  success,
  error,
  isResendLoading,
  resendSuccess,
  resendError,
});

const mapDispatchToProps = dispatch => ({
  submitResetPasswordParams: resetPasswordParams => {
    dispatch(resetPassword.submitResetPasswordParams(resetPasswordParams));
  },
  submitForgetPasswordParams: email => {
    dispatch(forgotPassword.submitForgotPasswordParams({ email }));
  },
  clearResetPasswordStatus: () => {
    dispatch(resetPassword.clearResetPasswordStatus());
  },
  clearForgetPasswordStatus: () => {
    dispatch(forgotPassword.clearForgotPasswordStatus());
  },
});

const formikResetPasswordScreen = withFormik({
  enableReinitialize: true,
  validate: resetPasswordValidator,
  mapPropsToValues: () => ({
    code: '',
    password: '',
    passwordConfirmation: '',
  }),
  handleSubmit: (payload, bag) => {
    Keyboard.dismiss();
    bag.props.submitResetPasswordParams({
      email: bag.props.route.params.email,
      ...payload,
    });
  },
})(ResetPasswordScreen);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(formikResetPasswordScreen);
