import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import {
  widthPercentage,
  heightPercentage,
  ScaleText,
  NavigationService,
} from '@module/utility';

import { Colors, Text } from '@module/acson-ui';

import { AUTH_USER_AGREEMENT_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: widthPercentage(10),
    marginVertical: heightPercentage(0.5),
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    color: Colors.buttonGrey,
    fontSize: ScaleText(12),
    lineHeight: 18,
  },
  link: {
    color: Colors.lightBlue,
  },
});

const Term = () => (
  <View style={styles.container}>
    <Text style={styles.text}>By signing up, you agree to Acson's</Text>
    <TouchableOpacity
      onPress={() => NavigationService.navigate(AUTH_USER_AGREEMENT_SCREEN)}>
      <Text style={[styles.text, styles.link]}>Terms of Use</Text>
    </TouchableOpacity>
  </View>
);

export default Term;
