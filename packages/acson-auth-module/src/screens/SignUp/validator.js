import {
  isValidPhoneNumber,
  hasFieldValue,
  isCorrectEmailFormat,
  addError,
} from '../../../../acson-mobile/app/shared/validation/validateHelper';

const signUpValidator = values => {
  const errors = {};

  const { email, fullName, phoneNumber } = values;

  addError(
    errors,
    'email',
    [hasFieldValue(email), 'Email address is required'],
    [isCorrectEmailFormat(email), 'Incorrect email format']
  );

  addError(errors, 'fullName', [
    hasFieldValue(fullName),
    'Full name is required',
  ]);

  addError(
    errors,
    'phoneNumber',
    [hasFieldValue(phoneNumber), 'Phone number is required'],
    [isValidPhoneNumber(phoneNumber), 'Please enter a valid phone number']
  );
  return errors;
};

export default signUpValidator;
