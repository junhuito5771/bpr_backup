import React, { useRef } from 'react';
import {
  View,
  StyleSheet,
  Image,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {
  heightPercentage,
  ScaleText,
  widthPercentage,
  emailPreValidator,
  phoneNoPreValidator,
  generalInputPreValidator,
  NavigationService,
} from '@module/utility';
import { withFormik } from 'formik';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Text, FormikTextInput, FormikButton } from '@module/acson-ui';
import Term from './Term';

import signUpValidator from './validator';

import { CONFIRM_PASSWORD_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  primaryText: {
    fontSize: ScaleText(18),
  },
  secondaryText: {
    fontSize: ScaleText(18),
    color: 'grey',
  },
  container: {
    paddingHorizontal: widthPercentage(6),
  },
  titleContainer: {
    marginBottom: heightPercentage(5),
  },
  inputSpaced: {
    marginBottom: heightPercentage(1),
  },
  checkboxSpaced: {
    marginBottom: heightPercentage(4),
  },
  buttonSpaced: {
    marginTop: heightPercentage(2),
  },
  verticalCentered: {
    justifyContent: 'center',
  },
  flexGrow: {
    flexGrow: 1,
  },
});

const SignUpScreen = () => {
  const emailInputRef = useRef(null);
  const fullNameInputRef = useRef(null);
  const phoneNoInputRef = useRef(null);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <KeyboardAwareScrollView
          enableOnAndroid
          contentContainerStyle={styles.flexGrow}
          extraScrollHeight={40}>
          <View
            style={[styles.flex, styles.container, styles.verticalCentered]}>
            <View style={[styles.center, styles.titleContainer]}>
              <Image source={require('../../shared/images/general/logo.png')} />
              <Text style={styles.primaryText} bold>
                Hello there,
              </Text>
              <Text style={styles.secondaryText}>Sign Up to get started</Text>
            </View>
            <View>
              <FormikTextInput
                name="email"
                label="Email *"
                textContentType="emailAddress"
                keyboardType="email-address"
                returnKeyType="next"
                autoCapitalize="none"
                forwardRef={emailInputRef}
                validator={emailPreValidator}
                onSubmitEditing={() => fullNameInputRef.current.focus()}
              />
              <FormikTextInput
                name="fullName"
                label="Full Name *"
                textContentType="name"
                returnKeyType="next"
                autoCapitalize="none"
                forwardRef={fullNameInputRef}
                validator={generalInputPreValidator}
                onSubmitEditing={() => phoneNoInputRef.current.focus()}
              />
              <FormikTextInput
                name="phoneNumber"
                label="Phone Number *"
                textContentType="telephoneNumber"
                keyboardType="number-pad"
                returnKeyType="send"
                autoCapitalize="none"
                forwardRef={phoneNoInputRef}
                validator={phoneNoPreValidator}
                maxLength={11}
                submitOnEditEnd
              />
              <FormikButton containerStyle={styles.buttonSpaced} raised>
                Next
              </FormikButton>
              <Term />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

SignUpScreen.propTypes = {};

const formikSignUp = withFormik({
  enableReinitialize: true,
  validate: signUpValidator,
  mapPropsToValues: () => ({
    email: '',
    fullName: '',
    phoneNumber: '',
  }),
  handleSubmit: payload => {
    NavigationService.replace(CONFIRM_PASSWORD_SCREEN, { payload });
  },
})(SignUpScreen);

export default formikSignUp;
