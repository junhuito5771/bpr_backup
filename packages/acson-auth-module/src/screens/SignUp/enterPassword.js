import React, { useRef } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {
  heightPercentage,
  widthPercentage,
  NavigationService,
  passwordPreValidator,
} from '@module/utility';
import { withFormik } from 'formik';
import { connect, signUp } from '@module/redux-auth';
import PropTypes from 'prop-types';

import {
  FormikTextInput,
  FormikButton,
  useHandleResponse,
} from '@module/acson-ui';
import confirmPasswordValidator from './confirmPasswordValidator';

import { SIGNUP_VERIFCATION_SCREEN } from '../../constants/routeNames';

import Term from './Term';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    paddingHorizontal: widthPercentage(6),
  },
  inputSpaced: {
    marginBottom: heightPercentage(1),
  },
  buttonSpaced: {
    marginTop: heightPercentage(15),
  },
  formContainer: {
    marginTop: heightPercentage(5),
  },
});

const handleOnSuccess = () => {
  NavigationService.replace(SIGNUP_VERIFCATION_SCREEN);
};

const ConfirmPasswordScreen = ({ success, error, clearStatus, isLoading }) => {
  const passwordInputRef = useRef(null);
  const confirmPasswordInputRef = useRef(null);

  useHandleResponse({
    success,
    error,
    onSuccess: handleOnSuccess,
    onLeave: clearStatus,
  });

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <View style={[styles.flex, styles.container]}>
          <View style={styles.formContainer}>
            <FormikTextInput
              name="password"
              label="Password *"
              textContentType="password"
              returnKeyType="next"
              hasSpaced
              autoCapitalize="none"
              forwardRef={passwordInputRef}
              onSubmitEditing={() => confirmPasswordInputRef.current.focus()}
              validator={passwordPreValidator}
            />
            <FormikTextInput
              name="passwordConfirmation"
              label="Password Confirmation *"
              textContentType="password"
              returnKeyType="send"
              hasSpaced
              autoCapitalize="none"
              forwardRef={confirmPasswordInputRef}
              validator={passwordPreValidator}
              submitOnEditEnd
              validateOnChange
            />
            <FormikButton
              containerStyle={styles.buttonSpaced}
              raised
              isLoading={isLoading}>
              Confirm
            </FormikButton>
            <Term />
          </View>
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

ConfirmPasswordScreen.propTypes = {
  success: PropTypes.string,
  error: PropTypes.string,
  clearStatus: PropTypes.func,
  isLoading: PropTypes.bool,
};

const formikConfirmPassword = withFormik({
  enableReinitialize: true,
  validate: confirmPasswordValidator,
  mapPropsToValues: () => ({
    password: '',
    passwordConfirmation: '',
  }),
  handleSubmit: (payload, bag) => {
    const params = bag.props.route.params.payload;
    bag.props.submitSignUpParams({ ...params, ...payload });
  },
})(ConfirmPasswordScreen);

ConfirmPasswordScreen.propTypes = {
  clearStatus: PropTypes.func,
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
};

const mapStateToProps = ({ signUp: { isLoading, success, error } }) => ({
  isLoading,
  success,
  error,
});

const mapDispatchToProps = dispatch => ({
  submitSignUpParams: signUpParams => {
    dispatch(signUp.submitSignUpParams(signUpParams));
  },
  clearStatus: () => {
    dispatch(signUp.clearSignUpMsg());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(formikConfirmPassword);
