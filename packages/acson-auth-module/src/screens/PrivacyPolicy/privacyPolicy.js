import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Linking,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  widthPercentage,
  heightPercentage,
  verticalScale,
  scale,
  NavigationService,
} from '@module/utility';
import { connect } from '@module/redux-auth';
import WebView from 'react-native-webview';
// eslint-disable-next-line import/no-unresolved
import { privacyPolicy } from '@module/redux-marketing';

import { Colors, Button, Checkbox } from '@module/acson-ui';

import { SIGNUP_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
    justifyContent: 'space-between',
  },
  content: {
    height: heightPercentage(40),
    marginVertical: verticalScale(10),
    marginHorizontal: scale(10),
    borderWidth: 1,
    borderColor: Colors.red,
  },
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  marginTop: {
    marginTop: 20,
  },
  button: {
    marginHorizontal: scale(10),
    marginVertical: verticalScale(16),
  },
  hyperlink: {
    color: Colors.red,
    textDecorationLine: 'underline',
  },
  checkRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: scale(10),
  },
  error: {
    paddingTop: verticalScale(30),
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

const ErrorMessage = () => (
  <View style={styles.error}>
    <Text>An error has occurred, please try again later</Text>
  </View>
);

const PrivacyPolicyScreen = ({ init, content }) => {
  const webviewRef = React.useRef(null);
  const [isEulaViewed, setIsEulaViewed] = React.useState(false);
  const [isEulaLoad, setIsEulaLoad] = React.useState(false);
  const [isAgreed, setIsAgreed] = React.useState(false);

  React.useEffect(() => {
    init('Acson');
  }, []);

  const onEndReached = ({ layoutMeasurement, contentOffset, contentSize }) =>
    isEulaLoad &&
    Math.round(layoutMeasurement.height + contentOffset.y, 2) >=
      Math.round(contentSize.height, 2);

  const handleEulaScroll = ({ nativeEvent }) => {
    if (onEndReached(nativeEvent)) {
      setIsEulaViewed(true);
    }
  };

  const handleAgreementPress = () => {
    setIsAgreed(!isAgreed);
  };

  const handleAlertConfirm = () => {
    Alert.alert(
      'Agreement Confirmation',
      content
        ? 'Please scroll down to privacy policy bottom and confirm the eula agreement to confirm the agreement'
        : 'Please confirm you have network connection in order to view the agreement'
    );
  };

  const handleConfirmAgreement = () => {
    NavigationService.replace(SIGNUP_SCREEN);
  };

  return (
    <SafeAreaView style={styles.flex}>
      <View style={[styles.flex, styles.content]}>
        {content && (
          <WebView
            ref={webviewRef}
            source={{ uri: content ? content.hyperlink : null }}
            onShouldStartLoadWithRequest={event => {
              if (event.url !== content.hyperlink) {
                webviewRef.current.stopLoading();
                Linking.openURL(event.url);
                return false;
              }
              return true;
            }}
            onScroll={handleEulaScroll}
            renderError={() => <ErrorMessage />}
            onLoadEnd={() => setIsEulaLoad(true)}
          />
        )}
      </View>
      <View style={styles.checkRow}>
        <Checkbox value={isAgreed} onPress={handleAgreementPress} />
        <Text>
          I have read and accept the &nbsp;
          <Text
            style={styles.hyperlink}
            onPress={() => Linking.openURL(content.hyperlink)}>
            End User License Agreement
          </Text>
          &nbsp;and&nbsp;
          <Text
            style={styles.hyperlink}
            onPress={() =>
              Linking.openURL('https://www.acson.com.my/privacy-policy')
            }>
            Privacy Policy
          </Text>
          &nbsp;
        </Text>
      </View>
      <TouchableOpacity
        onPress={(!isEulaViewed || !isAgreed) && handleAlertConfirm}>
        <View style={styles.button}>
          <Button
            onPress={handleConfirmAgreement}
            disabled={!isEulaViewed || !isAgreed}>
            Sign Up
          </Button>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

PrivacyPolicyScreen.propTypes = {
  init: PropTypes.func,
  content: PropTypes.object,
};

const mapStateToProps = ({ privacyPolicy: privacyPolicyState }) => ({
  content: privacyPolicyState.content,
});

const mapDispatchToProps = dispatch => ({
  init: appType => {
    dispatch(privacyPolicy.init(appType));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PrivacyPolicyScreen);
