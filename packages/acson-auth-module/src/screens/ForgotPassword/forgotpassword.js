import React from 'react';
import {
  View,
  Keyboard,
  StyleSheet,
  TouchableWithoutFeedback,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'react-native';
import PropTypes from 'prop-types';
import { withFormik } from 'formik';
import { connect, forgotPassword } from '@module/redux-auth';
import {
  heightPercentage,
  widthPercentage,
  emailPreValidator,
  NavigationService,
} from '@module/utility';

import {
  Text,
  FormikTextInput,
  FormikButton,
  Colors,
  useHandleResponse,
} from '@module/acson-ui';
import forgotPasswordValidator from './validator';

import { RESET_PASSWORD_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    paddingHorizontal: widthPercentage(6),
    paddingTop: heightPercentage(3),
  },
  buttonContainer: {
    justifyContent: 'flex-end',
    paddingVertical: heightPercentage(3),
  },
  instructionText: {
    color: Colors.labelGrey,
  },
});

const handleOnSuccess = values => {
  NavigationService.navigate(RESET_PASSWORD_SCREEN, values);
};

const ForgotPasswordScreen = ({
  isLoading,
  success,
  error,
  clearStatus,
  values,
}) => {
  useHandleResponse({
    success,
    error,
    onSuccess: () => handleOnSuccess(values),
    onLeave: clearStatus,
  });

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <KeyboardAvoidingView
          style={styles.flex}
          behavior="padding"
          enabled
          keyboardVerticalOffset={heightPercentage(10)}>
          <View style={[styles.flex, styles.container]}>
            <FormikTextInput
              name="email"
              label="Email *"
              textContentType="emailAddress"
              keyboardType="email-address"
              returnKeyType="send"
              autoCapitalize="none"
              validator={emailPreValidator}
              submitOnEditEnd
            />
            <Text style={styles.instructionText}>
              Don&apos;t worry. Resetting your password is easy, just tell us
              the email address you registered with MY Acson
            </Text>
            <View style={[styles.flex, styles.buttonContainer]}>
              <FormikButton isLoading={isLoading} hasShadow raised>
                Confirm
              </FormikButton>
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

ForgotPasswordScreen.propTypes = {
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  clearStatus: PropTypes.func,
  values: PropTypes.object,
};

const mapStateToProps = ({
  forgotPassword: { isLoading, success, error },
}) => ({
  isLoading,
  success,
  error,
});

const mapDispatchToProps = dispatch => ({
  submitForgetPasswordParams: forgotPasswordParams => {
    dispatch(forgotPassword.submitForgotPasswordParams(forgotPasswordParams));
  },
  clearStatus: () => {
    dispatch(forgotPassword.clearForgotPasswordStatus());
  },
});

const formikForgotPassword = withFormik({
  enableReinitialize: true,
  validate: forgotPasswordValidator,
  mapPropsToValues: () => ({
    email: '',
  }),
  handleSubmit: (payload, bag) => {
    bag.props.submitForgetPasswordParams(payload);
  },
})(ForgotPasswordScreen);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(formikForgotPassword);
