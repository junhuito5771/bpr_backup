import { ValidatorHelper } from '@module/utility';

const { hasFieldValue, isCorrectEmailFormat, addError } = ValidatorHelper;

const forgotPasswordValidator = values => {
  const errors = {};
  const { email } = values;

  addError(
    errors,
    'email',
    [hasFieldValue(email), 'Email address is required'],
    [isCorrectEmailFormat(email), 'Incorrect email format']
  );

  return errors;
};

export default forgotPasswordValidator;
