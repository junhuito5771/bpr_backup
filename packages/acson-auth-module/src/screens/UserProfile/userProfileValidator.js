import { ValidatorHelper } from '@module/utility';

const { hasFieldValue, addError, isValidPhoneNumber } = ValidatorHelper;

const userProfileValidator = values => {
  const errors = {};

  const { name, phoneNo } = values;

  addError(errors, 'name', [hasFieldValue(name), 'Full name is required']);

  addError(
    errors,
    'phoneNo',
    [hasFieldValue(phoneNo), 'Phone number is required'],
    [isValidPhoneNumber(phoneNo), 'Please enter a valid phone number']
  );

  return errors;
};

export default userProfileValidator;
