import React, { useRef, useEffect } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  BackHandler,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import PropTypes from 'prop-types';
import { withFormik } from 'formik';
import { connect, userProfile } from '@module/redux-auth';
import {
  widthPercentage,
  heightPercentage,
  phoneNoPreValidator,
  generalInputPreValidator,
} from '@module/utility';

import {
  ModalService,
  FormikTextInput,
  FormikButton,
  useHandleResponse,
} from '@module/acson-ui';
import userProfileValidator from './userProfileValidator';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  flexGrow: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    paddingTop: heightPercentage(5),
    paddingBottom: heightPercentage(2),
    paddingHorizontal: widthPercentage(6),
  },
  bottomContainer: {
    justifyContent: 'flex-end',
    paddingVertical: heightPercentage(3),
  },
});

function getModalProps(onPress) {
  const modalProps = {
    title: 'Are you sure?',
    desc: 'Changes made will not be saved.',
    buttons: [
      {
        title: 'Yes',
        onPress,
      },
      {
        title: 'No',
        isRaised: true,
      },
    ],
  };

  return modalProps;
}

const userProfileScreen = ({
  isLoading,
  error,
  clearUserStatus,
  handleReset,
  isValid,
  navigation,
  success,
}) => {
  const phoneNoInputRef = useRef(null);

  useFocusEffect(
    React.useCallback(() => {
      navigation.setOptions({
        title: 'User Profile',
      });
    }, [navigation])
  );

  useHandleResponse({
    success,
    error,
    onLeave: clearUserStatus,
    showSuccess: true,
  });

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', handleReset);

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    let backListener;
    let unsubscribeBeforeRemove;
    if (isValid) {
      // navigation.setParams({ modalProps });
      unsubscribeBeforeRemove = navigation.addListener(
        'beforeRemove',
        event => {
          if (isValid) {
            // If we don't have unsaved changes, then we don't need to do anything
            event.preventDefault();

            ModalService.show({
              ...getModalProps(() => navigation.dispatch(event.data.action)),
            });
          }
        }
      );

      backListener = BackHandler.addEventListener('hardwareBackPress', () => {
        ModalService.show({
          ...getModalProps(() => navigation.goBack()),
        });

        return isValid;
      });
    }

    return () => {
      if (backListener) backListener.remove();
      BackHandler.removeEventListener('hardwareBackPress', () => {});
      if (unsubscribeBeforeRemove) unsubscribeBeforeRemove();
    };
  }, [isValid]);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <KeyboardAvoidingView
          style={styles.flex}
          behavior="padding"
          enabled
          keyboardVerticalOffset={heightPercentage(10)}>
          <View style={styles.container}>
            <View style={[styles.flex, styles.topContainer]}>
              <FormikTextInput
                name="email"
                label="Email"
                error={!!error}
                textContentType="emailAddress"
                keyboardType="email-address"
                returnKeyType="next"
                autoCapitalize="none"
                disabled
              />
              <FormikTextInput
                name="name"
                label="Full Name *"
                textContentType="name"
                returnKeyType="next"
                autoCapitalize="none"
                validator={generalInputPreValidator}
                onSubmitEditing={() => phoneNoInputRef.current.focus()}
              />
              <FormikTextInput
                name="phoneNo"
                label="Phone Number *"
                textContentType="telephoneNumber"
                keyboardType="number-pad"
                returnKeyType="send"
                autoCapitalize="none"
                forwardRef={phoneNoInputRef}
                validator={phoneNoPreValidator}
                maxLength={11}
                submitOnEditEnd
              />
              <View style={[styles.flex, styles.bottomContainer]}>
                <FormikButton
                  containerStyle={styles.buttonSpaced}
                  isLoading={isLoading}
                  hasShadow
                  raised
                  disabledRaised>
                  Save
                </FormikButton>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

userProfileScreen.propTypes = {
  clearUserStatus: PropTypes.func,
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  navigation: PropTypes.object,
  handleReset: PropTypes.func,
  isValid: PropTypes.bool,
};

const formikUserProfile = withFormik({
  enableReinitialize: true,
  validate: userProfileValidator,
  mapPropsToValues: ({ name, email, phoneNo }) => ({ name, email, phoneNo }),
  handleSubmit: (payload, bag) => {
    Keyboard.dismiss();
    bag.props.submitUserParams(payload);
  },
})(userProfileScreen);

const mapStateToProps = ({
  user: {
    profile: { name, email, phoneNo },
    success,
    error,
    isLoading,
  },
}) => ({
  isLoading,
  success,
  error,
  name,
  email,
  phoneNo,
});

const mapDispatchToProps = dispatch => ({
  submitUserParams: userProfileParams => {
    dispatch(userProfile.submitUserParams(userProfileParams));
  },
  clearUserStatus: () => {
    dispatch(userProfile.clearUserStatus());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(formikUserProfile);
