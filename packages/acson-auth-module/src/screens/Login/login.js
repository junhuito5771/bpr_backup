import React, { useRef, useEffect } from 'react';
import {
  View,
  StyleSheet,
  Image,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {
  heightPercentage,
  ScaleText,
  widthPercentage,
  emailPreValidator,
  passwordPreValidator,
  NavigationService,
} from '@module/utility';
import { withFormik } from 'formik';
import { connect, login, userProfile, signUp } from '@module/redux-auth';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SplashScreen from 'react-native-splash-screen';

import {
  FormikCheckbox,
  Text,
  FormikTextInput,
  FormikButton,
  Button,
  TouchableText,
  useHandleResponse,
} from '@module/acson-ui';

import loginValidator from './validator';

import {
  HOME_SCREEN,
  FORGOT_PASSWORD_SCREEN,
  SIGNUP_VERIFCATION_SCREEN,
  PRIVACY_POLICY_SCREEN,
} from '../../constants/routeNames';
import { LoginModeControl } from '../../integratedModule';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  topContainer: {
    flex: 0.9,
    justifyContent: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  primaryText: {
    fontSize: ScaleText(18),
  },
  secondaryText: {
    fontSize: ScaleText(18),
    color: 'grey',
  },
  container: {
    paddingHorizontal: widthPercentage(6),
    flex: 1,
  },
  inputSpaced: {
    marginBottom: heightPercentage(1),
  },
  checkboxSpaced: {
    marginBottom: heightPercentage(4),
  },
  buttonSpaced: {
    marginBottom: heightPercentage(1),
  },
  bottomContainer: {
    justifyContent: 'flex-end',
    flex: 0.1,
  },
  modeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleContainer: {
    marginBottom: heightPercentage(5),
  },
  flexGrow: {
    flexGrow: 1,
  },
});

const handleOnSuccess = () => {
  NavigationService.reset(HOME_SCREEN);
};

const handleNavigateToSignUp = () => {
  NavigationService.navigate(PRIVACY_POLICY_SCREEN);
};

const handleNavigateToForgotPassword = () => {
  NavigationService.navigate(FORGOT_PASSWORD_SCREEN);
};

const LoginScreen = ({
  success,
  error,
  clearStatus,
  isLoading,
  errorCode,
  resendVerification,
  clearErrorCode,
  values,
  navigation,
}) => {
  const emailInputRef = useRef(null);
  const passwordInputRef = useRef(null);

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  useHandleResponse({
    success,
    error,
    onSuccess: handleOnSuccess,
    onLeave: clearStatus,
  });

  useEffect(() => {
    if (errorCode === 'UserNotConfirmedException') {
      clearStatus();
      resendVerification();
      NavigationService.navigate(SIGNUP_VERIFCATION_SCREEN, {
        password: values.password,
        continueWithLogin: true,
      });
    }
    return () => clearErrorCode();
  }, [errorCode]);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <KeyboardAwareScrollView
          enableOnAndroid
          contentContainerStyle={styles.flexGrow}
          extraScrollHeight={40}>
          <View style={styles.container}>
            <View style={[styles.flex, styles.topContainer]}>
              <View style={[styles.center, styles.titleContainer]}>
                <Image
                  source={require('../../shared/images/general/logo.png')}
                />
                <Text style={styles.primaryText} bold>
                  Welcome back,
                </Text>
                <Text style={styles.secondaryText}>sign in to continue</Text>
              </View>
              <FormikTextInput
                name="email"
                label="Email"
                error={!!error}
                textContentType="emailAddress"
                keyboardType="email-address"
                returnKeyType="next"
                autoCapitalize="none"
                forwardRef={emailInputRef}
                validator={emailPreValidator}
                onSubmitEditing={() => passwordInputRef.current.focus()}
              />
              <FormikTextInput
                name="password"
                label="Password"
                error={!!error}
                textContentType="password"
                returnKeyType="send"
                autoCapitalize="none"
                forwardRef={passwordInputRef}
                validator={passwordPreValidator}
                submitOnEditEnd
              />
              <FormikCheckbox
                containerStyle={styles.checkboxSpaced}
                name="isRemember">
                Remember me
              </FormikCheckbox>
              <FormikButton
                containerStyle={styles.buttonSpaced}
                isLoading={isLoading}
                hasShadow
                raised
                disabledRaised>
                Log in
              </FormikButton>
              <Button
                containerStyle={styles.buttonSpaced}
                onPress={handleNavigateToSignUp}>
                Sign up
              </Button>
              <TouchableText
                style={[styles.hasSpaced, styles.center]}
                onPress={handleNavigateToForgotPassword}>
                Forgot your password?
              </TouchableText>
            </View>
            {!!LoginModeControl && <LoginModeControl navigation={navigation} />}
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

LoginScreen.propTypes = {
  success: PropTypes.string,
  error: PropTypes.string,
  clearStatus: PropTypes.func,
  isLoading: PropTypes.bool,
  errorCode: PropTypes.string,
  resendVerification: PropTypes.func,
  clearErrorCode: PropTypes.func,
  values: PropTypes.object,
  navigation: PropTypes.object,
};

const formikLogin = withFormik({
  enableReinitialize: true,
  validate: loginValidator,
  mapPropsToValues: ({ profileEmail }) => ({
    email: profileEmail,
    isRemember: true,
  }),
  handleSubmit: (payload, bag) => {
    bag.props.submitLoginParams(payload);
  },
})(LoginScreen);

const mapStateToProps = ({
  login: { isLoading, errorCode, error, success },
  signUp: { confirmSuccess },
  user: {
    profile: { email },
  },
}) => ({
  error,
  success,
  errorCode,
  isLoading,
  confirmSuccess,
  profileEmail: email,
});

const mapDispatchToProps = dispatch => ({
  submitLoginParams: loginParams => {
    dispatch(login.clearLoginStatus());
    dispatch(login.submitLoginParams(loginParams));
  },
  clearStatus: () => {
    dispatch(login.clearLoginStatus());
    dispatch(userProfile.clearUserStatus());
    // dispatch(bleMode.clearBleStatus());
  },
  clearErrorCode: () => dispatch(login.clearErrorCode()),
  resendVerification: () => dispatch(signUp.resendVerificationCode()),
  dispatch,
});

const connectedLogin = connect(
  mapStateToProps,
  mapDispatchToProps
)(formikLogin);

export default connectedLogin;
