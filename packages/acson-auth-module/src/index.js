import { configuration } from '@module/redux-auth';

export { default as Stack, settingStacks, settingItems } from './navigator';
export * from '@module/redux-auth';
export { default as IntegratedModule } from './integratedModule';

function init(config) {
  configuration.init({
    apiEndpoint: config.API_URL,
    cognitoEndpoint: config.COGNITO_URL,
    cognitoClientId: config.COGNITO_CLIENT_ID,
    cognitoIdentifyPoolId: config.COGNITO_POOL_ID,
    cognitoUserPoolID: config.COGNITO_USER_POOL_ID,
    region: config.REGION,
    pinpointId: config.PINPOINT_ID,
    pinpointRegion: config.PINPOINT_REGION,
  });
}

export { init };
