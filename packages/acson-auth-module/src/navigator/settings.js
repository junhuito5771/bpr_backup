import React from 'react';
import { TouchableRow, Text, ModalService } from '@module/acson-ui';
import { connect, userProfile } from '@module/redux-auth';

import { NavigationService } from '@module/utility';

import { USER_PROFILE_SCREEN, HOME_MARKETING } from '../constants/routeNames';
import UserProfileScreen from '../screens/UserProfile/userProfile';

const LogoutRowComponent = ({ logout }) => {
  const handleOnPress = () => {
    ModalService.show({
      title: 'Log out',
      desc: 'Are you sure you want to logout?',
      buttons: [
        {
          title: 'OK',
          onPress: () => {
            logout();
            NavigationService.navigate(HOME_MARKETING);
          },
        },
        {
          title: 'Cancel',
          isRaised: true,
        },
      ],
    });
  };

  return (
    <TouchableRow onPress={handleOnPress}>
      <Text small>Log out</Text>
    </TouchableRow>
  );
};

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(userProfile.logOut()),
});

const LogoutRow = connect(null, mapDispatchToProps)(LogoutRowComponent);

const settingItems = [
  {
    index: 0,
    key: 'Profile',
    component: () => (
      <TouchableRow
        onPress={() => {
          NavigationService.navigate(USER_PROFILE_SCREEN);
        }}>
        <Text small>Edit profile</Text>
      </TouchableRow>
    ),
  },
  { index: 99, key: 'Logout', component: () => <LogoutRow /> },
];

const settingStacks = {
  [USER_PROFILE_SCREEN]: {
    name: USER_PROFILE_SCREEN,
    component: UserProfileScreen,
  },
};

export default settingStacks;

export { settingItems };
