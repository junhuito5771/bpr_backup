import LoginScreen from '../screens/Login/login';
import SignUpScreen from '../screens/SignUp/signUp';
import ConfirmPasswordScreen from '../screens/SignUp/enterPassword';
import SignUpVerificationScreen from '../screens/SignUpVerification/signUpVerification';
import ForgotPasswordScreen from '../screens/ForgotPassword/forgotpassword';
import ResetPasswordScreen from '../screens/ResetPassword/resetPassword';
import PrivacyPolicyScreen from '../screens/PrivacyPolicy/privacyPolicy';
import UserProfileScreen from '../screens/UserProfile/userProfile';

import {
  LOGIN_SCREEN,
  PRIVACY_POLICY_SCREEN,
  SIGNUP_SCREEN,
  CONFIRM_PASSWORD_SCREEN,
  SIGNUP_VERIFCATION_SCREEN,
  FORGOT_PASSWORD_SCREEN,
  RESET_PASSWORD_SCREEN,
  USER_PROFILE_SCREEN,
} from '../constants/routeNames';

const stack = {
  [LOGIN_SCREEN]: {
    name: LOGIN_SCREEN,
    component: LoginScreen,
  },
  [SIGNUP_SCREEN]: {
    name: SIGNUP_SCREEN,
    component: SignUpScreen,
    options: () => ({
      title: 'Sign Up',
    }),
  },
  [CONFIRM_PASSWORD_SCREEN]: {
    name: CONFIRM_PASSWORD_SCREEN,
    component: ConfirmPasswordScreen,
    options: () => ({
      title: 'Confirm Password',
    }),
  },
  [SIGNUP_VERIFCATION_SCREEN]: {
    name: SIGNUP_VERIFCATION_SCREEN,
    component: SignUpVerificationScreen,
    options: () => ({
      title: 'Verification',
    }),
  },
  [FORGOT_PASSWORD_SCREEN]: {
    name: FORGOT_PASSWORD_SCREEN,
    component: ForgotPasswordScreen,
    options: () => ({
      title: 'Forgot Password',
    }),
  },
  [RESET_PASSWORD_SCREEN]: {
    name: RESET_PASSWORD_SCREEN,
    component: ResetPasswordScreen,
    options: () => ({
      title: 'Forgot Password',
    }),
  },
  [PRIVACY_POLICY_SCREEN]: {
    name: PRIVACY_POLICY_SCREEN,
    component: PrivacyPolicyScreen,
    options: () => ({
      title: 'Confirmation',
    }),
  },
  [USER_PROFILE_SCREEN]: {
    name: USER_PROFILE_SCREEN,
    component: UserProfileScreen,
  },
};

export default stack;

export { default as settingStacks, settingItems } from './settings';

/*
import { View, Platform } from 'react-native';
// import { createStackNavigator, TransitionSpecs } from 'react-navigation-stack';
import { heightPercentage, ScaleText } from '@module/utility';
import { Colors, BackButton } from '@module/acson-ui';



const AuthStack = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: ({ navigation }) => ({
        headerShown: navigation.state.routeName !== 'Login',
      }),
    },
    PrivacyPolicy: {
      screen: PrivacyPolicyScreen,
      navigationOptions: () => ({
        title: 'Confirmation',
      }),
    },
    SignUp: {
      screen: SignUpScreen,
      navigationOptions: () => ({
        title: 'Sign Up',
      }),
    },
    ConfirmPassword: {
      screen: ConfirmPasswordScreen,
      navigationOptions: () => ({
        title: 'Confirm Password',
      }),
    },
    SignUpVerification: {
      screen: SignUpVerificationScreen,
      navigationOptions: () => ({
        title: 'Verification',
      }),
    },
    ForgotPassword: {
      screen: ForgotPasswordScreen,
      navigationOptions: () => ({
        title: 'Forgot Password',
      }),
    },
    ResetPassword: {
      screen: ResetPasswordScreen,
      navigationOptions: () => ({
        title: 'Forgot Password',
      }),
    },
    AuthUserAgreement: {
      screen: UserAgreementScreen,
      navigationOptions: ({ navigation }) => ({
        title: 'Terms of Use',
        headerLeft: () => <BackButton navigation={navigation} />,
      }),
    },
    // BleMode: {
    //   screen: BLEModeScreen,
    //   navigationOptions: () => ({
    //     title: 'Available Units',
    //   }),
    // },
    // WlanMode: {
    //   screen: WlanModeScreen,
    //   navigationOptions: () => ({
    //     title: 'Available Units',
    //   }),
    // },
    // OtherModeRemote: {
    //   screen: RemoteScreen,
    // },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: {
        height: heightPercentage(10),
        elevation: 5,
        shadowOpacity: 0.2,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
        },
      },
      headerTitleStyle: {
        fontFamily: 'Avenir-Black',
        color: Colors.blueGrey,
        fontWeight: '700',
        fontSize: ScaleText(18),
      },
      headerTitleAllowFontScaling: false,
      headerTitleContainerStyle: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
      },
      headerLeft: () => <BackButton navigation={navigation} />,
      headerRight: () => <View />,
      cardStyle: {
        backgroundColor: 'white',
      },
      headerTitleAlign: 'center',
      transitionSpec: {
        open: Platform.select({
          ios: TransitionSpecs.TransitionIOSSpec,
          android: TransitionSpecs.FadeInFromBottomAndroidSpec,
        }),
        close: Platform.select({
          ios: TransitionSpecs.TransitionIOSSpec,
          android: TransitionSpecs.FadeOutToBottomAndroidSpec,
        }),
      },
    }),
    headerMode: 'float',
  }
);

export default AuthStack;
*/
