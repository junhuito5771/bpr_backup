module.exports = {
  assets: ['./assets/fonts/'],
  dependencies: {
    'react-native-device-info': {
      platforms: {
        android: null,
        ios: null,
      },
    },
    'react-native-encrypted-storage': {
      platforms: {
        android: null,
        ios: null,
      },
    },
    'react-native-camera': {
      platforms: {
        android: null,
      },
    },
    '@module/react-native-hms-camera': {
      platforms: {
        android: null,
      },
    },
  },
};
