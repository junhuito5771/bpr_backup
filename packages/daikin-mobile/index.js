import { AppRegistry } from 'react-native';

import App from './app/index';
import './shim';

AppRegistry.registerComponent('DaikinMalaysia', () => App);
