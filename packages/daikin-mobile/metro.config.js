/* eslint-disable */
/* eslint no-useless-escape: 0 */
/* eslint max-len: 0 */
/**
 * For more info regarding metro config:
 * @see https://facebook.github.io/metro/docs/en/configuration
 */
const path = require('path');
const fs = require('fs');

// Get blacklist factory
const blacklist = require('metro-config/src/defaults/blacklist');

const baseModulePath = path.resolve(__dirname, 'node_modules');
let alternateModuleMap = {};

function getPackageFile(pkgRealPath) {
  const packagePath = path.join(pkgRealPath, 'package.json');
  const packageJSON = require(packagePath);

  return packageJSON;
}

function getRealPathIfSymlinks(filePath) {
  const stats = fs.lstatSync(filePath);

  if (stats.isSymbolicLink()) {
    return fs.realpathSync(filePath);
  }

  return filePath;
}

function checkModule(fileName, alternateRoots, modulePath) {
  try {
    const fullFileName = path.join(modulePath, fileName);
    const stats = fs.lstatSync(fullFileName);

    if (stats.isSymbolicLink()) {
      const realPath = fs.realpathSync(fullFileName);

      if (realPath.substring(0, __dirname.length) !== __dirname) {
        const pkgObj = getPackageFile(realPath);
        alternateRoots[pkgObj.name] = realPath;
      }
    }
  } catch (e) {
    // void
  }
}

function checkAllModules(modulePath, alternateRoots) {
  const stats = fs.lstatSync(modulePath);
  if (!stats.isDirectory()) return alternateRoots;

  fs.readdirSync(modulePath).forEach(fileName => {
    if (fileName.charAt(0) === '.') return;

    if (fileName.charAt(0) !== '@')
      checkModule(fileName, alternateRoots, modulePath);
    else {
      checkAllModules(path.join(modulePath, fileName), alternateRoots);
    }
  });

  return alternateRoots;
}

function getSymLinkedModules() {
  alternateModuleMap = checkAllModules(baseModulePath, {});

  return Object.values(alternateModuleMap);
}

const symLinkedModules = getSymLinkedModules();

function retrieveNodeModules(pkgRealPath) {
  const packageJSON = getPackageFile(pkgRealPath);
  const alternateModules = [
    ...Object.keys(packageJSON.dependencies || {}),
    ...Object.keys(packageJSON.devDependencies || {}),
  ];

  const extraModuleMap = {};

  // Get the node_modules for every alternate roots
  alternateModules.forEach(moduleName => {
    const modulePath = path.join(pkgRealPath, 'node_modules', moduleName);

    const moduleRealPath = getRealPathIfSymlinks(modulePath);

    if (modulePath !== moduleRealPath) {
      alternateModuleMap[moduleName] = moduleRealPath;
    }

    extraModuleMap[moduleName] = moduleRealPath;
  });

  // For peer dependecies, it will refer to the base node_modules
  const alternatePeerModules = Object.keys(packageJSON.peerDependencies || {});

  alternatePeerModules.forEach(moduleName => {
    try {
      const modulePath = path.join(baseModulePath, moduleName);
      const moduleRealPath = getRealPathIfSymlinks(modulePath);

      if (modulePath !== moduleRealPath) {
        alternateModuleMap[moduleName] = moduleRealPath;
      }
      extraModuleMap[moduleName] = moduleRealPath;
    } catch (error) {}
  });

  return extraModuleMap;
}

function getExtraModulesForAlternateRoot(alternateRootMaps) {
  const extraNodeModules = {};
  Object.entries(alternateRootMaps).forEach(([key, value]) => {
    if (extraNodeModules[key] === undefined) {
      extraNodeModules[key] = value;
    }
    const alternateNodeModules = retrieveNodeModules(value);

    Object.assign(extraNodeModules, alternateNodeModules);
  });

  return extraNodeModules;
}

const extraNodeModules = getExtraModulesForAlternateRoot(alternateModuleMap);

function retrieveBlackListModules(pkgRealPath) {
  const packageJSON = getPackageFile(pkgRealPath);
  const alternateModules = [].concat(
    Object.keys(packageJSON.peerDependencies || {})
  );
  const extraModules = {};

  for (let i = 0; i < alternateModules.length; i++) {
    const moduleName = alternateModules[i];
    extraModules[moduleName] = path.join(
      pkgRealPath,
      'node_modules',
      moduleName
    );
  }

  return extraModules;
}

function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

function getBlackListModules() {
  let blackListModules = [];
  symLinkedModules.forEach(modulePath => {
    const pkgBlackListMap = retrieveBlackListModules(modulePath);

    Object.keys(pkgBlackListMap).forEach(moduleName => {
      blackListModules.push(
        RegExp(`${escapeRegExp(`${pkgBlackListMap[moduleName]}\\`)}.*`)
      );
    });
  });

  blackListModules = blackListModules.concat(/\/android\/.*/, /\/ios\/.*/);

  blackListModules = blacklist(blackListModules);

  return blackListModules;
}

const moduleBlacklist = getBlackListModules();

function getWatchFolders() {
  return Object.values(alternateModuleMap);
}

const watchFolders = getWatchFolders();

if (watchFolders && watchFolders.length) {
  console.log('Found alternate project roots: ', watchFolders);
}

function enhanceMiddleware(middleware) {
  const watchFolderNameList = [];
  
  // Has to search for iot module's folder name to replace the request for assets.
  const iotModules = ['daikin-iot-module', 'acson-iot-module', 'redux-iot-module'];
  let iotFolderName = 'iot-module';

  watchFolders.forEach(folderPath => {
    const folderTokens = folderPath.split(/[\\\/]/);
    const folderName = folderTokens.pop();
    watchFolderNameList.push(folderName);

    if (iotModules.indexOf(folderName) > -1) {
      iotFolderName = folderTokens[folderTokens.length - 2];
    }
  });

  return (req, res, next) => {
    for (let i = 0; i < watchFolderNameList.length; i += 1) {
      const foldName = watchFolderNameList[i];
      if (req.url.startsWith(`/${foldName}`)) {
        req.url = req.url.replace(`/${foldName}`, `/assets/../${foldName}`);
        break;
      } if (req.url.startsWith(`/${iotFolderName}`)) {
        req.url = req.url.replace(`/${iotFolderName}`, `/assets/../../../${iotFolderName}`);
        break;
      }
    }
    return middleware(req, res, next);
  };
}

// @see https://github.com/facebook/metro/blob/master/packages/metro-config/src/defaults/index.js
module.exports = {
  watchFolders,
  resolver: {
    extraNodeModules,
    blacklistRE: moduleBlacklist,
  },
  // Metro doesn't currently handle assets from other packages within a monorepo.
  // This is the current workaround people use
  // Refer to this PR: https://github.com/facebook/metro/issues/290
  server: {
    enhanceMiddleware,
  },
};
