import React, { useEffect } from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import {
  ApiService,
  NavigationService,
  LocationHelper,
  NotificationHelper,
  setupApi,
} from '@module/utility';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Colors, NavDrawerMenu, WelcomeOverlay } from '@module/daikin-ui';
import { connect, getStore } from '@module/redux';
import { login, userProfile } from '@module/daikin-auth';
import { content, welcome } from '@module/daikin-market';
import Config from 'react-native-config';

import { init as initModules, store } from '../modules';

import GuestDrawer from './guestDrawer';
import UserDrawer from './remoteDrawer';
import { init } from '@module/service-ticket';

setupApi({
  getTokenParams: () => getStore().getState().user.credential,
  awsClientId: Config.COGNITO_CLIENT_ID,
  awsEndpoint: Config.COGNITO_URL,
  apiEndpoint: Config.API_URL,
  onRefreshedToken: refreshedIdToken => {
    getStore().dispatch(
      userProfile.setIDToken({
        idToken: refreshedIdToken,
        refreshToken: getStore().getState().user.credential.refreshToken,
        lastTokenRefreshedDT: Date.now() / 1000,
      })
    );
  },
});

LocationHelper.init(Config.PROVIDER);
init(ApiService.getAuthToken, {
  company: 'Daikin',
});
const RootStack = createStackNavigator();

const styles = StyleSheet.create({
  headerTitleContainer: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    backgroundColor: Colors.veryLightGrey,
  },
  card: {
    backgroundColor: Colors.white,
  },
});

const Routes = ({
  initConfigAutoLoginIfCan,
  isLogin,
  initContentHelper,
  initWelcome,
  isWelcomed,
  setIsWelcomed,
  welcomeItem,
}) => {
  useEffect(() => {
    if (NavigationService.isNavigatorReady) {
      NavigationService.setNavigationReady(false);
    }

    initModules(Config, store);
    initConfigAutoLoginIfCan({
      apiEndpoint: Config.API_URL,
      cognitoEndpoint: Config.COGNITO_URL,
      cognitoClientId: Config.COGNITO_CLIENT_ID,
      cognitoIdentifyPoolId: Config.COGNITO_POOL_ID,
      cognitoUserPoolID: Config.COGNITO_USER_POOL_ID,
      region: Config.REGION,
      pinpointId: Config.PINPOINT_ID,
      pinpointRegion: Config.PINPOINT_REGION,
    });

    initContentHelper({
      bucketUrl: Config.BUCKET_URL,
      bucketEndpoint: Config.BUCKET_ENDPOINT,
    });

    initWelcome();

    LocationHelper.syncGeocode({
      endpoint: Config.GEOCODE_URL,
      key: Config.GEOCODE_KEY,
    });
  }, []);

  const handleCloseOverlay = () => {
    setIsWelcomed(true);
  };

  return (
    <>
      <WelcomeOverlay
        visible={!isWelcomed && !!welcomeItem.length}
        data={welcomeItem}
        onClose={handleCloseOverlay}
      />
      <NavigationContainer
        ref={NavigationService.navigationRef}
        linking={[`https://${Config.UNI_LINK}`]}
        onReady={() => {
          NavigationService.setNavigationReady(true);
          NotificationHelper.handleBackgroundNotification();
        }}>
        <RootStack.Navigator
          screenOptions={({ navigation }) => ({
            headerTitleContainerStyle: styles.headerTitleContainer,
            headerStyle: styles.header,
            cardStyle: styles.card,
            headerTitleAlign: 'left',
            headerTitleAllowFontScaling: false,
            headerLeft: () => <NavDrawerMenu navigation={navigation} />,
          })}
          headerMode="float">
          {isLogin ? (
            <RootStack.Screen name="RemoteDrawer" component={UserDrawer} />
          ) : (
            <RootStack.Screen name="GuestDrawer" component={GuestDrawer} />
          )}
        </RootStack.Navigator>
      </NavigationContainer>
    </>
  );
};

const mapStateToProps = ({
  login: loginState,
  user,
  welcome: welcomeState,
}) => {
  const { success, error, alertTitle, isLogin } = loginState;
  const { isAutoLogin, hasRefreshToken } = user;
  const { isWelcomed, welcomeItem } = welcomeState;

  return {
    success,
    error,
    alertTitle,
    isLogin,
    canAutoLogin: isAutoLogin && hasRefreshToken,
    isWelcomed,
    welcomeItem,
  };
};

const mapDispatchToProps = dispatch => ({
  initConfigAutoLoginIfCan: config => {
    dispatch(login.autoLogin(config));
  },
  initContentHelper: config => {
    dispatch(content.init(config));
  },
  initWelcome: () => {
    dispatch(welcome.init());
  },
  setIsWelcomed: isWelcomed => {
    dispatch(welcome.setIsWelcomed(isWelcomed));
  },
});

Routes.propTypes = {
  initConfigAutoLoginIfCan: PropTypes.func.isRequired,
  isLogin: PropTypes.bool,
  initContentHelper: PropTypes.func,
  initWelcome: PropTypes.func,
  isWelcomed: PropTypes.bool,
  welcomeItem: PropTypes.array,
  setIsWelcomed: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
