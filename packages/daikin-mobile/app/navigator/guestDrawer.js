import React, { useMemo } from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { useHeaderHeight } from '@react-navigation/stack';
import { DrawerContent, setHeaderHeight } from '@module/daikin-ui';

import { getGuestNavigators } from '../modules';
import getDrawerScreen from './getDrawerScreen';

const Drawer = createDrawerNavigator();

const guestNavigators = getGuestNavigators();

const GuestDrawer = () => {
  const { drawerScreenList, drawerMenuList } = useMemo(
    () => getDrawerScreen(guestNavigators, Drawer),
    []
  );
  const headerHeight = useHeaderHeight();

  setHeaderHeight(headerHeight);

  return (
    <Drawer.Navigator
      drawerContent={props => (
        <DrawerContent {...props} items={drawerMenuList} />
      )}>
      {drawerScreenList}
    </Drawer.Navigator>
  );
};

export default GuestDrawer;
