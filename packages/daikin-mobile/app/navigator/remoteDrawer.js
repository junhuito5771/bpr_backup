import React, { useMemo } from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { useHeaderHeight } from '@react-navigation/stack';
import { DrawerContent, setHeaderHeight } from '@module/daikin-ui';
import { getUserNavigators } from '../modules';
import getDrawerScreen from './getDrawerScreen';

const Drawer = createDrawerNavigator();

const userNavigators = getUserNavigators();

const UserDrawer = () => {
  const { drawerScreenList, drawerMenuList } = useMemo(
    () => getDrawerScreen(userNavigators, Drawer),
    []
  );

  const headerHeight = useHeaderHeight();

  setHeaderHeight(headerHeight);

  return (
    <Drawer.Navigator
      initialRouteName="RemoteHome"
      drawerContent={props => (
        <DrawerContent {...props} items={drawerMenuList} />
      )}>
      {drawerScreenList}
    </Drawer.Navigator>
  );
};

export default UserDrawer;
