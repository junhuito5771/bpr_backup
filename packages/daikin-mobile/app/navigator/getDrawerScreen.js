import React from 'react';

import { DrawerMenuIcon, DrawerMenuText } from '@module/daikin-ui';

function getDrawerScreen(drawerItem, Drawer) {
  return (
    <Drawer.Screen
      {...drawerItem}
      key={drawerItem.name}
      options={{
        drawerLabel: () => <DrawerMenuText label={drawerItem.label} />,
        drawerIcon: () => (
          <DrawerMenuIcon
            icon={drawerItem.icon}
            {...(drawerItem.iconStyle !== undefined && {
              style: drawerItem.iconStyle,
            })}
          />
        ),
        ...drawerItem.options,
      }}
    />
  );
}

export default function getDrawerScreenList(navigators, Drawer) {
  let drawerScreenObj = {};
  const drawerMenuList = [];
  navigators.forEach(drawerItem => {
    const drawerScreen = getDrawerScreen(drawerItem, Drawer);
    if (drawerItem.component) {
      drawerScreenObj = {
        ...drawerScreenObj,
        [drawerItem.name]: drawerScreen,
      };
    }

    drawerMenuList.push(drawerItem);

    if (drawerItem.childDrawer) {
      drawerItem.childDrawer.forEach(childDrawerItem => {
        const childDrawerScreen = getDrawerScreen(childDrawerItem, Drawer);
        if (childDrawerItem.component) {
          drawerScreenObj = {
            ...drawerScreenObj,
            [childDrawerItem.name]: childDrawerScreen,
          };
        }
      });
    }
  });

  return { drawerMenuList, drawerScreenList: Object.values(drawerScreenObj) };
}
