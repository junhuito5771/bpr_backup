import { hasFieldValue, addError } from './validateHelper';

const maintenanceValidator = values => {
  const errors = {};

  const { unitName, reminderName } = values;

  addError(errors, 'unitName', [
    hasFieldValue(unitName.trim()),
    'Unit name is required',
  ]);

  addError(errors, 'reminderName', [
    hasFieldValue(reminderName.trim()),
    'Reminder name is required',
  ]);

  return errors;
};

export default maintenanceValidator;
