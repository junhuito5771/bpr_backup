import {
  hasFieldValue,
  isNotExceedLength,
  isValidLength,
  addError,
} from './validateHelper';

const bindNoQRValidator = values => {
  const errors = {};

  const { ssid, password, unitName } = values;

  addError(
    errors,
    'ssid',
    [hasFieldValue(ssid), 'SSID is required'],
    [isNotExceedLength(ssid, 32), 'SSID cannot exceed 32 characters']
  );

  addError(
    errors,
    'password',
    [hasFieldValue(password), 'Password is required'],
    [isValidLength(password, 8), 'Password must be at least 8 characters']
  );

  addError(errors, 'unitName', [
    hasFieldValue(unitName),
    'Unit name is required',
  ]);

  return errors;
};

export default bindNoQRValidator;
