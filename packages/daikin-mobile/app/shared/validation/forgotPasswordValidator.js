import {
  hasFieldValue,
  isCorrectEmailFormat,
  addError,
} from './validateHelper';

const forgotPasswordValidator = values => {
  const errors = {};
  const { email } = values;

  addError(
    errors,
    'email',
    [hasFieldValue(email), 'Email address is required'],
    [isCorrectEmailFormat(email), 'Incorrect email format']
  );

  return errors;
};

export default forgotPasswordValidator;
