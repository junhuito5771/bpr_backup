import {
  hasFieldValue,
  isCorrectEmailFormat,
  addError,
} from './validateHelper';

const loginValidator = values => {
  const errors = {};

  const { email, password } = values;

  addError(
    errors,
    'email',
    [hasFieldValue(email), 'Email address is required'],
    [isCorrectEmailFormat(email), 'Incorrect email format']
  );

  addError(errors, 'password', [
    hasFieldValue(password),
    'Password is required',
  ]);

  return errors;
};

export default loginValidator;
