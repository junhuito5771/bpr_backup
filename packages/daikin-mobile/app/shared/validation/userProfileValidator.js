import { hasFieldValue, addError, isValidPhoneNumber } from './validateHelper';

const userProfileValidator = values => {
  const errors = {};
  const { name, phoneNo } = values;

  addError(errors, 'name', [hasFieldValue(name), 'Name is required']);

  addError(
    errors,
    'phoneNo',
    [hasFieldValue(phoneNo), 'Phone number is required'],
    [isValidPhoneNumber(phoneNo), 'Please enter a valid phone number']
  );

  return errors;
};

export default userProfileValidator;
