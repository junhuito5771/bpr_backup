class PreInputValidator {
  init(value) {
    this.value = value;
    return this;
  }

  noSpace() {
    this.value = this.value.replace(/\s/g, '');
    return this;
  }

  lowerCase() {
    this.value = this.value.toLowerCase();
    return this;
  }

  phoneNoFormat() {
    this.value = this.value.replace(/(\d{2})(\d{7})/, '$1-$2');
    return this;
  }

  noLeadingSpace() {
    this.value = this.value && this.value === ' ' ? '' : this.value;
    return this;
  }

  result() {
    return this.value;
  }
}

export default new PreInputValidator();

export const generalInputPreValidator = value =>
  new PreInputValidator().init(value).noLeadingSpace().result();

export const emailPreValidator = value =>
  new PreInputValidator().init(value).noSpace().lowerCase().result();

export const passwordPreValidator = value =>
  new PreInputValidator().init(value).result();

export const phoneNoPreValidator = value =>
  new PreInputValidator().init(value).noSpace().phoneNoFormat().result();
