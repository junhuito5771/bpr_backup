export const hasFieldValue = fieldValue => fieldValue;

export const isCorrectEmailFormat = email => {
  const result = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i.test(email);
  return result;
};
export const isValidLength = (fieldValue, expectedLength) =>
  fieldValue && fieldValue.length >= expectedLength;

export const isNotExceedLength = (fieldValue, expectedLength) =>
  fieldValue && fieldValue.length <= expectedLength;

export const hasBigCapital = password => {
  const result = /^(?=.*[A-Z])/.test(password);
  return result;
};

export const isValidPhoneNumber = fieldValue => fieldValue.length >= 9;

export const hasSmallCapital = password => {
  const result = /^(?=.*[a-z])/.test(password);
  return result;
};

export const hasNumber = password => {
  const result = /^(?=.*[0-9])/.test(password);
  return result;
};

export const hasSpecialCharacter = password => {
  const result = /(?=.*[!@#$%^&*\s])/.test(password);
  return result;
};

export const matchPassword = (confirmation, password) =>
  confirmation === password;

export const addError = (errorObj, inputName, ...validationResult) => {
  const failedValidation = validationResult.find(result => !result[0]);

  if (failedValidation) {
    // eslint-disable-next-line no-param-reassign
    errorObj[inputName] = failedValidation[1];
  }
};
