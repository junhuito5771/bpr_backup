import {
  hasFieldValue,
  isValidLength,
  hasBigCapital,
  hasSmallCapital,
  hasNumber,
  hasSpecialCharacter,
  matchPassword,
  addError,
} from './validateHelper';

const resetPasswordValidator = values => {
  const errors = {};
  const { password, passwordConfirmation, code } = values;

  addError(
    errors,
    'password',
    [hasFieldValue(password), 'Password is required'],
    [isValidLength(password, 8), 'Password must be at least 8 characters']
  );

  if (!errors.password) {
    const optionlValidation = [
      [
        hasBigCapital(password),
        'Password must be contains at least one capital letter (A-Z)',
      ],
      [
        hasSmallCapital(password),
        'Password must be contains at least one small capital letter (a-z)',
      ],
      [
        hasNumber(password),
        'Password must be contains at least one numeric character (0-9)',
      ],
      [
        hasSpecialCharacter(password),
        'Password must be contains at least one special character',
      ],
    ];

    let nrValid = 0;
    let errorMsg = '';

    optionlValidation.forEach(curValidator => {
      if (curValidator[0]) nrValid += 1;
      // Only append the error message when cur validator is false
      // and it also the first false validator
      if (!curValidator[0] && !errorMsg) {
        errorMsg = curValidator[1];
      }
    });

    if (nrValid < 3 && errorMsg) {
      errors.password = errorMsg;
    }
  }

  addError(errors, 'passwordConfirmation', [
    matchPassword(passwordConfirmation, password),
    'Password confirmation does not match password',
  ]);

  addError(errors, 'code', [
    hasFieldValue(code),
    'Verification code is required',
  ]);

  addError(errors, 'code', [isValidLength(code, 6), '']);

  return errors;
};

export default resetPasswordValidator;
