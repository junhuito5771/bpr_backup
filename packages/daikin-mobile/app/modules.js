import {
  combineModuleReducers,
  combineModuleSaga,
  ConfigureStore,
} from '@module/redux';

import * as DaikinAuth from '@module/daikin-auth';
import * as DaikinMarket from '@module/daikin-market';
import * as DaikinIoT from '@daikin-dama/daikin-iot';
//import * as DaikinIoT from '@module/iot-dummy';

import DrawerSeq from './drawerSeq';

const TYPE_GUEST = 'GuestDrawer';
const TYPE_USER = 'UserDrawer';

const modules = [DaikinAuth, DaikinMarket, DaikinIoT];

function onLoad() {
  const sagaList = [];
  let reducerList = {};

  modules.forEach(module => {
    if (module && module.sagas) {
      sagaList.push(module.sagas);
    }

    if (module && module.reducers) {
      reducerList = {
        ...reducerList,
        ...module.reducers,
      };
    }
  });

  combineModuleReducers(reducerList);

  combineModuleSaga(sagaList);
}

onLoad();

function init(config, helpers) {
  const hasIoTModule = modules.some(module => module && module.isIoTModule);
  modules.forEach(module => {
    if (module && module.init) {
      module.init(config, helpers);
    }

    if (module && module.IntegratedModule) {
      module.IntegratedModule.setAllConfig({ isIoTEnabled: hasIoTModule });

      module.IntegratedModule.initExternalModule();
    }
  });
}

function getNavigatorMap(moduleList, type = TYPE_GUEST) {
  let navigatorMap = {};
  moduleList.forEach(module => {
    if (module && module.Drawer && module.Drawer[type]) {
      navigatorMap = {
        ...navigatorMap,
        ...module.Drawer[type],
      };
    }
  });

  return navigatorMap;
}

function getGuestNavigators() {
  const navigators = [];
  const navigatorMap = getNavigatorMap(modules, TYPE_GUEST);

  DrawerSeq.guest.forEach(key => {
    if (navigatorMap[key]) navigators.push(navigatorMap[key]);
  });

  return navigators;
}

function getUserNavigators() {
  const navigators = [];
  const navigatorMap = getNavigatorMap(modules, TYPE_USER);

  DrawerSeq.user.forEach(key => {
    if (navigatorMap[key]) navigators.push(navigatorMap[key]);
  });

  return navigators;
}

export {
  DaikinAuth,
  DaikinMarket,
  init,
  getGuestNavigators,
  getUserNavigators,
};

export const { store, persistor } = ConfigureStore();
// eslint-disable-next-line
const dispatch = store.dispatch;

export { dispatch };
