import React, { useEffect } from 'react';
import 'react-native-gesture-handler';
import { Alert, Platform, Linking, AppState, Text } from 'react-native';
import { StoreHelper, CrashReportHelper } from '@module/utility';
import { Provider, PersistGate } from '@module/redux';
import Config from 'react-native-config';
import { MenuProvider } from 'react-native-popup-menu';
import SplashScreen from 'react-native-splash-screen';

import { ErrorBoundary } from '@module/daikin-ui';
import PushNotification from './container/pushNotification';
import Routes from './navigator';

import { store, persistor } from './modules';

CrashReportHelper.init(Config.BUG_SNAG_ID);

SplashScreen.hide();

const promptAlertIfUpdateNeeded = async appID => {
  if (await StoreHelper.needUpdate()) {
    const storeURL = await StoreHelper.getStoreURL(appID);
    const latestVersion = await StoreHelper.getLatestVersion();
    const storeName = Platform.select({
      ios: 'AppStore',
      android: 'PlayStore',
    });
    Alert.alert(
      'Update Available',
      `We've become better! Kindly update to version ${latestVersion} to enjoy a better experience of the app.`,
      [
        {
          text: `Go to ${storeName}`,
          onPress: () => {
            Linking.openURL(storeURL);
          },
        },
      ],
      { cancelable: false, onDismiss: () => {} }
    );
  }
};

// NOTE: To handle Xiao MI MIUI12 issue, see https://github.com/facebook/react-native/issues/29259 for more details
const originTextRender = Text.render;
Text.render = function render(props, ref) {
  return originTextRender.apply(this, [
    {
      ...props,
      style: [
        Platform.OS === 'android' ? { fontFamily: '' } : null,
        props.style,
      ],
    },
    ref,
  ]);
};

const handleAppStateChange = nextAppState => {
  if (nextAppState === 'active') {
    const appID = Config.APP_STORE_ID;
    if (appID && !__DEV__) {
      promptAlertIfUpdateNeeded(appID);
    }
  }
};

const App = () => {
  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);

    return () => AppState.removeEventListener('change', handleAppStateChange);
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ErrorBoundary onError={CrashReportHelper.notify}>
          <MenuProvider>
            <Routes />
            <PushNotification />
          </MenuProvider>
        </ErrorBoundary>
      </PersistGate>
    </Provider>
  );
};

export default App;
