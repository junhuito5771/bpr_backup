// This file specificy the sequence being shown in the drawer
// It will based with module's user navigator and drawer navigator

const guest = [
  'MarketingHome',
  'Promotion',
  'Product',
  'Help',
  'ErrorCode',
  'LocateDealer',
  'Facebook',
  'HeatLoadCalculator',
  'ContactUs',
  'ServiceBooking',
  'ServiceListing',
  'Login',
];

const user = [
  'Home',
  'WeeklyTimer',
  'UsageList',
  'UserProfile',
  'Alexa',
  'AppSettings',
  'OperationManual',
  'MarketingHome',
  'Help',
  'ErrorCode',
  'Subscription',
  'Logout',
];

export default { guest, user };
