import { NavigationActions, StackActions } from '@react-navigation/compat';
import { DrawerActions, TabActions } from '@react-navigation/native';

let navigator;

function getTopLevelNavigator(navigation) {
  const parent = navigation.dangerouslyGetParent();

  if (parent) {
    return getTopLevelNavigator(parent);
  }
  return navigation;
}

function setOptions(navigation, options) {
  const rootNavigation = getTopLevelNavigator(navigation);
  if (rootNavigation) {
    rootNavigation.setOptions({
      headerRight: () => {}, // to prevent cascade effect of header right options
      ...options,
    });
  }
}

function setTopLevelNavigator(navigatorRef) {
  navigator = navigatorRef;
}

function navigate(routeName, params) {
  if (navigator) {
    navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      })
    );
  }
}

function reset(index, routeName, params) {
  navigator.dispatch(
    StackActions.reset({
      index,
      actions: [NavigationActions.navigate({ routeName, params })],
    })
  );
}

function drawerReset(childRouteName, params) {
  navigator.dispatch(DrawerActions.jumpTo('WeeklyTimer'));
  setTimeout(
    () => navigator.dispatch(TabActions.jumpTo(childRouteName, params)),
    1000
  );
}

function setParams(params) {
  navigator.setParams(params);
}

export default {
  getTopLevelNavigator,
  setOptions,
  navigate,
  setTopLevelNavigator,
  reset,
  drawerReset,
  setParams,
};
