import React from 'react';
import PropTypes from 'prop-types';
import { connect, settings } from '@module/redux';
import RNRestartHandler from 'react-native-restart';

import ErrorBoundry from 'common/components/ErrorBoundary';

const withSagaErrorBoundary = ({ sagaError, clearSagaError, ...restProps }) => {
  const handleOnReload = () => {
    clearSagaError();

    setTimeout(() => RNRestartHandler.Restart(), 100);
  };

  return (
    <ErrorBoundry
      otherError={sagaError}
      {...restProps}
      onReload={handleOnReload}
    />
  );
};

const mapStateToProps = ({ settings: { sagaError } }) => ({
  sagaError,
});

const mapDispatchToProps = dispatch => ({
  clearSagaError: () => dispatch(settings.setSagaError('')),
});

withSagaErrorBoundary.propTypes = {
  sagaError: PropTypes.string,
  clearSagaError: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withSagaErrorBoundary);
