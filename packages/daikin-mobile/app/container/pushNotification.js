import { useEffect } from 'react';
import { connect } from '@module/redux';
import { NotificationHelper, NavigationService } from '@module/utility';
import Config from 'react-native-config';

import { store } from '../modules';

const notificationHandler = notification => {
  const currentState = store.getState();
  const isLogin = currentState.user.credential.idToken;

  const targetDrawer = isLogin ? 'RemoteDrawer' : 'GuestDrawer';

  if (notification.data) {
    if (notification.data.action === 'URL') {
      NavigationService.navigate(targetDrawer, {
        screen: 'Promotion',
        params: {
          screen: 'PromotionWeb',
          params: {
            link: notification.data.url,
            title: notification.data.urlTitle,
          },
        },
      });
    } else if (notification.data.action === 'HOME' && isLogin) {
      NavigationService.navigate('RemoteDrawer', {
        screen: 'Home',
      });
    } else if (notification.data.action === 'ERROR_HISTORY' && isLogin) {
      NavigationService.navigate('RemoteDrawer', {
        screen: 'Home',
        params: {
          screen: 'ErrorHistory',
          params: { thingName: notification.data.thingName },
        },
      });
    }
  }
};

NotificationHelper.init(Config.PROVIDER, notificationHandler);

const PushNotificationContainer = ({ email, name, isLogin }) => {
  useEffect(() => {
    const curIsLogin = !!name && isLogin;
    NotificationHelper.executeSyncTokenProcess({
      username: email,
      isLogin: curIsLogin,
    });
  }, [email, name, isLogin]);
  return null;
};

const mapStateToProps = ({
  user: {
    profile: { email, name },
  },
  login: { isLogin },
}) => ({ email, name, isLogin });

export default connect(mapStateToProps)(PushNotificationContainer);
