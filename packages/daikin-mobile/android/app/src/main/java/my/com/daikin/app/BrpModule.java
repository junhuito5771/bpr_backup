package my.com.daikin.app;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import java.net.InetAddress;
import java.util.Map;
import java.util.HashMap;

// BYPASS SSL NEEDED LIBRARY
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.net.ssl.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
// BYPASS SSL NEEDED LIBRARY

import android.os.Build;
import android.util.Log;


public class BrpModule extends ReactContextBaseJavaModule {

    private String defaultIp = "192.168.127.1";

    BrpModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "BrpModule";
    }

    @ReactMethod
    public void getBasicInfo(String uuid, String ip, Callback callBack){
        String res;
        res = apiCall("/common/basic_info", null, uuid, ip);
        callBack.invoke(res);
    }

    @ReactMethod
    public void registerTerminal(String key, String uuid,Callback callBack){
        String res;
        String ip = "192.168.127.1"; // DEFAULT IP
        res = apiCall("/common/register_terminal?key=" + key, null, uuid, ip);
        callBack.invoke(res);
    }

//    @ReactMethod(isBlockingSynchronousMethod = true)
//    public String scanWifi(String uuid, String ip){
//        String res;
//        res = apiCall("/common/start_wifi_scan", null, uuid, ip);
//        return res;
//    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String scanWifi(String uuid){
        String res;
        res = apiCall("/common/start_wifi_scan", null, uuid, defaultIp);
        return res;
    }

//    @ReactMethod(isBlockingSynchronousMethod = true)
//    public String startWifiConnection(String key, String uuid, String ip){
//        String res;
//        res = apiCall("/common/start_wifi_connection?key=" + key, null, uuid, defaultIp);
//        return res;
//    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String startWifiConnection(String key, String uuid){
        String res;
        res = apiCall("/common/start_wifi_connection?key=" + key, null, uuid, defaultIp);
        return res;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String setWifiSetting(String ssid, String security, String key, String uuid){
        String res;
        res = apiCall("/common/set_wifi_setting?ssid=" + ssid + "&security=" + security + "&key=" + key, null, uuid, defaultIp);
        return res;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String setNetworkSetting(String param, String uuid){
        String res;
        res = apiCall("/common/set_network_setting?", param, uuid, defaultIp);
        return res;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String setControlInfo(String param, String uuid, String ip){
        String res;
        res = apiCall("/aircon/set_control_info?", param, uuid, ip);
        return res;
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String getControlInfo(String ip, String uuid){
        String res;
        res = apiCall("/aircon/get_control_info", null, uuid, ip);
        return res;
    }

    // CHECK IP IS NOT CONFLICT
    @ReactMethod(isBlockingSynchronousMethod = true)
    public boolean isAvailableIp(String ip){
        try{
            InetAddress address = InetAddress.getByName(ip);
            boolean reachable = !address.isReachable(1000);
            return reachable;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static void cl(String param){
        System.out.println(param);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String apiTest(String httpslink){
        HttpURLConnection http = null;
        try{
            URL url;
            url = new URL(httpslink);
            trustAllHosts();
            HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
            https.setConnectTimeout(5000); // SET CONNECTION TIMEOUT TO 5s
            https.setHostnameVerifier(DO_NOT_VERIFY);

            http = https;
            http.setRequestProperty("X-Daikin-uuid", "1bd02715c0c84ed2acc2d9cc62bd81e0");
            http.setRequestMethod("GET");
            http.setDoOutput(true);
            http.setDoInput(true);
            return readResponse(http.getInputStream());

        }catch(Exception e){
            e.printStackTrace();
            return "err='Connection time out'";
        }
    }

    public String apiCall(String action, String param, String uuid, String ip) {
        HttpURLConnection http = null;
        try{
            URL url;
            if(param != null){
                // REQUEST WITH BODY
                cl("GET: https://" + ip + action + param); // CONSOLE LOG
                url = new URL("https://" + ip + action + param);
            }else{
                cl("GET: https://" + ip + action); // CONSOLE LOG
                url = new URL("https://" + ip + action);
            }

            trustAllHosts();
            HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
            https.setConnectTimeout(5000); // SET CONNECTION TIMEOUT TO 5s
            https.setHostnameVerifier(DO_NOT_VERIFY);
            http = https;
            http.setRequestProperty("X-Daikin-uuid", uuid);
            http.setRequestMethod("GET");
            http.setDoOutput(true);
            http.setDoInput(true);
            return readResponse(http.getInputStream());

        }catch(Exception e){
            e.printStackTrace();
            return "err='Connection time out'";
        }

//        return null;
    }

    public static String readResponse(InputStream inputStream) throws IOException{

        BufferedReader in = new BufferedReader(
                new InputStreamReader(inputStream)
        );

        StringBuilder response = new StringBuilder();
        String currentLine;

        while ((currentLine = in.readLine()) != null)
            response.append(currentLine);

        in.close();
        return response.toString();
    }

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException
            {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}