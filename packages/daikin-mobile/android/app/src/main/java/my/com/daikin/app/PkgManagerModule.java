package my.com.daikin.app;

import androidx.annotation.NonNull;
import android.content.pm.PackageInfo;
import android.os.Build;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

public class PkgManagerModule extends ReactContextBaseJavaModule  {

	PkgManagerModule(ReactApplicationContext context) {
		super(context);
	}

	@NonNull
	@Override
	public String getName() {
		return "PkgManagerModule";
	}

	@ReactMethod
	public void isAppInstalled(String packageName, Integer minVersion, Promise promise) {
		try {
			PackageInfo pkgInfo = getReactApplicationContext().getPackageManager().getPackageInfo(packageName, 0);

			Boolean isInstalled = pkgInfo != null;;

			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
				isInstalled = pkgInfo.getLongVersionCode() > minVersion;
			}
			promise.resolve(isInstalled);
		} catch(Exception error) {
			promise.resolve(false);
		}
	}
}
