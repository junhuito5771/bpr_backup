package my.com.daikin.app;

import com.facebook.react.ReactPackage;
import io.invertase.firebase.app.ReactNativeFirebaseAppPackage;
import io.invertase.firebase.messaging.ReactNativeFirebaseMessagingPackage;
import org.reactnative.camera.RNCameraPackage;
import com.heanoria.library.reactnative.locationenabler.RNAndroidLocationEnablerPackage;
import com.airbnb.android.react.maps.MapsPackage;

import java.util.List;
import java.util.ArrayList;

public class FlavourPackage {
    static public List<ReactPackage> getFlavourPackage() {
        List<ReactPackage> packages = new ArrayList<ReactPackage>();
        packages.add(new ReactNativeFirebaseAppPackage());
        packages.add(new ReactNativeFirebaseMessagingPackage());
        packages.add(new RNCameraPackage());
        packages.add(new RNAndroidLocationEnablerPackage());
        packages.add(new MapsPackage());
        return packages;
    }
}
