package my.com.daikin.app;

import com.facebook.react.ReactPackage;
import com.huawei.hms.rn.push.HmsPushPackage;
import com.huawei.hms.rn.camera.RNHMSCameraPackage;
import com.huawei.hms.rn.location.RNHMSLocationPackage;
import com.huawei.hms.rn.map.RNHMSMapPackage;

import java.util.List;
import java.util.ArrayList;

public class FlavourPackage {
    static public List<ReactPackage> getFlavourPackage() {
        List<ReactPackage> packages = new ArrayList<ReactPackage>();
        packages.add(new HmsPushPackage());
        packages.add(new RNHMSCameraPackage());
        packages.add(new RNHMSLocationPackage());
        packages.add(new RNHMSMapPackage());
        return packages;
    }
}