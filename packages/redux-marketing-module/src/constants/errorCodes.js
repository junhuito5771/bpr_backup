export const damaErrorCodes = [
  {
    code: '00',
    description: 'Normal',
  },
  {
    code: 'A1',
    description: 'Indoor PCB Error',
  },
  {
    code: 'A3',
    description: 'Drain Pump Abnormal',
  },
  {
    code: 'A5',
    description: 'Antifreeze (Cooling)/ Heat Exchanger Overheat (Heating)',
  },
  {
    code: 'A6',
    description: 'Indoor Fan Motor Abnormal',
  },
  {
    code: 'AH',
    description: 'Electrical Air Cleaner Abnormal',
  },
  {
    code: 'C4',
    description: 'Indoor Heat Exchanger (1) Thermistor Short/ Open',
  },
  {
    code: 'C5',
    description: 'Indoor Heat Exchanger (2) Thermistor Short/ Open',
  },
  {
    code: 'C7',
    description: 'Louver Limit Switch Error',
  },
  {
    code: 'C9',
    description: 'Indoor Room Thermistor Short/ Open',
  },
  {
    code: 'E1',
    description: 'Outdoor PCB Error',
  },
  {
    code: 'E3',
    description: 'High Pressure Protection',
  },
  {
    code: 'E4',
    description: 'Low Pressure Protection',
  },
  {
    code: 'E5',
    description: 'Compressor Motor Lock/ Compressor Overloaded',
  },
  {
    code: 'E6',
    description: 'Compressor Start-up Error',
  },
  {
    code: 'E7',
    description: 'Outdoor DC Fan Motor Lock',
  },
  {
    code: 'E8',
    description: 'AC Input Over Current',
  },
  {
    code: 'E9',
    description: 'EXV Error',
  },
  {
    code: 'EA',
    description: '4 Way Valve Error',
  },
  {
    code: 'F3',
    description: 'Discharge Pipe Overheat',
  },
  {
    code: 'F6',
    description: 'Heat Exchanger Overheat',
  },
  {
    code: 'HO',
    description: 'Compressor Sensor System Error',
  },
  {
    code: 'H3',
    description: 'High Pressure Switch Error',
  },
  {
    code: 'H6',
    description: 'Compressor Feedback Detection Error',
  },
  {
    code: 'H7',
    description: 'Fan Motor Overloaded/ Overcurrent/ Sensor Abnormal',
  },
  {
    code: 'H8',
    description: 'AC Current Sensor Error',
  },
  {
    code: 'H9',
    description: 'Outdoor Air Thermistor Short/ Open',
  },
  {
    code: 'J1',
    description: 'Pressure Sensor Error',
  },
  {
    code: 'J3',
    description: 'Compressor Discharge Pipe Thermistor Short/ Open/ Misplaced',
  },
  {
    code: 'J5',
    description: 'Suction Pipe Thermistor Short/ Open',
  },
  {
    code: 'J6',
    description: 'Outdoor Heat Exchanger Thermistor Short/ Open',
  },
  {
    code: 'J7',
    description: 'Subcooling Heat Exchanger Thermistor Short/ Open',
  },
  {
    code: 'J8',
    description: 'Liquid Pipe Thermistor Short/ Open',
  },
  {
    code: 'J9',
    description: 'Gas Pipe Thermistor Short/ Open',
  },
  {
    code: 'L1',
    description: 'Inverter Outdoor PCB Error',
  },
  {
    code: 'L3',
    description: 'Outdoor Control Box Overheat',
  },
  {
    code: 'L4',
    description: 'Heat Sink Overheat',
  },
  {
    code: 'L5',
    description: 'IPM Error/ IGBT Error',
  },
  {
    code: 'L8',
    description: 'Inverter Compressor Overcurrent',
  },
  {
    code: 'L9',
    description: 'Compressor Overcurrent Prevention',
  },
  {
    code: 'LC',
    description: 'Communication Error (Outdoor Control PCB and Inverter PCB)',
  },
  {
    code: 'P1',
    description: 'Open Phase or Voltage Unbalance',
  },
  {
    code: 'P4',
    description: 'Heat Sink Thermistor Short/ Open',
  },
  {
    code: 'PJ',
    description: 'Capacity Setting Error',
  },
  {
    code: 'U0',
    description: 'Insufficient Gas',
  },
  {
    code: 'U2',
    description: 'DC Voltage Out of Range',
  },
  {
    code: 'U4',
    description: 'Communication Error',
  },
  {
    code: 'U7',
    description: 'Communication Error (Outdoor Control PCB and IPM PCB)',
  },
  {
    code: 'UA',
    description: 'Installation Error',
  },
  {
    code: 'UF',
    description:
      'Piping & Wiring Installation Mismatch/ Wrong Wiring/ Insufficient Gas',
  },
  {
    code: 'UH',
    description: 'Antifreeze (Other Rooms)',
  },
];

export const ditErrorCodes = [
  {
    code: '00',
    description: 'Normal',
  },
  {
    code: 'A0',
    description: 'External protection device activated',
  },
  {
    code: 'A1',
    description: 'Malfunction of indoor unit PCB',
  },
  {
    code: 'A3',
    description: 'Malfunction of drain level system',
  },
  {
    code: 'A4',
    description: 'Malfunction of freezing protection',
  },
  {
    code: 'A5',
    description:
      'High pressure control in heating, freeze up protection control in cooling',
  },
  {
    code: 'A6',
    description: 'Malfunction of fan motor',
  },
  {
    code: 'A7',
    description: 'Malfunction of swing flap motor',
  },
  {
    code: 'A8',
    description: 'Malfunction of power supply or AC input overcurrent',
  },
  {
    code: 'A9',
    description: 'Malfunction of electronic expansion valve',
  },
  {
    code: 'AA',
    description: 'Heater overheat',
  },
  {
    code: 'AC',
    description: 'Stop due to low water level',
  },
  {
    code: 'AE',
    description: 'Low water level - No water supply',
  },
  {
    code: 'AF',
    description: 'Malfunction of a humidifier system',
  },
  {
    code: 'AH',
    description: 'Malfunction of dust collector of air cleaner',
  },
  {
    code: 'AJ',
    description: 'Malfunction of capacity setting (Indoor unit PCB)',
  },
  {
    code: 'C0',
    description: 'Malfunction of sensor system (Unified)',
  },
  {
    code: 'C1',
    description:
      'Failure of transmission (Between indoor unit PCB and sub PCB)',
  },
  {
    code: 'C3',
    description: 'Malfunction of drain level sensor',
  },
  {
    code: 'C4',
    description: 'Malfunction of liquid pipe thermistor for heat exchanger',
  },
  {
    code: 'C5',
    description: 'Malfunction of gas pipe thermistor for heat exchanger',
  },
  {
    code: 'C6',
    description: 'Malfunction of fan motor sensor or fan control driver',
  },
  {
    code: 'C7',
    description: 'Front panel driving motor fault',
  },
  {
    code: 'C8',
    description: 'Malfunction of AC input current sensor system',
  },
  {
    code: 'C9',
    description: 'Malfunction of suction air thermistor',
  },
  {
    code: 'CA',
    description: 'Malfunction of discharge air thermistor',
  },
  {
    code: 'CC',
    description: 'Malfunction of humidity sensor system',
  },
  {
    code: 'CE',
    description: 'Malfunction of switch box thermistor',
  },
  {
    code: 'CF',
    description: 'Malfunction of high pressure switch',
  },
  {
    code: 'CJ',
    description: 'Malfunction of thermostat sensor in remote controller',
  },
  {
    code: 'E0',
    description: 'Protection devices activated (Unified)',
  },
  {
    code: 'E1',
    description: 'Defect of outdoor unit PCB',
  },
  {
    code: 'E2',
    description: 'Malfunction of cold room thermistor',
  },
  {
    code: 'E3',
    description: 'Actuation of high pressure switch (HPS)',
  },
  {
    code: 'E4',
    description: 'Actuation of low pressure switch (LPS)',
  },
  {
    code: 'E5',
    description: 'Inverter compressor motor or overheat',
  },
  {
    code: 'E6',
    description: 'STD compressor motor overcurrent/ lock',
  },
  {
    code: 'E7',
    description: 'Malfunction of outdoor unit fan motor system',
  },
  {
    code: 'E8',
    description: 'Overcurrent of inverter compressor',
  },
  {
    code: 'E9',
    description: 'Malfunction of electronic expansion valve coil',
  },
  {
    code: 'EA',
    description: 'Malfunction of four way valve or cool/ heat switching',
  },
  {
    code: 'EC',
    description: 'Malfunction of entering water temperature',
  },
  {
    code: 'EE',
    description: 'Malfunction of drain water level',
  },
  {
    code: 'EF',
    description: 'Malfunction of thermal storage unit',
  },
  {
    code: 'EH',
    description: 'Malfunction of cooling water pump',
  },
  {
    code: 'EJ',
    description: 'Actuation of option protection device',
  },
  {
    code: 'F3',
    description: 'Malfunction of discharge pipe temperature',
  },
  {
    code: 'F4',
    description: 'Malfunction of suction pipe thermistor',
  },
  {
    code: 'F6',
    description: 'Abnormal high pressure or refrigerant overcharged',
  },
  {
    code: 'FA',
    description: 'Abnormal high pressure actuation of HPS',
  },
  {
    code: 'FC',
    description: 'Abnormal low pressure',
  },
  {
    code: 'FE',
    description: 'Abnormal oil pressure',
  },
  {
    code: 'FF',
    description: 'Abnormal oil level or shortage of oil',
  },
  {
    code: 'FH',
    description: 'Abnormal high temperature of refrigerant oil',
  },
  {
    code: 'FJ',
    description: 'Abnormal exhaust temperature of engine',
  },
  {
    code: 'H0',
    description: 'Malfunction of sensor system of compressor',
  },
  {
    code: 'H1',
    description:
      'Malfunction of room temperature sensor or humidifier unit damper',
  },
  {
    code: 'H2',
    description: 'Malfunction of power supply sensor',
  },
  {
    code: 'H3',
    description: 'Malfunction of high pressure switch (HPS)',
  },
  {
    code: 'H4',
    description: 'Malfunction of low pressure switch (LPS)',
  },
  {
    code: 'H5',
    description: 'Malfunction of compressor motor overload thermistor',
  },
  {
    code: 'H6',
    description: 'Malfunction of position detection sensor',
  },
  {
    code: 'H7',
    description: 'Malfunction of outdoor fan motor signal',
  },
  {
    code: 'H8',
    description: 'Malfunction of compressor input (CT) system',
  },
  {
    code: 'H9',
    description: 'Malfunction of outdoor air thermistor',
  },
  {
    code: 'HA',
    description: 'Malfunction of discharge air thermistor',
  },
  {
    code: 'HC',
    description: 'Malfunction of (hot) water temperature thermistor',
  },
  {
    code: 'HE',
    description: 'Malfunction of drain water level sensor',
  },
  {
    code: 'HF',
    description: 'Alarm in thermal storage unit or storage controller',
  },
  {
    code: 'HH',
    description: 'High room temperature alarm',
  },
  {
    code: 'HJ',
    description: 'Malfunction of thermal storage tank water level',
  },
  {
    code: 'J0',
    description: 'Miswiring of thermistor',
  },
  {
    code: 'J1',
    description: 'Malfunction of pressure sensor',
  },
  {
    code: 'J2',
    description: 'Malfunction of current sensor of compressor',
  },
  {
    code: 'J3',
    description: 'Malfunction of discharge pipe thermistor',
  },
  {
    code: 'J4',
    description:
      'Malfunction of low pressure equivalent saturated temperature sensor system',
  },
  {
    code: 'J5',
    description: 'Malfunction of suction pipe thermistor',
  },
  {
    code: 'J6',
    description: 'Malfunction of heat exchanger thermistor',
  },
  {
    code: 'J7',
    description: 'Malfunction of thermistor (Refrigerant circuit)',
  },
  {
    code: 'J8',
    description: 'Malfunction of thermistor (Refrigerant circuit)',
  },
  {
    code: 'J9',
    description: 'Malfunction of thermistor (Refrigerant circuit)',
  },
  {
    code: 'JA',
    description: 'Malfunction of high pressure sensor',
  },
  {
    code: 'JC',
    description: 'Malfunction of low pressure sensor',
  },
  {
    code: 'JE',
    description: 'Malfunction of oil pressure sensor or subtank thermistor',
  },
  {
    code: 'JF',
    description:
      'Malfunction of oil level sensor or heating heat exchanger thermistor',
  },
  {
    code: 'JH',
    description: 'Malfunction of oil temperature thermistor',
  },
  {
    code: 'JJ',
    description:
      'Malfunction of engine room temperature sensor or exhaust temperature',
  },
  {
    code: 'L0',
    description: 'Malfunction of inverter system',
  },
  {
    code: 'L1',
    description: 'Malfunction of inverter PCB',
  },
  {
    code: 'L3',
    description: 'Electrical box temperature rise',
  },
  {
    code: 'L4',
    description: 'Malfunction of inverter radiating fin temperature rise',
  },
  {
    code: 'L5',
    description: 'Inverter instantaneous overcurrent (DC output)',
  },
  {
    code: 'L6',
    description: 'Inverter instantaneous overcurrent (AC output)',
  },
  {
    code: 'L7',
    description: 'Total input overcurrent',
  },
  {
    code: 'L8',
    description: 'Malfunction of overcurrent inverter compressor',
  },
  {
    code: 'L9',
    description:
      'Malfunction of inverter compressor startup error (Stall prevention)',
  },
  {
    code: 'LA',
    description: 'Malfunction of power transistor',
  },
  {
    code: 'LC',
    description: 'Malfunction of transmission between control and inverter PCB',
  },
  {
    code: 'LE',
    description: 'Malfunction of igniter system',
  },
  {
    code: 'LF',
    description: 'Engine startup error',
  },
  {
    code: 'LH',
    description: 'Malfunction of generator converter',
  },
  {
    code: 'LJ',
    description: 'Engine stop',
  },
  {
    code: 'P0',
    description: 'Shortage of refrigerant amount (Thermal storage unit)',
  },
  {
    code: 'P1',
    description: 'Power voltage imbalance or inverter PCB',
  },
  {
    code: 'P2',
    description: 'Automatic refrigerant charge operation stop',
  },
  {
    code: 'P3',
    description: 'Malfunction of thermistor in switch box',
  },
  {
    code: 'P4',
    description: 'Malfunction of radiating fin temperature sensor',
  },
  {
    code: 'P5',
    description: 'Malfunction of DC current sensor',
  },
  {
    code: 'P6',
    description: 'Malfunction of AC or DC output current sensor',
  },
  {
    code: 'P7',
    description: 'Malfunction of total input current sensor',
  },
  {
    code: 'P8',
    description:
      'Heat exchanger freezing protection during automatic refrigerant charging',
  },
  {
    code: 'P9',
    description: 'Automatic refrigerant charge operation completed',
  },
  {
    code: 'PA',
    description: 'Refrigerant cylinder during automatic refrigerant charging',
  },
  {
    code: 'PC',
    description: 'Refrigerant cylinder during automatic refrigerant charging',
  },
  {
    code: 'PE',
    description: 'Automatic refrigerant charge operation nearly completed',
  },
  {
    code: 'PF',
    description: 'Malfunction of starter actuation',
  },
  {
    code: 'PH',
    description: 'Refrigerant cylinder during automatic refrigerant charging',
  },
  {
    code: 'PJ',
    description: 'Improper combination between interver and fan driver',
  },
  {
    code: 'U0',
    description: 'Shortage of refrigerant',
  },
  {
    code: 'U1',
    description: 'Reverse phase, open phase',
  },
  {
    code: 'U2',
    description: 'Malfunction of power supply or instantaneous power failure',
  },
  {
    code: 'U3',
    description: 'Check operation not executed or transmission error',
  },
  {
    code: 'U4',
    description: 'Malfunction of transmission between indoor and outdoor unit',
  },
  {
    code: 'U5',
    description:
      'Malfunction of transmission between indoor unit and remote controller',
  },
  {
    code: 'U6',
    description: 'Malfunction of transmission between indoor units',
  },
  {
    code: 'U7',
    description:
      'Malfunction of transmission between outdoor units and outdoor storage unit',
  },
  {
    code: 'U8',
    description: 'Malfunction of transmission between remote controllers',
  },
  {
    code: 'U9',
    description: 'Malfunction of transmission (Other system)',
  },
  {
    code: 'UA',
    description: 'Improper combination of indoor and outdoor units',
  },
  {
    code: 'UC',
    description: 'Malfunction of setting of centralized controller address',
  },
  {
    code: 'UE',
    description:
      'Malfunction of transmission between indoor unit and centralized controller',
  },
  {
    code: 'UF',
    description: 'Wiring and piping mismatch',
  },
  {
    code: 'UH',
    description: 'Malfunction of system',
  },
  {
    code: 'UJ',
    description: 'Malfunction of transmission (Accessory device)',
  },
  {
    code: 'M1',
    description: 'Malfunction of centralized remote controller PCB',
  },
  {
    code: 'M8',
    description:
      'Malfunction of transmission between optional controllers for centralized control',
  },
  {
    code: 'MA',
    description:
      'Improper combination of optional controllers for centralized control',
  },
  {
    code: 'MC',
    description: 'Address duplication, improper setting',
  },
  {
    code: '60',
    description: 'External protection device activated (HRV)',
  },
  {
    code: '61',
    description: 'Malfunction of PCB',
  },
  {
    code: '62',
    description: 'Ozone density abnormal',
  },
  {
    code: '63',
    description: 'Contaminated sensor error',
  },
  {
    code: '64',
    description: 'Malfunction of thermistor for indoor air (HRV)',
  },
  {
    code: '65',
    description: 'Malfunction of thermistor for outdoor air (HRV)',
  },
  {
    code: '66',
    description: 'Supply air passage closed',
  },
  {
    code: '67',
    description: 'Exhaust air passage closed',
  },
  {
    code: '68',
    description: 'Malfunction of dust collection unit (HRV)',
  },
  {
    code: '6A',
    description: 'Malfunction of damper system (HRV)',
  },
  {
    code: '6C',
    description: 'Replace the humidify element',
  },
  {
    code: '6E',
    description: 'Replace the deodorising catalyst',
  },
  {
    code: '6F',
    description: 'Simplified remote controller malfunction (HRV)',
  },
  {
    code: '6H',
    description: 'Door switch open (HRV)',
  },
  {
    code: '6J',
    description: 'Replace the high efficient filter',
  },
  {
    code: '70',
    description: 'System No. 2 compressor overheat',
  },
  {
    code: '71',
    description: 'System No. 2 compressor overcurrent',
  },
  {
    code: '72',
    description: 'System No. 2 fan motor overcurrent',
  },
  {
    code: '73',
    description: 'System No. 2 actuation of high pressure switch (HPS)',
  },
  {
    code: '74',
    description: 'System No. 2 actuation of low pressure switch (LPS)',
  },
  {
    code: '75',
    description: 'System No. 2 malfunction of low pressure sensor',
  },
  {
    code: '76',
    description: 'System No. 2 malfunction of high pressure sensor',
  },
  {
    code: '77',
    description: 'System No. 1 malfunction of fan inter lock',
  },
  {
    code: '78',
    description: 'System No. 2 malfunction of fan inter lock',
  },
  {
    code: '7A',
    description: 'System No. 2 malfunction of compressor current sensor',
  },
  {
    code: '7C',
    description: 'Malfunction of pump inter lock',
  },
  {
    code: '80',
    description: 'Malfunction of entering water temperature thermistor',
  },
  {
    code: '81',
    description:
      'Malfunction of leaving water temperature thermistor or drain pipe heater',
  },
  {
    code: '82',
    description: 'System No. 1 malfunction of refrigerant thermistor',
  },
  {
    code: '83',
    description: 'System No. 2 malfunction of refrigerant thermistor',
  },
  {
    code: '84',
    description: 'System No. 1 malfunction of heat exchanger thermistor',
  },
  {
    code: '85',
    description: 'System No. 2 malfunction of heat exchanger thermistor',
  },
  {
    code: '86',
    description: 'System No. 1 malfunction of discharge pipe thermistor',
  },
  {
    code: '88',
    description: 'System No. 2 malfunction of discharge pipe temperature',
  },
  {
    code: '89',
    description: 'Malfunction of brazed-plate heat exchanger freezing',
  },
  {
    code: '8A',
    description:
      'Malfunction of dehumidification or leaving water temperature thermistor',
  },
  {
    code: '8E',
    description:
      'System No. 1 malfunction of suction pipe thermistor 1 for heating',
  },
  {
    code: '8F',
    description:
      'System No. 1 malfunction of suction pipe thermistor 2 for heating',
  },
  {
    code: '8H',
    description: 'Abnormal hot water high temperature',
  },
  {
    code: '90',
    description: 'Abnormal chilled water quantity for abnormal AXP',
  },
  {
    code: '91',
    description: 'System No. 2 malfunction of electronic expansion valve',
  },
  {
    code: '92',
    description: 'System No. 2 malfunction of suction pipe thermistor',
  },
  {
    code: '94',
    description:
      'Malfunction of transmission (Between heat reclaim ventilation unit and fan unit)',
  },
  {
    code: '95',
    description: 'System No. 1 malfunction of inverter system',
  },
  {
    code: '96',
    description: 'System No. 2 malfunction of inverter system',
  },
  {
    code: '97',
    description: 'Malfunction of thermal storage unit',
  },
  {
    code: '98',
    description: 'Malfunction of thermal storage brine pump',
  },
  {
    code: '99',
    description: 'Malfunction of thermal storage brine tank',
  },
  {
    code: '9E',
    description:
      'System No. 2 malfunction of suction pipe thermistor 1 for heating',
  },
  {
    code: '9F',
    description:
      'System No. 2 malfunction of suction pipe thermistor 2 for heating',
  },
];
