import { takeLatest, select, call, put } from 'redux-saga/effects';
import { INIT, setIsWelcomed, setWelcomeItem } from '../actions/welcomeAction';
import { mapPromotionLinkFromApi } from '../mapper/marketingHomeMapper';
import { cmsTypeEnum, getLatestRecord } from './contentSaga';

function* checkWelcomeItemUpdated(currentItems, newItems) {
  let isUpdated = false;

  if (currentItems.length !== newItems.length) {
    isUpdated = true;
  }

  newItems.forEach(newItem => {
    currentItems.forEach(item => {
      if (newItem.id === item.id && newItem.modified !== item.modified) {
        isUpdated = true;
      }
    });
  });
  return isUpdated;
}

function* onInit(action) {
  const cmsPromotionLink =
    action.appType === 'Acson'
      ? cmsTypeEnum.PromotionLink_Acson
      : cmsTypeEnum.PromotionLink;

  const welcomeItem = yield select(state => state.welcome.welcomeItem);

  // General Welcome Screen Category oid will always fixed to 1 (for faster indexing)
  const GENERAL_WELCOME_SCREEN = 'PromotionCategory_Oid1';
  const promotionLinks = yield call(
    getLatestRecord,
    cmsPromotionLink,
    GENERAL_WELCOME_SCREEN
  );

  if (promotionLinks) {
    const mappedPromotionLinks = mapPromotionLinkFromApi(
      promotionLinks,
      action.appType
    );

    // Check if welcome item is updated
    const updated = yield call(
      checkWelcomeItemUpdated,
      welcomeItem,
      mappedPromotionLinks
    );

    if (updated) {
      yield put(setIsWelcomed(false));
    }

    yield put(setWelcomeItem(mappedPromotionLinks));
  }
}

export default function* welcomeSaga() {
  yield takeLatest(INIT, onInit);
}
