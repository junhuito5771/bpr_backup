import { all, fork } from 'redux-saga/effects';

import locateDealerSaga from './locateDealerSaga';
import marketingHomeSaga from './marketingHomeSaga';
import heatLoadCalculatorSaga from './heatLoadCalculatorSaga';
import maintenanceSaga from './maintenanceSaga';
import contentSaga from './contentSaga';
import contactUsSaga from './contactUsSaga';
import errorCodeSaga from './errorCodeSaga';
import subscriptionSaga from './subscriptionSaga';
import privacyPolicySaga from './privacyPolicySaga';
import welcomeSaga from './welcomeSaga';
import serviceWelcomeSaga from './serviceWelcomeSaga';

export default function* rootSaga() {
  yield all([
    fork(marketingHomeSaga),
    fork(locateDealerSaga),
    fork(contentSaga),
    fork(heatLoadCalculatorSaga),
    fork(contactUsSaga),
    fork(errorCodeSaga),
    fork(subscriptionSaga),
    fork(maintenanceSaga),
    fork(contentSaga),
    fork(privacyPolicySaga),
    fork(welcomeSaga),
    fork(serviceWelcomeSaga),
  ]);
}
