import { takeLatest, call, put } from 'redux-saga/effects';
import FileSystem from 'react-native-fs';
import { ApiService } from '@module/utility';
import {
  setIsLoading,
  setMessage,
  setSubmitSuccess,
  SUBMIT_FEEDBACK_PARAMS,
} from '../actions/contactUsAction';
import { mapFeedbackToApi } from '../mapper/contactUsMapper';

async function checkFileSize(attachment) {
  return attachment.length > 0
    ? Promise.all(
        attachment.map(uri => FileSystem.stat(uri).then(stats => stats.size))
      )
    : null;
}

// 10000000 bytes = 10MB
function isExceedLimit(fileSizeMap, sizeLimit = 10000000) {
  return fileSizeMap
    ? fileSizeMap.reduce((accumulate, current) => accumulate + current) >
        sizeLimit
    : null;
}

function* onSubmitFeedbackParams({ feedbackParams }) {
  const mappedFeedback = mapFeedbackToApi(feedbackParams);

  yield put(setIsLoading(true));
  try {
    const fileSizeMap = yield call(checkFileSize, feedbackParams.attachment);
    if (isExceedLimit(fileSizeMap)) {
      yield put(setMessage('Total file size cannot be more than 10mb'));
    } else {
      const { data } = yield call(
        ApiService.postWithoutAuth,
        'usersubmission',
        mappedFeedback,
        'multipart'
      );
      if (data === 'Success') {
        yield put(setSubmitSuccess(true));
        yield put(setMessage('Feedback has been submitted'));
      }
    }
  } catch (error) {
    yield put(setMessage(error.errorMessage));
  } finally {
    yield put(setIsLoading(false));
  }
}

export default function* locateDealerSaga() {
  yield takeLatest(SUBMIT_FEEDBACK_PARAMS, onSubmitFeedbackParams);
}
