import { takeLatest, select, call, put } from 'redux-saga/effects';
import { INIT, setPlanDetail } from '../actions/subscriptionAction';
import {
  mapPromotionCategoryFromApi,
  mapPromotionLinkFromApi,
} from '../mapper/marketingHomeMapper';
import { cmsTypeEnum, checkLastModified, getLatestRecord } from './contentSaga';

function* onInit(action) {
  const isContentExpired = yield call(
    checkLastModified,
    action.appType === 'Acson'
      ? cmsTypeEnum.PromotionLink_Acson
      : cmsTypeEnum.PromotionLink
  );

  const planDetail = yield select(state => state.subscription.planDetail);

  if (isContentExpired || !planDetail.length) {
    const promotionLinks = yield call(
      getLatestRecord,
      action.appType === 'Acson'
        ? cmsTypeEnum.PromotionLink_Acson
        : cmsTypeEnum.PromotionLink
    );

    const promotionCategories = yield call(
      getLatestRecord,
      action.appType === 'Acson'
        ? cmsTypeEnum.PromotionCategory_Acson
        : cmsTypeEnum.PromotionCategory
    );

    if (promotionLinks && promotionCategories) {
      const mappedPromotionLinks = mapPromotionLinkFromApi(
        promotionLinks,
        action.appType
      );
      const mappedPromotionCategories = mapPromotionCategoryFromApi(
        promotionCategories
      );

      // Daikin or Acson CMS uses specific keywords for deciding promotion category
      const { id: promotionId } = mappedPromotionCategories
        .filter(item => item.name === 'Subscription')
        .find(item => item.id);
      const filteredPromotionLinks = mappedPromotionLinks.filter(
        item => item.promotionCategoryId === promotionId
      );
      yield put(setPlanDetail(filteredPromotionLinks));
    }
  }
}

export default function* subscriptionSaga() {
  yield takeLatest(INIT, onInit);
}
