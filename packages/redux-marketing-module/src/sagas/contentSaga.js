import produce from 'immer';
import { select, put, call, takeLatest } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import {
  INIT,
  setContentError,
  setLastModified,
} from '../actions/contentAction';

let host;
let endpoint;

function onInit({ configuration }) {
  host = configuration.bucketUrl;
  endpoint = configuration.bucketEndpoint;
}

export const cmsTypeEnum = {
  // Daikin
  Dealers: 'XPOCMS_Dealers',
  DealersCategory: 'XPOCMS_DealearsCategory',
  Specifications: 'XPOCMS_Specifications',
  PromotionLink: 'XPOCMS_PromotionLink',
  PromotionCategory: 'XPOCMS_PromotionCategory',
  Notification: 'XPOCMS_Notification',
  // Acson
  Specifications_Acson: 'XPOCMS_AcsonSpecifications',
  PromotionLink_Acson: 'XPOCMS_AcsonPromotionLink',
  PromotionCategory_Acson: 'XPOCMS_AcsonPromotionCategory',
};

// Base Method for getting content from Daikin CMS
function* getContent(url, fileName) {
  let result;
  try {
    const urlString = fileName
      ? `${host}/${endpoint}/${url}/${fileName}`
      : `${endpoint}/${url}`;
    result = yield call(ApiService.get, urlString);
  } catch (error) {
    yield put(
      setContentError('Please make sure you are connected to the internet')
    );
  }

  return result;
}

// Method for getting all.json content from Daikin CMS
export function* getLatestRecord(cmsType, filename = 'all') {
  let result;
  try {
    result = yield call(getContent, cmsType, `${filename}.json`);
  } catch (error) {
    yield put(
      setContentError('Please make sure you are connected to the internet')
    );
  }

  return result;
}

function* compareLastModified(cmsType, filteredData) {
  let isUpdated = false;

  const dbLastModified = yield select(s => s.content.lastModified);
  const { Modified: modified } = filteredData.find(item => item.Modified);

  const newLastModified = produce(dbLastModified, draftState => {
    draftState[cmsType] = modified;
  });
  yield put(setLastModified(newLastModified));

  if (dbLastModified[cmsType]) {
    const dbModified = dbLastModified[cmsType];
    isUpdated = modified !== dbModified;
  } else {
    isUpdated = true;
  }

  return isUpdated;
}

export function* checkLastModified(cmsType) {
  let isExpired = true;
  const data = yield getContent(cmsType, 'LastModified.json');

  if (data) {
    const result = data.filter(
      item => item.S3Key === `${endpoint}/${cmsType}/LastModified.json`
    );
    isExpired = yield call(compareLastModified, cmsType, result);
  }
  return isExpired;
}

export function setConfigure(configuration) {
  host = configuration.bucketUrl;
  endpoint = configuration.bucketEndpoint;
}

export default function* contentSaga() {
  yield takeLatest(INIT, onInit);
}
