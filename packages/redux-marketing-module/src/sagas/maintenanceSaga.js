import { takeLatest, put, select, call } from 'redux-saga/effects';
import produce from 'immer';
import RNCalendarEvents from 'react-native-calendar-events';
import {
  ADD_REMINDER,
  DELETE_REMINDERS,
  EDIT_REMINDER,
  INIT,
  setCalendarError,
  setReminders,
} from '../actions/maintenanceAction';

function* addCalendarEvent(eventId, title, reminderDetail) {
  const selectedEvent = eventId ? { id: eventId } : null;

  try {
    const eventId = yield call(RNCalendarEvents.saveEvent, title, {
      ...reminderDetail,
      ...selectedEvent,
    });

    return eventId;
  } catch (error) {
    yield put(setCalendarError(error.message));
    return null;
  }
}

function* onInit() {}

function* onAddReminder({ reminder }) {
  const reminders = yield select(state => state.maintenance.reminders);
  let filterEventId = null;
  let serviceEventId = null;

  if (reminder.setReminder) {
    let hasPermission = yield RNCalendarEvents.checkPermissions(
      (readOnly = false)
    );

    if (hasPermission !== 'authorized') {
      hasPermission = yield call(
        RNCalendarEvents.requestPermissions,
        (readOnly = false)
      );
    }

    if (hasPermission === 'authorized') {
      // add Filter reminder
      filterEventId = yield call(
        addCalendarEvent,
        null,
        reminder.reminderName,
        {
          startDate: new Date(reminder.nextFilter),
          endDate: new Date(reminder.nextFilter),
          recurrenceRule: {
            frequency: reminder.filterInterval.frequency,
            interval: reminder.filterInterval.interval,
          },
        }
      );

      // add service reminder
      serviceEventId = yield call(
        addCalendarEvent,
        null,
        reminder.reminderName,
        {
          startDate: new Date(reminder.nextService),
          endDate: new Date(reminder.nextService),
          recurrenceRule: {
            frequency: reminder.serviceInterval.frequency,
            interval: reminder.serviceInterval.interval,
          },
        }
      );
    }
  }

  const newReminder = {
    ...reminder,
    filterEventId,
    serviceEventId,
  };

  yield put(setReminders([...reminders, newReminder]));
}

function* onEditReminder({ reminder, index }) {
  const reminders = yield select(state => state.maintenance.reminders);

  let hasPermission = yield RNCalendarEvents.checkPermissions(
    (readOnly = false)
  );

  if (hasPermission !== 'authorized') {
    hasPermission = yield call(
      RNCalendarEvents.requestPermissions,
      (readOnly = false)
    );
  }

  if (reminder.setReminder && hasPermission === 'authorized') {
    yield call(
      addCalendarEvent,
      reminder.filterEventId,
      reminder.reminderName,
      {
        startDate: new Date(reminder.nextFilter),
        endDate: new Date(reminder.nextFilter),
        recurrenceRule: {
          frequency: reminder.filterInterval.frequency,
          interval: reminder.filterInterval.interval,
        },
      }
    );

    yield call(
      addCalendarEvent,
      reminder.serviceEventId,
      reminder.reminderName,
      {
        startDate: new Date(reminder.nextService),
        endDate: new Date(reminder.nextService),
        recurrenceRule: {
          frequency: reminder.serviceInterval.frequency,
          interval: reminder.serviceInterval.interval,
        },
      }
    );
  }

  const newReminders = produce(reminders, draft => {
    draft[index] = reminder;
  });

  yield put(setReminders(newReminders));
}

function* onDeleteReminders({ reminders }) {
  const currentReminders = yield select(state => state.maintenance.reminders);
  const newReminders = currentReminders.filter(
    data => !reminders.includes(data)
  );

  for (reminder of reminders) {
    if (reminder.setReminder) {
      yield call(RNCalendarEvents.removeEvent, reminder.filterEventId);
      yield call(RNCalendarEvents.removeEvent, reminder.serviceEventId);
    }
  }

  yield put(setReminders(newReminders));
}

export default function* mainSaga() {
  yield takeLatest(INIT, onInit);
  yield takeLatest(ADD_REMINDER, onAddReminder);
  yield takeLatest(EDIT_REMINDER, onEditReminder);
  yield takeLatest(DELETE_REMINDERS, onDeleteReminders);
}
