import { takeLatest, select, call, put } from 'redux-saga/effects';
import { INIT, setPopUp, setPromotions } from '../actions/marketingHomeAction';
import {
  mapPromotionCategoryFromApi,
  mapPromotionLinkFromApi,
} from '../mapper/marketingHomeMapper';
import { cmsTypeEnum, checkLastModified, getLatestRecord } from './contentSaga';

function* onInit(action) {
  const isContentExpired = yield call(
    checkLastModified,
    action.appType === 'Acson'
      ? cmsTypeEnum.PromotionLink_Acson
      : cmsTypeEnum.PromotionLink
  );

  const promotions = yield select(state => state.marketingHome.promotions);
  const popUp = yield select(state => state.marketingHome.popUp);

  if (isContentExpired || !promotions.length || !popUp.length) {
    const promotionLinks = yield call(
      getLatestRecord,
      action.appType === 'Acson'
        ? cmsTypeEnum.PromotionLink_Acson
        : cmsTypeEnum.PromotionLink
    );

    const promotionCategories = yield call(
      getLatestRecord,
      action.appType === 'Acson'
        ? cmsTypeEnum.PromotionCategory_Acson
        : cmsTypeEnum.PromotionCategory
    );

    if (promotionLinks && promotionCategories) {
      const mappedPromotionLinks = mapPromotionLinkFromApi(
        promotionLinks,
        action.appType
      );
      const mappedPromotionCategories = mapPromotionCategoryFromApi(
        promotionCategories
      );

      // Daikin or Acson CMS uses specific keywords for deciding promotion category
      const { id: promotionId } = mappedPromotionCategories
        .filter(item => item.name === 'Promotion')
        .find(item => item.id);
      const filteredPromotionLinks = mappedPromotionLinks.filter(
        item => item.promotionCategoryId === promotionId
      );
      yield put(setPromotions(filteredPromotionLinks));

      if (action.appType !== 'Acson') {
        const { id: popUpId } = mappedPromotionCategories
          .filter(item => item.name === 'PopUp')
          .find(item => item.id);
        const filteredPopUp = mappedPromotionLinks.filter(
          item => item.promotionCategoryId === popUpId
        );
        yield put(setPopUp(filteredPopUp));
      }
    }
  }
}

export default function* marketingHomeSaga() {
  yield takeLatest(INIT, onInit);
}
