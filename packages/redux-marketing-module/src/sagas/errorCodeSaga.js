import { takeLatest, select, put } from 'redux-saga/effects';
import { GET_RESULT, setResult } from '../actions/errorCodeAction';
import { damaErrorCodes, ditErrorCodes } from '../constants/errorCodes';

function* onGetResult() {
  const selectedRemote = yield select(state => state.errorCode.selectedRemote);
  const selectedErrorCodes = yield select(state => state.errorCode.errorCode);
  let result = null;

  if (selectedRemote.type === 'dama') {
    result = damaErrorCodes.find(item => item.code === selectedErrorCodes);
  } else {
    result = ditErrorCodes.find(item => item.code === selectedErrorCodes);
  }

  yield put(setResult(result ? result.description : null));
}

export default function* locateDealerSaga() {
  yield takeLatest(GET_RESULT, onGetResult);
}
