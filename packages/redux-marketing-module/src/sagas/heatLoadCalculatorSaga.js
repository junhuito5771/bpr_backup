import { takeLatest, call, select, put } from 'redux-saga/effects';
import {
  INIT,
  CALCULATE_BTU,
  setAllProduct,
  setTotalBTU,
} from '../actions/heatLoadCalculatorAction';
import { checkLastModified, cmsTypeEnum, getLatestRecord } from './contentSaga';

function* onInit(action) {
  const isContentExpired = yield call(
    checkLastModified,
    action.appType === 'Acson'
      ? cmsTypeEnum.Specifications_Acson
      : cmsTypeEnum.Specifications
  );

  const stateProductList = yield select(
    state => state.heatLoadCalculator.allProduct
  );

  if (isContentExpired || !stateProductList.length) {
    const allProductList = yield call(
      getLatestRecord,
      action.appType === 'Acson'
        ? cmsTypeEnum.Specifications_Acson
        : cmsTypeEnum.Specifications
    );
    yield put(setAllProduct(allProductList));
  }
}

function* calculateBTU() {
  // measurementScale: 0 = Meter , 1 = Feet
  // window direction : 0 = North/South , 1 = West/East
  const { measurementScale, room, noOfPeople, windows } = yield select(
    state => state.heatLoadCalculator
  );
  let totalBTU = 0;
  let totalA = 0;
  let totalC = 0;

  windows.forEach(k => {
    totalC += k.direction
      ? k.length * k.height * 100
      : k.length * k.height * 80;
  });

  totalA = room.width * room.length * 65;
  const A = measurementScale ? totalA : totalA / 0.3048;
  const B = noOfPeople * 500;
  const C = measurementScale ? totalC : totalC / 0.3048;

  totalBTU = Math.floor(A + B + C);

  yield put(setTotalBTU(totalBTU));
}

export default function* heatLoadCalculatorSaga() {
  yield takeLatest(INIT, onInit);
  yield takeLatest(CALCULATE_BTU, calculateBTU);
}
