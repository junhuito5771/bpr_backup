import { takeLatest, call, select, put } from 'redux-saga/effects';
import {
  INIT,
  SEARCH_LOCATE_DEALER,
  setCityStateOption,
  setDealerCategories,
  setDealers,
  setIsSearching,
  setSearchDealers,
  SET_SELECTED_MODEL,
} from '../actions/locateDealerAction';
import {
  mapCityStateFromDealers,
  mapDealerCategoriesFromApi,
  mapDealersFromApi,
} from '../mapper/locateDealerMapper';
import { checkLastModified, cmsTypeEnum, getLatestRecord } from './contentSaga';

function* onInit() {
  const isContentExpired = yield call(checkLastModified, cmsTypeEnum.Dealers);

  const stateDealers = yield select(state => state.locateDealer.dealers);

  const stateDealerCategories = yield select(
    state => state.locateDealer.dealerCategories
  );

  if (
    isContentExpired ||
    !stateDealers.length ||
    !stateDealerCategories.length
  ) {
    const dealers = yield call(getLatestRecord, cmsTypeEnum.Dealers);
    const dealerCategories = yield call(
      getLatestRecord,
      cmsTypeEnum.DealersCategory
    );

    if (dealers && dealerCategories) {
      const mappedDealer = mapDealersFromApi(dealers);
      const mappedDealerCategories = mapDealerCategoriesFromApi(
        dealerCategories
      );

      if (mappedDealer.length) {
        const cityStateOption = mapCityStateFromDealers(mappedDealer);
        yield put(setCityStateOption(cityStateOption));
      }

      yield put(setDealerCategories(mappedDealerCategories));
      yield put(setDealers(mappedDealer));
    }
  }
}

function* onSetSelectedModel({ selectedModel }) {
  const dealers = yield select(state => state.locateDealer.dealers);
  const dealerCategories = yield select(
    state => state.locateDealer.dealerCategories
  );

  const filteredCategories = dealerCategories.filter(category =>
    category.products.includes(selectedModel)
  );
  const filteredCategoriesId = filteredCategories.map(category => category.id);

  const filteredDealers = dealers.filter(dealer =>
    filteredCategoriesId.includes(dealer.dealerCategoryId)
  );

  const cityStateOption = mapCityStateFromDealers(
    filteredDealers.length ? filteredDealers : dealers
  );

  yield put(setCityStateOption(cityStateOption));
}

function* onSearchLocateDealer() {
  const {
    dealers,
    selectedModel,
    selectedState,
    selectedCity,
    dealerCategories,
  } = yield select(state => state.locateDealer);

  let filteredCategoryIds = [];
  if (selectedModel !== 'ALL') {
    filteredCategoryIds = dealerCategories
      .filter(category =>
        category.products
          .split(/[, ]+/)
          .map(c => c.trim())
          .includes(selectedModel)
      )
      .map(category => category.id);
  }

  const filteredDealers = dealers.filter(
    dealer =>
      dealer.state === selectedState &&
      dealer.city === selectedCity &&
      (filteredCategoryIds.includes(dealer.dealerCategoryId) ||
        selectedModel === 'ALL')
  );

  yield put(setSearchDealers(filteredDealers));
  yield put(setIsSearching(true));
}

export default function* locateDealerSaga() {
  yield takeLatest(INIT, onInit);
  yield takeLatest(SET_SELECTED_MODEL, onSetSelectedModel);
  yield takeLatest(SEARCH_LOCATE_DEALER, onSearchLocateDealer);
}
