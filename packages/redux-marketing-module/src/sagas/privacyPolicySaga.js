import { takeLatest, select, call, put } from 'redux-saga/effects';
import { INIT, setContent } from '../actions/privacyPolicyAction';
import {
  mapPromotionCategoryFromApi,
  mapPromotionLinkFromApi,
} from '../mapper/marketingHomeMapper';
import { cmsTypeEnum, checkLastModified, getLatestRecord } from './contentSaga';

function* onInit(action) {
  const isContentExpired = yield call(
    checkLastModified,
    action.appType === 'Acson'
      ? cmsTypeEnum.PromotionLink_Acson
      : cmsTypeEnum.PromotionLink
  );

  const content = yield select(state => state.privacyPolicy.content);

  if (isContentExpired || !content) {
    const promotionLinks = yield call(
      getLatestRecord,
      action.appType === 'Acson'
        ? cmsTypeEnum.PromotionLink_Acson
        : cmsTypeEnum.PromotionLink
    );

    const promotionCategories = yield call(
      getLatestRecord,
      action.appType === 'Acson'
        ? cmsTypeEnum.PromotionCategory_Acson
        : cmsTypeEnum.PromotionCategory
    );

    if (promotionLinks && promotionCategories) {
      const mappedPromotionLinks = mapPromotionLinkFromApi(
        promotionLinks,
        action.appType
      );
      const mappedPromotionCategories = mapPromotionCategoryFromApi(
        promotionCategories
      );

      // Daikin or Acson CMS uses specific keywords for deciding promotion category
      const eulaKey = action.appType === 'Acson' ? 'EULAAcson' : 'EULADaikin';

      const { id: promotionId } = mappedPromotionCategories
        .filter(item => item.name === eulaKey)
        .find(item => item.id);
      const filteredPromotionLinks = mappedPromotionLinks.filter(
        item => item.promotionCategoryId === promotionId
      );
      yield put(setContent(filteredPromotionLinks.find(link => link.id)));
    }
  }
}

export default function* subscriptionSaga() {
  yield takeLatest(INIT, onInit);
}
