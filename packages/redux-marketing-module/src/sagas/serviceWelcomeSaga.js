import { takeLatest, select, call, put } from 'redux-saga/effects';
import {
  INIT_SERVICE_WELCOME,
  setIsServiceWelcome,
  setServiceWelcomeItem,
} from '../actions/serviceWelcomeAction';
import { mapPromotionLinkFromApi } from '../mapper/marketingHomeMapper';
import { cmsTypeEnum, getLatestRecord } from './contentSaga';

function* checkWelcomeItemUpdated(currentItems, newItems) {
  let isUpdated = false;

  if (currentItems.length !== newItems.length) {
    isUpdated = true;
  }

  newItems.forEach(newItem => {
    currentItems.forEach(item => {
      if (newItem.id === item.id && newItem.modified !== item.modified) {
        isUpdated = true;
      }
    });
  });
  return isUpdated;
}

function* onInit(action) {
  const cmsPromotionLink =
    action.appType === 'Acson'
      ? cmsTypeEnum.PromotionLink_Acson
      : cmsTypeEnum.PromotionLink;

  const serviceWelcomeItem = yield select(
    state => state.serviceWelcome.serviceWelcomeItem
  );

  // General Welcome Screen Category oid will always fixed to 1 (for faster indexing)
  const GENERAL_WELCOME_SCREEN = 'PromotionCategory_Oid2';
  const promotionLinks = yield call(
    getLatestRecord,
    cmsPromotionLink,
    GENERAL_WELCOME_SCREEN
  );

  if (promotionLinks) {
    const mappedPromotionLinks = mapPromotionLinkFromApi(
      promotionLinks,
      action.appType
    );

    // Check if welcome item is updated
    const updated = yield call(
      checkWelcomeItemUpdated,
      serviceWelcomeItem,
      mappedPromotionLinks
    );

    if (updated) {
      yield put(setIsServiceWelcome(false));
    }

    yield put(setServiceWelcomeItem(mappedPromotionLinks));
  }
}

export default function* serviceWelcomeSaga() {
  yield takeLatest(INIT_SERVICE_WELCOME, onInit);
}
