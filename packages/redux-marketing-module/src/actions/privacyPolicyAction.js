const CONTEXT = 'IOT/PRIVACY_POLICY';

export const INIT = `${CONTEXT}/INIT`;
export const SET_CONTENT = `${CONTEXT}/SET_CONTENT`;
export const SET_EULA_VIEWED = `${CONTEXT}/SET_EULA_VIEWED`;
export const SET_PRIVACY_POLICY_VIEWED = `${CONTEXT}/SET_PRIVACY_POLICY_VIEWED`;
export const SET_AGREEMENT_CONFIRMED = `${CONTEXT}/SET_AGREEMENT_CONFIRMED`;

export const init = appType => ({
  type: INIT,
  appType,
});

export const setContent = content => ({
  type: SET_CONTENT,
  content,
});
