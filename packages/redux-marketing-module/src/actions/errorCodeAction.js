const CONTEXT = 'MARKETING/ERROR_CODE';

export const INIT = `${CONTEXT}/INIT`;
export const SET_INITIAL_STATE = `${CONTEXT}/SET_INITIAL_STATE`;
export const SET_SELECTED_REMOTE = `${CONTEXT}/SET_SELECTED_REMOTE`;
export const SET_SELECTECTED_ERROR_CODE = `${CONTEXT}/SET_SELECTED_ERROR_CODE`;
export const GET_RESULT = `${CONTEXT}/GET_RESULT`;
export const SET_RESULT = `${CONTEXT}/SET_RESULT`;

export const init = () => ({
  type: INIT,
});

export const setInitialState = () => ({
  type: SET_INITIAL_STATE,
});

export const setSelectedRemote = selectedRemote => ({
  type: SET_SELECTED_REMOTE,
  selectedRemote,
});

export const setSelectedErrorCode = errorCode => ({
  type: SET_SELECTECTED_ERROR_CODE,
  errorCode,
});

export const getResult = () => ({
  type: GET_RESULT,
});

export const setResult = result => ({
  type: SET_RESULT,
  result,
});
