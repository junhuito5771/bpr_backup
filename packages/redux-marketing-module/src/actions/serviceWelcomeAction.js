const CONTEXT = 'MARKETING/WELCOME';

export const INIT_SERVICE_WELCOME = `${CONTEXT}/INIT_SERVICE_WELCOME`;
export const SET_IS_SERVICE_WELCOMED = `${CONTEXT}/SET_IS_SERVICE_WELCOMED`;
export const SET_SERVICE_WELCOME_ITEM = `${CONTEXT}/SET_SERVICE_WELCOME_ITEM`;

export const initServiceWelcome = appType => ({
  type: INIT_SERVICE_WELCOME,
  appType,
});

export const setIsServiceWelcome = isServiceWelcomed => ({
  type: SET_IS_SERVICE_WELCOMED,
  isServiceWelcomed,
});

export const setServiceWelcomeItem = serviceWelcomeItem => ({
  type: SET_SERVICE_WELCOME_ITEM,
  serviceWelcomeItem,
});
