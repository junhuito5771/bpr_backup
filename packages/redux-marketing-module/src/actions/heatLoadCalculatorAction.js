const CONTEXT = 'HEATLOADCALCULATOR';

export const INIT = `${CONTEXT}/INIT`;
export const SET_MEASUREMENT_SCALE = `${CONTEXT}/SET_MEASUREMENT_SCALE`;
export const SET_ROOMS = `${CONTEXT}/SET_ROOMS`;
export const SET_NO_OF_PEOPLE = `${CONTEXT}/SET_NO_OF_PEOPLE`;
export const SET_WINDOWS = `${CONTEXT}/SET_WINDOWS`;
export const CALCULATE_BTU = `${CONTEXT}/CALCULATE_BTU`;
export const SET_TOTAL_BTU = `${CONTEXT}/SET_TOTAL_BTU`;
export const SET_ALL_PRODUCT = `${CONTEXT}/SET_ALL_PRODUCT`;
export const CLEAR_STATUS = `${CONTEXT}/CLEAR_STATUS`;

export const init = appType => ({
  type: INIT,
  appType,
});

export const setMeasurementScale = measurementScale => ({
  type: SET_MEASUREMENT_SCALE,
  measurementScale,
});

export const setRooms = room => ({
  type: SET_ROOMS,
  room,
});

export const setNoOfPeople = noOfPeople => ({
  type: SET_NO_OF_PEOPLE,
  noOfPeople,
});

export const setWindows = windows => ({
  type: SET_WINDOWS,
  windows,
});

export const calculateBTU = () => ({
  type: CALCULATE_BTU,
});

export const setTotalBTU = totalBTU => ({
  type: SET_TOTAL_BTU,
  totalBTU,
});

export const setAllProduct = allProduct => ({
  type: SET_ALL_PRODUCT,
  allProduct,
});

export const clearStatus = () => ({
  type: CLEAR_STATUS,
});
