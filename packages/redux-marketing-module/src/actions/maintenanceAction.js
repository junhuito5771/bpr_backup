const CONTEXT = 'MARKETING/MAINTENANCE';

export const INIT = `${CONTEXT}/INIT`;
export const SET_REMINDERS = `${CONTEXT}/SET_REMINDERS`;
export const ADD_REMINDER = `${CONTEXT}/ADD_REMINDER`;
export const EDIT_REMINDER = `${CONTEXT}/EDIT_REMINDER`;
export const DELETE_REMINDERS = `${CONTEXT}/DELETE_REMINDERS`;
export const SET_CALENDAR_ERROR = `${CONTEXT}/SET_CALENDAR_ERROR`;

export const init = configuration => ({
  type: INIT,
  configuration,
});

export const setReminders = reminders => ({
  type: SET_REMINDERS,
  reminders,
});

export const addReminder = reminder => ({
  type: ADD_REMINDER,
  reminder,
});

export const editReminder = (reminder, index) => ({
  type: EDIT_REMINDER,
  reminder,
  index,
});

export const deleteReminders = reminders => ({
  type: DELETE_REMINDERS,
  reminders,
});

export const setCalendarError = error => ({
  type: SET_CALENDAR_ERROR,
  error,
});
