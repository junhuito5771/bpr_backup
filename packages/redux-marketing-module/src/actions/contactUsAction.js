const CONTEXT = 'MARKETING/CONTACT_US';

export const SET_INITIAL_STATE = `${CONTEXT}/SET_INITIAL_STATE`;
export const SUBMIT_FEEDBACK_PARAMS = `${CONTEXT}/SUBMIT_FEEDBACK_PARAMS`;
export const SET_MESSAGE = `${CONTEXT}/SET_MESSAGE`;
export const SET_IS_LOADING = `${CONTEXT}/SET_IS_LOADING`;
export const SET_SUBMIT_SUCCESS = `${CONTEXT}/SET_SUBMIT_SUCCESS`;

export const setInitialState = () => ({
  type: SET_INITIAL_STATE,
});

export const submitFeedbackParams = feedbackParams => ({
  type: SUBMIT_FEEDBACK_PARAMS,
  feedbackParams,
});

export const setMessage = message => ({
  type: SET_MESSAGE,
  message,
});

export const setIsLoading = isLoading => ({
  type: SET_IS_LOADING,
  isLoading,
});

export const setSubmitSuccess = submitSuccess => ({
  type: SET_SUBMIT_SUCCESS,
  submitSuccess,
});
