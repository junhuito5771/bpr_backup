const CONTEXT = 'LOCATE_DEALER';

export const INIT = `${CONTEXT}/INIT`;
export const SET_SELECTED_MODEL = `${CONTEXT}/SET_SELECTED_MODEL`;
export const SET_SELECTED_CITY = `${CONTEXT}/SET_SELECTED_CITY`;
export const SET_SELECTED_STATE = `${CONTEXT}/SET_SELECTED_STATE`;
export const SEARCH_LOCATE_DEALER = `${CONTEXT}/SEARCH_LOCATE_DEALER`;
export const SET_DEALERS = `${CONTEXT}/SET_DEALERS`;
export const SET_DEALER_CATEGORIES = `${CONTEXT}/SET_DEALER_CATEGORIES`;
export const SET_SEARCH_DEALERS = `${CONTEXT}/SET_SEARCH_DEALERS`;
export const SET_CITY_STATE_OPTION = `${CONTEXT}/SET_CITY_STATE_OPTION`;
export const SET_IS_SEARCHING = `${CONTEXT}/SET_IS_SEARCHING`;

export const init = () => ({
  type: INIT,
});

export const setSelectedModel = selectedModel => ({
  type: SET_SELECTED_MODEL,
  selectedModel,
});

export const setSelectedCity = selectedCity => ({
  type: SET_SELECTED_CITY,
  selectedCity,
});

export const setSelectedState = selectedState => ({
  type: SET_SELECTED_STATE,
  selectedState,
});

export const setDealers = dealers => ({
  type: SET_DEALERS,
  dealers,
});

export const setDealerCategories = dealerCategories => ({
  type: SET_DEALER_CATEGORIES,
  dealerCategories,
});

export const searchLocateDealer = () => ({
  type: SEARCH_LOCATE_DEALER,
});

export const setSearchDealers = searchDealers => ({
  type: SET_SEARCH_DEALERS,
  searchDealers,
});

export const setCityStateOption = cityStateOption => ({
  type: SET_CITY_STATE_OPTION,
  cityStateOption,
});

export const setIsSearching = isSearching => ({
  type: SET_IS_SEARCHING,
  isSearching,
});
