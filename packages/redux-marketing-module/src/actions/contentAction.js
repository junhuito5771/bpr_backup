const CONTEXT = 'MARKETING/CONTENT';

export const INIT = `${CONTEXT}/INIT`;
export const SET_LAST_MODIFIED = `${CONTEXT}/SET_LAST_MODIFIED`;
export const GET_LATEST_RECORD = `${CONTEXT}/GET_LATEST_RECORD`;
export const SET_CONTENT_ERROR = `${CONTEXT}/SET_CONTENT_ERROR`;

export const init = configuration => ({
  type: INIT,
  configuration,
});

export const setLastModified = lastModified => ({
  type: SET_LAST_MODIFIED,
  lastModified,
});

export const setContentError = error => ({
  type: SET_CONTENT_ERROR,
  error,
});
