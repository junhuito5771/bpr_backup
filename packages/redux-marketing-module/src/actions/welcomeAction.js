const CONTEXT = 'MARKETING/WELCOME';

export const INIT = `${CONTEXT}/INIT`;
export const SET_IS_WELCOMED = `${CONTEXT}/SET_IS_WELCOMED`;
export const SET_WELCOME_ITEM = `${CONTEXT}/SET_WELCOME_ITEM`;

export const init = appType => ({
  type: INIT,
  appType,
});

export const setIsWelcomed = isWelcomed => ({
  type: SET_IS_WELCOMED,
  isWelcomed,
});

export const setWelcomeItem = welcomeItem => ({
  type: SET_WELCOME_ITEM,
  welcomeItem,
});
