const CONTEXT = 'MARKETING_HOME';

export const INIT = `${CONTEXT}/INIT`;
export const SET_PROMOTIONS = `${CONTEXT}/SET_PROMOTIONS`;
export const SET_POPUP = `${CONTEXT}/SET_POPUP`;
export const SET_IS_POPUP_SHOWN = `${CONTEXT}/SET_IS_POPUP_SHOWN`;

export const init = appType => ({
  type: INIT,
  appType,
});

export const setPromotions = promotions => ({
  type: SET_PROMOTIONS,
  promotions,
});

export const setPopUp = popUp => ({
  type: SET_POPUP,
  popUp,
});

export const setIsPopUpShown = isPopUpShown => ({
  type: SET_IS_POPUP_SHOWN,
  isPopUpShown,
});
