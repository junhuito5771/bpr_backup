const CONTEXT = 'SUBSCRIPTION';

export const INIT = `${CONTEXT}/INIT`;
export const SET_PLAN_DETAIL = `${CONTEXT}/SET_PLAN_DETAIL`;

export const init = () => ({
  type: INIT,
});

export const setPlanDetail = planDetail => ({
  type: SET_PLAN_DETAIL,
  planDetail,
});
