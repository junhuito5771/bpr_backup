import { StringHelper } from '@module/utility';

export const mapDealersFromApi = data =>
  data.map(item => ({
    id: item.Oid,
    name: item.Name,
    status: item.Status,
    imageLink: item.ImageLink,
    address1: item.Address1 || '',
    address2: item.Address2 || '',
    address3: item.Address3 || '',
    city: item.City ? item.City.trim() : '',
    postcode: item.Postcode || '',
    state: item.State ? item.State.trim() : '',
    telNo: item.Tel1,
    longtitude: item.CoordinateLongtitude,
    latitude: item.CoordinateLatitude,
    daikinPhysicalStoreFront: item.PhysicalStoreFront,
    daikinDisplayRack: item.DaikinDisplayRack,
    daikinProductDisplay: item.DaikinProductDisplay,
    daikinSignboard: item.DaikinSignboard,
    daikinBrandOnly: item.DaikinBrandOnly,
    dealerCategoryId: item.Category.Oid,
  }));

export const mapDealerCategoriesFromApi = data =>
  data.map(item => ({
    id: item.Oid,
    category: item.Category,
    description: item.Description,
    products: item.Products,
  }));

export const mapCityStateFromDealers = dealers => {
  const cityStateOption = {};

  dealers.forEach(dealer => {
    const state = StringHelper.capitalize(dealer.state);
    const city = StringHelper.capitalize(dealer.city);

    if (!cityStateOption[state]) {
      cityStateOption[state] = [city];
    } else if (!cityStateOption[state].includes(city)) {
      cityStateOption[state].push(city);
    }
  });

  return cityStateOption;
};
