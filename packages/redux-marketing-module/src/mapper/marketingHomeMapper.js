export const mapPromotionLinkFromApi = (data, appType) => {
  return data.map(item => ({
    id: item.Oid,
    title: item.Title,
    smallImageLink: item.SmallImageLink,
    description: item.Description,
    imageLink: item.ImageLink,
    fullImageLink: item.FullImageLink,
    smallImageS3Key: item.SmallImageS3Key,
    imageS3Key: item.ImageS3Key,
    fullImageS3Key: item.FullImageS3Key,
    hyperlink: item.Hyperlink,
    content: item.Content,
    sortSequence: item.SortSequence,
    remark: item.Remark,
    modified: item.Modified,
    promotionCategoryId:
      appType === 'Acson'
        ? item.CMS_AcsonPromotionCategory.Oid
        : item.CMS_PromotionCategory.Oid,
  }));
};

export const mapPromotionCategoryFromApi = data => {
  return data.map(item => ({
    id: item.Oid,
    name: item.Name,
    description: item.Description,
    remark: item.Remark,
  }));
};
