export const mapFeedbackToApi = data => {
  const formData = new FormData();

  if (data.attachment.length) {
    data.attachment.map((file, index) => {
      formData.append(`File${index ? index + 1 : ''}`, file);
    });
  }

  formData.append('Name', data.fullName);
  formData.append('Mobile', data.mobileNo);
  formData.append('Email', data.email);
  formData.append('State', data.state);
  formData.append('Subject', data.subject);
  formData.append('MessageContent', data.message);
  formData.append('MessageType', data.inquiryType);

  return formData;
};
