import {
  SET_SELECTED_REMOTE,
  SET_SELECTECTED_ERROR_CODE,
  SET_RESULT,
  SET_INITIAL_STATE,
} from '../actions/errorCodeAction';

const initialState = {
  selectedRemote: null,
  errorCode: null,
  result: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_INITIAL_STATE:
      return initialState;
    case SET_SELECTED_REMOTE:
      return { ...state, selectedRemote: action.selectedRemote };
    case SET_SELECTECTED_ERROR_CODE:
      return { ...state, errorCode: action.errorCode };
    case SET_RESULT:
      return { ...state, result: action.result };
    default:
      return state;
  }
};

export default reducer;
