const { SET_REMINDERS } = require('../actions/maintenanceAction');

const initialState = {
  reminders: [],
  calendarError: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_REMINDERS:
      return { ...state, reminders: action.reminders };
    default:
      return state;
  }
};

export default reducer;
