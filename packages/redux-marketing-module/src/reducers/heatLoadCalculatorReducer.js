import {
  SET_MEASUREMENT_SCALE,
  SET_ROOMS,
  SET_NO_OF_PEOPLE,
  SET_WINDOWS,
  SET_TOTAL_BTU,
  SET_ALL_PRODUCT,
  CLEAR_STATUS,
} from '../actions/heatLoadCalculatorAction';

const initialState = {
  measurementScale: 1, // 0 = Meter , 1 = Feet
  room: {
    width: 0,
    length: 0,
  },
  noOfPeople: 0,
  windows: [],
  totalBTU: 0,
  allProduct: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MEASUREMENT_SCALE:
      return { ...state, measurementScale: action.measurementScale };
    case SET_ROOMS:
      return { ...state, room: action.room };
    case SET_NO_OF_PEOPLE:
      return { ...state, noOfPeople: action.noOfPeople };
    case SET_WINDOWS:
      return { ...state, windows: action.windows };
    case SET_TOTAL_BTU:
      return { ...state, totalBTU: action.totalBTU };
    case SET_ALL_PRODUCT:
      return { ...state, allProduct: action.allProduct };
    case CLEAR_STATUS:
      return (state = initialState);
    default:
      return state;
  }
};

export default reducer;
