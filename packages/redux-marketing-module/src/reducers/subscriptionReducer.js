import { SET_PLAN_DETAIL } from '../actions/subscriptionAction';

const initialState = {
  planDetail: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PLAN_DETAIL:
      return { ...state, planDetail: action.planDetail };
    default:
      return state;
  }
};

export default reducer;
