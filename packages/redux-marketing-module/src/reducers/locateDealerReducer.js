import {
  SET_CITY_STATE_OPTION,
  SET_DEALERS,
  SET_DEALER_CATEGORIES,
  SET_IS_SEARCHING,
  SET_SEARCH_DEALERS,
  SET_SELECTED_CITY,
  SET_SELECTED_MODEL,
  SET_SELECTED_STATE,
} from '../actions/locateDealerAction';

const initialState = {
  selectedModel: null,
  selectedState: null,
  selectedCity: null,
  dealers: [],
  dealerCategories: [],
  searchDealers: null,
  cityStateOption: null,
  isSearching: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SELECTED_MODEL:
      return { ...state, selectedModel: action.selectedModel };
    case SET_SELECTED_CITY:
      return { ...state, selectedCity: action.selectedCity };
    case SET_SELECTED_STATE:
      return { ...state, selectedState: action.selectedState };
    case SET_DEALERS:
      return { ...state, dealers: action.dealers };
    case SET_DEALER_CATEGORIES:
      return { ...state, dealerCategories: action.dealerCategories };
    case SET_SEARCH_DEALERS:
      return { ...state, searchDealers: action.searchDealers };
    case SET_CITY_STATE_OPTION:
      return { ...state, cityStateOption: action.cityStateOption };
    case SET_IS_SEARCHING:
      return { ...state, isSearching: action.isSearching };
    default:
      return state;
  }
};

export default reducer;
