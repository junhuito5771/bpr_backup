import {
  SET_INITIAL_STATE,
  SET_IS_LOADING,
  SET_MESSAGE,
  SET_SUBMIT_SUCCESS,
} from '../actions/contactUsAction';

const initialState = {
  message: '',
  isLoading: false,
  submitSuccess: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_INITIAL_STATE:
      return initialState;
    case SET_MESSAGE:
      return { ...state, message: action.message };
    case SET_IS_LOADING:
      return { ...state, isLoading: action.isLoading };
    case SET_SUBMIT_SUCCESS:
      return { ...state, submitSuccess: action.submitSuccess };
    default:
      return state;
  }
};

export default reducer;
