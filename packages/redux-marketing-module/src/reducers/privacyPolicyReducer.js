import { SET_CONTENT } from '../actions/privacyPolicyAction';

const initialState = {
  content: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CONTENT:
      return { ...state, content: action.content };
    default:
      return state;
  }
};

export default reducer;
