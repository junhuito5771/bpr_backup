import {
  SET_IS_SERVICE_WELCOMED,
  SET_SERVICE_WELCOME_ITEM,
} from '../actions/serviceWelcomeAction';

const initialState = {
  isServiceWelcomed: false,
  serviceWelcomeItem: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_IS_SERVICE_WELCOMED:
      return { ...state, isServiceWelcomed: action.isServiceWelcomed };
    case SET_SERVICE_WELCOME_ITEM:
      return { ...state, serviceWelcomeItem: action.serviceWelcomeItem };
    default:
      return state;
  }
};

export default reducer;
