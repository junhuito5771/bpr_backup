import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import content from './contentReducer';
import marketingHome from './marketingHomeReducer';
import locateDealer from './locateDealerReducer';
import heatLoadCalculator from './heatLoadCalculatorReducer';
import contactUs from './contactUsReducer';
import errorCode from './errorCodeReducer';
import subscription from './subscriptionReducer';
import maintenance from './maintenanceReducer';
import privacyPolicy from './privacyPolicyReducer';
import welcome from './welcomeReducer';
import serviceWelcome from './serviceWelcomeReducer';

const contentPersistConfig = {
  key: 'content',
  storage: AsyncStorage,
  whitelist: ['lastModified'],
};

const marketingHomePersistConfig = {
  key: 'marketingHome',
  storage: AsyncStorage,
  whitelist: ['promotions', 'popUp'],
};

const locateDealerPersistConfig = {
  key: 'locateDealer',
  storage: AsyncStorage,
  whitelist: ['dealers', 'dealerCategories', 'cityStateOption'],
};

const heatLoadCalculatorPersistConfig = {
  key: 'heatLoadCalculator',
  storage: AsyncStorage,
  whitelist: ['allProduct'],
};

const subscriptionPersistConfig = {
  key: 'subscription',
  storage: AsyncStorage,
  whitelist: ['planDetail'],
};

const maintenancePersistConfig = {
  key: 'maintenance',
  storage: AsyncStorage,
  whitelist: ['reminders'],
};

const privacyPolicyPersistConfig = {
  key: 'privacyPolicy',
  storage: AsyncStorage,
};

const welcomePersistConfig = {
  key: 'welcome',
  storage: AsyncStorage,
};

const serviceWelcomePersistConfig = {
  key: 'serviceWelcome',
  storage: AsyncStorage,
};

const rootReducer = {
  content: persistReducer(contentPersistConfig, content),
  marketingHome: persistReducer(marketingHomePersistConfig, marketingHome),
  locateDealer: persistReducer(locateDealerPersistConfig, locateDealer),
  heatLoadCalculator: persistReducer(
    heatLoadCalculatorPersistConfig,
    heatLoadCalculator
  ),
  subscription: persistReducer(subscriptionPersistConfig, subscription),
  privacyPolicy: persistReducer(privacyPolicyPersistConfig, privacyPolicy),
  contactUs,
  errorCode,
  maintenance: persistReducer(maintenancePersistConfig, maintenance),
  welcome: persistReducer(welcomePersistConfig, welcome),
  serviceWelcome: persistReducer(serviceWelcomePersistConfig, serviceWelcome),
};

export default rootReducer;
