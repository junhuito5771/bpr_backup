import { SET_IS_WELCOMED, SET_WELCOME_ITEM } from '../actions/welcomeAction';

const initialState = {
  isWelcomed: false,
  welcomeItem: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_IS_WELCOMED:
      return { ...state, isWelcomed: action.isWelcomed };
    case SET_WELCOME_ITEM:
      return { ...state, welcomeItem: action.welcomeItem };
    default:
      return state;
  }
};

export default reducer;
