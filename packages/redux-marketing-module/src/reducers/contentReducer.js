import { SET_CONTENT_ERROR, SET_LAST_MODIFIED } from '../actions/contentAction';

const initialState = {
  lastModified: {},
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LAST_MODIFIED:
      return { ...state, lastModified: action.lastModified };
    case SET_CONTENT_ERROR:
      return { ...state, error: action.error };
    default:
      return state;
  }
};

export default reducer;
