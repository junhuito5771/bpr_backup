import {
  SET_IS_POPUP_SHOWN,
  SET_POPUP,
  SET_PROMOTIONS,
} from '../actions/marketingHomeAction';

const initialState = {
  promotions: [],
  popUp: [],
  isPopUpShown: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PROMOTIONS:
      return { ...state, promotions: action.promotions };
    case SET_POPUP:
      return { ...state, popUp: action.popUp };
    case SET_IS_POPUP_SHOWN:
      return { ...state, isPopUpShown: action.isPopUpShown };
    default:
      return state;
  }
};

export default reducer;
