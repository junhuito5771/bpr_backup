import { connect } from 'react-redux';

import * as content from './actions/contentAction';
import * as marketingHome from './actions/marketingHomeAction';
import * as locateDealer from './actions/locateDealerAction';
import * as heatLoadCalculator from './actions/heatLoadCalculatorAction';
import * as contactUs from './actions/contactUsAction';
import * as errorCode from './actions/errorCodeAction';
import * as subscription from './actions/subscriptionAction';
import * as maintenance from './actions/maintenanceAction';
import * as privacyPolicy from './actions/privacyPolicyAction';
import * as welcome from './actions/welcomeAction';
import * as serviceWelcome from './actions/serviceWelcomeAction';

import reducers from './reducers';
import sagas from './sagas';
import {
  getLatestRecord,
  cmsTypeEnum,
  checkLastModified,
} from './sagas/contentSaga';

import { damaErrorCodes, ditErrorCodes } from './constants/errorCodes';

export { damaErrorCodes, ditErrorCodes };

export { connect };

export {
  content,
  marketingHome,
  locateDealer,
  heatLoadCalculator,
  contactUs,
  errorCode,
  subscription,
  maintenance,
  privacyPolicy,
  welcome,
  serviceWelcome,
};

const marketHelper = {
  getLatestRecord,
  cmsTypeEnum,
  checkLastModified,
};

export { reducers, sagas, marketHelper };

export { setConfigure } from './sagas/contentSaga';
