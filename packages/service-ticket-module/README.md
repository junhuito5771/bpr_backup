# service-ticket-module

## Getting started

`$ npx lerna add @alphacode.my/service-ticket-module`

## Usage

```javascript
// 1. Add following code to packages/daikin-mobile/app/routes.js
// import to navigation file
import { init, NewServiceStack, MyServiceStack } from '@module/service-ticket';

// initialize configuration and theme
// init(config, theme)

/**
 * Default theme object
 *
 *  primary: '#4A90E2',
 *  success: '#6AC259',
 *  error: '#E45D5D',
 *  placeholder: '#A3A0A0',
 *  rating: '#E45D5D',
 *  white: '#fff',
 *  black: '#000',
 *
 **/
init(
  {
    company: 'Daikin',
  },
  {
    primary: '#4A90E2',
    success: '#6AC259',
    error: '#E45D5D',
    placeholder: '#A3A0A0',
    rating: '#E45D5D',
    white: '#fff',
    black: '#000',
  },
);

// Add the following to main navigation stack
<Drawer.Navigator>
  <Drawer.Screen name="Booking" component={MyServiceStack} />
</Drawer.Navigator>;
```
