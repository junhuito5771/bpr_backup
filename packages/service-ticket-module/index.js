// import { NativeModules } from 'react-native';
import { NewServiceStack, MyServiceStack } from './src/navigation/AppContainer';
import { setTheme } from './src/helpers/themes';
import { setAuthToken, setGetAuthTokenFunc } from './src/service/api';
import { setConfig } from './src/helpers/config';

// const { ServiceTicket } = NativeModules;

// export default ServiceTicket;
const init = (getTokenFunction, config, theme = {}) => {
  setGetAuthTokenFunc(getTokenFunction);
  setConfig(config);
  setTheme(theme);
};

export {
  NewServiceStack,
  MyServiceStack,
  setAuthToken,
  setGetAuthTokenFunc,
  init,
};
