/**
 *
 * @description hooks.js Custom Hooks
 * @version 1.0.0
 * @since 24 June 2020
 *
 */

import { useState, useEffect } from 'react';
import NetInfo from '@react-native-community/netinfo';

export function useModal() {
  const [visible, setVisible] = useState(false);

  function open() {
    setVisible(true);
  }

  function close() {
    setVisible(false);
  }

  return { visible, open, close };
}

export function useInternetConenction() {
  const [isOnline, setIsOnline] = useState(true);

  useEffect(() => {
    function handleFirstConnectivityChange(isConnected) {
      setIsOnline(isConnected);
      NetInfo.isConnected.removeEventListener(
        'connectionChange',
        handleFirstConnectivityChange,
      );
    }

    NetInfo.isConnected.fetch().then(handleFirstConnectivityChange);

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      handleFirstConnectivityChange,
    );
  }, []);

  return { isOnline };
}
