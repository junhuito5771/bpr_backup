import { Dimensions } from 'react-native';

export const { width, height } = Dimensions.get('window');

export let theme = {
  primary: '#4A90E2',
  secondary: '#E6EFFA',
  success: '#6AC259',
  error: '#E45D5D',
  placeholder: '#A3A0A0',
  rating: '#E45D5D',
  white: '#fff',
  black: '#000',
};

export const setTheme = (newThemes = {}) => {
  theme = {
    ...theme,
    ...newThemes,
  };

  // Freeze the config to prevent additional update
  Object.freeze(theme);
};
