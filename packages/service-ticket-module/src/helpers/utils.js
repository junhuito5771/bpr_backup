import EncryptedStorage from 'react-native-encrypted-storage';

export const priceFormat = price => {
  const formatted = parseFloat(price).toFixed(2);

  if (isNaN(formatted)) {
    return '-';
  }

  return formatted;
};

export const storage = {
  save: async (key, value) => {
    await EncryptedStorage.setItem(key, value);
  },
  load: async (key) => {
    return await EncryptedStorage.getItem(key);
  },
};

export const convertSecondToMinute = totalCountdownInSeconds => {
  const minutes = Math.floor(totalCountdownInSeconds / 60); // 60 seconds
  const seconds = totalCountdownInSeconds - minutes * 60; // Get reminder

  return `${minutes}:${seconds}`;
};

export const getConsumerQuotationTierBasedOnQuantity = (
  consumerQuotations,
  quantity,
) => {
  for (let i = consumerQuotations.length - 1; i >= 0; i--) {
    if (quantity >= consumerQuotations[i].minQuantity) {
      return consumerQuotations[i];
    }
  }
  return consumerQuotations[0];
};

export const isValidEmail = email => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return re.test(String(email).toLowerCase());
};

export const resetPopulatedValues = async () => {
  try {
    const hasPackages = await EncryptedStorage.getItem('@packages');
    const hasOtp = await EncryptedStorage.getItem('@otp');

    if (hasPackages !== null && hasPackages !== undefined) {
      await EncryptedStorage.removeItem('@packages');
    }

    if (hasOtp !== null && hasOtp !== undefined) {
      await EncryptedStorage.removeItem('@otp');
    }
  } catch (error) {
    console.log(error);
  }
}