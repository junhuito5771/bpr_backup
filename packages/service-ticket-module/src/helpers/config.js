import { theme } from './themes';
import Config from 'react-native-config';

export let config = {};
export const setConfig = (newConfig = {}) => {
  if (!newConfig?.company) {
    console.error("'company' property is required");
  } else {
    if (
      !['daikin', 'acson'].includes(String(newConfig?.company).toLowerCase())
    ) {
      console.error(
        "'company' property only allowed either 'daikin' or 'acson'",
      );
    }
  }

  config = {
    ...config,
    ...newConfig,
  };

  // Freeze the config to prevent additional update
  Object.freeze(config);
};

export const JOB_STARTED = 'JOB_STARTED';
export const TRAVELLING = 'TRAVELING';
export const JOB_COMPLETED = 'JOB_COMPLETED';
export const NOT_STARTED = 'NOT_STARTED';
export const FAILED = 'FAILED';
export const ASSIGNED = 'ASSIGNED';
export const CANCELLED_BY_SERVICE_PROVIDER = 'CANCELLED_BY_SERVICE_PROVIDER';
export const CANCELLED_BY_OPERATOR = 'CANCELLED_BY_OPERATOR';
export const CANCELLED_BY_CUSTOMER = 'CANCELLED_BY_CUSTOMER';
export const OPEN = 'OPEN';
export const AWAITING_PAYMENT = 'AWAITING_PAYMENT';
export const ALLOCATED = 'ALLOCATED';
export const TRAVEL = 'TRAVEL';

export const STATUS_COLOR = {
  [OPEN]: '#7367F0',
  [NOT_STARTED]: '#777',
  [FAILED]: theme.error,
  [TRAVELLING]: '#4DA6F8',
  [TRAVEL]: '#4DA6F8',
  [ASSIGNED]: '#4DA6F8',
  [JOB_STARTED]: '#4DA6F8',
  [JOB_COMPLETED]: '#01A369',
  [CANCELLED_BY_OPERATOR]: '#ff4444',
  [CANCELLED_BY_CUSTOMER]: '#ff4444',
  [CANCELLED_BY_SERVICE_PROVIDER]: '#ff4444',
  [AWAITING_PAYMENT]: '#F68159',
  [ALLOCATED]: '#4DA6F8',
};

export const isCallable = (status) => {
  return status === ASSIGNED || status === TRAVELLING || status === ALLOCATED;
};

export const isCancellable = (status) => {
  return [OPEN, ALLOCATED, NOT_STARTED, ASSIGNED, JOB_STARTED, AWAITING_PAYMENT].includes(
    status,
  );
};

export const isCompleted = (status) => status === JOB_COMPLETED;
export const isCancelled = (status) =>
  status === CANCELLED_BY_CUSTOMER ||
  status === CANCELLED_BY_OPERATOR ||
  status === CANCELLED_BY_SERVICE_PROVIDER;
