import { Alert } from 'react-native';

export const groupingServicePackages = (servicePackages: Array<Object>) => {
  try {
    const packages: Array<{
      name: string,
      code: string,
      nodes: Array<Object>,
    }> = [];
    servicePackages.forEach((servicePackage) => {
      const index = packages.findIndex(
        (group) => group.code === servicePackage.consumerDisplayGroup?.code,
      );

      if (index !== -1) {
        packages[index].nodes.push(servicePackage);
      } else {
        packages.push({
          code: servicePackage.consumerDisplayGroup?.code,
          name:
            servicePackage.consumerDisplayGroup?.name ||
            `Group name ${packages.length}`,
          nodes: [servicePackage],
          iconUrl: servicePackage.consumerDisplayGroup?.iconUrl,
        });
      }
    });

    return packages;
  } catch (error) {
    console.log(error);
    Alert.alert('Error', 'Something went error. Please try again later.');
  }
};
