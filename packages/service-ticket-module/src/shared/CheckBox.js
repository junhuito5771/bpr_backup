/**
 *
 * @description CheckBox.js
 * @version 1.0.0
 * @since 16 June 2020
 *
 */

import * as React from 'react';
import { TouchableOpacity, StyleSheet } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../helpers/themes';

type Props = {
    value: boolean,
    onValueChange: (val) => void
}
export default function CheckBox(props: Props) {
    const { value, onValueChange } = props
    const [checked, setChecked] = React.useState(value)

    function handleOnPressCheckBox() {
        setChecked(prev => {
            if (value !== prev && onValueChange) {
                onValueChange(!prev)
            }
            return !prev
        })
    }

    const checkedStyle = checked && styles.checked || {}
    return <TouchableOpacity style={[styles.checkbox, checkedStyle]} activeOpacity={0.7} onPress={handleOnPressCheckBox}>
        {checked && <MaterialIcons name="check" size={18} color={checked && theme.white || null} />}
    </TouchableOpacity>;
}

const styles = StyleSheet.create({
    checkbox: {
        borderColor: theme.black,
        borderWidth: 1,
        minHeight: 21,
        minWidth: 21,
        justifyContent: 'center',
        alignItems: 'center'
    },
    checked: {
        backgroundColor: theme.black
    }
})