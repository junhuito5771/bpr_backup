/**
 *
 * @description ModalSelectList.js
 * @version 1.0.0
 * @since 24 June 2020
 *
 */

import * as React from 'react';
import {
  Modal,
  FlatList,
  FlatListProps,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Divider from './Divider';
import Header from './Header';
import { theme } from '../helpers/themes';

type Props = {
  visible: boolean,
  list: Array,
  selectedID: string | number,
  onSelect: id => void,
  title?: string,
  onDone: () => void
};
export default function ModalSelectList(props: Props & FlatListProps) {
  const { visible, list, selectedID, onSelect, title, onDone } = props;

  function handleOnPressListItem(id) {
    return () => {
      if (onSelect) {
        onSelect(id);
      }
    };
  }

  function renderListItem({ item }) {
    return (
      <>
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.listItem}
          onPress={handleOnPressListItem(item.id)}>
          <Text>{item.name}</Text>
          {selectedID === item.id && (
            <MaterialIcons name="check" size={20} color={theme.success} />
          )}
        </TouchableOpacity>
        <Divider />
      </>
    );
  }

  return (
    <Modal visible={visible} animationType="slide">
      <SafeAreaView style={styles.safeAreaView}>
        <Header title={title || 'Modal'} onDone={onDone} right />
        <FlatList
          data={list}
          keyExtractor={(item, index) => `list-item-${item.id || index}`}
          renderItem={renderListItem}
          style={styles.list}
        />
      </SafeAreaView>
    </Modal>
  );
}

const styles = StyleSheet.create({
  list: {
    marginHorizontal: 15,
  },
  listItem: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  safeAreaView: {
    flex: 1,
  }
});
