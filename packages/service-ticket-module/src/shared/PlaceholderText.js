/**
 *
 * @description PlaceholderText.js
 * @version 1.0.0
 * @since 24 June 2020
 *
 */

import * as React from 'react';
import { Text, TextProps, StyleSheet } from 'react-native';

type Props = {
  children: string | React.ReactNode,
};
export default function PlaceholderText(props: Props & TextProps) {
  const { children, style, ...rest } = props;
  return (
    <Text style={[styles.placeholder, style]} {...rest}>
      {children}
    </Text>
  );
}

const styles = StyleSheet.create({
  placeholder: {
    color: '#aaa',
  },
});
