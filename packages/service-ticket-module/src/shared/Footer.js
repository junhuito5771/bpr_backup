/**
 *
 * @description Footer.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { theme } from '../helpers/themes';

type Props = {
  children: React.ReactNode,
  style?: object,
};
export default function Footer(props: Props) {
  const { children, style } = props;
  return <View style={[styles.footer, style]}>{children}</View>;
}

const styles = StyleSheet.create({
  footer: {
    paddingVertical: 16,
    paddingHorizontal: 16,
    shadowColor: '#777',
    shadowOffset: { height: -1, width: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 10,
    elevation: 20,
    backgroundColor: theme.white,
  },
});
