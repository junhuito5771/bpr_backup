/**
 *
 * @description Content.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import { KeyboardAvoidingView, StyleSheet } from 'react-native';

type Props = {
  children: React.ReactNode,
  style?: object,
};
export default function Content(props: Props) {
  const { children, style, ...rest } = props;

  return (
    <KeyboardAvoidingView style={[styles.content, style]} {...rest}>
      {children}
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  content: {
    padding: 16,
  },
});
