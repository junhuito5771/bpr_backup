/**
 *
 * @description Input.js
 * @version 1.0.0
 * @since 24 June 2020
 *
 */

import * as React from 'react';
import { TextInput, TextInputProps, StyleSheet, View } from 'react-native';
import FormHelper from './FormHelper';
import { theme } from '../helpers/themes';

type Props = {
  error?: string,
};
export default function Input(props: Props & TextInputProps) {
  const { style, error, ...rest } = props;

  const errorStyle = (!!error && styles.error) || {};
  return (
    <>
      <View style={styles.container}>
        <TextInput
          underlineColorAndroid="transparent"
          style={[styles.textInput, errorStyle, style]}
          autoCapitalize={false}
          {...rest}
        />
      </View>
      {!!error && <FormHelper mode="error">{error}</FormHelper>}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    justifyContent: 'center',
    borderRadius: 5,
  },
  textInput: {
    paddingHorizontal: 15,
    height: 50,
  },
  error: {
    borderColor: theme.error,
  },
});
