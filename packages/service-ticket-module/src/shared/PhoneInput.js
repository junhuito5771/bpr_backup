/**
 *
 * @description PhoneInput.js
 * @version 1.0.0
 * @since 24 June 2020
 *
 */

import * as React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  TextInputProps,
  TouchableOpacity,
} from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';

type Props = {
  containerStyle?: object,
  textInput: TextInputProps,
  onSelect: (countryCallingCode) => void,
};
export default function PhoneInput(props: Props) {
  const {
    textInput: { style, containerStyle, onSelect, ...textInputRest } = {
      ...props,
    },
  } = props;
  const [countryCode, setCountryCode] = React.useState('MY');
  const [countryCallingCode, setCallingCountryCode] = React.useState('60');
  const [visible, setVisible] = React.useState(false);

  function handleOnSelect(country) {
    setCountryCode(country.cca2);
    setCallingCountryCode(country.callingCode[0]);

    if (onSelect) {
      onSelect(country.callingCode[0]);
    }
  }

  return (
    <View style={[styles.inputContainer, containerStyle]}>
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.phoneNumberPrefix}
        onPress={() => setVisible(true)}
      >
        <Text>{`+${countryCallingCode}`}</Text>
      </TouchableOpacity>
      <TextInput
        underlineColorAndroid="transparent"
        style={[styles.phoneNumber, style]}
        {...textInputRest}
      />

      <CountryPicker
        countryCode={countryCode}
        renderFlagButton={() => null}
        visible={visible}
        onClose={() => setVisible(false)}
        onSelect={handleOnSelect}
        withFilter
      />
    </View>
  );
}

const styles = StyleSheet.create({
  inputContainer: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginVertical: 5,
    flexDirection: 'row',
    borderRadius: 8,
  },
  phoneNumberPrefix: {
    flex: 0.2,
  },
  phoneNumber: {
    flex: 0.8,
  },
});
