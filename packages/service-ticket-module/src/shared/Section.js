/**
 *
 * @description Section.js
 * @version 1.0.0
 * @since 25 June 2020
 *
 */

import * as React from 'react';
import { View, StyleSheet, ViewProps } from 'react-native';

type Props = {
  children: React.ReactNode,
};
export default function Section(props: Props & ViewProps) {
  const { children, style, ...rest } = props;
  return (
    <View style={[styles.container, styles]} {...rest}>
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 24,
  },
});
