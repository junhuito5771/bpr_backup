/**
 *
 * @description Payment.js
 * @version 1.0.0
 * @since 28 July 2020
 *
 */

import React, { useEffect } from 'react';
import axios from 'axios';
import {
  Modal,
  Alert,
  StyleSheet,
  Dimensions,
  StatusBar,
  Platform,
} from 'react-native';
import { WebView } from 'react-native-webview';
import Config from 'react-native-config';
import Content from './Content';
import Loading from './Loading';
import Button from './Button';
import Container from './Container';
import Title from './Title';

let open = false;
let interval = null;
const { width } = Dimensions.get('window');

function encodeObject(params) {
  const query = [];
  for (const key in params) {
    const val = `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
    query.push(val);
  }
  return query.join('&');
}

type Props = {
  visible: boolean,
  uri: string,
  payload: object,
  onCancel: (prompt: boolean) => void,
  onSuccess: () => void,
  onPeriodicCheck: () => boolean,
};
export default function Payment({
  visible,
  uri,
  payload,
  onCancel,
  onSuccess,
  onPeriodicCheck,
}: Props) {
  const [loading, setLoading] = React.useState(false);
  const [isPreview, setIsPreview] = React.useState(false);
  const [html, setHtml] = React.useState(undefined);

  console.log(['payment', uri, Config.IPAY88_REFERER_URL, payload]);

  useEffect(() => {
    async function init() {
      if (Platform.OS === 'android') {
        const result = await axios.post(uri, encodeObject(payload), {
          headers: {
            referer:
              Config.IPAY88_REFERER_URL ||
              'https://daikin.daikinservishub.com/checkout',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        });
        setHtml(result.data);
      }

      interval = setInterval(async () => {
        if (typeof onPeriodicCheck === 'function') {
          const result = await onPeriodicCheck();
          if (result) {
            onSuccess();
          }
        }
      }, 5000);
    }

    if (visible && uri && payload) {
      init();
    }

    return () => {
      if (interval) {
        clearInterval(interval);
      }
    };
  }, [visible]);

  function handleOnStateChange(stateOrRequest) {
    const { url } = stateOrRequest;
    const [endpoint, qs] = url?.split('?');

    if (
      endpoint.match(/(daikin.servishub.co)/g) ||
      endpoint.match(/(acson.servishub.co)/g) ||
      endpoint.match(/(daikin.daikinservishub.com)/g) ||
      endpoint.match(/(acson.daikinservishub.com)/g) ||
      endpoint.match(/(myacson.acson.com.my)/g) ||
      endpoint.match(/(godaikin-booking.daikin.com.my)/g)
    ) {
      setIsPreview(true);
    }

    if (qs) {
      const queryParams = qs?.split('&');

      let response = null;
      for (let index = 0; index < queryParams.length; index++) {
        const element = queryParams[index];
        const [key, value] = element?.split('=');

        if (response === null) {
          response = {};
        }

        response[key] = decodeURI(value);
      }

      if (response) {
        if (response?.status === 'failed' && response?.reason) {
          const { status, reason } = response;

          if (!open) {
            open = true;
            Alert.alert(status.toUpperCase(), reason, [
              {
                text: 'Ok',
                onPress: () => {
                  open = false;
                  onCancel(false);
                },
              },
            ]);
          }
        }

        if (response?.status === 'succeeded') {
          const { status } = response;
          if (!open) {
            open = true;
            Alert.alert(status.toUpperCase(), 'Successfully checkout', [
              {
                text: 'Ok',
                onPress: () => {
                  open = false;
                  onSuccess();
                },
              },
            ]);
          }
        }
      }
    }
  }

  return (
    <Modal
      visible={visible}
      animationType="slide"
      onRequestClose={() => onCancel(true)}
    >
      <StatusBar barStyle="dark-content" />
      <Container>
        <Content style={{ flex: 1 }}>
          <Title>Checkout</Title>
          <Loading visible={loading} />
          {!isPreview &&
            (Platform.OS === 'android' ? (
              html && (
                <WebView
                  source={{
                    baseUrl: uri,
                    html,
                    headers: {
                      referer:
                        Config.IPAY88_REFERER_URL ||
                        'https://daikin.daikinservishub.com/checkout',
                      'Content-Type': 'application/x-www-form-urlencoded',
                    },
                  }}
                  onError={(err) => console.warn('err', err)}
                  onNavigationStateChange={handleOnStateChange}
                  onLoadStart={(event) => {
                    setLoading(event.nativeEvent.loading);
                  }}
                  onLoadEnd={(event) => {
                    const onEndloading = event.nativeEvent.loading;
                    setTimeout(() => {
                      setLoading(onEndloading);
                    }, 2000);
                  }}
                />
              )
            ) : (
              <WebView
                source={{
                  uri,
                  method: 'POST',
                  body: encodeObject(payload),
                  headers: {
                    referer:
                      Config.IPAY88_REFERER_URL ||
                      'https://daikin.daikinservishub.com/checkout',
                    'Content-Type': 'application/x-www-form-urlencoded',
                  },
                }}
                onError={(err) => console.warn('err', err)}
                onNavigationStateChange={handleOnStateChange}
                onLoadStart={(event) => {
                  setLoading(event.nativeEvent.loading);
                }}
                onLoadEnd={(event) => {
                  setLoading(event.nativeEvent.loading);
                }}
              />
            ))}
        </Content>
        <Button error style={styles.close} onPress={() => onCancel(true)}>
          Cancel Payment
        </Button>
      </Container>
    </Modal>
  );
}

const styles = StyleSheet.create({
  close: {
    marginBottom: 16,
    marginHorizontal: 16,
    width: width - 16 * 2,
  },
});
