/**
 *
 * @description Title.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import { Text, StyleSheet, TextProps } from 'react-native';

type Props = {
  children: string,
};
export default function Title(props: Props & TextProps) {
  const { children, style, ...rest } = props;
  return (
    <Text style={[styles.title, style]} {...rest}>
      {children}
    </Text>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 22,
    fontWeight: 'bold',
  },
});
