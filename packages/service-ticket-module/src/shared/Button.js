/**
 *
 * @description Button.js
 * @version 1.0.0
 * @since 10 June 2020
 *
 */

import * as React from 'react';
import { ReactNode } from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import { theme, makeStyle } from '../helpers/themes';

type Props = {
  children: string | ReactNode,
  success?: boolean,
  error?: boolean,
  style?: object,
  labelStyle?: object,
  disabled: boolean,
};
export default function Button(props: Props) {
  const {
    primary,
    success,
    error,
    children,
    style,
    labelStyle,
    disabled,
    ...rest
  } = props;

  if (disabled) {
    return (
      <TouchableOpacity
        activeOpacity={1}
        disabled
        style={[
          styles.button,
          { backgroundColor: theme.placeholder, color: theme.white },
          style,
        ]}
        {...rest}
      >
        <Text style={[styles.label, labelStyle]}>{children}</Text>
      </TouchableOpacity>
    );
  }

  if (success) {
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        style={[
          styles.button,
          { backgroundColor: theme.success, color: theme.white },
          style,
        ]}
        {...rest}
      >
        <Text style={[styles.label, labelStyle]}>{children}</Text>
      </TouchableOpacity>
    );
  }

  if (error) {
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        style={[
          styles.button,
          {
            backgroundColor: theme.error,
            color: theme.white,
          },
          style,
        ]}
        {...rest}
      >
        <Text style={[styles.label, labelStyle]}>{children}</Text>
      </TouchableOpacity>
    );
  }

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={[
        styles.button,
        {
          backgroundColor: theme.primary,
          color: theme.white,
        },
        style,
      ]}
      {...rest}
    >
      <Text style={[styles.label, labelStyle]}>{children}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    justifyContent: 'center',
    height: 40,
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  label: {
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
    color: theme.white,
  },
});
