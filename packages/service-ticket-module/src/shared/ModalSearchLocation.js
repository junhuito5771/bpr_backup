/**
 *
 * @description ModalSearchLocation.js
 * @version 1.0.0
 * @since 27 June 2020
 *
 */

import * as React from 'react';
import {
  Modal,
  FlatList,
  View,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import axios from 'axios';
import Config from 'react-native-config';
import EncryptedStorage from 'react-native-encrypted-storage';

import Header from './Header';
import SearchBox from './SearchBox';
import Label from './Label';
import PlaceholderText from './PlaceholderText';
import Divider from './Divider';
import { theme } from '../helpers/themes';
import { api } from '../service/api';
import FormHelper from './FormHelper';
import Loading from './Loading';

let searchTimeout = null;
type Props = {
  visible: boolean,
  onDone?: location => void,
  onSelected?: location => void,
};
export default function ModalSearchLocation(props: Props) {
  const { visible, onDone, onSelected } = props;
  const [search, setSearch] = React.useState('');
  const [places, setPlaces] = React.useState([]);
  const [hasCoverage, setHasCoverage] = React.useState(true);
  const [loading, setLoading] = React.useState(false);

  function handleOnSelected(index) {
    return async () => {
      const res = await axios({
        method: 'GET',
        url: `https://maps.googleapis.com/maps/api/place/details/json?place_id=${places[index].place_id}&fields=geometry,formatted_address,address_component&key=${Config.GOOGLE_MAP_KEY}`,
      }).catch(err => {
        console.log(err);
      });

      if (res?.data?.result?.geometry?.location) {
        const { lat, lng } = res.data.result.geometry.location;
        const coverageRes = await api.get(
          `service-areas/coverageValidity?latitude=${lat}&longitude=${lng}`,
        );

        if (coverageRes) {
          setHasCoverage(coverageRes.validity);

          if (!coverageRes.validity) {
            // Area not within coverage, not proceed further
            return;
          }
        }
      }

      if (res?.data?.result?.address_components?.length > 0) {
        const {
          data: {
            result: { address_components },
          },
        } = res;
        for (let index = 0; index < address_components.length; index++) {
          const component = address_components[index];
          if (component?.types?.includes('locality')) {
            await EncryptedStorage.setItem('@city', component.long_name);
          }

          if (component?.types?.includes('administrative_area_level_1')) {
            await EncryptedStorage.setItem('@state', component.long_name);
          }

          if (component?.types?.includes('postal_code')) {
            await EncryptedStorage.setItem('@postcode', component.long_name);
          }
        }
      }

      if (res?.data?.result?.geometry?.location) {
        await EncryptedStorage.setItem(
          '@latlng',
          JSON.stringify(res.data.result.geometry.location),
        );
      }

      if (onSelected) {
        onSelected(places[index]);
      }
    };
  }

  function handleOnDone() {
    if (onDone) {
      onDone();
    }
  }

  function renderLocation({ item, index }) {
    return (
      <>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.row}
          onPress={handleOnSelected(index)}
        >
          <MaterialIcons
            name="place"
            size={24}
            style={styles.locationIcon}
            color={theme.placeholder}
          />
          <View style={styles.listItemLocation}>
            <PlaceholderText
              style={styles.subtitle}
              numberOfLines={2}
              ellipsizeMode="tail"
            >
              {item.description}
            </PlaceholderText>
          </View>
        </TouchableOpacity>
        <Divider />
      </>
    );
  }

  function handleOnChangeText(text) {
    setSearch(text);

    if (searchTimeout) {
      clearTimeout(searchTimeout);
    }

    setLoading(true);
    searchTimeout = setTimeout(async () => {
      const res = await axios({
        method: 'GET',
        url: `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${text}&components=country:my&key=${Config.GOOGLE_MAP_KEY}`,
      }).catch(err => {
        console.log(err);
        setLoading(false);
      });

      if (res.status === 200 && res.data?.predictions?.length > 0) {
        setPlaces(res.data.predictions);
      }

      setLoading(false);
    }, 2000);
  }

  return (
    <Modal
      visible={visible}
      animationType="slide"
      onRequestClose={handleOnDone}
    >
      <SafeAreaView style={styles.safeAreaView}>
        <Header title="Location" onDone={handleOnDone} right />
        <SearchBox
          value={search}
          onChangeText={handleOnChangeText}
          placeholder="Search location"
          clearButtonMode="while-editing"
        />

        {places.length > 0 && (
          <FlatList
            data={places}
            renderItem={renderLocation}
            keyExtractor={(item, index) => `location-${index}`}
            contentContainerStyle={styles.list}
            keyboardShouldPersistTaps="handled"
            ListHeaderComponent={
              !hasCoverage && (
                <FormHelper mode="error">
                  Sorry, service is not available here.
                </FormHelper>
              )
            }
          />
        )}

        {loading && (
          <View style={styles.loadingContainer}>
            <ActivityIndicator />
          </View>
        )}
      </SafeAreaView>
    </Modal>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  subtitle: {
    fontSize: 12,
    paddingVertical: 0,
  },
  list: {
    marginHorizontal: 16,
  },
  listItemLocation: {
    flex: 1,
  },
  locationIcon: {
    margin: 10,
  },
  checkedIcon: {
    margin: 10,
  },
  safeAreaView: {
    flex: 1,
  },
  loadingContainer: {
    flex: 0.5,
    justifyContent: 'center',
  },
});
