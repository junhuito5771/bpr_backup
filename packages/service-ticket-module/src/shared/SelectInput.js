/**
 *
 * @description SelectInput.js
 * @version 1.0.0
 * @since 23 June 2020
 *
 */

import * as React from 'react';
import {
  TouchableOpacity,
  Text,
  TouchableOpacityProps,
  StyleSheet,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

type Props = {
  placeholder: string,
  value: string,
};
export default function SelectInput(props: Props & TouchableOpacityProps) {
  const { placeholder, style, value, ...rest } = props;

  return (
    <TouchableOpacity
      style={[styles.inputContainer, style]}
      activeOpacity={0.5}
      {...rest}>
      <Text style={[value ? {} : styles.placholder]}>
        {value || placeholder}
      </Text>
      <MaterialIcons name="keyboard-arrow-down" size={20} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  inputContainer: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginVertical: 5,
    flexDirection: 'row',
    borderRadius: 8,
  },
  placholder: {
    color: '#aaa',
  },
});
