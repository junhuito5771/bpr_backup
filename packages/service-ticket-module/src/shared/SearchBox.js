/**
 *
 * @description SearchBox.js
 * @version 1.0.0
 * @since 27 June 2020
 *
 */

import * as React from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TextInputProps,
  Platform,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const { OS } = Platform;

type Props = {
  onClearAndroid: () => void,
  searchText: string,
};
export default function SearchBox(props: Props & TextInputProps) {
  const { searchText, style, onClearAndroid, ...rest } = props;
  return (
    <View style={[styles.row, styles.container]}>
      <View style={[styles.row, styles.textinputContainer]}>
        <MaterialIcons
          name="search"
          size={24}
          style={styles.searchIcon}
          color="#777"
        />
        <TextInput style={[styles.textInputStyle, style]} {...rest} />

        {OS === 'android' && onClearAndroid && (
          <MaterialIcons
            name="close"
            size={14}
            style={styles.clearIcon}
            color="white"
          />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 16,
    marginHorizontal: 8,
    borderRadius: 8,
    height: 40,
  },
  container: {
    justifyContent: 'space-between',
  },
  textinputContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#eee',
  },
  textInputStyle: {
    flex: 1,
  },
  searchIcon: {
    marginHorizontal: 5,
  },
  clearIcon: {
    backgroundColor: '#777',
    borderRadius: 50,
    marginHorizontal: 10,
  },
});
