/**
 *
 * @description Loading.js
 * @version 1.0.0
 * @since 20 July 2020
 *
 */

import * as React from 'react';
import { Modal, View, ActivityIndicator, StyleSheet } from 'react-native';
import { theme } from '../helpers/themes';

type Props = {
  visible: boolean,
};
export default function Loading(props: Props) {
  const { visible } = props;
  return (
    <Modal visible={visible} animationType="fade" transparent>
      <View style={styles.overlay}>
        <View style={styles.loadingContainer}>
          <ActivityIndicator color={theme.primary} />
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(100, 100, 100, 0.5)',
  },
  loadingContainer: {
    backgroundColor: 'white',
    borderRadius: 13,
    height: 60,
    width: 60,
    justifyContent: 'center',
  },
});
