import * as React from 'react';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

type Props = {
  title: string,
  children?: React.ReactChildren,
  shouldExpand?: boolean | undefined,
  onExpand?: (status: boolean) => void
};
export default function Accordion({
  title,
  children,
  shouldExpand = false,
  onExpand

}: Props) {
  const [expanded, setExpanded] = React.useState(shouldExpand);

  return (
    <View style={styles.groupWrapper}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => {
          setExpanded((prev) => {
            const status = !prev

            if (onExpand) {
              onExpand(status)
            }

            return status
          });
        }}
        style={styles.groupTitleWrapper}
      >
        <Text style={styles.groupTitle}>{title}</Text>

        {expanded && <MaterialIcons name="keyboard-arrow-down" size={24} />}
        {!expanded && <MaterialIcons name="keyboard-arrow-up" size={24} />}
      </TouchableOpacity>

      {expanded && (
        <View style={styles.childWrapper}>
          {children?.map((child) => {
            if (React.isValidElement(child)) {
              return child;
            }
            return null;
          })}
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  groupWrapper: {
    marginBottom: 16,
  },
  groupTitleWrapper: {
    flexDirection: 'row',
    height: 45,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'rgba(175, 175, 175, 0.3)',
    paddingHorizontal: 16,
  },
  groupTitle: {
    fontWeight: '600',
    fontSize: 18,
  },
  childWrapper: {
    paddingHorizontal: 16,
  },
});
