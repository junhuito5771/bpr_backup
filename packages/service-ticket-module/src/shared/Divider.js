/**
 *
 * @description Divider.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { theme } from '../helpers/themes';

type Props = {
  style?: object,
};
export default function Divider(props: Props) {
  const { style } = props;
  return <View style={[styles.divider, style]} />;
}

const styles = StyleSheet.create({
  divider: {
    borderBottomColor: theme.placeholder,
    borderBottomWidth: 1,
  },
});
