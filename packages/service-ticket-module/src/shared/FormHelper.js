/**
 *
 * @description FormHelper.js
 * @version 1.0.0
 * @since 26 July 2020
 *
 */

import * as React from 'react';
import { Text, StyleSheet } from 'react-native';
import { theme } from '../helpers/themes';

type Props = {
  mode: 'error' | 'success',
  children: string | React.ReactNode,
};
export default function FormHelper(props: Props) {
  const { mode, children } = props;

  return <Text style={[styles.helperText, styles[mode]]}>{children}</Text>;
}

const styles = StyleSheet.create({
  error: {
    color: theme.error,
  },
  success: {
    color: theme.success,
  },
  helperText: {
    fontSize: 12,
  },
});
