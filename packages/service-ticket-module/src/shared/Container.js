/**
 *
 * @description Container.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';

import { theme } from '../helpers/themes';

type Props = {
  children: React.ReactNode,
  style?: object,
};
export default function Container(props: Props) {
  const { children, style } = props;
  return (
    <SafeAreaView style={[styles.safeArewView, style]}>{children}</SafeAreaView>
  );
}

const styles = StyleSheet.create({
  safeArewView: {
    flex: 1,
    backgroundColor: theme.white,
  },
});
