/**
 *
 * @description Header.js
 * @version 1.0.0
 * @since 24 June 2020
 *
 */

import * as React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

type Props = {
  title: string | React.ReactNode,
  left?: boolean,
  right?: boolean,
  onDone: () => void,
  rightProps: { label: string },
};
export default function Header(props: Props) {
  const { title, left, right, onDone, rightProps } = props;

  return (
    <View style={styles.row}>
      <TouchableOpacity style={[styles.left, styles.centerContent]}>
        {left && <MaterialIcons name="close" size={30} />}
      </TouchableOpacity>
      <TouchableOpacity style={[styles.center, styles.centerContent]}>
        <Text style={{ fontSize: 21 }}>{title}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.right, styles.centerContent]}
        onPress={onDone}
      >
        {right && (
          <Text style={styles.rightText}>{rightProps?.label || 'Done'}</Text>
        )}
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    borderBottomColor: '#aaa',
    borderBottomWidth: 1,
    backgroundColor: 'white',
    height: 40,
  },
  left: {
    flex: 0.2,
  },
  center: {
    flex: 0.6,
  },
  right: {
    flex: 0.2,
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 40,
  },
  rightText: {
    fontSize: 14,
    paddingHorizontal: 5,
  },
});
