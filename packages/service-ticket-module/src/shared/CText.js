/**
 *
 * @description CText.js
 * @author yikkok <yikok.yong@gmail.com>
 * @version 1.0.0
 * @since 26 August 2020
 *
 */

import * as React from 'react';
import { Text as RNText, TextProps } from 'react-native';
import { theme } from '../helpers/themes';

type Props = {
  children: string | React.ReactNode,
  onPress: () => void,
};
export default function CText({ children, style, onPress }: Props & TextProps) {
  return (
    <RNText style={[{ color: theme.primary }, style]} onPress={onPress}>
      {children}
    </RNText>
  );
}
