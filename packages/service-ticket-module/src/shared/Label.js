/**
 *
 * @description Label.js
 * @version 1.0.0
 * @since 17 June 2020
 *
 */

import * as React from 'react';
import { Text, StyleSheet } from 'react-native';

type Props = {
  title: string,
  style?: object,
};
export default function Label(props: Props) {
  const { title, style } = props;
  return <Text style={[styles.label, style]}>{title}</Text>;
}

const styles = StyleSheet.create({
  label: {
    fontSize: 12,
    fontWeight: '500',
    paddingBottom: 8
  },
});
