import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  full: {
    flex: 1,
  },
  'ai-center': {
    alignItems: 'center',
  },
});
