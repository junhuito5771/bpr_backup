import * as React from 'react';

export const ServiceRequestContext = React.createContext({});

type Props = {
  children: React.ReactNode,
};
export default function ServiceRequestProvider({ children }: Props) {
  const [packages, setPackages] = React.useState({
    total: 0,
    subTotal: 0,
    discount: 0,
  });
  const [
    validExpectedArrivalTime,
    setValidExpectedArrivalView,
  ] = React.useState(false);
  const [openHelpWeb, setOpenHelpWeb] = React.useState(false);

  return (
    <ServiceRequestContext.Provider
      value={{
        packages,
        setPackages,
        validExpectedArrivalTime,
        setValidExpectedArrivalView,
        openHelpWeb,
        setOpenHelpWeb,
      }}
    >
      {children}
    </ServiceRequestContext.Provider>
  );
}
