/**
 *
 * @description ServiceContactPersonProvider.js
 * @author yikkok <yikok.yong@gmail.com>
 * @version 1.0.0
 * @since 16 August 2020
 *
 */

import * as React from 'react';

export const ServiceContactPersonContext = React.createContext();

export default function ServiceContactPersonProvider({ children }) {
  const [info, setInfo] = React.useState({
    prefix: '',
    fullname: '',
    email: '',
    primaryPhoneNumber: '',
    secondaryPhoneNumber: '',
    otp: '',
  });

  return (
    <ServiceContactPersonContext.Provider
      value={{
        info,
        onChangeInfo: (key, value) => {
          setInfo((prev) => ({ ...prev, [key]: value }));
        },
      }}
    >
      {children}
    </ServiceContactPersonContext.Provider>
  );
}
