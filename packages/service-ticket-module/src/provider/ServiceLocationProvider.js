/**
 *
 * @description ServiceLocationProvider.js
 * @author yikkok <yikok.yong@gmail.com>
 * @version 1.0.0
 * @since 16 August 2020
 *
 */

import * as React from 'react';

export const ServiceLocationContext = React.createContext();

export default function ServiceLocationProvider({ children }) {
  const [locationInfo, setLocationInfo] = React.useState({
    street1: '',
    street2: '',
    postcode: '',
    city: '',
    state: '',
    propertyType: '',
    building: '',
  });

  return (
    <ServiceLocationContext.Provider
      value={{
        info: locationInfo,
        onChangeText: (key, value) => {
          setLocationInfo((prev) => ({ ...prev, [key]: value }));
        },
      }}
    >
      {children}
    </ServiceLocationContext.Provider>
  );
}
