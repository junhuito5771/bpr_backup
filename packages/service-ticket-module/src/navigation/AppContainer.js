/**
 *
 * @description AppContainer.js
 * @version 1.0.0
 * @since 20 May 2020
 *
 */

import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { WelcomeOverlay } from '@module/daikin-ui';

import DaikinNewService from '../screens/BookingService/NewService.daikin';
import AcsonNewService from '../screens/BookingService/NewService.acson';

import { MyServiceListFC } from '../screens/MyService/MyServiceList';
import { MyServiceDetailsFC } from '../screens/MyService/MyServiceDetails';
import Feedback from '../screens/MyService/Feedback';
import { CancelOrderFC } from '../screens/MyService/CancelOrder';
import RequestSummary from '../screens/RequestSummary/RequestSummary';
import ServiceLocation from '../screens/Location/Location';

import { config } from '../helpers/config';
import ServiceLocationProvider from '../provider/ServiceLocationProvider';
import ServiceContactPersonProvider from '../provider/ServiceContactPersonProvider';
import ServiceRequestProvider from '../provider/ServiceRequestProvider';
import PropertyType from '../screens/BookingService/PropertyType';
import ArrivalTime from '../screens/BookingService/ArrivalTime';
import Location from '../screens/BookingService/Location';
import ContactPerson from '../screens/BookingService/ContactPerson';
import AcsonServicePackages from '../screens/BookingService/ServicePackages.acson';

const Stack = createStackNavigator();
export const NewServiceStack = ({
  initServiceWelcome,
  isServiceWelcomed,
  serviceWelcomeItem,
  setIsServiceWelcome,
}) => {
  React.useEffect(() => {
    if (String(config.company).toLowerCase() === 'daikin') {
      initServiceWelcome();
    } else {
      initServiceWelcome('Acson');
    }
  }, []);

  function handleCloseOverlay() {
    setIsServiceWelcome(true);
  }

  return (
    <ServiceLocationProvider>
      <ServiceContactPersonProvider>
        <ServiceRequestProvider>
          <WelcomeOverlay
            visible={!isServiceWelcomed && !!serviceWelcomeItem?.length}
            data={serviceWelcomeItem}
            onClose={handleCloseOverlay}
          />
          <Stack.Navigator initialRouteName="ServiceLocation">
            <Stack.Screen
              name="ServiceLocation"
              component={ServiceLocation}
              initialParams={{ resetPackages: true }}
            />
            {String(config.company).toLowerCase() === 'daikin' && (
              <>
                <Stack.Screen name="NewService" component={DaikinNewService} />
                <Stack.Screen name="PropertyType" component={PropertyType} />
                <Stack.Screen name="ArrivalTime" component={ArrivalTime} />
                <Stack.Screen name="Location" component={Location} />
                <Stack.Screen name="ContactPerson" component={ContactPerson} />
              </>
            )}
            {String(config.company).toLowerCase() === 'acson' && (
              <>
                <Stack.Screen name="NewService" component={AcsonNewService} />
                <Stack.Screen
                  name="Packages"
                  component={AcsonServicePackages}
                />
                <Stack.Screen name="PropertyType" component={PropertyType} />
                <Stack.Screen name="ArrivalTime" component={ArrivalTime} />
                <Stack.Screen name="Location" component={Location} />
                <Stack.Screen name="ContactPerson" component={ContactPerson} />
              </>
            )}
            <Stack.Screen name="RequestSummary" component={RequestSummary} />
          </Stack.Navigator>
        </ServiceRequestProvider>
      </ServiceContactPersonProvider>
    </ServiceLocationProvider>
  );
};

export const MyServiceStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="MyServiceList"
      component={MyServiceListFC}
      options={{ title: 'Booking' }}
    />
    <Stack.Screen name="MyServiceDetails" component={MyServiceDetailsFC} />
    <Stack.Screen name="Feedback" component={Feedback} />
    <Stack.Screen name="CancelOrder" component={CancelOrderFC} />
  </Stack.Navigator>
);
