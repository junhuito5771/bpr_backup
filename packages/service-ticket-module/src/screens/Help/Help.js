import React from 'react';
import { useFocusEffect } from '@react-navigation/native';
import WebView from 'react-native-webview';
import { ActivityIndicator, Modal, StyleSheet, View } from 'react-native';
import {
  verticalScale,
  viewportHeight,
  NavigationService,
} from '@module/utility';

import { Colors, HeaderText, HeaderBackButton } from '@module/daikin-ui';
import Container from '../../shared/Container';
import Header from '../../shared/Header';
import { config } from '../../helpers/config';

const styles = StyleSheet.create({
  loadingCenter: {
    paddingTop: verticalScale(30),
    height: viewportHeight,
    justifyContent: 'flex-start',
  },
});

type Props = {
  visible: boolean,
  onClose: () => void,
};
const HelpWebScreen = ({ visible, onClose }: Props) => {
  const renderLoading = React.useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    [],
  );

  const uri =
    String(config.company).toLowerCase() === 'acson'
      ? 'http://doc.aircontrol.acson.com.my/CMS/XPOCMS_AcsonPromotionLink/24/Content.html'
      : 'http://public.daikin.io/DMSS_CMS/XPOCMS_PromotionLink/21/Content.html';

  return (
    <Modal visible={visible} onRequestClose={onClose} animationType="slide">
      <Container>
        <Header
          title="Help"
          right
          rightProps={{ label: 'Close' }}
          onDone={onClose}
        />
        <WebView
          startInLoadingState
          renderLoading={renderLoading}
          source={{
            uri,
          }}
        />
      </Container>
    </Modal>
  );
};

export default HelpWebScreen;
