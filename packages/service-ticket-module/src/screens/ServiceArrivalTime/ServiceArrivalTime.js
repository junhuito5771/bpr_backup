/**
 *
 * @description ServiceArrivalTime.js
 * @version 1.0.0
 * @since 17 June 2020
 *
 */

import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {
  format,
  parseISO,
  addHours,
  addDays,
  isBefore,
  isSunday,
} from 'date-fns';
import RNPickerSelect from 'react-native-picker-select';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {
  setExpectedArrivalTime,
  schedules,
  minDate,
  filterMoreThan24Hours,
  isBefore6PM,
  isWithin24Hours,
} from './utils';
import Label from '../../shared/Label';
import Content from '../../shared/Content';
import SelectInput from '../../shared/SelectInput';
import Section from '../../shared/Section';
import FormHelper from '../../shared/FormHelper';

export default function ServiceArrivalTime({ onChange }) {
  const [visiblePicker, setVisiblePicker] = React.useState(false);
  const [date, setDate] = React.useState('');
  const [selectedStart, setSelectedStart] = React.useState('');
  const [errorOnDate, setErrorOnDate] = React.useState('');

  function handleOnPressDate() {
    setVisiblePicker(true);
  }

  function handleOnCloseDatetimePicker() {
    setVisiblePicker(false);
  }

  function handleOnConfirm(newDate) {
    setVisiblePicker(false);

    if (isSunday(newDate)) {
      setErrorOnDate('Unavailable on Sunday');
      setVisiblePicker(false);
      return;
    }

    if (errorOnDate) {
      setErrorOnDate('');
    }

    setDate(newDate);

    if (selectedStart) {
      setExpectedArrivalTime(newDate, selectedStart);

      if (onChange) {
        onChange(newDate, selectedStart);
      }
    }
  }

  return (
    <Content style={{ flex: 1 }}>
      <Section>
        <Label title="Preferred date" />
        <SelectInput
          placeholder="Pick a date"
          onPress={handleOnPressDate}
          value={(date && format(date, 'dd / MM / yyyy')) || ''}
        />
        {!!errorOnDate && <FormHelper mode="error">{errorOnDate}</FormHelper>}
      </Section>

      <Section>
        <Label title="Estimated arrival time" />

        <RNPickerSelect
          disabled={!!errorOnDate}
          placeholder={{
            label: 'Choose your preferred arrival time',
            value: 'default',
          }}
          items={
            isWithin24Hours(date) && isBefore6PM()
              ? schedules.filter(filterMoreThan24Hours)
              : schedules
          }
          value={selectedStart}
          onValueChange={async (value) => {
            setSelectedStart(value);
            setExpectedArrivalTime(date, value);

            if (onChange) {
              onChange(date, value);
            }
          }}
          Icon={() => <MaterialIcons name="keyboard-arrow-down" size={20} />}
          style={PickerStyles}
          useNativeAndroidPickerStyle={false}
        />
      </Section>

      <DateTimePickerModal
        isVisible={visiblePicker}
        mode={'date'}
        date={date || minDate()}
        onCancel={handleOnCloseDatetimePicker}
        onConfirm={handleOnConfirm}
        minimumDate={minDate()}
        maximumDate={addDays(new Date(), 30)}
      />
    </Content>
  );
}

const PickerStyles = StyleSheet.create({
  inputIOS: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    color: 'black',
    paddingRight: 30,
    backgroundColor: 'white',
  },
  iconContainer: {
    top: 5,
    right: 17,
    height: 50,
    justifyContent: 'center',
  },
});
