/**
 *
 * @description utils.js
 * @version 1.0.0
 * @since 20 July 2020
 *
 */

import EncryptedStorage from 'react-native-encrypted-storage';
import {
  parseISO,
  format,
  addHours,
  isBefore,
  addDays,
  startOfToday,
  startOfDay,
  differenceInHours,
} from 'date-fns';

type ScheduleItem = {
  start: number,
  end: number,
  value: string,
  label: string,
};
export async function setExpectedArrivalTime(date, time) {
  try {
    const expectedStart = parseISO(
      `${format(date || new Date(), 'yyyy-MM-dd')} ${time}`,
    );
    const expectedEnd = addHours(expectedStart, 1);

    await EncryptedStorage.setItem(
      '@expectedArrival',
      JSON.stringify({
        start: expectedStart,
        end: expectedEnd,
      }),
    );
  } catch (error) {
    console.log(error);
  }
}

export const schedules: Array<ScheduleItem> = [
  {
    start: 9,
    end: 10,
    value: '09:00:00',
    label: '09:00 AM - 10:00 AM',
  },
  {
    start: 10,
    end: 11,
    value: '10:00:00',
    label: '10:00 AM - 11:00 AM',
  },
  {
    start: 11,
    end: 12,
    value: '11:00:00',
    label: '11:00 AM - 12:00 PM',
  },
  {
    start: 12,
    end: 13,
    value: '12:00:00',
    label: '12:00 PM - 01:00 PM',
  },
  {
    start: 13,
    end: 14,
    value: '13:00:00',
    label: '01:00 PM - 02:00 PM',
  },
  {
    start: 14,
    end: 15,
    value: '14:00:00',
    label: '02:00 PM - 03:00 PM',
  },
  {
    start: 15,
    end: 16,
    value: '15:00:00',
    label: '03:00 PM - 04:00 PM',
  },
  {
    start: 16,
    end: 17,
    value: '16:00:00',
    label: '04:00 PM - 05:00 PM',
  },
  {
    start: 17,
    end: 18,
    value: '17:00:00',
    label: '05:00 PM - 06:00 PM',
  },
];

export const minDate = () => {
  if (isBefore(new Date(), addHours(startOfToday(), 18))) {
    return addDays(new Date(), 1);
  }

  return addDays(new Date(), 2);
};

export const filterMoreThan24Hours = (val: ScheduleItem) => {
  return isBefore(new Date(), addHours(startOfToday(), val.start));
};

export const isBefore6PM = () => {
  return isBefore(new Date(), addHours(startOfToday(), 18));
};

export const isWithin24Hours = (date) => {
  if (date) {
    return differenceInHours(date, new Date()) < 24;
  }

  return false;
};
