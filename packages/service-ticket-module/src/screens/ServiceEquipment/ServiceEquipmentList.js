/**
 *
 * @description ServiceEquipmentList.js
 * @version 1.0.0
 * @since 16 June 2020
 *
 */

import * as React from 'react';
import { FlatList, StyleSheet } from 'react-native';
import ServiceEquipment from './components/ServiceEquipment';
import Divider from '../../shared/Divider';

export default function ServiceEquipmentList() {
  function renderServiceEquipment({ item, index }) {
    const { location, image, serialNumber, certificate } = item;
    return (
      <>
        <ServiceEquipment
          location={location}
          source={image}
          serialNumber={serialNumber}
          certificate={certificate}
        />
        <Divider />
      </>
    );
  }

  return (
    <FlatList
      data={[]}
      renderItem={renderServiceEquipment}
      keyExtractor={(item, index) => `service-equipment-${index}`}
      style={styles.list}
    />
  );
}

const styles = StyleSheet.create({
  list: {
    paddingHorizontal: 15,
  },
});
