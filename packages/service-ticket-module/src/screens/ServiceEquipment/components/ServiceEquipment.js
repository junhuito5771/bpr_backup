/**
 *
 * @description ServiceEquipment.js
 * @version 1.0.0
 * @since 16 June 2020
 *
 */

import * as React from 'react';
import { View, Image, ImageSourcePropType, StyleSheet, Text } from 'react-native';

import CheckBox from "../../../shared/CheckBox";

import { theme } from '../../../helpers/themes';

type Props = {
  source: ImageSourcePropType,
  location: string,
  serialNumber: number,
  certificate?: string,
};
export default function ServiceEquipment(props: Props) {
  const { source, location, serialNumber, certificate } = props;

  const [checked, setChecked] = React.useState(false);

  return (
    <View style={[styles.row, styles.container]}>
      <View style={styles.left}>
        <CheckBox value={checked} onValueChange={value => setChecked(value)} />
      </View>
      <View style={[styles.row, styles.right]}>
        <Image source={source} style={styles.image} />
        <View>
          <Text style={styles.location}>{location}</Text>
          <Text>{serialNumber}</Text>
          {!!certificate && <Text>Certificate Number: {certificate}</Text>}
          {!!certificate && <Text style={styles.registered}>Register</Text>}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
  row: {
    flexDirection: 'row',
  },
  left: {
    flex: 0.1,
    alignItems: 'center',
  },
  right: {
    flex: 0.9,
  },
  image: {
    width: 40,
    height: 30,
    marginVertical: 5,
    marginHorizontal: 15,
  },
  registered: {
    color: theme.success,
  },
  location: {
    fontWeight: '700',
  },
});
