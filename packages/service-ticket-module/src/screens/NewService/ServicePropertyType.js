import * as React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { ServiceLocationContext } from '../../provider/ServiceLocationProvider';
import { theme } from '../../helpers/themes';
import { storage } from '../../helpers/utils';
import Button from '../../shared/Button';

type Props = {
  onPress: () => void,
};
export const ServicePropertyTypeNextButton = ({ onPress }: Props) => {
  const context = React.useContext(ServiceLocationContext);

  return (
    <Button onPress={onPress} disabled={!context.info.propertyType}>
      Next
    </Button>
  );
};

export default function ServicePropertyType() {
  const context = React.useContext(ServiceLocationContext);

  React.useEffect(() => {
    async function initAddress() {
      const propertyType = await storage.load('@propertyType');

      if (propertyType) {
        context.onChangeText('propertyType', propertyType);
      }
    }

    initAddress();
  }, []);

  return (
    <ScrollView>
      <View style={styles.descriptionWrapper}>
        <Text style={styles.description}>
          For the high rise buildings, we may be unable to fulfill your job
          request on weekends due to entry restrictions.
        </Text>
        <Text style={styles.description}>
          Please gain permissions from your residential management before
          booking.
        </Text>
      </View>

      <View style={styles.propertyTypeContainer}>
        <TouchableOpacity
          style={[
            styles.propertyTypeWrapper,
            {
              elevation: 5,
              shadowColor:
                context.info.propertyType === 'LANDED' ? theme.primary : '#777',
              shadowOpacity: context.info.propertyType === 'LANDED' ? 1 : 0.2,
            },
            Platform.OS === 'android' ? { borderColor: context.info.propertyType === 'LANDED' ? theme.primary : 'transparent', borderWidth: 1 } : {}
          ]}
          onPress={() => {
            context.onChangeText('propertyType', 'LANDED');
            storage.save('@propertyType', 'LANDED');
          }}
        >
          <Image
            source={require('../../shared/assets/ico-landed-house.png')}
            style={[styles.icon, { tintColor: theme.primary }]}
            resizeMode="contain"
          />
          <Text style={[styles.propertyType, { color: theme.primary }]}>
            Landed house
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.propertyTypeWrapper,
            {
              elevation: 5,
              shadowColor:
                context.info.propertyType === 'CONDO' ? theme.primary : '#777',
              shadowOpacity: context.info.propertyType === 'CONDO' ? 1 : 0.2,
            },
            Platform.OS === 'android' ? { borderColor: context.info.propertyType === 'CONDO' ? theme.primary : 'transparent', borderWidth: 1 } : {}
          ]}
          onPress={() => {
            context.onChangeText('propertyType', 'CONDO');
            storage.save('@propertyType', 'CONDO');
          }}
        >
          <Image
            source={require('../../shared/assets/ico-condo.png')}
            style={[styles.icon, { tintColor: theme.primary }]}
            resizeMode="contain"
          />
          <Text style={[styles.propertyType, { color: theme.primary }]}>
            Condo Apartment
          </Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
          style={[
            styles.propertyTypeWrapper,
            {
              shadowColor:
                context.info.propertyType === 'OFFICE' ? theme.primary : '#777',
              shadowOpacity: context.info.propertyType === 'OFFICE' ? 1 : 0.2,
            },
          ]}
          onPress={() => {
            context.onChangeText('propertyType', 'OFFICE');
            storage.save('@propertyType', 'OFFICE');
          }}
        >
          <Image
            source={require('../../shared/assets/ico-office.png')}
            style={[styles.icon, { tintColor: theme.primary }]}
            resizeMode="contain"
          />
          <Text style={[styles.propertyType, { color: theme.primary }]}>
            Office
          </Text>
        </TouchableOpacity> */}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  descriptionWrapper: {
    marginVertical: 16,
    paddingHorizontal: 16,
  },
  description: {
    fontWeight: '600',
    fontSize: 16,
    marginBottom: 8,
  },
  propertyTypeContainer: {
    alignItems: 'center',
  },
  propertyTypeWrapper: {
    height: 180,
    width: 230,
    marginBottom: 24,
    shadowColor: '#777',
    shadowRadius: 3,
    shadowOpacity: 0.2,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    elevation: 3,
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  propertyType: {
    fontSize: 21,
    fontWeight: '700',
    textAlign: 'center',
  },
  icon: {
    height: 100,
    width: 100,
  },
});
