import * as React from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import EncryptedStorage from 'react-native-encrypted-storage';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useFocusEffect } from '@react-navigation/native';

import ServiceItem from './components/ServiceItem';
import Divider from '../../shared/Divider';

import { api } from '../../service/api';
import DescriptionModal from './components/DescriptionModal';
import { config } from '../../helpers/config';
import Loading from '../../shared/Loading';
import { groupingServicePackages } from '../../helpers/service-packages';
import Accordion from '../../shared/Accordion';

let selectedPackages = null;
export default function ServiceList({ onChange }) {
  const [servicePackages, setServicePackages] = React.useState([]);
  const [selected, setSelected] = React.useState({});
  const [loading, setLoading] = React.useState(false);

  const [description, setDescription] = React.useState('');

  React.useEffect(() => {
    async function fetchServicePackageGroup() {
      setLoading(true);
      const res = await api
        .get(
          `service-packages?group=${
            String(config.company).toLowerCase() === 'acson' ? 'AMSS' : 'DMSS'
          }`,
        )
        .catch((err) => {
          console.log(err);
          setLoading(false);
        });
      if (res) {
        const packages = groupingServicePackages(res);
        setServicePackages(packages);
      }
      setLoading(false);
    }

    fetchServicePackageGroup();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      async function fetchSavedPackages() {
        selectedPackages = await EncryptedStorage.getItem('@packages');

        if (selectedPackages && selectedPackages.match(/^\{/)) {
          const selectedJSON = JSON.parse(selectedPackages);
          setSelected(selectedJSON);
          if (onChange) {
            onChange(selectedJSON);
          }
        } else {
          setSelected([]);
          if (onChange) {
            onChange([]);
          }
        }

        return () => {
          selectedPackages = null;
        };
      }
      fetchSavedPackages();
    }, [])
  )

  function renderServiceItem({ item: group, index }) {
    return (
      <Accordion title={group.name} shouldExpand={index === 0} onExpand={ async (isExpanded) => {
        if (!isExpanded) {
          selectedPackages = await EncryptedStorage.getItem('@packages');
          if (selectedPackages && selectedPackages.match(/^\{/)) {
            const selectedJSON = JSON.parse(selectedPackages);
            setSelected(selectedJSON);
            if (onChange) {
              onChange(selectedJSON);
            }
          }
        }
      }}>
        {group.nodes?.map((item) => {
          return (
            <React.Fragment key={item.id}>
              <ServiceItem
                onPressInfo={() => {
                  setDescription(item.description);
                }}
                name={item.name}
                price={`RM ${item.consumerQuotations?.[0].unitPrice}`}
                initQty={selected?.[item.id]?.quantity || 0}
                onChangeQuantity={async (qty) => {
                  const {
                    id,
                    name,
                    quantity,
                    consumerQuotations,
                    servicePackageGroupCode,
                  } = item;

                  const selectedJSON =
                    (typeof selectedPackages === 'string' &&
                      selectedPackages.match(/^\{/) &&
                      JSON.parse(selectedPackages)) ||
                    selectedPackages;

                  selectedPackages = {
                    ...selectedJSON,
                    [id]: {
                      servicePackageGroupCode,
                      consumerQuotations,
                      unitPrice: consumerQuotations[0].unitPrice,
                      id,
                      name,
                      quantity: qty,
                    },
                  };

                  await EncryptedStorage.setItem(
                    '@packages',
                    JSON.stringify(selectedPackages),
                  );

                  if (onChange) {
                    onChange(selectedPackages);
                  }
                }}
              />
              <Divider />
            </React.Fragment>
          );
        })}
      </Accordion>
    );
  }

  async function fetchServicePackage() {
    setLoading(true);
    const res = await api
      .get(
        `service-packages?group=${
          String(config.company).toLowerCase() === 'acson' ? 'AMSS' : 'DMSS'
        }`,
      )
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });

    if (res) {
      setServicePackages(res);
    }

    setLoading(false);
  }

  return (
    <>
      {!loading && servicePackages.length > 0 && (
        <FlatList
          scrollEnabled={false}
          data={servicePackages}
          renderItem={renderServiceItem}
          keyExtractor={(item, index) => `service-item-${index}`}
          style={styles.list}
        />
      )}
      {!loading && servicePackages.length === 0 && (
        <View
          style={{
            justifyContent: 'flex-end',
            alignItems: 'center',
            minHeight: '50%',
          }}
        >
          <TouchableOpacity onPress={fetchServicePackage}>
            <MaterialIcons name="refresh" size={50} color="#aaa" />
          </TouchableOpacity>
        </View>
      )}
      <DescriptionModal
        open={!!description}
        description={description}
        onClose={() => setDescription('')}
      />
      <Loading visible={loading} />
    </>
  );
}

const styles = StyleSheet.create({
  list: {
    marginTop: 10,
  },
  notFound: {
    textAlign: 'center',
  },
});
