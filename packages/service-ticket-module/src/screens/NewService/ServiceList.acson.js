/**
 *
 * @description ServiceList.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import { FlatList, StyleSheet } from 'react-native';
import EncryptedStorage from 'react-native-encrypted-storage';
import { useFocusEffect } from '@react-navigation/native';

import ServiceItem from './components/ServiceItem';
import Divider from '../../shared/Divider';

import DescriptionModal from './components/DescriptionModal';

let selectedPackages = null;
export default function ServiceList({ onChange, servicePackages, loading }) {
  const [selected, setSelected] = React.useState({});
  const [ready, setReady] = React.useState(false);
  const [description, setDescription] = React.useState('');

  useFocusEffect(
    React.useCallback(() => {
      async function fetchSavedPackages() {
        selectedPackages = await EncryptedStorage.getItem('@packages');

        if (selectedPackages && selectedPackages.match(/^\{/)) {
          const selectedJSON = JSON.parse(selectedPackages);
          setSelected(selectedJSON);
          setReady(true);

          if (onChange) {
            onChange(selectedJSON);
          }
        } else {
          setSelected([]);
          setReady(true);

          if (onChange) {
            onChange([]);
          }
        }

        return () => {
          selectedPackages = null;
        };
      }
      fetchSavedPackages();
    }, []),
  );

  function renderServiceItem({ item, index }) {
    return (
      <>
        <ServiceItem
          onPressInfo={() => {
            setDescription(item.description);
          }}
          name={item.name}
          price={`RM ${item.consumerQuotations?.[0].unitPrice}`}
          initQty={selected?.[item.id]?.quantity || 0}
          onChangeQuantity={async (qty) => {
            const {
              id,
              name,
              consumerQuotations,
              servicePackageGroupCode,
            } = item;

            const selectedJSON =
              (typeof selectedPackages === 'string' &&
                selectedPackages.match(/^\{/) &&
                JSON.parse(selectedPackages)) ||
              selectedPackages;

            selectedPackages = {
              ...selectedJSON,
              [id]: {
                servicePackageGroupCode,
                consumerQuotations,
                unitPrice: consumerQuotations[0].unitPrice,
                id,
                name,
                quantity: qty,
              },
            };

            await EncryptedStorage.setItem(
              '@packages',
              JSON.stringify(selectedPackages),
            );

            if (onChange) {
              onChange(selectedPackages);
            }
          }}
        />
        <Divider />
      </>
    );
  }

  return (
    <>
      {!loading && servicePackages.length > 0 && ready && (
        <FlatList
          scrollEnabled={false}
          data={servicePackages}
          renderItem={renderServiceItem}
          keyExtractor={(item, index) => `service-item-${index}`}
          style={styles.list}
        />
      )}
      <DescriptionModal
        open={!!description}
        description={description}
        onClose={() => setDescription('')}
      />
    </>
  );
}

const styles = StyleSheet.create({
  list: {
    marginTop: 10,
    paddingHorizontal: 15,
  },
  notFound: {
    textAlign: 'center',
  },
});
