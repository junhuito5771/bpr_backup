/**
 *
 * @description DescriptionModal.js
 * @author yikkok <yikok.yong@gmail.com>
 * @version 1.0.0
 * @since 25 August 2020
 *
 */

import * as React from 'react';
import { Modal, Text, View, ScrollView, StyleSheet } from 'react-native';
import Container from '../../../shared/Container';
import Button from '../../../shared/Button';
import Title from '../../../shared/Title';

type Props = {
  open: boolean,
  description: string,
  onClose: () => void,
};
export default function DescriptionModal({
  open,
  description,
  onClose,
}: Props) {
  return (
    <Modal
      visible={open}
      animationType="fade"
      transparent
      onRequestClose={onClose}
    >
      <View style={styles.overlay}>
        <View style={styles.content}>
          <Title>Description</Title>
          <ScrollView style={styles.scrollView}>
            <Text>{description}</Text>
          </ScrollView>
          <Button onPress={onClose}>Close</Button>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: '80%',
    height: '50%',
    backgroundColor: 'white',
    padding: 16,
  },
  scrollView: {
    marginVertical: 16,
  },
});
