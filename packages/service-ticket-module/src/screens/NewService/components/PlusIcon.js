/**
 *
 * @description PlusIcon.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  TouchableOpacityProps,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default function PlusIcon(props: TouchableOpacityProps) {
  const { style, ...rest } = props;
  return (
    <TouchableOpacity style={[styles.container, style]} activeOpacity={0.5} {...rest}>
      <MaterialIcons name="add" size={14} color="#777" />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 21,
    width: 21,
    borderWidth: 1,
    borderColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
