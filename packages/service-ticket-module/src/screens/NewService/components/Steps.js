/**
 *
 * @description Steps.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import { Text } from 'react-native';

type Props = {
  current: number,
  total: number,
};
export default function Steps(props: Props) {
  const { current, total } = props;
  return <Text>{`${current}/${total}`}</Text>;
}
