/**
 *
 * @description ServiceItem.js
 * @version 1.0.0
 * @since 14 June 2020
 *
 */

import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import MinusIcon from './MinusIcon';
import PlusIcon from './PlusIcon';
import { theme } from '../../../helpers/themes';

type Props = {
  name: string,
  price: string,
  initQty: number,
  onChangeQuantity: qty => void,
  onPressInfo: () => void,
};
export default function ServiceItem(props: Props) {
  const { name, price, initQty, onChangeQuantity, onPressInfo } = props;
  const [quantity, setQuantity] = React.useState(initQty);

  function handleOnAddQuantity() {
    setQuantity(prev => {
      const qty = prev + 1;
      if (onChangeQuantity) {
        onChangeQuantity(qty);
      }

      return qty;
    });
  }

  function handleOnReduceQuantity() {
    setQuantity(prev => {
      let current = prev;

      if (current > 0) {
        current = current - 1;
      }

      if (onChangeQuantity) {
        onChangeQuantity(current);
      }

      return current;
    });
  }

  return (
    <View style={styles.row}>
      <TouchableOpacity
        style={[styles.infoRow, styles.left]}
        onPress={onPressInfo}
      >
        <MaterialIcons name="info-outline" size={18} style={styles.icon} />
        <View>
          <Text>{name}</Text>
          <Text style={styles.placeholder}>{price}</Text>
        </View>
      </TouchableOpacity>
      <View style={[styles.right, styles.row]}>
        <MinusIcon onPress={handleOnReduceQuantity} />
        <Text style={styles.quantity}>{quantity}</Text>
        <PlusIcon onPress={handleOnAddQuantity} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 16,
    justifyContent: 'space-between',
  },
  left: {
    // flex: 0.6,
    width: '70%',
  },
  right: {
    // flex: 0.2,
    width: '20%',
    justifyContent: 'space-around',
  },
  placeholder: {
    color: theme.placeholder,
    paddingTop: 8,
  },
  infoRow: {
    flexDirection: 'row',
  },
  icon: {
    paddingRight: 4,
  },
});
