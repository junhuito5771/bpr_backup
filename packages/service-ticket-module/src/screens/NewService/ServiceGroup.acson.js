import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import EncryptedStorage from 'react-native-encrypted-storage';
import { useFocusEffect } from '@react-navigation/native';

type Props = {
  groups: Array<{
    name: string,
    code: string,
    iconUrl: string,
    nodes: Array<Object>,
  }>,
  onPress: (groupName: string, node: Object) => void,
};
export default function ServiceGroup({ groups, onPress, onChange }: Props) {
  useFocusEffect(
    React.useCallback(() => {
      async function fetchSavedPackages() {
        selectedPackages = await EncryptedStorage.getItem('@packages');

        if (selectedPackages && selectedPackages.match(/^\{/)) {
          const selectedJSON = JSON.parse(selectedPackages);

          if (onChange) {
            onChange(selectedJSON);
          }
        } else {
          if (onChange) {
            onChange([]);
          }
        }
      }
      fetchSavedPackages();
    }, []),
  );

  return (
    <View>
      {groups.map((group) => (
        <TouchableOpacity
          onPress={() => onPress(group.name, group.nodes)}
          key={group.code}
          style={styles.groupTitleWrapper}
        >
          <Image
            source={
              (group.iconUrl && { uri: group.iconUrl }) ||
              require('../../shared/assets/ico-service.png')
            }
            resizeMode="contain"
            style={styles.groupImage}
          />
          <Text style={styles.groupTitle}>{group.name}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  groupTitleWrapper: {
    alignItems: 'center',
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(175, 175, 175, 0.4)',
    paddingVertical: 16,
  },
  groupTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  groupImage: {
    height: 100,
    width: 100,
    marginBottom: 16,
  },
});
