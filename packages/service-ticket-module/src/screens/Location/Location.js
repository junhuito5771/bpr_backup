/**
 *
 * @description Location.js
 * @version 1.0.0
 * @since 12 July 2020
 *
 */

import * as React from 'react';
import {
  FlatList,
  TouchableOpacity,
  View,
  StyleSheet,
  ActivityIndicator,
  Modal,
  SafeAreaView,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import axios from 'axios';
import EncryptedStorage from 'react-native-encrypted-storage';
import Config from 'react-native-config';
import { NavigationService } from '@module/utility';
import {
  HeaderBackButton as DaikinBackButton,
  HeaderText as DaikinHeaderText,
  HeaderButton,
} from '@module/daikin-ui';
import { BackButton as AcsonBackButton } from '@module/acson-ui';
import { useFocusEffect } from '@react-navigation/native';

import Container from '../../shared/Container';
import SearchBox from '../../shared/SearchBox';
import Label from '../../shared/Label';
import PlaceholderText from '../../shared/PlaceholderText';
import Divider from '../../shared/Divider';
import { theme, width } from '../../helpers/themes';

import Footer from '../../shared/Footer';
import Button from '../../shared/Button';
import { api } from '../../service/api';
import FormHelper from '../../shared/FormHelper';
import Loading from '../../shared/Loading';
import HelpWebScreen from '../Help/Help';
import { config } from '../../helpers/config';
import { resetPopulatedValues } from '../../helpers/utils';

let searchTimeout = null;

export default function Location(props) {
  const [search, setSearch] = React.useState('');
  const [places, setPlaces] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [hasCoverage, setHasCoverage] = React.useState(true);
  const [requestLoading, setRequestLoading] = React.useState(false);
  const [lastLocation, setLastLocation] = React.useState({
    '@street1': '',
    '@state': '',
    '@city': '',
    '@postcode': '',
    '@latlng': null,
  });
  const [openHelpWeb, setOpenHelpWeb] = React.useState(false);

  const {
    params: { resetPackages },
  } = props.route;

  useFocusEffect(
    React.useCallback(() => {
      if (String(config.company).toLowerCase() === 'daikin') {
        NavigationService.setOptions(props.navigation, {
          title: <DaikinHeaderText>Location</DaikinHeaderText>,
          headerLeft: () => (
            <DaikinBackButton
              onPress={() => {
                props.navigation.setParams({ resetPackages: true });
                props.navigation.goBack();
              }}
            />
          ),
          headerRight: () => (
            <HeaderButton onPress={() => setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      } else {
        props.navigation.setOptions({
          headerTitle: 'Location',
          headerLeft: () => <AcsonBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      }

      if (resetPackages) {
        props.navigation.setParams({ resetPackages: false });
        (async () => await resetPopulatedValues())();
      }
    }, [props.navigation, resetPackages]),
  );

  React.useEffect(() => {
    async function fetchLocationLocalstorage() {
      const keys = ['@street1', '@city', '@state', '@postcode', '@latlng'];

      if (keys?.length > 0) {
        const saved = {};

        for (let index = 0; index < keys.length; index++) {
          const key = keys[index];
          const result = await EncryptedStorage.getItem(key);
          if (result !== null && result !== undefined) {
            const value = (result.match(/^{/) && JSON.parse(result)) || result;
            saved[key] = value;
          }
        }
        setLastLocation(saved);
      }
    }

    fetchLocationLocalstorage();
  }, []);

  function handleOnSelected(index) {
    return async () => {
      await EncryptedStorage.setItem('@street1', places[index].description);

      setRequestLoading(true);
      const res = await axios({
        method: 'GET',
        url: `https://maps.googleapis.com/maps/api/place/details/json?place_id=${places[index].place_id}&fields=geometry,formatted_address,address_component&key=${Config.GOOGLE_MAP_KEY}`,
      }).catch((err) => {
        setRequestLoading(false);
        console.log(err);
      });

      if (res?.data?.result?.geometry?.location) {
        const { lat, lng } = res.data.result.geometry.location;
        const coverageRes = await api.get(
          `service-areas/coverageValidity?latitude=${lat}&longitude=${lng}`,
        );

        if (coverageRes) {
          setHasCoverage(coverageRes.validity);

          if (!coverageRes.validity) {
            // Area not within coverage, not proceed further
            setRequestLoading(false);
            return;
          }
        }
      }

      setRequestLoading(false);

      if (res?.data?.result?.address_components?.length > 0) {
        const {
          data: {
            result: { address_components },
          },
        } = res;
        for (let index = 0; index < address_components.length; index++) {
          const component = address_components[index];
          if (component?.types?.includes('locality')) {
            await EncryptedStorage.setItem('@city', component.long_name);
          }

          if (component?.types?.includes('administrative_area_level_1')) {
            await EncryptedStorage.setItem('@state', component.long_name);
          }

          if (component?.types?.includes('postal_code')) {
            await EncryptedStorage.setItem('@postcode', component.long_name);
          }
        }
      }

      if (res?.data?.result?.geometry?.location) {
        EncryptedStorage.setItem(
          '@latlng',
          JSON.stringify(res.data.result.geometry.location),
        );
      }

      props.navigation.navigate('NewService');
    };
  }

  function renderLocation({ item, index }) {
    return (
      <>
        <TouchableOpacity
          activeOpacity={0.7}
          style={[styles.row, styles.locationRow]}
          onPress={handleOnSelected(index)}
        >
          <MaterialIcons
            name="place"
            size={24}
            style={styles.locationIcon}
            color={theme.placeholder}
          />
          <View style={styles.listItemLocation}>
            <PlaceholderText
              style={styles.subtitle}
              numberOfLines={2}
              ellipsizeMode="tail"
            >
              {item.description}
            </PlaceholderText>
          </View>
        </TouchableOpacity>
        <Divider />
      </>
    );
  }

  function handleOnChangeText(text) {
    setSearch(text);

    if (searchTimeout) {
      clearTimeout(searchTimeout);
    }

    if (!hasCoverage) {
      setHasCoverage(true);
    }

    setLoading(true);
    searchTimeout = setTimeout(async () => {
      const res = await axios({
        method: 'GET',
        url: `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${text}&components=country:my&key=${Config.GOOGLE_MAP_KEY}`,
      }).catch((err) => {
        console.log(err);
        setLoading(false);
      });

      if (res.status === 200 && res.data?.predictions?.length > 0) {
        setPlaces(res.data.predictions);
      }

      setLoading(false);
    }, 2000);
  }

  return (
    <Container>
      <SearchBox
        value={search}
        onChangeText={handleOnChangeText}
        placeholder="Search location"
        clearButtonMode="while-editing"
      />

      {!!lastLocation?.['@latlng'] && (
        <View style={styles.list}>
          <PlaceholderText>Last used location</PlaceholderText>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.row}
            onPress={() => props.navigation.navigate('NewService')}
          >
            <MaterialIcons
              name="bookmark"
              size={24}
              style={styles.locationIcon}
              color={theme.placeholder}
            />
            <View style={styles.listItemLocation}>
              <PlaceholderText
                style={styles.subtitle}
                numberOfLines={2}
                ellipsizeMode="tail"
              >
                {lastLocation?.['@street1'] || ''}
              </PlaceholderText>
            </View>
          </TouchableOpacity>
          <Divider />
        </View>
      )}

      {loading && (
        <View style={styles.loadingContainer}>
          <ActivityIndicator />
        </View>
      )}

      {!loading && places.length > 0 && (
        <>
          <View style={[styles.list, { marginTop: 8 }]}>
            <PlaceholderText>Search results</PlaceholderText>
          </View>
          <FlatList
            data={places}
            renderItem={renderLocation}
            keyExtractor={(item, index) => `location-${index}`}
            contentContainerStyle={styles.list}
            keyboardShouldPersistTaps="handled"
            ListHeaderComponent={
              !hasCoverage && (
                <FormHelper mode="error">
                  Sorry, service is not available here.
                </FormHelper>
              )
            }
          />
        </>
      )}

      <Loading visible={requestLoading} />

      <HelpWebScreen
        visible={openHelpWeb}
        onClose={() => setOpenHelpWeb(false)}
      />
    </Container>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  locationRow: {
    paddingVertical: 8,
  },
  subtitle: {
    fontSize: 12,
    paddingVertical: 0,
  },
  list: {
    marginHorizontal: 16,
  },
  listItemLocation: {
    flex: 1,
  },
  locationIcon: {
    margin: 10,
  },
  checkedIcon: {
    margin: 10,
  },
  safeAreaView: {
    flex: 1,
  },
  loadingContainer: {
    flex: 0.5,
    justifyContent: 'center',
  },
  lastUsedContainer: {
    margin: 16,
  },
  lastUsedLocation: {
    fontSize: 12,
    marginBottom: 8,
  },
});
