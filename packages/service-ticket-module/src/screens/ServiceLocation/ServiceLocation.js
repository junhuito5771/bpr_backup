/**
 *
 * @description ServiceLocation.js
 * @version 1.0.0
 * @since 24 June 2020
 *
 */

import * as React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import RNPickerSelect from 'react-native-picker-select';

import Content from '../../shared/Content';
import Label from '../../shared/Label';
import SelectInput from '../../shared/SelectInput';
import Input from '../../shared/Input';
import ModalSelectList from '../../shared/ModalSelectList';
import Button from '../../shared/Button';
import ModalSearchLocation from '../../shared/ModalSearchLocation';

import { useModal } from '../../helpers/hooks';
import { storage } from '../../helpers/utils';
import { theme } from '../../helpers/themes';

import states from '../../data/state';
import towns from '../../data/town';
import PlaceholderText from '../../shared/PlaceholderText';
import Section from '../../shared/Section';

import { ServiceLocationContext } from '../../provider/ServiceLocationProvider';

const TYPE_OF_PROPERTIES = [
  { value: 'LANDED', label: 'Landed' },
  { value: 'CONDO', label: 'Condominium' },
  // { value: 'Office Premises', label: 'Office Premises' },
];

export default function ServiceLocation() {
  const locationModal = useModal();

  const context = React.useContext(ServiceLocationContext);

  React.useEffect(() => {
    async function initAddress() {
      const street1 = await storage.load('@street1');
      const street2 = await storage.load('@street2');
      const _building = await storage.load('@building');
      const _postcode = await storage.load('@postcode');
      const _city = await storage.load('@city');
      const _state = await storage.load('@state');

      if (street1 || street2) {
        context.onChangeText('street1', street1 || street2);
      }

      if (_postcode) {
        context.onChangeText('postcode', _postcode);
      }

      if (_city) {
        context.onChangeText('city', _city);
      }

      if (_state) {
        context.onChangeText('state', _state);
      }

      if (_building) {
        context.onChangeText('building', _building);
      }
    }

    initAddress();
  }, []);

  return (
    <View style={{ paddingHorizontal: 16 }}>
      <Section>
        <Label title="Type of Property" />
        <Text>{context.info.propertyType}</Text>
      </Section>

      <Section>
        <View style={[styles.row, styles.addressRow]}>
          <Label title="Address" />
          <Text onPress={locationModal.open} style={styles.changeAddress}>
            Change Address
          </Text>
        </View>
        <PlaceholderText>{context.info.street1 || 'None'}</PlaceholderText>
      </Section>

      <Section>
        <Label title="Address Details" />
        <Input
          placeholder="Eg: block, unit number"
          value={context.info.building}
          onChangeText={(text) => {
            context.onChangeText('building', text);
            storage.save('@building', text);
          }}
        />
      </Section>

      <Section>
        <Label title="State" />
        <RNPickerSelect
          placeholder={{
            label: 'Select State',
          }}
          items={states}
          value={context.info.state}
          onValueChange={async (value) => {
            context.onChangeText('state', value);
            storage.save('@state', value || '');
          }}
          Icon={() => <MaterialIcons name="keyboard-arrow-down" size={20} />}
          style={PickerStyles}
          useNativeAndroidPickerStyle={false}
        />
      </Section>

      <Section>
        <Label title="City" />
        <Input
          placeholder="Enter a city/town"
          value={context.info.city}
          onChangeText={(text) => {
            context.onChangeText('city', text);
            storage.save('@city', text);
          }}
        />
      </Section>

      <Section>
        <Label title="Postcode" />
        <Input
          placeholder="Postcode, eg, 55200"
          value={context.info.postcode}
          onChangeText={(text) => {
            context.onChangeText('postcode', text);
            storage.save('@postcode', text);
          }}
        />
      </Section>

      {locationModal.visible && (
        <ModalSearchLocation
          onSelected={async (addr) => {
            context.onChangeText('street1', addr?.description || '');
            locationModal.close();
            storage.save('@street1', addr?.description);
            context.onChangeText('city', await storage.load('@city'));
            context.onChangeText('postcode', await storage.load('@postcode'));
            context.onChangeText('state', await storage.load('@state'));
          }}
          visible={locationModal.visible}
          onDone={() => {
            locationModal.close();
          }}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchIconContainer: {
    width: 50,
    height: 50,
    marginLeft: 10,
    borderColor: theme.placeholder,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addressRow: {
    justifyContent: 'space-between',
  },
  changeAddress: {
    textDecorationColor: theme.primary,
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    color: theme.primary,
  },
});

const PickerStyles = StyleSheet.create({
  inputIOS: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    color: 'black',
  },
  inputAndroid: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    color: 'black',
  },
  iconContainer: {
    top: 5,
    right: 17,
    height: 50,
    justifyContent: 'center',
  },
});
