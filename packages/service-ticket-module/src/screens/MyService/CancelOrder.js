import React, {
  Fragment,
  PureComponent,
  useCallback,
  useEffect,
  useState,
} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  Alert,
  TextInput,
  ScrollView,
  Platform,
} from 'react-native';
import { NavigationService } from '@module/utility';
import { HeaderBackButton, HeaderText } from '@module/daikin-ui';
import { useFocusEffect } from '@react-navigation/native';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { theme, width } from '../../helpers/themes';
import Button from '../../shared/Button';
import { api } from '../../service/api';
import Divider from '../../shared/Divider';
import Loading from '../../shared/Loading';
import { config } from '../../helpers/config';

export function CancelOrderFC({ navigation, route }) {
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  useFocusEffect(
    useCallback(() => {
      if (String(config.company).toLowerCase() === 'daikin') {
        NavigationService.setOptions(navigation, {
          title: <HeaderText>Cancel Order</HeaderText>,
          headerLeft: () => <HeaderBackButton navigation={navigation} />,
        });
      } else {
        navigation.setOptions({
          title: <HeaderText>Cancel Order</HeaderText>,
          headerLeft: () => <HeaderBackButton navigation={navigation} />,
        });
      }
    }, [navigation]),
  );

  useEffect(() => {
    async function fetchCancelOptions() {
      setLoading(true);
      const res = await api
        .get('feedbacks/consumer-cancellations/options')
        .catch((err) => {
          setLoading(false);
          console.log(err);
        });

      if (res?.length > 0) {
        setOptions(res);
      }

      setLoading(false);
    }

    fetchCancelOptions();
  }, []);

  return (
    <CancelOrder
      navigation={navigation}
      route={route}
      options={options}
      loading={loading}
    />
  );
}
class CancelOrder extends PureComponent {
  state = {
    selectedReason: {
      id: '',
      comment: '',
    },
    requestLoading: false,
    extraComment: '',
  };

  toggleCheckBox = (index, value) => {
    const { options } = this.props;
    const { selectedReason } = this.state;

    if (selectedReason.id === options[index].id) {
      this.setState({
        selectedReason: {
          id: '',
          comment: '',
        },
      });
      return;
    }

    this.setState({
      selectedReason: {
        id: options[index].id,
        comment: options[index].title,
      },
    });
  };

  _renderItem = ({ item, index }) => {
    const { selectedReason } = this.state;
    return (
      <Fragment key={`option-${item.id}`}>
        <TouchableOpacity
          onPress={() => this.toggleCheckBox(index, item?.isChecked)}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: '5%',
          }}
        >
          <Text style={styles.itemText}>{item.title}</Text>
          <MaterialIcons
            name={
              item.id === selectedReason.id
                ? 'check-box'
                : 'check-box-outline-blank'
            }
            size={25}
          />
        </TouchableOpacity>
        {this.props.options && index !== this.props.options.length - 1 ? (
          <Divider />
        ) : null}
      </Fragment>
    );
  };

  submitCancelOrder = () => {
    const { selectedReason, extraComment } = this.state;
    const {
      route: { params: serviceRequestID },
    } = this.props;

    this.setState({ requestLoading: true }, async () => {
      const res = await api
        .post(
          'feedbacks/consumer-cancellation/responses',
          JSON.stringify({
            optionId: selectedReason.id,
            extraComment: extraComment,
            rating: 0,
            serviceTicketId: serviceRequestID,
          }),
          {
            'Content-Type': 'application/json',
          },
        )
        .catch((err) => {
          console.log(err);
          this.setState({ requestLoading: false });
        });

      if (res) {
        Alert.alert(
          'Success',
          'Your feedback was successfully submitted to us. Thank you.',
          [
            {
              text: 'Close',
              onPress: () => {
                this.props.navigation.navigate('MyServiceList');
                this.setState({ requestLoading: false });
              },
            },
          ],
        );
      }
    });
  };

  render() {
    const { selectedReason, requestLoading } = this.state;
    const { options, loading } = this.props;

    return (
      <SafeAreaView style={{ flex: 1, marginHorizontal: 16 }}>
        <Text style={styles.itemTitle}>Service request cancelled</Text>
        <Text style={styles.itemDesc}>
          We would like to hear more from you so we could improve, please submit
          your feedback to us.
        </Text>

        <ScrollView>
          {options.map((option, index) => {
            return this._renderItem({ item: option, index: index });
          })}

          <View style={[styles.comments]}>
            <TextInput
              maxLength={500}
              onChangeText={(extraComment) => this.setState({ extraComment })}
              value={this.state.extraComment}
              defaultValue={this.state.extraComment}
              placeholder={'Tell us more (optional)'}
              underlineColorAndroid="transparent"
              multiline
              style={{ height: 60 }}
            />
          </View>
        </ScrollView>

        <View style={styles.emptyRequestContainer}>
          <Button
            onPress={this.submitCancelOrder}
            disabled={!selectedReason.id}
          >
            Submit
          </Button>
        </View>

        <Loading visible={loading || requestLoading} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  itemDesc: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.black,
    paddingBottom: 28,
  },
  titleContainer: {
    flexDirection: 'row',
  },
  itemTitle: {
    fontSize: 22,
    lineHeight: 26,
    color: theme.black,
    paddingVertical: 8,
    fontWeight: 'bold',
  },
  emptyRequestContainer: {
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 16,
  },
  itemText: {
    flex: 1,
    fontSize: 14,
    lineHeight: 17,
    paddingVertical: 28,
  },
  list: {
    flex: 1,
  },
  comments: {
    marginTop: 32,
    paddingHorizontal: 16,
    paddingVertical: Platform.OS === 'ios' ? 8 : 0,
    height: 80,
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
  },
});

export default CancelOrder;
