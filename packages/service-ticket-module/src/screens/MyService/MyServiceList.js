import React, { PureComponent, useCallback, useEffect, useState } from 'react';
import Carousel from 'react-native-snap-carousel';
import {
  Dimensions,
  View,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ActionButton from 'react-native-action-button';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { format } from 'date-fns';
import { NavigationService } from '@module/utility';
import { HeaderBackButton, HeaderText } from '@module/daikin-ui';
import { BackButton } from '@module/acson-ui';
import { useFocusEffect } from '@react-navigation/native';

import Title from '../../shared/Title';
import Footer from '../../shared/Footer';
import { theme, width } from '../../helpers/themes';
import Button from '../../shared/Button';
import { api } from '../../service/api';
import {
  JOB_STARTED,
  JOB_COMPLETED,
  TRAVELLING,
  NOT_STARTED,
  FAILED,
  CANCELLED_BY_CUSTOMER,
  CANCELLED_BY_OPERATOR,
  CANCELLED_BY_SERVICE_PROVIDER,
  OPEN,
  STATUS_COLOR,
  config,
} from '../../helpers/config';

import ServiceStatus from './components/ServiceStatus';
import Rating from './components/Rating';
import Loading from '../../shared/Loading';

const SCHEDULED = 'SCHEDULED';
const HISTORY = 'HISTORY';

export function MyServiceListFC({ navigation }) {
  const [list, setList] = useState([]);
  const [history, setHistory] = useState([]);
  const [loading, setLoading] = useState(false);

  useFocusEffect(
    useCallback(() => {
      if (String(config.company).toLowerCase() === 'daikin') {
        NavigationService.setOptions(navigation, {
          title: <HeaderText>My Booking</HeaderText>,
          headerLeft: () => <HeaderBackButton navigation={navigation} />,
        });
      } else {
        navigation.setOptions({
          title: <HeaderText>My Booking</HeaderText>,
          headerLeft: () => <BackButton navigation={navigation} />,
        });
      }
    }, [navigation]),
  );

  useFocusEffect(
    useCallback(() => {
      async function fetchServiceList() {
        const res = await api.get('service-requests').catch((err) => {
          console.log(err);
        });

        if (res) {
          setList(res);
        }
      }

      fetchServiceList();
    }, []),
  );

  useEffect(() => {
    async function fetchHistoryList() {
      const res = await api.get('service-requests/history').catch((err) => {
        console.log(err);
      });

      if (res) {
        setHistory(res);
      }
    }

    fetchHistoryList();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      const res = await api.get('service-requests').catch((err) => {
        console.log(err);
      });

      if (res) {
        setList(res);
      }

      const resHistory = await api
        .get('service-requests/history')
        .catch((err) => {
          console.log(err);
        });

      if (resHistory) {
        setHistory(resHistory);
      }
    });

    return () => unsubscribe();
  }, [navigation]);

  return (
    <MyServiceList
      data={list}
      history={history}
      navigation={navigation}
      loading={loading}
    />
  );
}
class MyServiceList extends PureComponent {
  state = {
    selectedTab: SCHEDULED,
    isEmptyRecord: false, // need to remove later, based on API to render
  };

  selectTab = (type) => {
    this.setState({ selectedTab: type });
  };

  _emptyContainer = () => (
    <View style={styles.emptyRequestContainer}>
      <Text style={styles.emptyTitle}>You don't have any service requests</Text>
      <Button style={{ width: 216 }}>Book Service</Button>
    </View>
  );

  _rating = (item) => {
    if (!item?.service?.status?.code.match(/COMPLETED/g)) {
      return null;
    }
    if (item?.rating) {
      return <Rating number={item?.rating?.rating || 0} readOnly />;
    }
    return (
      <Text
        style={[styles.itemTitle, { color: '#4A90E2', fontWeight: 'normal' }]}
      >
        Rate this service
      </Text>
    );
  };

  navigateTo = (routeName, params = {}) => {
    if (routeName) {
      this.props.navigation.navigate(routeName, params);
    }
  };

  _renderItem = ({ item, index }) => {
    const { selectedTab } = this.state;
    const isHistoryTab = selectedTab == HISTORY;
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() =>
          this.navigateTo('MyServiceDetails', {
            serviceID: item.id,
          })
        }
      >
        <View style={{ flexDirection: 'row' }}>
          <Text style={[styles.itemTitle, { flex: 1 }]}>{item?.id}</Text>
          <ServiceStatus
            status={item?.service?.status?.code}
            label={item?.service?.status?.name}
          />
        </View>
        <Text
          style={[styles.itemPlaceholder, { paddingBottom: 16 }]}
        >{`${format(
          new Date(item.expectedArrivalPeriod.start),
          'EEE, dd MMM yyyy hh:mm a',
        )} - ${format(
          new Date(item.expectedArrivalPeriod.end),
          'hh:mm a',
        )}`}</Text>
        <Text
          style={[
            styles.itemPlaceholder,
            { paddingBottom: isHistoryTab ? 16 : 0 },
          ]}
        >
          {item?.customerAddress?.formattedAddress}
        </Text>
        {isHistoryTab && this._rating(item)}
      </TouchableOpacity>
    );
  };

  _requestList = () => {
    const { selectedTab } = this.state;
    const { data, history } = this.props;

    return (
      <FlatList
        data={selectedTab === SCHEDULED ? data : history}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => `service-${item.id}-${index}`}
        style={styles.list}
      />
    );
  };

  render() {
    const { selectedTab } = this.state;
    const { loading } = this.props;

    return (
      <>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.selectTab(SCHEDULED)}
            style={[
              styles.tabTitleContainer,
              {
                borderBottomWidth: selectedTab == SCHEDULED ? 1 : 0,
                borderBottomColor:
                  selectedTab == SCHEDULED ? theme.primary : 'transparent',
              },
            ]}
          >
            <Text
              style={[
                styles.tabTitle,
                {
                  color:
                    selectedTab == SCHEDULED ? theme.black : theme.placeholder,
                },
              ]}
            >
              Scheduled
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.selectTab(HISTORY)}
            style={[
              styles.tabTitleContainer,
              {
                borderBottomWidth: selectedTab == HISTORY ? 1 : 0,
                borderBottomColor:
                  selectedTab == HISTORY ? theme.primary : 'transparent',
              },
            ]}
          >
            <Text
              style={[
                styles.tabTitle,
                {
                  color:
                    selectedTab == HISTORY ? theme.black : theme.placeholder,
                },
              ]}
            >
              History
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.container}>
          {this.state.isEmptyRecord && this._emptyContainer()}
          {!this.state.isEmptyRecord && this._requestList()}
        </View>

        <ActionButton
          buttonColor={theme.primary}
          onPress={() => this.props.navigation.navigate('ServiceBooking')}
        />

        <Loading visible={loading} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    marginTop: 8,
    paddingHorizontal: 8,
  },
  itemContainer: {
    backgroundColor: theme.white,
    borderWidth: 1,
    borderColor: '#C8C8C8',
    borderRadius: 2,
    padding: 16,
    marginBottom: 8,
  },
  itemTitle: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.black,
    paddingBottom: 8,
    fontWeight: 'bold',
  },
  itemPlaceholder: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.placeholder,
  },
  emptyRequestContainer: {
    flex: 1,
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  emptyTitle: {
    fontSize: 15,
    lineHeight: 18,
    fontWeight: '500',
    paddingBottom: 52,
    color: theme.black,
  },
  emptyDesc: {
    paddingBottom: '8%',
    color: theme.placeholder,
  },
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
  tabTitleContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabTitle: {
    paddingTop: 17,
    paddingBottom: 10,
    fontSize: 14,
    lineHeight: 17,
  },
});

export default MyServiceList;
