import React, { PureComponent } from 'react';
import Carousel from 'react-native-snap-carousel';
import {
  Dimensions,
  View,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  TextInput,
  Alert,
  Platform,
} from 'react-native';
import { NavigationService } from '@module/utility';
import { HeaderBackButton, HeaderText } from '@module/daikin-ui';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ActionButton from 'react-native-action-button';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Title from '../../shared/Title';
import Footer from '../../shared/Footer';
import { theme, width } from '../../helpers/themes';
import Button from '../../shared/Button';
import { api } from '../../service/api';
import Label from '../../shared/Label';
import Loading from '../../shared/Loading';
import { config } from '../../helpers/config';
import Divider from '../../shared/Divider';

class Feedback extends PureComponent {
  state = {
    feedbacks: [],
    selectedFeedback: null,
    extraComment: '',
    loading: false,
  };

  componentDidMount() {
    const { params } = this.props.route;
    if (String(config.company).toLowerCase() === 'daikin') {
      NavigationService.setOptions(this.props.navigation, {
        title: <HeaderText>Feedback</HeaderText>,
        headerLeft: () => (
          <HeaderBackButton
            onPress={() => {
              params?.resetRating();
              this.props.navigation.goBack();
            }}
            navigation={this.props.navigation}
          />
        ),
      });
    } else {
      this.props.navigation.setOptions({
        title: <HeaderText>Feedback</HeaderText>,
        headerLeft: () => (
          <HeaderBackButton
            onPress={() => {
              params?.resetRating();
              this.props.navigation.goBack();
            }}
            navigation={this.props.navigation}
          />
        ),
      });
    }

    this.setState(
      {
        loading: true,
      },
      async () => {
        const res = await api
          .get('feedbacks/consumer-rating-feedbacks/options')
          .catch((err) => {
            console.log(err);
            this.setState({
              loading: false,
            });
          });

        if (res) {
          this.setState({
            feedbacks: res,
          });
        }

        this.setState({
          loading: false,
        });
      },
    );
  }

  _renderItem = ({ item, index }) => {
    const { selectedFeedback } = this.state;

    return (
      <>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            if (item.id === selectedFeedback) {
              this.setState({ selectedFeedback: null });
              return;
            }

            this.setState({ selectedFeedback: item.id });
          }}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: '5%',
          }}
        >
          <Text style={styles.itemText}>{item.title}</Text>
          <MaterialIcons
            name={
              item.id === selectedFeedback
                ? 'check-box'
                : 'check-box-outline-blank'
            }
            size={25}
          />
        </TouchableOpacity>
        {index !== this.state.feedbacks.length - 1 ? <Divider /> : null}

        {index === this.state.feedbacks.length - 1 && (
          <View style={styles.row}>
            <View style={[styles.others, styles.comments]}>
              <TextInput
                maxLength={500}
                onChangeText={(extraComment) => this.setState({ extraComment })}
                defaultValue={this.state.extraComment}
                value={this.state.extraComment}
                placeholder="Tell us more (optional)"
                underlineColorAndroid="transparent"
                multiline
                style={{ height: 60 }}
              />
            </View>
          </View>
        )}
      </>
    );
  };

  _separator = () => (
    <View style={{ borderTopWidth: 0.5, borderTopColor: theme.placeholder }} />
  );

  submitFeedback = () => {
    const { selectedFeedback, extraComment } = this.state;
    const {
      route: {
        params: { serviceRequestID, rating },
      },
    } = this.props;

    this.setState(
      {
        loading: true,
      },
      async () => {
        const res = await api
          .post(
            'feedbacks/consumer-rating-feedbacks/responses',
            JSON.stringify({
              optionId: selectedFeedback?.id || '',
              extraComment,
              rating,
              serviceTicketId: serviceRequestID,
            }),
            {
              'Content-Type': 'application/json',
            },
          )
          .catch((err) => {
            this.setState({ loading: false });
            console.log(err);
          });

        if (res?.id) {
          Alert.alert(
            'Success',
            'Your feedback is successfully send to us. We will look into it and keep improve.',
            [
              {
                text: 'Close',
                onPress: () => {
                  this.props.navigation.navigate('MyServiceList');
                  this.setState({
                    loading: false,
                  });
                },
              },
            ],
          );
        } else {
          this.setState({
            loading: false,
          });
        }
      },
    );
  };

  render() {
    const { feedbacks, loading } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <FlatList
          data={feedbacks}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => 'reason-item-' + `${index}`}
          style={styles.list}
          extraData={this.state.selectedFeedback}
          ItemSeparatorComponent={this._separator}
        />
        <View style={styles.emptyRequestContainer}>
          <Button onPress={this.submitFeedback}>Submit</Button>
        </View>

        <Loading visible={loading} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  emptyRequestContainer: {
    paddingHorizontal: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 16,
  },
  itemText: {
    fontSize: 14,
    lineHeight: 17,
    paddingVertical: 28,
    flex: 1,
  },
  list: {
    // marginTop: '2%',
    flex: 1,
  },
  others: {
    flex: 1,
  },
  left: {
    flex: 0.15,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 16,
  },
  comments: {
    paddingHorizontal: 16,
    paddingVertical: Platform.OS === 'ios' ? 8 : 0,
    height: 80,
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
    marginTop: 16,
  },
});

export default Feedback;
