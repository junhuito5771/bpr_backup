/**
 *
 * @description CancelForm.js
 * @version 1.0.0
 * @since 24 July 2020
 *
 */

import * as React from 'react';
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native';
import { theme } from '../../../helpers/themes';

export default function CancelForm({
  onPressCancel,
  onPressReschedule,
  allowReschedule,
  allowCancel,
  isCancellable,
}) {
  return (
    <TouchableOpacity style={styles.cancelJobContainer}>
      {isCancellable && allowCancel && (
        <Text style={[styles.statusDesc, { paddingBottom: 11 }]}>
          Do not need this service anymore?
        </Text>
      )}
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        {isCancellable && allowCancel && (
          <Text
            style={[styles.statusDesc, { color: theme.primary }]}
            onPress={onPressCancel}
          >
            Cancel this booking
          </Text>
        )}
        {isCancellable && allowCancel && allowReschedule && (
          <Text style={{ fontSize: 12, color: '#777' }}> Or </Text>
        )}
        {allowReschedule && (
          <Text
            style={[styles.statusDesc, { color: theme.primary }]}
            onPress={onPressReschedule}
          >
            Reschedule
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cancelJobContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 8,
  },
  statusDesc: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.placeholder,
  },
});
