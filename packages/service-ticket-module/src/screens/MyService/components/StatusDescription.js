/**
 *
 * @description StatusDescription.js
 * @version 1.0.0
 * @since 24 July 2020
 *
 */

import * as React from 'react';
import { Text, StyleSheet } from 'react-native';
import { theme } from '../../../helpers/themes';
import {
  OPEN,
  NOT_STARTED,
  JOB_STARTED,
  TRAVELLING,
  ASSIGNED,
  JOB_COMPLETED,
  FAILED,
  CANCELLED_BY_CUSTOMER,
  CANCELLED_BY_OPERATOR,
  CANCELLED_BY_SERVICE_PROVIDER,
  AWAITING_PAYMENT,
  ALLOCATED,
  TRAVEL,
} from '../../../helpers/config';

const STATUS_DESCRIPTION = {
  [OPEN]: {
    description: 'Finding you a service provider now',
  },
  [NOT_STARTED]: {
    description: 'Service provider accept the request',
  },
  [JOB_STARTED]: {
    description: 'Service provider start the request',
  },
  [TRAVELLING]: {
    description: 'Service provider is on the way',
  },
  [ASSIGNED]: {
    description: 'We found you a service provider',
  },
  [JOB_COMPLETED]: {
    description: 'Service provider has marked this service request completed',
  },
  [FAILED]: {
    description: 'Service provider failed to deliver service',
  },
  [CANCELLED_BY_CUSTOMER]: {
    description: 'Service request was cancelled by customer',
  },
  [CANCELLED_BY_OPERATOR]: {
    description: 'Service request was cancelled by operator',
  },
  [CANCELLED_BY_SERVICE_PROVIDER]: {
    description: 'Service request was cancelled by service provider',
  },
  [AWAITING_PAYMENT]: {
    description: 'Service request is waiting of payment',
  },
  [ALLOCATED]: {
    description: 'Service provider has accepted the request',
  },
  [TRAVEL]: {
    description: 'Service provider is on the way',
  },
};
export default function StatusDescription({ status, name }) {
  return (
    <>
      <Text style={[styles.statusTitle]}>{name || ''}</Text>
      <Text style={[styles.statusDesc]}>
        {STATUS_DESCRIPTION?.[status]?.description || ''}
      </Text>
    </>
  );
}

const styles = StyleSheet.create({
  statusTitle: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.black,
    marginBottom: 4,
  },
  statusDesc: {
    fontSize: 12,
    lineHeight: 12,
    color: theme.placeholder,
  },
});
