/**
 *
 * @description RatingForm.js
 * @version 1.0.0
 * @since 24 July 2020
 *
 */

import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Rating from './Rating';
import { theme } from '../../../helpers/themes';

export default function RatingForm({ onRate, ...rest }) {
  return (
    <View
      style={[styles.ratingFormContainer, { backgroundColor: theme.secondary }]}
    >
      <Text style={[styles.ratingTitleText]}>How was your experience?</Text>
      <Rating onRate={onRate} {...rest} />
    </View>
  );
}

const styles = StyleSheet.create({
  ratingFormContainer: {
    paddingHorizontal: 24,
    paddingTop: 16,
    paddingBottom: 10,
  },
  ratingTitleText: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 17,
    paddingBottom: 24.94,
  },
});
