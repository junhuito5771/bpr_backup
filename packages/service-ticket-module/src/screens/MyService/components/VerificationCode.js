import * as React from 'react';
import { Alert, StyleSheet, Text, TouchableOpacity } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { theme } from '../../../helpers/themes';
import GeneralStyles from '../../../shared/GeneralStyles';

export default function VerificationCode({ verificationCode }) {
  return (
    <>
      <TouchableOpacity
        style={GeneralStyles.row}
        onPress={() => {
          Alert.alert(
            'Info',
            `Verification Code is ${verificationCode}. This code is to verify that the job has completed. Please provide the code to Service Provider once the job has completed.`,
          );
        }}
      >
        <MaterialIcons
          name="info-outline"
          size={18}
          style={{ paddingRight: 8 }}
        />
        <Text style={[styles.statusTitle]}>Verification Code</Text>
      </TouchableOpacity>
      <Text style={{ color: theme.success, fontSize: 17 }}>
        {verificationCode}
      </Text>
    </>
  );
}

const styles = StyleSheet.create({
  statusTitle: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.black,
    paddingBottom: 8,
  },
});
