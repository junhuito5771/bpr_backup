/**
 *
 * @description ServiceStatus.js
 * @version 1.0.0
 * @since 23 July 2020
 *
 */

import * as React from 'react';
import { Text, StyleSheet } from 'react-native';

import { STATUS_COLOR } from '../../../helpers/config';
import { theme } from '../../../helpers/themes';

export default function ServiceStatus({ status, label }) {
  return (
    <Text
      style={[
        styles.itemTitle,
        { color: STATUS_COLOR[status || 'OPEN'], fontWeight: 'normal' },
      ]}
    >
      {label || ''}
    </Text>
  );
}

const styles = StyleSheet.create({
  itemTitle: {
    fontSize: 14,
    lineHeight: 16,
    color: theme.black,
    paddingBottom: 8,
    fontWeight: 'bold',
  },
});
