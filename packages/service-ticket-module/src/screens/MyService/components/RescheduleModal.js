/**
 *
 * @description RescheduleModal.js
 * @author yikkok <yikok.yong@gmail.com>
 * @version 1.0.0
 * @since 10 March 2021
 *
 */

import { format } from 'date-fns';
import * as React from 'react';
import { Alert, Modal, ScrollView, StyleSheet, Text, View } from 'react-native';
import { theme } from '../../../helpers/themes';

import { storage } from '../../../helpers/utils';
import { api } from '../../../service/api';
import Button from '../../../shared/Button';
import Container from '../../../shared/Container';
import Footer from '../../../shared/Footer';
import Header from '../../../shared/Header';
import Payment from '../../../shared/Payment';
import ServiceArrivalTime from '../../ServiceArrivalTime/ServiceArrivalTime';

type Props = {
  navigation: Object,
  id: string,
  open: boolean,
  onClose: () => void,
  start: string | Date,
  end: string | Date,
  onReschedule: () => void,
};
export default function RescheduleModal({
  navigation,
  id,
  open,
  onClose,
  start,
  end,
  onReschedule,
}: Props) {
  const [
    validExpectedArrivalTime,
    setValidExpectedArrivalView,
  ] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [checkout, setCheckout] = React.useState({
    url: '',
    payload: null,
    visible: false,
  });

  function handleReschedule() {
    Alert.alert('Reschedule', 'Are you confirm to schedule this service?', [
      { text: 'No', style: 'cancel' },
      {
        text: 'Yes',
        onPress: async () => {
          const storedItem = await storage.load('@expectedArrival');

          if (storedItem !== null && storedItem !== undefined) {
            const arrivalTime = JSON.parse(storedItem);

            const res = await api
              .post(
                `service-requests/${id}/reschedule`,
                JSON.stringify({
                  start: arrivalTime.start,
                  end: arrivalTime.end,
                }),
                {
                  'Content-Type': 'application/json',
                },
              )
              .catch((err) => {
                Alert.alert('Error', err.message);
              });

            if (res) {
              if (res.paymentCheckoutInfo !== null) {
                setTimeout(() => {
                  setCheckout({
                    url: res.paymentCheckoutInfo.checkoutUrl,
                    payload: res.paymentCheckoutInfo.checkoutPayload,
                    visible: true,
                  });
                }, 1000);
              } else {
                Alert.alert('Success', 'Successfully reschedule.', [
                  {
                    text: 'Ok',
                    onPress: () => {
                      if (onReschedule) {
                        onReschedule();
                      }
                    },
                  },
                ]);
              }
            }
          }
        },
      },
    ]);
  }

  return (
    <Modal visible={open} animationType="slide" onRequestClose={onClose}>
      <Container>
        <Header
          title="Reschedule"
          right
          rightProps={{ label: 'Close' }}
          onDone={onClose}
        />
        <ScrollView scrollEnabled={false}>
          <View style={styles.commonContainer}>
            <Text style={[styles.statusTitle]}>
              Current Estimated Arrival Time
            </Text>
            <Text style={[styles.statusDesc]}>{`${
              !!start && format(new Date(start), 'EEE, dd MMM yyyy hh:mm a')
            } - ${!!end && format(new Date(end), 'hh:mm a')}`}</Text>
          </View>

          <ServiceArrivalTime
            onChange={(date, time) => {
              setValidExpectedArrivalView(!!date && !!time);
            }}
          />
        </ScrollView>

        <Footer>
          <Button
            disabled={!validExpectedArrivalTime}
            onPress={handleReschedule}
          >
            Confirm Reschedule
          </Button>
        </Footer>

        <Payment
          visible={checkout.visible}
          uri={checkout.url}
          payload={checkout.payload}
          onCancel={(prompt) => {
            if (prompt) {
              Alert.alert(
                'Confirm to Cancel',
                'Confirm to cancel payment for the booking?',
                [
                  {
                    text: 'Confirm Cancel',
                    onPress: () => {
                      setCheckout((prev) => ({ ...prev, visible: false }));
                    },
                  },
                  { text: 'Continue Payment' },
                ],
              );
            } else {
              setCheckout((prev) => ({ ...prev, visible: false }));
            }
          }}
          onPeriodicCheck={async () => {
            const res = await api
              .get(`service-requests/${id}`)
              .catch(() => false);
            return (
              res &&
              res.customerRescheduleOrder &&
              res.customerRescheduleOrder.executedAt
            );
          }}
          onSuccess={async () => {
            setCheckout((prev) => ({ ...prev, visible: false }));
            setTimeout(() => {
              Alert.alert('Success', 'Successfully reschedule.', [
                {
                  text: 'Ok',
                  onPress: () => {
                    if (onReschedule) {
                      onReschedule();
                    }
                  },
                },
              ]);
            }, 1000);
          }}
        />
      </Container>
    </Modal>
  );
}

const styles = StyleSheet.create({
  commonContainer: {
    padding: 16,
  },
  statusTitle: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.black,
    paddingBottom: 8,
  },
  statusDesc: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.placeholder,
  },
});
