import * as React from 'react';
import { View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { theme } from '../../../helpers/themes';

export default function Rating({ number, readOnly, onRate }) {
  return (
    <View style={{ flexDirection: 'row' }}>
      {new Array(5).fill(0).map((rate, index) => {
        if (readOnly) {
          return (
            <FontAwesome
              key={index}
              name={number >= index + 1 ? 'star' : 'star-o'}
              size={25}
              color={index + 1 >= 1 ? '#FFC107' : null}
              style={{ paddingRight: 5 }}
            />
          );
        }

        return (
          <FontAwesome
            key={index}
            onPress={() => onRate(index + 1)}
            name={number >= index + 1 ? 'star' : 'star-o'}
            size={50}
            color={number >= index + 1 ? '#FFAA1D' : theme.primary}
            style={{ flex: 1, alignSelf: 'center' }}
          />
        );
      })}
    </View>
  );
}
