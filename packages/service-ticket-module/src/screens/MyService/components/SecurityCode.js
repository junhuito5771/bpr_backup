import * as React from 'react';
import { StyleSheet, Text } from 'react-native';
import { theme } from '../../../helpers/themes';

export default function SecurityCode({ code }) {
  return (
    <Text style={styles.container}>
      Security Code: <Text style={styles.securityCode}>{code}</Text>
    </Text>
  );
}

const styles = StyleSheet.create({
  container: {
    fontSize: 10,
    lineHeight: 12,
    paddingTop: 18,
  },
  securityCode: {
    color: theme.success,
  },
});
