import React, { PureComponent, useCallback, useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  Alert,
  Linking,
  Image,
  Platform,
} from 'react-native';
import { CommonActions, useFocusEffect } from '@react-navigation/native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { NavigationService } from '@module/utility';
import { HeaderBackButton, HeaderText } from '@module/daikin-ui';
import { BackButton } from '@module/acson-ui';
import { format } from 'date-fns';
import { theme } from '../../helpers/themes';
import Button from '../../shared/Button';
import MinusIcon from '../NewService/components/MinusIcon';
import { priceFormat } from '../../helpers/utils';
import ServiceStatus from './components/ServiceStatus';
import { api } from '../../service/api';
import Container from '../../shared/Container';
import Content from '../../shared/Content';
import Payment from '../../shared/Payment';
import StatusDescription from './components/StatusDescription';
import {
  isCancellable,
  isCompleted,
  AWAITING_PAYMENT,
  config,
} from '../../helpers/config';

import CancelForm from './components/CancelForm';
import RatingForm from './components/RatingForm';
import Header from '../../shared/Header';
import Loading from '../../shared/Loading';
import VerificationCode from './components/VerificationCode';
import RescheduleModal from './components/RescheduleModal';

let unsubscribe = null;
let shouldFetch = true;

export function MyServiceDetailsFC({
  navigation,
  onCancelWithoutFeedback,
  onClose,
  onRepayment,
  route,
}) {
  const [details, setDetails] = useState({});
  const [feedback, setFeedback] = useState({});
  const [loading, setLoading] = useState(false);

  const {
    params: { serviceID, backToRoot = false },
  } = route;

  useFocusEffect(
    useCallback(() => {
      if (String(config.company).toLowerCase() === 'daikin') {
        NavigationService.setOptions(navigation, {
          title: <HeaderText>Booking Details</HeaderText>,
          headerLeft: () => (
            <HeaderBackButton
              onPress={() => {
                if (backToRoot) {
                  navigation.dispatch(
                    CommonActions.reset({
                      index: 0,
                      routes: [
                        {
                          name: 'RemoteDrawer',
                        },
                      ],
                    }),
                  );
                } else {
                  navigation.goBack();
                }
              }}
              navigation={navigation}
            />
          ),
        });
      } else {
        navigation.setOptions({
          title: <HeaderText>Booking Details</HeaderText>,
          headerLeft: () => (
            <BackButton
              basicOnPress={() => {
                if (backToRoot) {
                  navigation.dispatch(
                    CommonActions.reset({
                      index: 0,
                      routes: [
                        {
                          name: 'HomeMarketing',
                        },
                      ],
                    }),
                  );
                } else {
                  navigation.goBack();
                }
              }}
              navigation={navigation}
            />
          ),
        });
      }
    }, [navigation]),
  );

  useEffect(() => {
    async function fetchDetails(id) {
      setLoading(true);
      const res = await api.get(`service-requests/${id}`).catch((err) => {
        console.log(err);
        setTimeout(() => {
          setLoading(false);
        }, 1000);
      });

      if (res) {
        setDetails(res);
      }

      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }

    if (navigation?.addListener) {
      if (shouldFetch) {
        unsubscribe = navigation?.addListener?.('focus', async () => {
          shouldFetch = false;
          await fetchDetails(serviceID);
        });
      }

      if (shouldFetch) {
        fetchDetails(serviceID);
      }
    } else {
      fetchDetails(serviceID);
    }

    return () => {
      if (unsubscribe) {
        unsubscribe();
      }
      shouldFetch = true;
    };
  }, [serviceID]);

  async function onRefetchDetails() {
    setLoading(true);
    const res = await api.get(`service-requests/${serviceID}`).catch((err) => {
      console.log(err);
      setTimeout(() => {
        setLoading(false);
      }, 1000);
    });

    if (res) {
      setDetails(res);
    }

    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }

  return (
    <MyServiceDetails
      navigation={navigation}
      details={details}
      onCancelWithoutFeedback={onCancelWithoutFeedback}
      onClose={onClose}
      loading={loading}
      onRepayment={onRepayment}
      onRefetchDetails={onRefetchDetails}
    />
  );
}
class MyServiceDetails extends PureComponent {
  state = {
    rating: 0,
    requestLoading: false,
    checkout: {
      uri: '',
      payload: null,
      visible: false,
    },
    openReschedule: false,
  };

  cancelJob = () => {
    const { details } = this.props;
    Alert.alert(
      'Service Request',
      'Are you sure want to cancel the service request ?',
      [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          onPress: async () => {
            this.setState({ requestLoading: true });
            const res = await api
              .post(
                `service-requests/${details.id}/cancel`,
                JSON.stringify({}),
                { 'Content-Type': 'application/json' },
              )
              .catch((err) => {
                console.log(err);
                Alert.alert(
                  'Error',
                  err?.message || 'Please try again later.',
                  [
                    {
                      text: 'Ok',
                      onPress: () => {
                        this.setState({ requestLoading: false });
                      },
                    },
                  ],
                );
              });

            if (res) {
              if (this.props.onCancelWithoutFeedback) {
                this.props.onCancelWithoutFeedback();
                return;
              }

              this.setState({ requestLoading: false });

              this.props.navigation.navigate('CancelOrder', {
                serviceRequestID: details.id,
              });
            }
          },
        },
      ],
    );
  };

  rateNow = async (rating) => {
    this.setState({ rating }, () => {
      this.props.navigation.navigate('Feedback', {
        serviceRequestID: this.props.details.id,
        rating: this.state.rating,
        resetRating: () => {
          this.setState({
            rating: 0,
          });
        },
      });
    });
  };

  handleOnMakePayment = async () => {
    if (this.props.onRepayment) {
      this.props.onRepayment();
      return;
    }

    this.setState({ requestLoading: true }, async () => {
      const res = await api
        .get(`service-requests/${this.props.details.id}/checkout`)
        .catch((err) => {
          Alert.alert('Error', err.message);
        });

      if (res && res.checkoutUrl) {
        this.setState(
          {
            requestLoading: false,
          },
          () => {
            setTimeout(() => {
              this.setState({
                checkout: {
                  uri: res.checkoutUrl,
                  payload: res.checkoutPayload,
                  visible: true,
                },
              });
            }, 500);
          },
        );
      } else {
        Alert.alert(
          'Error',
          'Unable to proceed to payment due to unexpected error. Please try again later.',
        );
      }
    });
  };

  render() {
    const {
      details: {
        service,
        id,
        securityCode,
        verificationCode,
        customerContact,
        customerOrder,
        customerAddress,
        expectedArrivalPeriod,
        rating,
      },
      loading,
    } = this.props;

    const { checkout } = this.state;

    return (
      <Container>
        {this.props.onClose && (
          <Header
            title="Cancel Request"
            right
            rightProps={{ label: 'Close' }}
            onDone={this.props.onClose}
          />
        )}
        <Content style={{ flex: 1, paddingHorizontal: 0 }}>
          <ScrollView style={{ flex: 1, paddingHorizontal: 16 }}>
            <View style={styles.titleContainer}>
              <Text style={[styles.itemTitle, { flex: 1 }]}>{`#${id}`}</Text>
              <ServiceStatus
                status={service?.status?.code}
                label={service?.status?.name}
              />
            </View>

            <View style={styles.statusDescContainer}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View>
                  {!!service?.provider?.worker?.profilePicture && (
                    <Image
                      source={{
                        uri: service?.provider?.worker?.profilePicture,
                      }}
                      resizeMode="cover"
                      style={styles.avatar}
                    />
                  )}
                  {!service?.provider?.worker?.profilePicture && <MinusIcon />}
                </View>
                <View style={styles.statusDescContent}>
                  <StatusDescription
                    name={service?.provider?.worker?.name}
                    status={service?.status?.code}
                  />
                </View>
                {!!service?.provider?.worker?.phone &&
                  !isCompleted(service?.status?.code) && (
                    <FontAwesome
                      name="phone"
                      size={25}
                      onPress={async () => {
                        const phone = Platform.select({
                          ios: `telprompt:${service?.provider?.worker?.phone}`,
                          android: `tel:${service?.provider?.worker?.phone}`,
                        });
                        const isSupported = await Linking.canOpenURL(phone);

                        if (isSupported) {
                          Linking.openURL(phone);
                        }
                      }}
                    />
                  )}
              </View>
              {/* <SecurityCode code={securityCode} /> */}
            </View>

            <View style={styles.commonContainer}>
              <Text style={[styles.statusTitle]}>Security Code</Text>
              <Text style={{ color: theme.success, fontSize: 17 }}>
                {securityCode}
              </Text>
            </View>

            <View style={styles.commonContainer}>
              <VerificationCode verificationCode={verificationCode} />
            </View>

            <View style={styles.commonContainer}>
              <Text style={[styles.statusTitle]}>Estimated Arrival Time</Text>
              <Text style={[styles.statusDesc]}>{`${
                !!expectedArrivalPeriod?.start &&
                format(
                  new Date(expectedArrivalPeriod?.start),
                  'EEE, dd MMM yyyy hh:mm a',
                )
              } - ${
                !!expectedArrivalPeriod?.end &&
                format(new Date(expectedArrivalPeriod?.end), 'hh:mm a')
              }`}</Text>
            </View>

            <View style={styles.commonContainer}>
              <Text style={[styles.statusTitle]}>Location</Text>
              <Text style={[styles.statusDesc]}>
                {customerAddress?.formattedAddress || ''}
              </Text>
            </View>

            <View style={styles.commonContainer}>
              <Text style={[styles.statusTitle, { paddingBottom: 10 }]}>
                Service Package
              </Text>
              {customerOrder?.servicePackages?.map((servicePackage, index) => (
                <Text
                  style={[styles.statusDesc, { paddingBottom: 4 }]}
                  key={index}
                >
                  {`${servicePackage.quantity}x ${servicePackage.name}`}
                </Text>
              ))}
            </View>

            <View style={styles.commonContainer}>
              <Text style={[styles.statusTitle, { paddingBottom: 10 }]}>
                Contact Details
              </Text>
              <Text style={[styles.statusDesc, { paddingBottom: 10 }]}>
                {customerContact?.name || ''}
              </Text>
              <Text style={[styles.statusDesc, { paddingBottom: 10 }]}>
                {customerContact?.email || ''}
              </Text>
              <Text style={[styles.statusDesc, { paddingBottom: 10 }]}>
                {`${customerContact?.phone || ''}`}
                {customerContact?.secondaryPhone &&
                  `, ${customerContact?.secondaryPhone || ''}`}
              </Text>
            </View>

            <View style={styles.commonContainer}>
              <Text style={[styles.statusTitle, { paddingBottom: 10 }]}>
                Remark
              </Text>
              <Text style={[styles.statusDesc, { paddingBottom: 10 }]}>
                {customerOrder?.remarks || '-'}
              </Text>
            </View>

            {service?.status?.code === AWAITING_PAYMENT && (
              <View style={styles.commonContainer}>
                <Button onPress={this.handleOnMakePayment}>Make Payment</Button>
              </View>
            )}

            {(customerOrder?.consumerDiscountAmount > 0 ||
              customerOrder?.consumerPromotionAmount > 0) && (
              <View style={styles.titleContainer}>
                <Text style={{ flex: 1 }} />
                <Text
                  style={[
                    styles.itemTitle,
                    {
                      fontSize: 16,
                      lineHeight: 21,
                      textDecorationLine: 'line-through',
                    },
                  ]}
                >
                  RM {priceFormat(customerOrder?.consumerSubTotal)}
                </Text>
              </View>
            )}

            {customerOrder?.consumerDiscountAmount > 0 && (
              <View style={styles.titleContainer}>
                <Text style={[styles.itemTitle, { flex: 1 }]}>
                  Volume Discount
                </Text>
                <Text
                  style={[styles.itemTitle, { fontSize: 16, lineHeight: 21 }]}
                >
                  - RM {priceFormat(customerOrder?.consumerDiscountAmount)}
                </Text>
              </View>
            )}

            {customerOrder?.consumerPromotionAmount > 0 && (
              <View style={styles.titleContainer}>
                <Text style={[styles.itemTitle, { flex: 1 }]}>
                  Promo ({customerOrder?.consumerPromotionCode})
                </Text>
                <Text
                  style={[styles.itemTitle, { fontSize: 16, lineHeight: 21 }]}
                >
                  - RM {priceFormat(customerOrder?.consumerPromotionAmount)}
                </Text>
              </View>
            )}

            <View style={[styles.titleContainer, { paddingBottom: 28 }]}>
              <Text style={[styles.itemTitle, { flex: 1 }]}>Total Amount</Text>
              <Text
                style={[
                  styles.itemTitle,
                  { color: theme.primary, fontSize: 18, lineHeight: 21 },
                ]}
              >
                RM {priceFormat(customerOrder?.consumerTotal)}
              </Text>
            </View>
          </ScrollView>
          {isCompleted(service?.status?.code) && (
            <RatingForm
              onRate={this.rateNow}
              number={(!!rating && rating?.rating) || this.state.rating}
              readOnly={(!!rating && rating?.rating) || this.state.rating > 0}
            />
          )}
          <CancelForm
            allowReschedule={this.props.details.allowReschedule}
            allowCancel={this.props.details.allowCancel}
            isCancellable={isCancellable(service?.status?.code)}
            onPressCancel={this.cancelJob}
            onPressReschedule={() => {
              this.setState({
                openReschedule: true,
              });
            }}
          />
          <Loading visible={loading || this.state.requestLoading} />

          <RescheduleModal
            onReschedule={() => {
              this.setState({ openReschedule: false }, () => {
                this.props.onRefetchDetails();
              });
            }}
            start={expectedArrivalPeriod?.start}
            end={expectedArrivalPeriod?.end}
            id={id}
            open={this.state.openReschedule}
            onClose={() => {
              this.setState({
                openReschedule: false,
              });
            }}
          />
          <Payment
            visible={checkout.visible}
            uri={checkout.uri}
            payload={checkout.payload}
            onPeriodicCheck={async () => {
              const res = await api
                .get(`service-requests/${id}`)
                .catch(() => false);
              if (res && res.service) {
                if (
                  res.service.status?.code &&
                  res.service.status?.code !== AWAITING_PAYMENT
                ) {
                  return true;
                }
              }
              return false;
            }}
            onCancel={(prompt) => {
              if (prompt) {
                Alert.alert(
                  'Confirm to Cancel',
                  'Confirm to cancel payment for the booking?',
                  [
                    {
                      text: 'Confirm Cancel',
                      onPress: () => {
                        this.setState((state) => ({
                          ...state,
                          checkout: {
                            ...this.state.checkout,
                            visible: false,
                          },
                        }));
                      },
                    },
                    { text: 'Continue Payment' },
                  ],
                );
              } else {
                this.setState((state) => ({
                  ...state,
                  checkout: {
                    ...this.state.checkout,
                    visible: false,
                  },
                }));
              }
            }}
            onSuccess={async () => {
              try {
                await EncryptedStorage.removeItem('@packages');
                await EncryptedStorage.removeItem('@otp');
              } catch (error) {
                console.log(error.code);
              }

              // Close modal
              this.setState(
                (state) => ({
                  ...state,
                  checkout: {
                    uri: '',
                    payload: null,
                    visible: false,
                  },
                }),
                () => {
                  this.props.onRefetchDetails();
                },
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  commonContainer: {
    paddingBottom: 24,
  },
  statusTitle: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.black,
    paddingBottom: 8,
  },
  statusDesc: {
    fontSize: 14,
    lineHeight: 17,
    color: theme.placeholder,
  },
  statusDescContent: {
    flex: 1,
    paddingHorizontal: 16,
  },
  statusDescContainer: {
    backgroundColor: '#F5F5F5',
    padding: 16,
    borderRadius: 4,
    marginBottom: 20,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemTitleWrapper: {
    paddingTop: 13,
  },
  itemTitle: {
    color: theme.black,
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 17,
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
});

export default MyServiceDetails;
