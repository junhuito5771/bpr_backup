/**
 *
 * @description RequestSummary.js
 * @version 1.0.0
 * @since 25 June 2020
 *
 */

import * as React from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  Dimensions,
  Text,
  Alert,
  Linking,
} from 'react-native';
import { format } from 'date-fns';
import { CommonActions, useFocusEffect } from '@react-navigation/native';
import EncryptedStorage from 'react-native-encrypted-storage';
import Config from 'react-native-config';
import { NavigationService } from '@module/utility';
import {
  HeaderBackButton as DaikinBackButton,
  HeaderText as DaikinHeaderText,
  HeaderButton,
} from '@module/daikin-ui';
import { BackButton as AcsonBackButton } from '@module/acson-ui';

import Container from '../../shared/Container';
import Title from '../../shared/Title';
import Footer from '../../shared/Footer';
import Label from '../../shared/Label';
import Section from '../../shared/Section';
import PlaceholderText from '../../shared/PlaceholderText';
import { theme } from '../../helpers/themes';
import Content from '../../shared/Content';
import Button from '../../shared/Button';
import {
  priceFormat,
  getConsumerQuotationTierBasedOnQuantity,
} from '../../helpers/utils';
import { api } from '../../service/api';
import Loading from '../../shared/Loading';
import Payment from '../../shared/Payment';
import ModalCancelRequest from './components/ModalCancelRequest';
import CText from '../../shared/CText';
import { AWAITING_PAYMENT, config } from '../../helpers/config';
import PromoCode from './components/PromoCode';
import HelpWebScreen from '../Help/Help';

const { height, width } = Dimensions.get('screen');

export default function RequestSummary(props) {
  const [summary, setSummary] = React.useState({
    '@street1': '',
    '@street2': '',
    '@postcode': '',
    '@email': '',
    '@secondaryPhoneNumber': '',
    '@packages': {},
    '@expectedArrival': {},
    '@phoneNumber': '',
    '@state': '',
    '@namePrefix': '',
    '@fullname': '',
    '@building': '',
    '@city': '',
    '@latlng': {},
    '@propertyType': '',
    '@remarks': '',
    '@callingCode': '',
    '@secondaryPhoneCountryCallingCode': '',
  });
  const [loading, setLoading] = React.useState(false);
  const [checkout, setCheckout] = React.useState({
    url: '',
    payload: null,
    visible: false,
  });
  const [cancelRequest, setCancelRequest] = React.useState({
    serviceID: '',
    visible: false,
  });
  const [packages, setPackages] = React.useState({});
  const [promoCode, setPromoCode] = React.useState('');
  const [promotionAmount, setPromotionAmount] = React.useState(0);
  const [promotionDiscount, setPromotionDiscount] = React.useState(0);
  const [readOnlyPromoCode, setReadonlyPromoCode] = React.useState(false);
  const [openHelpWeb, setOpenHelpWeb] = React.useState(false);

  useFocusEffect(
    React.useCallback(() => {
      if (String(config.company).toLowerCase() === 'daikin') {
        NavigationService.setOptions(props.navigation, {
          title: <DaikinHeaderText>Request Summary</DaikinHeaderText>,
          headerLeft: () => <DaikinBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      } else {
        props.navigation.setOptions({
          headerTitle: 'Request Summary',
          headerLeft: () => <AcsonBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      }
    }, [props.navigation]),
  );

  React.useEffect(() => {
    async function init() {
      const keys = [
        '@street1',
        '@street2',
        '@postcode',
        '@email',
        '@secondaryPhoneNumber',
        '@packages',
        '@expectedArrival',
        '@phoneNumber',
        '@state',
        '@namePrefix',
        '@fullname',
        '@building',
        '@city',
        '@latlng',
        '@propertyType',
        '@remarks',
        '@callingCode',
        '@otp',
        '@secondaryPhoneCountryCallingCode',
      ];

      if (keys?.length > 0) {
        const details = { ...summary };
        for (let index = 0; index < keys.length; index++) {
          const key = keys[index];
          if (key.match(/^@/)) {
            const result = await EncryptedStorage.getItem(key);

            console.log(result);
            if (result !== null && result !== undefined) {
              details[key] =
                (result.match(/^{/) && JSON.parse(result)) || result;

              const groupQuantity = {};
              if (details?.['@packages']) {
                Object.values(details?.['@packages']).forEach((p) => {
                  groupQuantity[p.servicePackageGroupCode] = isNaN(
                    groupQuantity[p.servicePackageGroupCode],
                  )
                    ? p.quantity
                    : groupQuantity[p.servicePackageGroupCode] + p.quantity;
                });

                let total = 0;
                let subTotal = 0;
                let discount = 0;

                Object.values(details?.['@packages']).forEach((p) => {
                  const consumerQuotation = getConsumerQuotationTierBasedOnQuantity(
                    p.consumerQuotations,
                    groupQuantity[p.servicePackageGroupCode],
                  );
                  subTotal += p.unitPrice * p.quantity;
                  discount +=
                    (p.unitPrice - consumerQuotation.unitPrice) * p.quantity;
                });

                total = subTotal - discount;
                setPackages({
                  total,
                  subTotal,
                  discount,
                });
              }
            }
          }
        }

        setSummary(details);
      }
    }

    init();
  }, []);

  function generateRequestFormBody() {
    const filtered = Object.values(summary?.['@packages'])?.filter(
      (val) => val.quantity > 0,
    );

    const body = {
      requestCategory:
        String(config.company).toLowerCase() === 'acson' ? 'AMSS' : 'DMSS',
      customerAddress: {
        building: summary['@building'],
        city: summary['@city'],
        countryCode: 'MY',
        latitude: summary['@latlng']?.lat,
        longitude: summary['@latlng']?.lng,
        postalCode: summary['@postcode'],
        propertyType: summary['@propertyType'].toUpperCase(),
        state: summary['@state'],
        street1: summary['@street1'],
        street2: summary['@street2'],
      },
      customerContact: {
        email: summary['@email'],
        name: `${summary['@namePrefix']}. ${summary['@fullname']}`,
        phone: `${summary['@callingCode']}${summary['@phoneNumber']}`,
        secondaryPhone: summary['@secondaryPhoneNumber']
          ? `${summary['@secondaryPhoneCountryCallingCode']}${summary['@secondaryPhoneNumber']}`
          : '',
      },
      customerOrder: {
        remarks: summary['@remarks'],
        servicePackages: filtered,
        promoCode,
        total: 0,
      },
      expectedArrivalPeriod: {
        start: summary['@expectedArrival'].start,
        end: summary['@expectedArrival'].end,
      },
    };

    return body;
  }

  async function handleOnSubmitCreateServiceRequest() {
    if (checkout.payload) {
      setCheckout((prev) => ({ ...prev, visible: true }));
      return;
    }

    const body = generateRequestFormBody();

    setLoading(true);
    const res = await api
      .post('service-requests', JSON.stringify(body), {
        'Content-Type': 'application/json',
        'x-otp-token': summary['@otp'],
      })
      .catch((err) => {
        setLoading(false);

        setTimeout(() => {
          Alert.alert('Error', err.message, [
            {
              text: 'Ok',
            },
          ]);
        }, 500);
      });

    if (res?.id) {
      setCancelRequest((prev) => ({
        ...prev,
        serviceID: res.id,
      }));

      if (res.paymentCheckoutInfo) {
        setTimeout(() => {
          setCheckout({
            url: res.paymentCheckoutInfo.checkoutUrl,
            payload: res.paymentCheckoutInfo.checkoutPayload,
            visible: true,
          });
        }, 1000);
      } else {
        try {
          await EncryptedStorage.removeItem('@packages');
          await EncryptedStorage.removeItem('@otp');
          await EncryptedStorage.removeItem('@remarks');
        } catch (error) {
          console.log(error);
        }

        const routeName =
          String(config.company).toLowerCase() === 'daikin'
            ? 'RemoteDrawer'
            : 'Home';
        props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: routeName }],
          }),
        );
      }
    }

    setLoading(false);
  }

  async function onCalculatePromotionPrice() {
    const body = generateRequestFormBody();

    setLoading(true);
    const res = await api
      .post('service-requests/calculate', JSON.stringify(body), {
        'Content-Type': 'application/json',
        'x-otp-token': summary['@otp'],
      })
      .catch((err) => {
        Alert.alert('Error', err?.message || 'Please try again later.', [
          {
            text: 'Ok',
            onPress: () => {
              setPromoCode('');
              setLoading(false);
            },
          },
        ]);
      });

    if (res) {
      setLoading(false);
      setReadonlyPromoCode(true);
      setPromotionDiscount(res.customerOrder.consumerPromotionAmount);
      setPromotionAmount(res.customerOrder.consumerTotal);
    }
  }

  return (
    <Container>
      <Content
        behavior="padding"
        keyboardVerticalOffset={100}
        style={{ flexGrow: 1, padding: 0, paddingHorizontal: 16 }}
      >
        <ScrollView
          style={styles.itemContainer}
          keyboardShouldPersistTaps="handled"
        >
          <Title style={styles.title}>Aircond Servicing</Title>

          <Section>
            <Label title="Date and time" />
            <PlaceholderText>
              {format(
                new Date(summary?.['@expectedArrival']?.start || new Date()),
                'EEE, dd MMM yyyy hh:mm a',
              )}
            </PlaceholderText>
          </Section>

          <Section>
            <Label title="Location" />
            <PlaceholderText>{summary['@street1']}</PlaceholderText>
            <PlaceholderText>{summary['@street2']}</PlaceholderText>
          </Section>

          <View style={styles.row}>
            <Section>
              <Label title="State" />
              <PlaceholderText>{summary['@state']}</PlaceholderText>
            </Section>
            <Section>
              <Label title="City" />
              <PlaceholderText>{summary['@city']}</PlaceholderText>
            </Section>
          </View>

          <Section>
            <Label title="Location Details" />
            <PlaceholderText>{summary['@building']}</PlaceholderText>
          </Section>

          <Section>
            <Label title="Service package" />
            {Object.values(summary?.['@packages'] || {}).map(
              (dPackage) =>
                dPackage.quantity > 0 && (
                  <View style={styles.row} key={dPackage.id}>
                    <Text
                      style={styles.serviceQuantity}
                    >{`${dPackage.quantity}x`}</Text>
                    <PlaceholderText
                      numberOfLines={2}
                      style={[{ paddingVertical: 4 }]}
                    >
                      {dPackage.name}
                    </PlaceholderText>
                  </View>
                ),
            )}
          </Section>

          <Section>
            <Label title="Type of Property" />
            <PlaceholderText>{summary['@propertyType']}</PlaceholderText>
          </Section>

          <Section>
            <Label title="Contact details" />
            <PlaceholderText style={[{ paddingVertical: 4 }]}>
              {`${summary['@namePrefix']}. ${summary['@fullname']}`}
            </PlaceholderText>
            <PlaceholderText style={[{ paddingVertical: 4 }]}>
              {summary['@email']}
            </PlaceholderText>
            <PlaceholderText style={[{ paddingVertical: 4 }]}>
              {`${summary['@callingCode']}${summary['@phoneNumber']}`}
              {summary['@secondaryPhoneNumber'] &&
                `, ${summary['@secondaryPhoneCountryCallingCode']}${summary['@secondaryPhoneNumber']}`}
            </PlaceholderText>
          </Section>

          <Section>
            <Label title="Remarks" />
            <PlaceholderText style={[{ paddingVertical: 4 }]}>
              {summary['@remarks'] || '-'}
            </PlaceholderText>
          </Section>

          <Section>
            <Label title="Promo Code" />
            <PromoCode
              value={promoCode}
              onChangeText={(text) => setPromoCode(text)}
              onPress={() => {
                if (readOnlyPromoCode) {
                  setReadonlyPromoCode(false);
                  setPromoCode('');
                  setPromotionAmount(0);
                  setPromotionDiscount(0);
                } else {
                  onCalculatePromotionPrice();
                }
              }}
              readOnly={readOnlyPromoCode}
            />
          </Section>
        </ScrollView>
      </Content>
      <Footer>
        <Text style={styles.agreementText}>
          By submitting your request, you agree to our{' '}
          <CText
            onPress={async () => {
              const canOpen = await Linking.canOpenURL(Config.TERMS_OF_USE);

              if (canOpen) {
                Linking.openURL(Config.TERMS_OF_USE).catch((err) => {
                  console.log(err);
                });
              }
            }}
          >
            Terms of Use
          </CText>
          {' & '}
          <CText
            onPress={async () => {
              const canOpen = await Linking.canOpenURL(Config.PRIVACY_POLICY);

              if (canOpen) {
                Linking.openURL(Config.PRIVACY_POLICY).catch((err) => {
                  console.log(err);
                });
              }
            }}
          >
            Privacy Policy
          </CText>
        </Text>

        {(packages.discount > 0 || promotionDiscount > 0) && (
          <View style={[styles.row, styles.spaceBetween, { paddingBottom: 0 }]}>
            <Text style={{ flex: 1 }} />
            <View>
              <Text style={styles.originalPrice}>
                RM {priceFormat(packages.subTotal)}
              </Text>
            </View>
          </View>
        )}
        {packages.discount > 0 && (
          <View style={[styles.row, styles.spaceBetween, { paddingBottom: 0 }]}>
            <Text
              style={[
                styles.totalLabel,
                { fontSize: 14, color: 'black', paddingVertical: 2 },
              ]}
            >
              Volume Discount
            </Text>
            <View>
              <CText
                style={[
                  styles.estimatedPrice,
                  {
                    fontSize: 14,
                    color: 'black',
                    fontWeight: 'normal',
                    paddingVertical: 2,
                  },
                ]}
              >
                {`- RM ${priceFormat(packages.discount)}`}
              </CText>
            </View>
          </View>
        )}
        {promotionDiscount > 0 && (
          <View style={[styles.row, styles.spaceBetween]}>
            <Text style={[styles.totalLabel, { fontSize: 14 }]}>
              {`Promo (${promoCode})`}
            </Text>
            <View>
              <CText
                style={[
                  styles.estimatedPrice,
                  { fontSize: 14, color: 'black', fontWeight: 'normal' },
                ]}
              >
                {`- RM ${priceFormat(promotionDiscount)}`}
              </CText>
            </View>
          </View>
        )}
        <View style={[styles.row, styles.spaceBetween]}>
          <Text style={styles.totalLabel}>Total</Text>

          <View>
            <CText style={styles.estimatedPrice}>
              RM{' '}
              {promotionDiscount === 0
                ? priceFormat(packages.total)
                : priceFormat(promotionAmount)}
            </CText>
          </View>
        </View>
        <Button onPress={handleOnSubmitCreateServiceRequest}>
          Submit Request
        </Button>
      </Footer>
      <Loading visible={loading} />
      {checkout.visible && (
        <Payment
          visible={checkout.visible}
          uri={checkout.url}
          payload={checkout.payload}
          onCancel={(prompt) => {
            if (prompt) {
              Alert.alert(
                'Confirm to Cancel',
                'Confirm to cancel payment for the booking?',
                [
                  {
                    text: 'Confirm Cancel',
                    onPress: () => {
                      setCheckout((prev) => ({ ...prev, visible: false }));
                      setCancelRequest((prev) => ({
                        ...prev,
                        visible: true,
                      }));
                    },
                  },
                  { text: 'Continue Payment' },
                ],
              );
            } else {
              setCheckout((prev) => ({ ...prev, visible: false }));
              setCancelRequest((prev) => ({
                ...prev,
                visible: true,
              }));
            }
          }}
          onPeriodicCheck={async () => {
            const res = await api
              .get(`service-requests/${checkout.payload.RefNo}`)
              .catch(() => false);
            if (res && res.service) {
              if (
                res.service.status?.code &&
                res.service.status?.code !== AWAITING_PAYMENT
              ) {
                return true;
              }
            }
            return false;
          }}
          onSuccess={async () => {
            try {
              await EncryptedStorage.removeItem('@packages');
              await EncryptedStorage.removeItem('@otp');
            } catch (error) {
              console.log(error);
            }

            setCheckout((prev) => ({ ...prev, visible: false }));
            props.navigation.navigate('ServiceListing', {
              screen: 'MyServiceDetails',
              params: {
                serviceID: checkout.payload.RefNo,
                backToRoot: true,
              },
            });
          }}
        />
      )}
      {cancelRequest.visible && (
        <ModalCancelRequest
          navigation={props.navigation}
          visible={cancelRequest.visible}
          serviceID={cancelRequest.serviceID}
          onClose={() => {
            props.navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [
                  {
                    name:
                      String(config.company).toLowerCase() === 'daikin'
                        ? 'RemoteDrawer'
                        : 'HomeMarketing',
                  },
                ],
              }),
            );
            setCancelRequest((prev) => ({
              ...prev,
              visible: false,
            }));
          }}
          onCancelWithoutFeedback={() => {
            setCancelRequest({
              serviceID: '',
              visible: false,
            });
            props.navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [
                  {
                    name:
                      String(config.company).toLowerCase() === 'daikin'
                        ? 'RemoteDrawer'
                        : 'HomeMarketing',
                  },
                ],
              }),
            );
          }}
          onRepayment={() => {
            setCancelRequest((prev) => ({ ...prev, visible: false }));

            setTimeout(async () => {
              setLoading(true);
              const res = await api
                .get(`service-requests/${checkout.payload.RefNo}/checkout`)
                .catch((err) => {
                  setLoading(false);

                  setTimeout(() => {
                    Alert.alert('Error', err.message);
                  }, 500);
                });

              if (res && res.checkoutUrl) {
                setTimeout(() => {
                  setCheckout({
                    url: res.checkoutUrl,
                    payload: res.checkoutPayload,
                    visible: true,
                  });
                }, 1000);
              } else {
                setTimeout(() => {
                  Alert.alert(
                    'Error',
                    'Unable to proceed to payment due to unexpected error. Please try again later.',
                  );
                }, 500);
              }

              setLoading(false);
            }, 500);
          }}
        />
      )}

      <HelpWebScreen
        visible={openHelpWeb}
        onClose={() => setOpenHelpWeb(false)}
      />
    </Container>
  );
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 24,
  },
  itemContainer: {
    flex: 1,
    height,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  spaceBetween: {
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  agreementText: {
    paddingBottom: 8,
  },
  noPaddingVertical: {
    width: width - 80,
    flexWrap: 'wrap',
    paddingVertical: 0,
  },
  serviceQuantity: {
    paddingRight: 15,
    color: '#aaa',
  },

  estimatedPrice: {
    fontWeight: 'bold',
  },
  totalLabel: {
    fontSize: 16,
    alignSelf: 'flex-end',
  },
  estimatedPrice: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'right',
  },
  originalPrice: {
    textAlign: 'right',
    fontWeight: 'bold',
    textDecorationLine: 'line-through',
  },
  consumerDiscount: {
    fontWeight: 'bold',
    color: theme.success,
  },
});
