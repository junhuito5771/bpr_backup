import * as React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { theme } from '../../../helpers/themes';
import GeneralStyles from '../../../shared/GeneralStyles';
import Input from '../../../shared/Input';

type Props = {
  onChangeText: (text: string) => void,
  value: string,
  onPress: (e) => void,
  readOnly: boolean,
};
export default function PromoCode({
  onChangeText,
  value,
  onPress,
  readOnly,
}: Props) {
  return (
    <View style={[GeneralStyles.row, GeneralStyles['ai-center']]}>
      {!readOnly && (
        <Input
          placeholder="Enter Promo Code"
          value={value}
          onChangeText={onChangeText}
          autoCapitalize="none"
        />
      )}
      {readOnly && <Text style={GeneralStyles.full}>{value}</Text>}
      <TouchableOpacity
        disabled={!value}
        onPress={onPress}
        style={{ flex: 0.5 }}
      >
        <Text
          style={[styles.applyCode, { color: !value ? '#777' : theme.primary }]}
        >
          {!readOnly ? 'Apply Code' : 'Remove Code'}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  applyCode: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
});
