/**
 *
 * @description ModalCancelRequest.js
 * @version 1.0.0
 * @since 02 August 2020
 *
 */

import * as React from 'react';
import { Modal, SafeAreaView } from 'react-native';
import { MyServiceDetailsFC } from '../../MyService/MyServiceDetails';
import Header from '../../../shared/Header';

type Props = {
  visible: boolean,
  serviceID: string,
  onCancelWithoutFeedback: () => void,
  onClose: () => void,
  onRepayment: () => void,
};
export default function ModalCancelRequest({
  navigation,
  visible,
  serviceID,
  onCancelWithoutFeedback,
  onClose,
  onRepayment,
}: Props) {
  return (
    <Modal visible={visible} animationType="slide">
      <MyServiceDetailsFC
        navigation={navigation}
        route={{ params: { serviceID } }}
        onCancelWithoutFeedback={onCancelWithoutFeedback}
        onClose={onClose}
        onRepayment={onRepayment}
      />
    </Modal>
  );
}
