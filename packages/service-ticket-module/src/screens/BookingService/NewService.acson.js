import * as React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { BackButton } from '@module/acson-ui';

import { ServiceRequestContext } from '../../provider/ServiceRequestProvider';
import { config } from '../../helpers/config';
import { groupingServicePackages } from '../../helpers/service-packages';
import { api } from '../../service/api';
import {
  getConsumerQuotationTierBasedOnQuantity,
  priceFormat,
} from '../../helpers/utils';
import Container from '../../shared/Container';
import Title from '../../shared/Title';
import Steps from '../NewService/components/Steps';
import { ServiceRequestStyles } from './NewService.daikin';
import Content from '../../shared/Content';
import ServiceGroup from '../NewService/ServiceGroup.acson';
import Footer from '../../shared/Footer';
import CText from '../../shared/CText';
import Button from '../../shared/Button';
import Loading from '../../shared/Loading';

export default function AcsonNewService(props) {
  const context = React.useContext(ServiceRequestContext);
  const [loading, setLoading] = React.useState(false);
  const [servicePackagesGroup, setServicePackagesGroup] = React.useState([]);

  useFocusEffect(
    React.useCallback(() => {
      props.navigation.setOptions({
        headerTitle: 'Aircond Service',
        headerLeft: () => (
          <BackButton
            navigation={props.navigation}
            onPress={() => {
              props.navigation.navigate('Location');
            }}
          />
        ),
      });
    }, [props.navigation]),
  );

  React.useEffect(() => {
    async function fetchServicePackage() {
      setLoading(true);
      const res = await api.get('service-packages?group=AMSS').catch((err) => {
        console.log(err);
        setLoading(false);
      });

      if (res) {
        const group = groupingServicePackages(res);
        setServicePackagesGroup(group || []);
      }
      setLoading(false);
    }
    fetchServicePackage();
  }, []);

  function handleOnChange(selectedPackages) {
    const groupQuantity = {};

    if (selectedPackages) {
      Object.values(selectedPackages).forEach((p) => {
        groupQuantity[p.servicePackageGroupCode] = !groupQuantity[
          p.servicePackageGroupCode
        ]
          ? p.quantity
          : groupQuantity[p.servicePackageGroupCode] + p.quantity;
      });

      let total = 0;
      let subTotal = 0;
      let discount = 0;

      Object.values(selectedPackages).forEach((p) => {
        const consumerQuotation = getConsumerQuotationTierBasedOnQuantity(
          p.consumerQuotations,
          groupQuantity[p.servicePackageGroupCode],
        );
        subTotal += p.unitPrice * p.quantity;
        discount += (p.unitPrice - consumerQuotation.unitPrice) * p.quantity;
      });

      total = subTotal - discount;
      context.setPackages({
        total,
        subTotal,
        discount,
      });
    }
  }

  return (
    <Container>
      <View style={ServiceRequestStyles.titleContainer}>
        <Title>How many units of aircond do you need servicing?</Title>
      </View>

      <Content
        behavior="padding"
        keyboardVerticalOffset={130}
        style={{ flexGrow: 1, padding: 0 }}
      >
        <ScrollView style={ServiceRequestStyles.itemContainer}>
          <ServiceGroup
            groups={servicePackagesGroup}
            onPress={(groupName, nodes) => {
              props.navigation.navigate('Packages', {
                servicePackages: nodes,
              });
            }}
            onChange={handleOnChange}
          />
        </ScrollView>
      </Content>
      <Footer>
        <View style={ServiceRequestStyles.estimatedContainer}>
          <Text style={ServiceRequestStyles.totalLabel}>Total</Text>

          <View>
            {context.packages.discount > 0 && (
              <Text style={ServiceRequestStyles.originalPrice}>
                RM {priceFormat(context.packages.subTotal)}
              </Text>
            )}
            <CText style={ServiceRequestStyles.estimatedPrice}>
              RM {priceFormat(context.packages.total)}
            </CText>
          </View>
        </View>
      </Footer>

      <Loading visible={loading} />
    </Container>
  );
}
