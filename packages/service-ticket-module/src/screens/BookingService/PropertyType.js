/* eslint-disable */
import * as React from 'react';
import {
  HeaderBackButton as DaikinBackButton,
  HeaderText as DaikinHeaderText,
  HeaderButton,
} from '@module/daikin-ui';
import { BackButton as AcsonBackButton } from '@module/acson-ui';
import { useFocusEffect } from '@react-navigation/native';
import { NavigationService } from '@module/utility';

import ServicePropertyType, {
  ServicePropertyTypeNextButton,
} from '../NewService/ServicePropertyType';
import { ServiceRequestStyles } from './NewService.daikin';
import { config } from '../../helpers/config';
import { ServiceRequestContext } from '../../provider/ServiceRequestProvider';
import Content from '../../shared/Content';
import Container from '../../shared/Container';
import { ScrollView, Text, View } from 'react-native';
import Steps from '../NewService/components/Steps';
import Footer from '../../shared/Footer';
import CText from '../../shared/CText';
import { priceFormat } from '../../helpers/utils';
import Title from '../../shared/Title';
import HelpWebScreen from '../Help/Help';

export default function PropertyType(props) {
  const context = React.useContext(ServiceRequestContext);

  useFocusEffect(
    React.useCallback(() => {
      if (String(config.company).toLowerCase() === 'daikin') {
        NavigationService.setOptions(props.navigation, {
          title: <DaikinHeaderText>Aircond Service</DaikinHeaderText>,
          headerLeft: () => <DaikinBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => context.setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      } else {
        props.navigation.setOptions({
          headerTitle: 'Aircond Service',
          headerLeft: () => <AcsonBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => context.setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      }
    }, [props.navigation]),
  );

  function handleOnNextStep() {
    props.navigation.navigate('ArrivalTime');
  }

  return (
    <Container>
      <View style={ServiceRequestStyles.stepContainer}>
        <Steps current={2} total={5} />
      </View>

      <View style={ServiceRequestStyles.titleContainer}>
        <Title>What type of property will the service be done at?</Title>
      </View>

      <Content
        behavior="padding"
        keyboardVerticalOffset={130}
        style={{ flexGrow: 1, padding: 0 }}
      >
        <ScrollView style={ServiceRequestStyles.itemContainer}>
          <ServicePropertyType />
        </ScrollView>
      </Content>
      <Footer>
        <View style={ServiceRequestStyles.estimatedContainer}>
          <Text style={ServiceRequestStyles.totalLabel}>Total</Text>

          <View>
            {context.packages.discount > 0 && (
              <Text style={ServiceRequestStyles.originalPrice}>
                RM {priceFormat(context.packages.subTotal)}
              </Text>
            )}
            <CText style={ServiceRequestStyles.estimatedPrice}>
              RM {priceFormat(context.packages.total)}
            </CText>
          </View>
        </View>
        <ServicePropertyTypeNextButton onPress={handleOnNextStep} />
      </Footer>

      <HelpWebScreen
        visible={context.openHelpWeb}
        onClose={() => context.setOpenHelpWeb(false)}
      />
    </Container>
  );
}
