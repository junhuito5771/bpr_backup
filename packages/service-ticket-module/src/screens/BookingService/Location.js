/* eslint-disable */

import * as React from 'react';
import { ScrollView, Text, View } from 'react-native';
import {
  HeaderBackButton as DaikinBackButton,
  HeaderText as DaikinHeaderText,
  HeaderButton,
} from '@module/daikin-ui';
import { BackButton as AcsonBackButton } from '@module/acson-ui';
import { useFocusEffect } from '@react-navigation/native';
import { NavigationService } from '@module/utility';

import { priceFormat } from '../../helpers/utils';
import { ServiceRequestContext } from '../../provider/ServiceRequestProvider';
import Container from '../../shared/Container';
import Content from '../../shared/Content';
import CText from '../../shared/CText';
import Footer from '../../shared/Footer';
import Steps from '../NewService/components/Steps';
import ServiceLocation from '../ServiceLocation/ServiceLocation';
import { ServiceRequestStyles } from './NewService.daikin';
import { config } from '../../helpers/config';
import Title from '../../shared/Title';
import HelpWebScreen from '../Help/Help';
import { ServiceLocationContext } from '../../provider/ServiceLocationProvider';
import Button from '../../shared/Button';

const NextButton = (props) => {
  const context = React.useContext(ServiceLocationContext);

  return (
    <Button
      {...props}
      disabled={
        !context?.info?.building ||
        !context?.info?.street1 ||
        !context?.info?.propertyType ||
        !context?.info?.postcode ||
        !context?.info?.city ||
        !context?.info?.state
      }
    >
      Next
    </Button>
  );
};

export default function Location(props) {
  const context = React.useContext(ServiceRequestContext);

  useFocusEffect(
    React.useCallback(() => {
      if (String(config.company).toLowerCase() === 'daikin') {
        NavigationService.setOptions(props.navigation, {
          title: <DaikinHeaderText>Aircond Service</DaikinHeaderText>,
          headerLeft: () => <DaikinBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => context.setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      } else {
        props.navigation.setOptions({
          headerTitle: 'Aircond Service',
          headerLeft: () => <AcsonBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => context.setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      }
    }, [props.navigation]),
  );

  function handleOnNextStep() {
    props.navigation.navigate('ContactPerson');
  }

  return (
    <Container>
      <View style={ServiceRequestStyles.stepContainer}>
        <Steps current={4} total={5} />
      </View>

      <View style={ServiceRequestStyles.titleContainer}>
        <Title>Where is the location of the service?</Title>
      </View>

      <Content
        behavior="padding"
        keyboardVerticalOffset={130}
        style={{ flexGrow: 1, padding: 0 }}
      >
        <ScrollView style={ServiceRequestStyles.itemContainer}>
          <ServiceLocation />
        </ScrollView>
      </Content>
      <Footer>
        <View style={ServiceRequestStyles.estimatedContainer}>
          <Text style={ServiceRequestStyles.totalLabel}>Total</Text>

          <View>
            {context.packages.discount > 0 && (
              <Text style={ServiceRequestStyles.originalPrice}>
                RM {priceFormat(context.packages.subTotal)}
              </Text>
            )}
            <CText style={ServiceRequestStyles.estimatedPrice}>
              RM {priceFormat(context.packages.total)}
            </CText>
          </View>
        </View>
        <NextButton onPress={handleOnNextStep} />
      </Footer>

      <HelpWebScreen
        visible={context.openHelpWeb}
        onClose={() => context.setOpenHelpWeb(false)}
      />
    </Container>
  );
}
