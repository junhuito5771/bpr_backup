import * as React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { HeaderButton } from '@module/daikin-ui';
import { BackButton } from '@module/acson-ui';

import Container from '../../shared/Container';
import Steps from '../NewService/components/Steps';
import Title from '../../shared/Title';
import Content from '../../shared/Content';
import ServiceList from '../NewService/ServiceList.acson';
import Footer from '../../shared/Footer';
import CText from '../../shared/CText';
import Button from '../../shared/Button';
import { ServiceRequestContext } from '../../provider/ServiceRequestProvider';
import { ServiceRequestStyles } from './NewService.daikin';
import {
  getConsumerQuotationTierBasedOnQuantity,
  priceFormat,
} from '../../helpers/utils';
import HelpWebScreen from '../Help/Help';

export default function AcsonServicePackages(props) {
  const context = React.useContext(ServiceRequestContext);

  const {
    params: { servicePackages = [] },
  } = props.route;

  useFocusEffect(
    React.useCallback(() => {
      props.navigation.setOptions({
        headerTitle: 'Aircond Service',
        headerLeft: () => (
          <BackButton
            navigation={props.navigation}
            onPress={() => {
              props.navigation.navigate('Location');
            }}
          />
        ),
        headerRight: () => (
          <HeaderButton onPress={() => context.setOpenHelpWeb(true)}>
            Help
          </HeaderButton>
        ),
      });
    }, [props.navigation]),
  );

  function handleOnChange(selectedPackages) {
    const groupQuantity = {};

    if (selectedPackages) {
      Object.values(selectedPackages).forEach((p) => {
        groupQuantity[p.servicePackageGroupCode] = !groupQuantity[
          p.servicePackageGroupCode
        ]
          ? p.quantity
          : groupQuantity[p.servicePackageGroupCode] + p.quantity;
      });

      let total = 0;
      let subTotal = 0;
      let discount = 0;

      Object.values(selectedPackages).forEach((p) => {
        const consumerQuotation = getConsumerQuotationTierBasedOnQuantity(
          p.consumerQuotations,
          groupQuantity[p.servicePackageGroupCode],
        );
        subTotal += p.unitPrice * p.quantity;
        discount += (p.unitPrice - consumerQuotation.unitPrice) * p.quantity;
      });

      total = subTotal - discount;
      context.setPackages({
        total,
        subTotal,
        discount,
      });
    }
  }

  function handleOnNextStep() {
    props.navigation.navigate('PropertyType');
  }

  return (
    <Container>
      <View style={ServiceRequestStyles.stepContainer}>
        <Steps current={1} total={5} />
      </View>

      <View style={ServiceRequestStyles.titleContainer}>
        <Title>How many units of aircond do you need servicing?</Title>
      </View>

      <Content
        behavior="padding"
        keyboardVerticalOffset={130}
        style={{ flexGrow: 1, padding: 0 }}
      >
        <ScrollView style={ServiceRequestStyles.itemContainer}>
          <ServiceList
            step={2}
            onChange={handleOnChange}
            servicePackages={servicePackages}
          />
        </ScrollView>
      </Content>
      <Footer>
        <View style={ServiceRequestStyles.estimatedContainer}>
          <Text style={ServiceRequestStyles.totalLabel}>Total</Text>

          <View>
            {context.packages.discount > 0 && (
              <Text style={ServiceRequestStyles.originalPrice}>
                RM {priceFormat(context.packages.subTotal)}
              </Text>
            )}
            <CText style={ServiceRequestStyles.estimatedPrice}>
              RM {priceFormat(context.packages.total)}
            </CText>
          </View>
        </View>
        <Button
          onPress={handleOnNextStep}
          disabled={context.packages.total === 0}
        >
          Next
        </Button>
      </Footer>

      <HelpWebScreen
        visible={context.openHelpWeb}
        onClose={() => context.setOpenHelpWeb(false)}
      />
    </Container>
  );
}
