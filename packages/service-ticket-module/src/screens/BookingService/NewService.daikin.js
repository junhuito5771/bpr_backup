/* eslint-disable */
import * as React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { NavigationService } from '@module/utility';
import { HeaderBackButton, HeaderText, HeaderButton } from '@module/daikin-ui';

import {
  getConsumerQuotationTierBasedOnQuantity,
  priceFormat,
} from '../../helpers/utils';
import { ServiceRequestContext } from '../../provider/ServiceRequestProvider';
import Button from '../../shared/Button';
import Container from '../../shared/Container';
import CText from '../../shared/CText';
import Footer from '../../shared/Footer';
import Steps from '../NewService/components/Steps';
import { height } from '../../helpers/themes';
import Content from '../../shared/Content';
import ServiceList from '../NewService/ServiceList.daikin';
import Title from '../../shared/Title';
import HelpWebScreen from '../Help/Help';

export default function DaikinNewService(props) {
  const context = React.useContext(ServiceRequestContext);

  useFocusEffect(() => {
    NavigationService.setOptions(props.navigation, {
      title: <HeaderText>Aircond Service</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={props.navigation} />,
      headerRight: () => (
        <HeaderButton onPress={() => context.setOpenHelpWeb(true)}>
          Help
        </HeaderButton>
      ),
    });
  });

  function handleOnNextStep() {
    props.navigation.navigate('PropertyType');
  }

  function handleOnChange(selectedPackages) {
    const groupQuantity = {};
    if (selectedPackages) {
      Object.values(selectedPackages).forEach((p) => {
        groupQuantity[p.servicePackageGroupCode] = isNaN(
          groupQuantity[p.servicePackageGroupCode],
        )
          ? p.quantity
          : groupQuantity[p.servicePackageGroupCode] + p.quantity;
      });

      let total = 0;
      let subTotal = 0;
      let discount = 0;

      Object.values(selectedPackages).forEach((p) => {
        const consumerQuotation = getConsumerQuotationTierBasedOnQuantity(
          p.consumerQuotations,
          groupQuantity[p.servicePackageGroupCode],
        );
        subTotal += p.unitPrice * p.quantity;
        discount += (p.unitPrice - consumerQuotation.unitPrice) * p.quantity;
      });

      total = subTotal - discount;
      context.setPackages({
        total,
        subTotal,
        discount,
      });
    }
  }

  return (
    <Container>
      <View style={ServiceRequestStyles.stepContainer}>
        <Steps current={1} total={5} />
      </View>

      <View style={ServiceRequestStyles.titleContainer}>
        <Title>How many units of aircond do you need servicing?</Title>
      </View>

      <Content
        behavior="padding"
        keyboardVerticalOffset={130}
        style={{ flexGrow: 1, padding: 0 }}
      >
        <ScrollView style={ServiceRequestStyles.itemContainer}>
          <ServiceList onChange={handleOnChange} />
        </ScrollView>
      </Content>
      <Footer>
        <View style={ServiceRequestStyles.estimatedContainer}>
          <Text style={ServiceRequestStyles.totalLabel}>Total</Text>

          <View>
            {context.packages.discount > 0 && (
              <Text style={ServiceRequestStyles.originalPrice}>
                RM {priceFormat(context.packages.subTotal)}
              </Text>
            )}
            <CText style={ServiceRequestStyles.estimatedPrice}>
              RM {priceFormat(context.packages.total)}
            </CText>
          </View>
        </View>
        <Button
          onPress={handleOnNextStep}
          disabled={context.packages.total === 0}
        >
          Next
        </Button>
      </Footer>

      <HelpWebScreen
        visible={context.openHelpWeb}
        onClose={() => context.setOpenHelpWeb(false)}
      />
    </Container>
  );
}

export const ServiceRequestStyles = StyleSheet.create({
  stepContainer: {
    paddingTop: 16,
    paddingHorizontal: 16,
  },
  itemContainer: {
    flex: 1,
    height,
    marginTop: 10,
  },
  estimatedContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
  totalLabel: {
    fontSize: 16,
    alignSelf: 'flex-end',
  },
  estimatedPrice: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'right',
  },
  originalPrice: {
    textAlign: 'right',
    fontWeight: 'bold',
    textDecorationLine: 'line-through',
  },
  titleContainer: {
    paddingTop: 16,
    paddingHorizontal: 15,
  },
});
