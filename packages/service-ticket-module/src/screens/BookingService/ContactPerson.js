/* eslint-disable */

import * as React from 'react';
import {
  HeaderBackButton as DaikinBackButton,
  HeaderText as DaikinHeaderText,
  HeaderButton,
} from '@module/daikin-ui';
import { BackButton as AcsonBackButton } from '@module/acson-ui';
import { useFocusEffect } from '@react-navigation/native';
import { NavigationService } from '@module/utility';

import { config } from '../../helpers/config';
import { ServiceRequestContext } from '../../provider/ServiceRequestProvider';
import Container from '../../shared/Container';
import { ScrollView, Text, View } from 'react-native';
import { ServiceRequestStyles } from './NewService.daikin';
import Steps from '../NewService/components/Steps';
import Content from '../../shared/Content';
import ServiceContactPerson from '../ServiceContactPerson/ServiceContactPerson';
import Footer from '../../shared/Footer';
import { priceFormat } from '../../helpers/utils';
import CText from '../../shared/CText';
import Title from '../../shared/Title';
import HelpWebScreen from '../Help/Help';
import { ServiceContactPersonContext } from '../../provider/ServiceContactPersonProvider';
import Button from '../../shared/Button';

const ButtonNextSummary = (props) => {
  const context = React.useContext(ServiceContactPersonContext);

  return (
    <Button
      {...props}
      disabled={
        !context.info.prefix ||
        !context.info.fullname ||
        !context.info.email ||
        !context.info.primaryPhoneNumber ||
        !context.info.otp
      }
    >
      Next
    </Button>
  );
};

export default function ContactPerson(props) {
  const context = React.useContext(ServiceRequestContext);

  useFocusEffect(
    React.useCallback(() => {
      if (String(config.company).toLowerCase() === 'daikin') {
        NavigationService.setOptions(props.navigation, {
          title: <DaikinHeaderText>Aircond Service</DaikinHeaderText>,
          headerLeft: () => <DaikinBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => context.setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      } else {
        props.navigation.setOptions({
          headerTitle: 'Aircond Service',
          headerLeft: () => <AcsonBackButton navigation={props.navigation} />,
          headerRight: () => (
            <HeaderButton onPress={() => context.setOpenHelpWeb(true)}>
              Help
            </HeaderButton>
          ),
        });
      }
    }, [props.navigation]),
  );

  function handleOnNextStep() {
    props.navigation.navigate('RequestSummary');
  }

  return (
    <Container>
      <View style={ServiceRequestStyles.stepContainer}>
        <Steps current={5} total={5} />
      </View>

      <View style={ServiceRequestStyles.titleContainer}>
        <Title>Who is the contact person?</Title>
      </View>

      <Content
        behavior="padding"
        keyboardVerticalOffset={130}
        style={{ flexGrow: 1, padding: 0 }}
      >
        <ScrollView style={ServiceRequestStyles.itemContainer}>
          <ServiceContactPerson />
        </ScrollView>
      </Content>
      <Footer>
        <View style={ServiceRequestStyles.estimatedContainer}>
          <Text style={ServiceRequestStyles.totalLabel}>Total</Text>

          <View>
            {context.packages.discount > 0 && (
              <Text style={ServiceRequestStyles.originalPrice}>
                RM {priceFormat(context.packages.subTotal)}
              </Text>
            )}
            <CText style={ServiceRequestStyles.estimatedPrice}>
              RM {priceFormat(context.packages.total)}
            </CText>
          </View>
        </View>
        <ButtonNextSummary onPress={handleOnNextStep} />
      </Footer>

      <HelpWebScreen
        visible={context.openHelpWeb}
        onClose={() => context.setOpenHelpWeb(false)}
      />
    </Container>
  );
}
