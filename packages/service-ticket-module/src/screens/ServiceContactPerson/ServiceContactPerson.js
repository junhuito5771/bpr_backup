/**
 *
 * @description ServiceContactPerson.js
 * @version 1.0.0
 * @since 24 June 2020
 *
 */

import * as React from 'react';
import { View, StyleSheet, Alert, AppState, Platform } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import EncryptedStorage from 'react-native-encrypted-storage';
import { getBrand } from 'react-native-device-info';

import Content from '../../shared/Content';
import Label from '../../shared/Label';
import Input from '../../shared/Input';
import PhoneInput from '../../shared/PhoneInput';
import Button from '../../shared/Button';
import PlaceholderText from '../../shared/PlaceholderText';
import Section from '../../shared/Section';

import { storage, isValidEmail } from '../../helpers/utils';
import { api } from '../../service/api';
import { theme } from '../../helpers/themes';
import Loading from '../../shared/Loading';
import { ServiceContactPersonContext } from '../../provider/ServiceContactPersonProvider';
import { config } from '../../helpers/config';

const PREFIX = [
  { label: 'Mr', value: 'Mr' },
  { label: 'Ms', value: 'Ms' },
  { label: 'Mrs', value: 'Mrs' },
];

storage.save('@callingCode', '+60');
storage.save('@secondaryPhoneCountryCallingCode', '+60');

const { Version } = Platform;

export default function ServiceContactPerson() {
  const [loading, setLoading] = React.useState(false);
  const [countdown, setCountdown] = React.useState(120);
  const [isCountdown, setIsCountdown] = React.useState(false);
  const [remarks, setRemarks] = React.useState('');
  const [countryCallingCode, setCountryCallingCode] = React.useState('60');
  const appState = React.useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = React.useState(
    appState.current,
  );
  const [caretHiddenHack, setCaretHiddenHack] = React.useState(false);

  const context = React.useContext(ServiceContactPersonContext);

  React.useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, []);

  React.useEffect(() => {
    const deviceName = getBrand().toLowerCase();
    if (deviceName === 'xiaomi' && Version > 28) {
      setCaretHiddenHack(true);
    }
  }, []);

  React.useEffect(() => {
    async function init() {
      const _prefix = await storage.load('@namePrefix');
      const _name = await storage.load('@fullname');
      const _email = await storage.load('@email');
      const _phoneNumber = await storage.load('@phoneNumber');
      const _secondaryPhoneNumber = await storage.load('@secondaryPhoneNumber');

      try {
        await EncryptedStorage.removeItem('@remarks');
      } catch (error) {
        console.log(error.code);
      }

      if (_prefix) {
        context.onChangeInfo('prefix', _prefix);
      }

      if (_name) {
        context.onChangeInfo('fullname', _name);
      }

      if (_email) {
        context.onChangeInfo('email', _email);
      }

      if (_phoneNumber) {
        context.onChangeInfo('primaryPhoneNumber', _phoneNumber);
      }

      if (_secondaryPhoneNumber) {
        context.onChangeInfo('secondaryPhoneNumber', _secondaryPhoneNumber);
      }
    }

    init();
  }, []);

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      setCountdown((prev) => {
        if (prev >= 60) {
          return prev;
        }

        return 60;
      });
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
  };

  async function handleOnRequestOTP() {
    setLoading(true);
    setIsCountdown(true);

    const res = await api
      .post(
        'auth/otp-token',
        JSON.stringify({
          phoneNumber: `${countryCallingCode}${context.info.primaryPhoneNumber.trim()}`,
          requestCategory:
            String(config.company).toLowerCase() === 'acson' ? 'AMSS' : 'DMSS',
        }),
        {
          'Content-Type': 'application/json',
        },
      )
      .catch((err) => {
        setLoading(false);

        setTimeout(() => {
          Alert.alert('Error', err.message, [
            {
              text: 'Ok',
              onPress: () => {
                const interval = setInterval(() => {
                  setCountdown((prev) => {
                    if (prev > 0) {
                      const balance = prev - 1;
                      if (balance === 0) {
                        clearInterval(interval);
                        setIsCountdown(false);
                        context.onChangeInfo('otp', '');
                        storage.save('@otp', '');
                        return 120;
                      }

                      return balance;
                    }

                    return 60;
                  });
                }, 1000);
              },
            },
          ]);
        }, 500);
      });

    if (res?.success) {
      const interval = setInterval(() => {
        setCountdown((prev) => {
          const balance = prev - 1;
          if (balance === 0) {
            clearInterval(interval);
            setIsCountdown(false);
            context.onChangeInfo('otp', '');
            storage.save('@otp', '');
            setLoading(false);
            return 120;
          }

          return balance;
        });
      }, 1000);
    } else {
      setIsCountdown(false);
      console.log(res);
    }

    setLoading(false);
  }

  return (
    <View style={{ paddingHorizontal: 16 }}>
      <Section>
        <Label title="Title" />
        <View style={styles.row}>
          <View style={styles.namePrefix}>
            <RNPickerSelect
              placeholder={{
                label: '-',
              }}
              items={PREFIX}
              onValueChange={async (value) => {
                context.onChangeInfo('prefix', value);
                storage.save('@namePrefix', value || '-');
              }}
              value={context.info.prefix}
              Icon={() => (
                <MaterialIcons name="keyboard-arrow-down" size={20} />
              )}
              style={PickerStyles}
              useNativeAndroidPickerStyle={false}
            />
          </View>
          <Input
            placeholder="Full name"
            style={styles.fullname}
            value={context.info.fullname}
            onChangeText={(text) => {
              context.onChangeInfo('fullname', text);
              storage.save('@fullname', text);
            }}
          />
        </View>
      </Section>

      <Section>
        <Label title="Email address" />
        <Input
          keyboardType="email-address"
          placeholder="Enter your email address"
          caretHidden={caretHiddenHack}
          value={context.info.email}
          onChangeText={(text) => {
            context.onChangeInfo('email', text);
            storage.save('@email', text);
          }}
          error={
            context.info.email &&
            !isValidEmail(context.info.email) &&
            'Invalid email address.'
          }
        />
      </Section>

      <Section>
        <Label title="Phone number" />

        <View style={styles.row}>
          <PhoneInput
            keyboardType="phone-pad"
            containerStyle={styles.phoneInput}
            placeholder="Phone number"
            value={context.info.primaryPhoneNumber}
            onChangeText={(text) => {
              const removedLeadingZero = text.replace(/^0*/, '');

              context.onChangeInfo('primaryPhoneNumber', removedLeadingZero);
              storage.save('@phoneNumber', removedLeadingZero);
            }}
            onSelect={(callingCode) => {
              setCountryCallingCode(callingCode);
              storage.save('@callingCode', `+${callingCode}`);
            }}
          />
          <Button
            labelStyle={
              context.info.primaryPhoneNumber.match(/\d{9,10}/g) && !isCountdown
                ? styles.enabledRequestLabel
                : styles.disabledRequestLabel
            }
            style={[
              styles.requestOTPBtn,
              context.info.primaryPhoneNumber.match(/\d{9,10}/g) && !isCountdown
                ? styles.enabledRequest
                : styles.disabledRequest,
            ]}
            onPress={handleOnRequestOTP}
            disabled={
              !context.info.primaryPhoneNumber.match(/\d{9,10}/g) || isCountdown
            }
          >
            {(isCountdown && `${countdown} sec`) || 'Request OTP'}
          </Button>
        </View>
      </Section>

      <Section>
        <Label title="OTP code" />
        <Input
          placeholder="Enter OTP"
          value={context.info.otp}
          onChangeText={(text) => {
            context.onChangeInfo('otp', text);
            storage.save('@otp', text);
          }}
        />
      </Section>

      <Section>
        <Label title="Secondary phone number" />
        <PlaceholderText style={styles.placeholder}>
          We will contact this person, just in case we could not contact the
          primary contact
        </PlaceholderText>
        <PhoneInput
          keyboardType="phone-pad"
          containerStyle={styles.phoneInput}
          placeholder="Phone number"
          value={context.info.secondaryPhoneNumber}
          onChangeText={(text) => {
            context.onChangeInfo('secondaryPhoneNumber', text);
            storage.save('@secondaryPhoneNumber', text);
          }}
          onSelect={(countryCallingCode) => {
            storage.save(
              '@secondaryPhoneCountryCallingCode',
              `+${countryCallingCode}`,
            );
          }}
        />
      </Section>

      <Section>
        <Label title="Remarks" />
        <Input
          multiline
          numberOfLines={4}
          placeholder="Enter your remarks here"
          value={remarks}
          onChangeText={(text) => {
            setRemarks(text);
            storage.save('@remarks', text);
          }}
        />
      </Section>
      <Loading visible={loading} />
    </View>
  );
}

const styles = StyleSheet.create({
  section: {
    marginBottom: 15,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  namePrefix: {
    flex: 0.3,
    marginRight: 10,
  },
  fullname: {
    flex: 0.7,
  },
  phoneInput: {
    flex: 0.7,
  },
  disabledRequestLabel: {
    color: '#aaa',
  },
  enabledRequestLabel: {
    color: theme.primary,
  },
  disabledRequest: {
    borderColor: '#ccc',
  },
  enabledRequest: {
    borderColor: theme.primary,
  },
  requestOTPBtn: {
    borderWidth: 1,
    backgroundColor: 'transparent',
    marginLeft: 10,
    flex: 0.3,
    height: 50,
  },
  placeholder: {
    paddingVertical: 8,
  },
});

const PickerStyles = StyleSheet.create({
  inputIOS: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    color: 'black',
  },
  inputAndroid: {
    borderColor: '#ccc',
    borderWidth: 1,
    height: 50,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    color: 'black',
  },
  iconContainer: {
    top: 5,
    right: 0,
    height: 50,
    justifyContent: 'center',
  },
});
