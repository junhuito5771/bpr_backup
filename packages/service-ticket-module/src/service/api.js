import { Alert } from 'react-native';
import Config from 'react-native-config';
import axios from 'axios';

let authToken = null;
let handleError = false;
let authFunction = null;
export const setAuthToken = (token) => (authToken = token);
export const setGetAuthTokenFunc = (f) => (authFunction = f);

export const createInstance = async () => {
  const token = await getToken();
  new axios({
    baseURL: Config.API_URL,
    headers: {
      Authorization: token,
    },
  });
};

const timeout = (promise: Promise, timeoutMillseconds = 100000) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new Error('Request timeout'));
    }, timeoutMillseconds);

    promise.then(resolve, reject);
  });
};

const getBaseUrl = () => {
  return Config.SERVICEHUB_API_URL || Config.API_URL;
};

const getToken = async () => {
  if (authFunction) {
    return await authFunction();
  }

  if (authToken) {
    return Config.SERVICEHUB_API_URL ? `Bearer ${authToken}` : `${authToken}`;
  }
  return '';
};

export const api = {
  get: async (endpoint, headers = {}) => {
    const token = await getToken();
    return timeout(
      fetch(`${getBaseUrl()}/${endpoint}`, {
        method: 'GET',
        headers: {
          ...headers,
          Authorization: token,
        },
      }).then(async (res) => {
        if (res.status !== 200 && !handleError) {
          handleError = true;
          Alert.alert(
            'Error',
            'Something went wrong. Please try again later.',
            [
              {
                text: 'Ok',
                onPress: () => {
                  handleError = false;
                },
              },
            ],
          );
        }

        const response = await res.json();
        if (__DEV__) {
          console.log(response);
          console.log('URL', `${getBaseUrl()}/${endpoint}`);
        }

        return response;
      }),
    );
  },
  post: async (endpoint, body = {}, headers = {}) => {
    const token = await getToken();
    return timeout(
      fetch(`${getBaseUrl()}/${endpoint}`, {
        method: 'POST',
        headers: {
          ...headers,
          Authorization: token,
        },
        body,
      }).then(async (res) => {
        console.log('res', res);
        const response = await res.json();

        if (__DEV__) {
          console.log(response);
          console.log('BASE_URL', getBaseUrl());
        }

        if (response.statusCode >= 400) {
          throw new Error(response.message);
        }

        return response;
      }),
    );
  },
};

export const OTP_TOKEN_EXPIRED = 'OTP_TOKEN_EXPIRED';
export const NOT_SPECIFIED = 'NOT_SPECIFIED';
export const TOO_MANY_OTP_TOKEN_REQUEST = 'TOO_MANY_OTP_TOKEN_REQUEST';
