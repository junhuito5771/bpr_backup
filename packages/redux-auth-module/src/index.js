// eslint-disable-next-line import/no-unresolved
import { connect } from 'react-redux';
import reducers from './reducers';
import sagas from './sagas';

import configuration from './configuration';

// eslint-disable-next-line import/no-mutable-exports
let dispatch;

if (configuration && configuration.store && configuration.store.dispatch) {
  dispatch = configuration.store.dispatch;
}

export { reducers, connect, sagas, dispatch, configuration };
export * from './actions';
