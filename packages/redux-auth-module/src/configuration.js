import { Auth, Analytic } from '@module/utility';

// window.LOG_LEVEL = 'DEBUG';

class Configuration {
  constructor() {
    if (!Configuration.instance) {
      this.apiEndpoint = '';
      Configuration.instance = this;
    }
  }

  init = (config, store) => {
    // Loop through the config and assign the key and value to the configuration
    Object.keys(config).forEach(key => {
      this[key] = config[key];
    });

    if (store) {
      this.store = store;
    }

    this.check();

    this.cognitoUserPool = Auth.configure({
      identityPoolId: config.cognitoIdentifyPoolId,
      userPoolId: config.cognitoUserPoolID,
      userPoolWebClientId: config.cognitoClientId,
      region: config.region,
    });

    Analytic.configure({
      disabled: false,
      autoSessionRecord: true,
      AWSPinpoint: {
        appId: config.pinpointId,
        region: config.pinpointRegion || config.region,
      },
    });

    // Do not allow this configuration to be edited once it has been init
    Object.freeze(Configuration.instance);
  };

  check() {
    if (
      !this.cognitoIdentifyPoolId ||
      !this.cognitoUserPoolID ||
      !this.cognitoClientId ||
      !this.region
    ) {
      throw new Error('Missing authentication configuration');
    }
  }

  getCognitoUserPool = () => this.cognitoUserPool;
}

const config = new Configuration();

export default config;
