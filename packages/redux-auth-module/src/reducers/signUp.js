import {
  SET_SIGN_UP_PARAMS,
  BEGIN_SIGN_UP_LOADING,
  SET_SIGN_UP_STATUS,
  CLEAR_SIGN_UP_MSG,
  BEGIN_CONFIRM_LOADING,
  SET_CONFIRM_STATUS,
  END_CONFIRM_LOADING,
  CLEAR_CONFIRM_SUCCESS_MSG,
  CLEAR_CONFIRM_ERROR_MSG,
  RESEND_LOADING,
  SET_RESEND_STATUS,
  CLEAR_RESEND_CODE_MSG,
} from '../actions/signUp';

const initialState = {
  isLoading: false,
  success: '',
  error: '',
  signUpParams: {
    email: '',
  },
  confirmSuccess: '',
  confirmError: '',
  isResendLoading: false,
  resendSuccess: '',
  resendError: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SIGN_UP_PARAMS:
      return {
        ...state,
        signUpParams: action.signUpParams,
      };
    case BEGIN_SIGN_UP_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case SET_SIGN_UP_STATUS:
      return {
        ...state,
        isLoading: false,
        [action.payload.messageType]: action.payload.message,
      };
    case CLEAR_SIGN_UP_MSG:
      return {
        ...state,
        success: '',
        error: '',
      };
    case RESEND_LOADING:
      return {
        ...state,
        isResendLoading: action.isResendLoading,
      };
    case SET_RESEND_STATUS: {
      return {
        ...state,
        [action.payload.messageType]: action.payload.message,
      };
    }
    case BEGIN_CONFIRM_LOADING:
      return {
        ...state,
        confirmIsLoading: true,
      };
    case SET_CONFIRM_STATUS:
      return {
        ...state,
        confirmIsLoading: false,
        [action.payload.messageType]: action.payload.message,
      };
    case END_CONFIRM_LOADING:
      return {
        ...state,
        confirmIsLoading: false,
      };
    case CLEAR_CONFIRM_SUCCESS_MSG:
      return {
        ...state,
        confirmSuccess: '',
      };
    case CLEAR_CONFIRM_ERROR_MSG:
      return {
        ...state,
        confirmError: '',
        resendSuccess: '',
        resendError: '',
      };
    case CLEAR_RESEND_CODE_MSG:
      return {
        ...state,
        resendSuccess: '',
        resendError: '',
      };
    default:
      return state;
  }
};

export default reducer;
