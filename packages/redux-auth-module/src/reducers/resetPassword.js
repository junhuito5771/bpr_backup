import {
  BEGIN_RESET_PASSWORD_LOADING,
  SET_RESET_PASSWORD_STATUS,
  CLEAR_RESET_PASSWORD_STATUS,
} from '../actions/resetPassword';

const initialState = {
  isLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case BEGIN_RESET_PASSWORD_LOADING:
      return {
        ...state,
        isLoading: true,
        success: '',
        error: '',
      };
    case SET_RESET_PASSWORD_STATUS:
      return {
        ...state,
        isLoading: false,
        [action.payload.messageType]: action.payload.message,
      };
    case CLEAR_RESET_PASSWORD_STATUS:
      return {
        ...state,
        error: '',
        success: '',
      };
    default:
      return state;
  }
};

export default reducer;
