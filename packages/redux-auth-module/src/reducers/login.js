import {
  SET_LOGIN_LOADING,
  SET_LOGIN_STATUS,
  CLEAR_LOGIN_STATUS,
  CLEAR_ERROR_CODE,
  SET_IS_LOGIN,
} from '../actions/login';

import { SUBMIT_LOGOUT_PARAMS } from '../actions/user';

const initialState = {
  isLoading: false,
  error: '',
  success: '',
  alertTitle: '',
  isLogin: false,
  errorCode: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_LOGIN_STATUS: {
      const defaultTitle =
        action.loginStatusParams.messageType === 'success'
          ? 'Success'
          : 'Error';
      return {
        ...state,
        success: '',
        error: '',
        errorCode: action.loginStatusParams.errorCode,
        [action.loginStatusParams.messageType]:
          action.loginStatusParams.message,
        alertTitle: action.loginStatusParams.title || defaultTitle,
        isLogin: action.loginStatusParams.messageType === 'success',
      };
    }
    case CLEAR_LOGIN_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    case CLEAR_ERROR_CODE:
      return {
        ...state,
        errorCode: '',
      };
    case SET_IS_LOGIN: {
      return {
        ...state,
        isLogin: action.isLogin,
      };
    }
    default:
      return state;
  }
};

export default reducer;
