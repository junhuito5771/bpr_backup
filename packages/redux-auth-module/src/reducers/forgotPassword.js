import {
  BEGIN_FORGOT_PASSWORD_LOADING,
  SET_FORGOT_PASSWORD_STATUS,
  CLEAR_FORGOT_PASSWORD_STATUS,
} from '../actions/forgotPassword';

const initialState = {
  isLoading: false,
  success: '',
  error: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case BEGIN_FORGOT_PASSWORD_LOADING:
      return {
        ...state,
        isLoading: true,
        success: '',
        error: '',
      };
    case SET_FORGOT_PASSWORD_STATUS:
      return {
        ...state,
        isLoading: false,
        [action.payload.messageType]: action.payload.message,
      };
    case CLEAR_FORGOT_PASSWORD_STATUS:
      return {
        ...state,
        error: '',
        success: '',
      };
    default:
      return state;
  }
};

export default reducer;
