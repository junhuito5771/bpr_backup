import {
  SET_ID_TOKEN,
  SET_USER_PROFILE,
  SET_AUTO_LOGIN_PARAMS,
  SET_USER_STATUS,
  CLEAR_USER_STATUS,
  CLEAR_USER_PROFILE,
  SET_USER_IS_LOADING,
  CLEAR_LOGOUT_PARAMS,
  SET_IOT_POLICY,
} from '../actions/user';

const initialState = {
  credential: {
    idToken: '',
    refreshToken: '',
    lastTokenRefreshedDT: null,
  },
  profile: {
    name: '',
    email: '',
    phoneNo: '',
  },
  isAutoLogin: false,
  hasRefreshToken: false,
  attachIOTPolicy: false,
  // Status used to fetch user profile and also update user profile
  success: '',
  error: '',
  isLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ID_TOKEN:
      return {
        ...state,
        credential: {
          idToken: action.tokenParams.idToken,
          refreshToken: action.tokenParams.refreshToken,
          lastTokenRefreshedDT: action.tokenParams.lastTokenRefreshedDT,
        },
      };
    case SET_USER_PROFILE:
      return {
        ...state,
        profile: {
          name: action.userProfileParams.name,
          email: action.userProfileParams.email,
          phoneNo: action.userProfileParams.phoneNo,
        },
      };
    case SET_AUTO_LOGIN_PARAMS:
      return {
        ...state,
        isAutoLogin: action.autoLoginParams.isAutoLogin,
        hasRefreshToken: action.autoLoginParams.hasRefreshToken,
      };
    case SET_USER_STATUS:
      return {
        ...state,
        [action.userParams.messageType]: action.userParams.message,
      };
    case CLEAR_USER_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    case CLEAR_USER_PROFILE:
      return {
        ...initialState,
        isAutoLogin: state.isAutoLogin,
      };
    case CLEAR_LOGOUT_PARAMS: {
      return {
        ...initialState,
        isAutoLogin: state.isAutoLogin,
        profile: {
          ...initialState.profile,
          email: state.isAutoLogin ? state.profile.email : '',
        },
      };
    }
    case SET_IOT_POLICY: {
      return {
        ...state,
        attachIOTPolicy: action.status,
      };
    }
    case SET_USER_IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    default:
      return state;
  }
};

export default reducer;
