/* eslint-disable import/no-unresolved */
import { persistReducer, getStoredState } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import EncryptedStorage from 'react-native-encrypted-storage';

import login from './login';
import signUp from './signUp';
import user from './user';
import resetPassword from './resetPassword';
import forgotPassword from './forgotPassword';

const migrate = async state => {
  if (
    state === null ||
    state === undefined ||
    (state && Object.keys(state).length === 0)
  ) {
    try {
      const asyncState = await getStoredState({
        key: 'user',
        storage: AsyncStorage,
      });
      if (
        !(
          asyncState === null ||
          asyncState === undefined ||
          (asyncState && Object.keys(asyncState).length === 0)
        )
      ) {
        return asyncState;
      }
    } catch (getStateError) {
      // Empty
    }
  }
  return state;
};

const loginPersistConfig = {
  key: 'login',
  storage: AsyncStorage,
  whitelist: ['isLogin'],
  migrate,
};

const userPersistConfig = {
  key: 'user',
  storage: EncryptedStorage,
  blacklist: ['success', 'error', 'isLoading'],
  migrate,
};

const rootReducer = {
  login: persistReducer(loginPersistConfig, login),
  resetPassword,
  signUp,
  forgotPassword,
  user: persistReducer(userPersistConfig, user),
};

export default rootReducer;
/* eslint-enable import/no-unresolved */
