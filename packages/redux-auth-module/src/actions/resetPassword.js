export const SUBMIT_RESET_PASSWORD_PARAMS = 'SUBMIT_RESET_PASSWORD_PARAMS';
export const BEGIN_RESET_PASSWORD_LOADING = 'BEGIN_RESET_PASSWORD_LOADING';
export const SET_RESET_PASSWORD_STATUS = 'SET_RESET_PASSWORD_STATUS';
export const CLEAR_RESET_PASSWORD_STATUS = 'CLEAR_RESET_PASSWORD_STATUS';

export const submitResetPasswordParams = resetPasswordParams => ({
  type: SUBMIT_RESET_PASSWORD_PARAMS,
  resetPasswordParams,
});

export const beginResetPasswordLoading = () => ({
  type: BEGIN_RESET_PASSWORD_LOADING,
});

export const clearResetPasswordStatus = () => ({
  type: CLEAR_RESET_PASSWORD_STATUS,
});
