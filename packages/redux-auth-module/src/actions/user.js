export const SET_ID_TOKEN = 'SET_ID_TOKEN';
export const REFRESH_ID_TOKEN = 'REFRESH_ID_TOKEN';
export const FETCH_USER_PROFILE = 'FETCH_USER_PROFILE';
export const SET_USER_PROFILE = 'SET_USER_PROFILE';
export const SET_AUTO_LOGIN_PARAMS = 'SET_AUTO_LOGIN_PARAMS';
export const SET_USER_STATUS = 'SET_USER_STATUS';
export const CLEAR_USER_STATUS = 'CLEAR_USER_STATUS';
export const CLEAR_USER_PROFILE = 'CLEAR_USER_PROFILE';
export const CLEAR_LOGOUT_PARAMS = 'CLEAR_LOGOUT_PARAMS';
export const SUBMIT_LOGOUT_PARAMS = 'SUBMIT_LOGOUT_PARAMS';
export const SUBMIT_USER_PARAMS = 'SUBMIT_USER_PARAMS';
export const SET_USER_IS_LOADING = 'SET_IS_LOADING';
export const SET_IOT_POLICY = 'SET_IOT_POLICY';
export const SUBMIT_ATTACH_POLICY = 'SUBMIT_ATTACH_POLICY';
export const SUBMIT_APP_INFO = 'SUBMIT_APP_INFO';

export const setIDToken = tokenParams => ({
  type: SET_ID_TOKEN,
  tokenParams,
});

export const refreshToken = () => ({
  type: REFRESH_ID_TOKEN,
});

export const fetchUserProfile = userProfileParams => ({
  type: FETCH_USER_PROFILE,
  userProfileParams,
});

export const setUserProfile = userProfileParams => ({
  type: SET_USER_PROFILE,
  userProfileParams,
});

export const setAutoLoginParams = autoLoginParams => ({
  type: SET_AUTO_LOGIN_PARAMS,
  autoLoginParams,
});

export const setUserStatus = userParams => ({
  type: SET_USER_STATUS,
  userParams,
});

export const clearUserStatus = () => ({
  type: CLEAR_USER_STATUS,
});

export const clearUserProfile = () => ({
  type: CLEAR_USER_PROFILE,
});

export const logOut = () => ({
  type: SUBMIT_LOGOUT_PARAMS,
});

export const clearLogoutParams = () => ({
  type: CLEAR_LOGOUT_PARAMS,
});

export const submitUserParams = userProfileParams => ({
  type: SUBMIT_USER_PARAMS,
  userProfileParams,
});

export const setIsLoading = isLoading => ({
  type: SET_USER_IS_LOADING,
  isLoading,
});

export const setIoTPolicy = status => ({
  type: SET_IOT_POLICY,
  status,
});

export const submitAttachPolicy = attachPolicyParams => ({
  type: SUBMIT_ATTACH_POLICY,
  attachPolicyParams,
});

export const submitAppInfo = appInfoParams => ({
  type: SUBMIT_APP_INFO,
  appInfoParams,
});
