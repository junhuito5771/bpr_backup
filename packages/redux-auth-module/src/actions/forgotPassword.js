export const SUBMIT_FORGOT_PASSWORD_PARAMS = 'SUBMIT_FORGOT_PASSWORD_PARAMS';
export const BEGIN_FORGOT_PASSWORD_LOADING = 'BEGIN_FORGOT_PASSWORD_LOADING';
export const SET_FORGOT_PASSWORD_STATUS = 'SET_FORGOT_PASSWORD_STATUS';
export const CLEAR_FORGOT_PASSWORD_STATUS = 'CLEAR_FORGOT_PASSWORD_STATUS';

export const submitForgotPasswordParams = forgotPasswordParams => ({
  type: SUBMIT_FORGOT_PASSWORD_PARAMS,
  forgotPasswordParams,
});

export const beginForgotPasswordLoading = () => ({
  type: BEGIN_FORGOT_PASSWORD_LOADING,
});

export const clearForgotPasswordStatus = () => ({
  type: CLEAR_FORGOT_PASSWORD_STATUS,
});
