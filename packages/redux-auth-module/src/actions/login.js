export const SUBMIT_LOGIN_PARAMS = 'SUBMIT_LOGIN_PARAMS';
export const SET_LOGIN_LOADING = 'SET_LOGIN_LOADING';
export const SET_LOGIN_STATUS = 'SET_LOGIN_STATUS';
export const AUTO_LOGIN = 'AUTO_LOGIN';
export const REFRESH_TOKEN = 'REFRESH_TOKEN';
export const CLEAR_LOGIN_STATUS = 'CLEAR_LOGIN_STATUS';
export const CLEAR_ERROR_CODE = 'CLEAR_ERROR_CODE';
export const SET_IS_LOGIN = 'SET_IS_LOGIN';

export const submitLoginParams = loginParams => ({
  type: SUBMIT_LOGIN_PARAMS,
  loginParams,
});

export const setLoginLoading = isLoading => ({
  type: SET_LOGIN_LOADING,
  isLoading,
});

export const clearLoginStatus = () => ({
  type: CLEAR_LOGIN_STATUS,
});

export const clearErrorCode = () => ({
  type: CLEAR_ERROR_CODE,
});

export const autoLogin = config => ({
  type: AUTO_LOGIN,
  config,
});

export const refreshToken = refreshTokenParams => ({
  type: REFRESH_TOKEN,
  refreshTokenParams,
});

export const setLoginStatus = loginStatusParams => ({
  type: SET_LOGIN_STATUS,
  loginStatusParams,
});

export const setIsLogin = isLogin => ({
  type: SET_IS_LOGIN,
  isLogin,
});
