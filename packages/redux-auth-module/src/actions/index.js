import * as login from './login';
import * as forgotPassword from './forgotPassword';
import * as resetPassword from './resetPassword';
import * as signUp from './signUp';
import * as userProfile from './user';

export { login, forgotPassword, resetPassword, signUp, userProfile };
