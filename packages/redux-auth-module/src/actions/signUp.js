export const SUBMIT_SIGN_UP_PARAMS = 'SUBMIT_SIGN_UP_PARAMS';
export const SET_SIGN_UP_PARAMS = 'SET_SIGN_UP_PARAMS';
export const BEGIN_SIGN_UP_LOADING = 'BEGIN_SIGN_UP_LOADING';
export const SET_SIGN_UP_STATUS = 'SET_SIGN_UP_STATUS';
export const CLEAR_SIGN_UP_MSG = 'CLEAR_SIGN_UP_MSG';

export const RESEND_LOADING = 'RESEND_LOADING';
export const SUBMIT_CONFIRM_PARAMS = 'SUBMIT_CONFIRM_PARAMS';
export const RESEND_VERIFICATION_CODE = 'RESEND_VERIFICATION_CODE';
export const SET_RESEND_STATUS = 'SET_RESEND_STATUS';
export const CLEAR_RESEND_CODE_MSG = 'CLEAR_RESEND_CODE_MSG';

export const BEGIN_CONFIRM_LOADING = 'BEGIN_CONFIRM_LOADING';
export const END_CONFIRM_LOADING = 'END_CONFIRM_LOADING';
export const SET_CONFIRM_STATUS = 'SET_CONFIRM_STATUS';
export const CLEAR_CONFIRM_SUCCESS_MSG = 'CLEAR_CONFIRM_SUCCESS_MSG';
export const CLEAR_CONFIRM_ERROR_MSG = 'CLEAR_CONFIRM_ERROR_MSG';

export const submitSignUpParams = signUpParams => ({
  type: SUBMIT_SIGN_UP_PARAMS,
  signUpParams,
});

export const setSignUpParams = signUpParams => ({
  type: SET_SIGN_UP_PARAMS,
  signUpParams,
});

export const beginSignUpLoading = () => ({
  type: BEGIN_SIGN_UP_LOADING,
});

export const clearSignUpMsg = () => ({
  type: CLEAR_SIGN_UP_MSG,
});

export const submitConfirmParams = (
  confirmParams,
  navigationService,
  loginScreen
) => ({
  type: SUBMIT_CONFIRM_PARAMS,
  confirmParams,
  navigationService,
  loginScreen,
});

export const resendLoading = isResendLoading => ({
  type: RESEND_LOADING,
  isResendLoading,
});

export const resendVerificationCode = () => ({
  type: RESEND_VERIFICATION_CODE,
});

export const clearResendCodeMsg = () => ({
  type: CLEAR_RESEND_CODE_MSG,
});

export const beginConfirmLoading = () => ({
  type: BEGIN_CONFIRM_LOADING,
});

export const endConfirmLoading = () => ({
  type: END_CONFIRM_LOADING,
});

export const clearConfirmErrorMsg = () => ({
  type: CLEAR_CONFIRM_ERROR_MSG,
});

export const clearConfirmSuccessMsg = () => ({
  type: CLEAR_CONFIRM_SUCCESS_MSG,
});
