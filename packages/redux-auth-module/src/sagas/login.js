import { call, put, select, takeLatest } from 'redux-saga/effects';

import { Auth } from '@module/utility';

import {
  setLoginStatus,
  setLoginLoading,
  SUBMIT_LOGIN_PARAMS,
} from '../actions/login';
import { setAutoLoginParams, setIDToken, logOut } from '../actions/user';
import { setSignUpParams } from '../actions/signUp';
// import {
//   setGroups,
//   setGroupUnits,
//   clearUnitsStatus,
//   getAllUnits,
// } from '../../../redux-module/src/actions/units';
import fetchUserProfile from './fetchUser';
import attachIoTPolicy from './attachIoTPolicy';

async function login(username, password) {
  const result = await Auth.signIn(username, password);

  return {
    refreshToken: result.signInUserSession.refreshToken.token,
    idToken: result.signInUserSession.idToken.jwtToken,
  };
}

export function* submitLogin(action) {
  yield put(setSignUpParams({ email: action.loginParams.email }));

  const loginTimeStamp = Math.floor(Date.now() / 1000);

  const response = yield call(
    login,
    action.loginParams.email,
    action.loginParams.password
  );

  // Update the auto login params
  yield put(
    setAutoLoginParams({
      isAutoLogin: action.loginParams.isRemember,
      hasRefreshToken: !!response.refreshToken,
    })
  );

  // Update the id token
  yield put(
    setIDToken({
      idToken: response.idToken,
      refreshToken: response.refreshToken,
      lastTokenRefreshedDT: loginTimeStamp,
    })
  );

  const credential = yield call(Auth.currentCredentials);

  yield call(attachIoTPolicy, {
    username: action.loginParams.email,
    identityID: credential.IdentityId,
  });

  // yield put(getAllUnits({ email: action.loginParams.email }));
  // yield put(setGroups({ groups: {}, lastGroupIndex: 0 }));
  // yield put(setGroupUnits({}));
  // yield put(clearUnitsStatus());

  yield call(fetchUserProfile, {
    fetchUserProfileParams: { email: action.loginParams.email },
  });

  const {
    profile: { name: userName },
    error,
  } = yield select(state => state.user);

  const isLoginSuccess = userName && !error;

  if (!isLoginSuccess) {
    // Log out the user if user's token is invalid
    yield put(logOut());
  }

  return isLoginSuccess;
}

function* submitLoginForm(action) {
  try {
    yield put(setLoginLoading(true));
    const isLoginSuccess = yield call(submitLogin, action);

    if (isLoginSuccess) {
      yield put(
        setLoginStatus({
          messageType: 'success',
          message: 'success',
        })
      );
    } else {
      throw new Error('Invalid credential. Please try again');
    }
  } catch (error) {
    yield put(
      setLoginStatus({
        messageType: 'error',
        message: error.message,
        errorCode: error.code,
      })
    );
  } finally {
    yield put(setLoginLoading(false));
  }
}

export default function* root() {
  yield takeLatest(SUBMIT_LOGIN_PARAMS, submitLoginForm);
}
