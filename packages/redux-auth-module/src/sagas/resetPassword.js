import { call, put, takeLatest } from 'redux-saga/effects';
import { Auth } from '@module/utility';

import {
  BEGIN_RESET_PASSWORD_LOADING,
  SET_RESET_PASSWORD_STATUS,
  SUBMIT_RESET_PASSWORD_PARAMS,
} from '../actions/resetPassword';

async function resetPassword({ username, code, password }) {
  await Auth.forgotPasswordSubmit(username, code, password);
}

function* submitResetPasswordForm(action) {
  try {
    yield put({
      type: BEGIN_RESET_PASSWORD_LOADING,
    });

    const requestData = {
      username: action.resetPasswordParams.email,
      code: action.resetPasswordParams.code,
      password: action.resetPasswordParams.password,
    };

    yield call(resetPassword, requestData);

    yield put({
      type: SET_RESET_PASSWORD_STATUS,
      payload: {
        messageType: 'success',
        message:
          'Your password has been changed. You can now log in with your new credentials.',
      },
    });
  } catch (error) {
    yield put({
      type: SET_RESET_PASSWORD_STATUS,
      payload: {
        messageType: 'error',
        message: error.message,
      },
    });
  }
}

export default function* rootSaga() {
  yield takeLatest(SUBMIT_RESET_PASSWORD_PARAMS, submitResetPasswordForm);
}
