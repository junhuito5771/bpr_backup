import { call, put, select } from 'redux-saga/effects';
import { ApiService, Auth } from '@module/utility';

import { setIoTPolicy } from '../actions/user';

export default function* attachIoTPolicy(action) {
  try {
    const { username, identityID } = action;

    const { idToken } = yield select(state => state.user.credential);

    if (!idToken) throw new Error('Missing Authentication token');

    const response = yield call(ApiService.post, 'attachpolicy', {
      requestData: {
        username,
        identityID,
      },
    });

    const isAttachPolicy =
      response && response.message && response.message === 'success';

    yield put(setIoTPolicy(isAttachPolicy));
  } catch (error) {
    yield put(setIoTPolicy(false));
  }
}

export function* autoAttachIoTPolicy() {
  const { email } = yield select(state => state.user.profile);

  const credential = yield call(Auth.currentCredentials);

  yield call(attachIoTPolicy, {
    username: email,
    identityID: credential.identityId,
  });
}
