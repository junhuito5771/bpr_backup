import { call, put } from 'redux-saga/effects';
import { ApiService, CrashReportHelper } from '@module/utility';

import {
  clearUserStatus,
  setUserStatus,
  setUserProfile,
} from '../actions/user';

export function* fetchUserProfileAPI(action) {
  // Fetch user
  const userData = yield call(ApiService.post, 'getuserprofile', {
    requestData: {
      type: 1,
      value: action.fetchUserProfileParams.email,
    },
  });

  console.log("USER DATA ===================>");
  console.log(userData);

  const { name, email, phoneNo, userID } = userData.user;

  // Update user
  yield put(
    setUserProfile({
      name,
      email,
      phoneNo: phoneNo
        .replace(`${userData.user.countryCode}`, '')
        .replace(/(\d{2})(\d{7})/, '$1-$2'),
      userID,
    })
  );

  CrashReportHelper.setUser({ email, name, id: email });

  return email;
}

export default function* fetchUserProfile(action) {
  try {
    // Clear the user status
    yield put(clearUserStatus());

    const email = yield call(fetchUserProfileAPI, action);

    if (email) {
      yield put(setUserStatus({ messageType: 'success', message: 'success' }));
    } else {
      yield put(
        setUserStatus({
          messageType: 'error',
          message: 'Error while trying to fetch user profile',
        })
      );
    }
  } catch (error) {
    yield put(
      setUserStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  }
}
