import { call, put, takeLatest } from 'redux-saga/effects';
import { Auth } from '@module/utility';

import {
  BEGIN_FORGOT_PASSWORD_LOADING,
  SET_FORGOT_PASSWORD_STATUS,
  SUBMIT_FORGOT_PASSWORD_PARAMS,
} from '../actions/forgotPassword';

async function forgotPassword(username) {
  await Auth.forgotPassword(username);
}

function* submitForgotPWForm(action) {
  try {
    yield put({
      type: BEGIN_FORGOT_PASSWORD_LOADING,
    });

    const username = action.forgotPasswordParams.email;
    yield call(forgotPassword, username);

    yield put({
      type: SET_FORGOT_PASSWORD_STATUS,
      payload: {
        messageType: 'success',
        message: 'A new verification code has been sent to your email address',
      },
    });
  } catch (error) {
    
	let msg = error.message;
	if(msg.match(/combination/g) && msg.match(/found/g)){
	  msg = "Email address is not found.";
	}
	
    yield put({
      type: SET_FORGOT_PASSWORD_STATUS,
      payload: {
        messageType: 'error',
        message: msg,
      },
    });
  }
}

export default function* rootSaga() {
  yield takeLatest(SUBMIT_FORGOT_PASSWORD_PARAMS, submitForgotPWForm);
}
