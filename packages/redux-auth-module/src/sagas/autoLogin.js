import {
  call,
  put,
  select,
  delay,
  takeLatest,
  retry,
  // eslint-disable-next-line import/no-unresolved
} from 'redux-saga/effects';
import { NetworkHelper, Auth, AnalyticHelper, setupApi } from '@module/utility';

import { setIDToken, logOut, clearUserStatus } from '../actions/user';
import { setLoginStatus, AUTO_LOGIN } from '../actions/login';

import { fetchUserProfileAPI } from './fetchUser';
import configuration from '../configuration';
import { getStore } from '../../../redux-module/src/store';

const ERROR_NO_INTERNET = 'ERROR_NO_INTERNET';
const ERROR_AUTO_LOGIN_FAIL = 'ERROR_AUTO_LOGIN_FAIL';
const ERROR_NO_TOKEN = 'ERROR_NO_TOKEN';
const ERROR_NO_CREDENTIAL = 'ERROR_NO_CREDENTIAL';

function initCognitoConfig() {
  // Set the api configuration
  setupApi({
    getTokenParams: () => getStore().getState().user.credential,
    awsClientId: configuration.cognitoClientId,
    awsEndpoint: configuration.cognitoEndpoint,
    apiEndpoint: configuration.apiEndpoint,
    onRefreshedToken: refreshedIdToken => {
      getStore().dispatch(
        setIDToken({
          idToken: refreshedIdToken,
          refreshToken: getStore().getState().user.credential.refreshToken,
          lastTokenRefreshedDT: Date.now() / 1000,
        })
      );
    },
  });

  AnalyticHelper.syncDeviceInfo();
}

function getCurrentUserCredential() {
  return new Promise((resolve, reject) => {
    Auth.currentSession()
      .then(({ accessToken }) => {
        resolve(accessToken.jwtToken);
      })
      .catch(error => reject(new Error(error)));
  });
}

function* retryCheckUserCredential() {
  try {
    const currentCrendetial = yield retry(3, 3000, getCurrentUserCredential);

    return currentCrendetial;
  } catch (error) {
    // Empty error block
  }

  throw new Error(ERROR_NO_CREDENTIAL);
}

function getAlertTitleAndErrMsg(error) {
  const errorKey = error && error.message ? error.message : '';
  switch (errorKey) {
    case ERROR_NO_INTERNET:
      return {
        title: 'No internet connection',
        message:
          'You have been logged out automatically due to lost of Internet connection. Please ensure your phone has active Internet connection.',
      };
    case ERROR_AUTO_LOGIN_FAIL:
      return {
        title: 'Auto login failure',
        message: 'Current credential has been expired.',
      };
    case ERROR_NO_TOKEN:
      return {
        title: 'Auto login failure',
        message: 'No credential is found in storage.',
      };
    case ERROR_NO_CREDENTIAL:
      return {
        title: 'Auto login failure',
        message: 'Fail to get user credential from server',
      };
    default: {
      if (error && error.message) {
        return {
          title: 'Error',
          message: error.message,
        };
      }
      return {
        title: 'Error',
        message: 'Unexpected error occured',
      };
    }
  }
}

function* checkInternetConnection() {
  const retryNum = 5;
  for (let i = 0; i < retryNum; i += 1) {
    const hasInternet = yield call(NetworkHelper.isConnectedToInternetOnLaunch);
    if (!hasInternet) {
      if (i < retryNum - 1) {
        yield delay(2000);
      }
    } else {
      return true;
    }
  }
  throw new Error(ERROR_NO_INTERNET);
}

function* autoLogin(action) {
  try {
    yield call(initCognitoConfig, action);

    const {
      user: {
        credential: { idToken, refreshToken },
        profile: { email },
        isAutoLogin,
        hasRefreshToken,
      },
    } = yield select(state => state);

    // Only set login status to success
    // if user logged in before and its credential still valid
    if (isAutoLogin && hasRefreshToken) {
      // Check whether has access to Internet or not
      const hasInternetAccess = yield call(
        NetworkHelper.isConnectedToInternetOnLaunch
      );

      if (!hasInternetAccess) {
        yield call(checkInternetConnection);
      }

      if (!idToken || !refreshToken) {
        throw new Error(ERROR_NO_TOKEN);
      }

      const currentCrendetial = yield call(retryCheckUserCredential);

      if (!currentCrendetial) {
        throw new Error(ERROR_NO_CREDENTIAL);
      }

      const hasUser = yield call(fetchUserProfileAPI, {
        fetchUserProfileParams: { email },
      });

      const {
        profile: { name: userName },
      } = yield select(state => state.user);

      if (!hasUser || !userName) {
        throw new Error(ERROR_AUTO_LOGIN_FAIL);
      } else if (userName) {
        yield put(
          setLoginStatus({ messageType: 'success', message: 'success' })
        );
      }
    } else {
      yield put(logOut());
    }
  } catch (error) {
    const { title, message } = getAlertTitleAndErrMsg(error);
    yield put(setLoginStatus({ messageType: 'error', message, title }));
  } finally {
    yield put(clearUserStatus());
  }
}

export default function* root() {
  yield takeLatest(AUTO_LOGIN, autoLogin);
}
