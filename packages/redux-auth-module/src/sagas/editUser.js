import { call, put, takeLatest } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setUserProfile,
  setIsLoading,
  setUserStatus,
  clearUserStatus,
  SUBMIT_USER_PARAMS,
} from '../actions/user';

function* editUserProfile(action) {
  try {
    yield put(clearUserStatus());
    yield put(setIsLoading(true));
    // Update user
    const userResponse = yield call(ApiService.post, 'setuserprofile', {
      requestData: {
        type: '2',
        email: action.userProfileParams.email,
        field: {
          name: action.userProfileParams.name,
          phoneNo: `+60${action.userProfileParams.phoneNo.replace('-', '')}`,
          countryCode: '+60',
        },
      },
    });

    if (userResponse.message === 'success') {
      // Update user
      yield put(
        setUserProfile({
          name: action.userProfileParams.name,
          email: action.userProfileParams.email,
          phoneNo: action.userProfileParams.phoneNo,
        })
      );

      yield put(
        setUserStatus({
          messageType: 'success',
          message: 'User profile updated successfully.',
        })
      );
    } else {
      yield put(
        setUserStatus({
          messageType: 'error',
          message: userResponse.message,
        })
      );
    }
  } catch (error) {
    yield put(
      setUserStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setIsLoading(false));
  }
}

export default function* rootSaga() {
  yield takeLatest(SUBMIT_USER_PARAMS, editUserProfile);
}
