import { all, fork } from 'redux-saga/effects';

import LoginSaga from './login';
import AutoLoginSaga from './autoLogin';
import LogoutSaga from './logout';
import ForgotPasswordSaga from './forgotPassword';
import ResetPasswordSaga from './resetPassword';
import SignUpSaga from './signUp';
import EditUserSaga from './editUser';

export default function* rootSaga() {
  yield all([
    fork(LoginSaga),
    fork(AutoLoginSaga),
    fork(LogoutSaga),
    fork(ForgotPasswordSaga),
    fork(ResetPasswordSaga),
    fork(SignUpSaga),
    fork(EditUserSaga),
  ]);
}
