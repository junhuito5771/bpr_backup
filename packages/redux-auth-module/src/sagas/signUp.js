import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ApiService, Auth } from '@module/utility';
import {
  BEGIN_SIGN_UP_LOADING,
  SET_SIGN_UP_STATUS,
  setSignUpParams,
  BEGIN_CONFIRM_LOADING,
  SET_CONFIRM_STATUS,
  END_CONFIRM_LOADING,
  SET_RESEND_STATUS,
  resendLoading,
  SUBMIT_SIGN_UP_PARAMS,
  SUBMIT_CONFIRM_PARAMS,
  RESEND_VERIFICATION_CODE,
} from '../actions/signUp';

import { submitLogin } from './login';

function getAuthStatus({ data }) {
  // Failure if response has the code field
  if (data && data.code) {
    return {
      messageType: 'error',
      message: data.message,
      errorCode: data.code,
    };
  }

  return {
    messageType: 'success',
    message: 'success',
  };
}

function* submitSignUpForm(action) {
  try {
    yield put({
      type: BEGIN_SIGN_UP_LOADING,
    });

    const responseData = yield call(ApiService.postWithoutAuth, 'register', {
      requestData: {
        username: action.signUpParams.email,
        password: action.signUpParams.password,
        name: action.signUpParams.fullName,
        countryCode: '+60',
        phone:
          action.signUpParams.phoneNumber === ''
            ? ''
            : `+60${action.signUpParams.phoneNumber.replace('-', '')}`,
      },
    });

    yield put(setSignUpParams(action.signUpParams));
    yield put({
      type: SET_SIGN_UP_STATUS,
      payload: getAuthStatus(responseData),
    });
  } catch (error) {
    yield put({
      type: SET_SIGN_UP_STATUS,
      payload: {
        messageType: 'error',
        message: error.message,
      },
    });
  }
}

function confirmSignUp({ username, code }) {
  return new Promise((resolve, reject) => {
    Auth.confirmSignUp(username, code)
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(new Error(error.message));
      });
  });
}

function* submitConfirmSignUpForm(action) {
  try {
    yield put({
      type: BEGIN_CONFIRM_LOADING,
    });

    const { email, password } = yield select(
      state => state.signUp.signUpParams
    );
    const { userPw, isContinueWithLogin = false, code } = action.confirmParams;

    if (!email) throw new Error('Email cannot be empty');
    if (!code) throw new Error('Confirmation code cannot be empty');

    const response = yield call(confirmSignUp, { username: email, code });

    // Only check the login status when unconfirmed flow, else skip the checking
    const isLogin = isContinueWithLogin
      ? yield call(submitLogin, {
          loginParams: { email, password: password || userPw },
        })
      : true;

    if (response === 'SUCCESS' && isLogin) {
      // cannot navigate in submitLoginForm, need to set status first
      yield put({
        type: SET_CONFIRM_STATUS,
        payload: {
          messageType: 'confirmSuccess',
          message: 'Your account has been verified!',
        },
      });
    } else {
      action.navigationService.navigate(action.loginScreen);
    }
  } catch (error) {
    yield put({
      type: SET_CONFIRM_STATUS,
      payload: {
        messageType: 'confirmError',
        message: error.message,
      },
    });

    if (error.code === 'NotAuthorizedException') {
      action.navigationService.navigate(action.loginScreen);
    }
  } finally {
    yield put({
      type: END_CONFIRM_LOADING,
    });
  }
}

function resendCode(username) {
  return Auth.resendSignUp(username);
}

function* submitResendCode() {
  try {
    yield put(resendLoading(true));
    const { email } = yield select(state => state.signUp.signUpParams);

    yield call(resendCode, email);

    yield put({
      type: SET_RESEND_STATUS,
      payload: {
        messageType: 'resendSuccess',
        message: 'A new verification code has been sent to your email address',
      },
    });
  } catch (error) {
    yield put({
      type: SET_RESEND_STATUS,
      payload: {
        messageType: 'resendError',
        message: error.message,
      },
    });
  } finally {
    yield put(resendLoading(false));
  }
}

export default function* rootSaga() {
  yield takeLatest(SUBMIT_SIGN_UP_PARAMS, submitSignUpForm);
  yield takeLatest(SUBMIT_CONFIRM_PARAMS, submitConfirmSignUpForm);
  yield takeLatest(RESEND_VERIFICATION_CODE, submitResendCode);
}

export { submitSignUpForm, submitConfirmSignUpForm, submitResendCode };
