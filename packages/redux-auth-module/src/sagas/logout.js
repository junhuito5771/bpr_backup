import { call, select, put, apply, takeLatest } from 'redux-saga/effects';
import {
  Auth,
  CrashReportHelper,
  DeviceInfo,
  ApiService,
  NotificationHelper,
} from '@module/utility';

import { setIsLogin } from '../actions/login';
import { clearLogoutParams, SUBMIT_LOGOUT_PARAMS } from '../actions/user';

function* submitLogout() {
  try {
    const { idToken } = yield select(state => state.user.credential);
    const { email } = yield select(state => state.user.profile);

    if (idToken) {
      yield call(Auth.signOut, { global: true });
    }

    // Get Notification Token from async storage
    const notificationToken = yield apply(
      NotificationHelper,
      NotificationHelper.getTokenFrmStg
    );

    // Remove app info to prevent notification after log out
    if (idToken && notificationToken) {
      try {
        yield call(ApiService.post, 'removeappinfo', {
          requestData: {
            username: email,
            IMEI: DeviceInfo.getUniqueId(),
            Token: notificationToken,
          },
        });
      } catch (error) {
        // not handling error since won't cause impact for app
      }
    }

    // Clear logout params action
    yield put(clearLogoutParams());
    yield put(setIsLogin(false));

    CrashReportHelper.clearUser();
  } catch (error) {}
}

export default function* rootSaga() {
  yield takeLatest(SUBMIT_LOGOUT_PARAMS, submitLogout);
}
