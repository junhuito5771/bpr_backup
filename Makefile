bootstrap:
	npx lerna clean && npx lerna bootstrap
	cd packages/daikin-mobile/ios && bundler exec pod install
	cd packages/acson-mobile/ios && bundler exec pod install